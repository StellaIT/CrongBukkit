# SwiftBukkit
Spigot tweak bukkit

성능 개선
-----
객체 타입 List -> Set 로 변경
> PlayerChunkMap

마지막으로 접근한 청크 캐싱
> ChunkProviderServer

NetworkManagerPacket Handling 개선, 불필요한 Queue 대기열을 제거
> NetworkManager 282

필요할 때만 정보 업데이트
> CraftPlayer 935

플레이어 Lookup 개선
> PlayerList 277
> CraftOfflinePlayer 110
> CraftServer 342
> CraftPlayer 154

레드스톤 렉 문제 해결
> WorldServer 553

더 나은 컨테이너 체크
> PlayerConnection 1402

폭발에 의해 엔티티가 죽을 때 생기는 렉 해결
> Explosion 121

폭발물 최적화
> Explosion 138

최소 청크 쓰레드 변경
> ChunkIOExecutor 16

네트워크 Queue 최적화
> MinecraftServer 71

EntityPlayer removeQueue 최적화
> EntityPlayer 177, 672

맵 파일 생성 시 I/O 사용량 감소
> RegionFile 79

맵 파일 캐시 최적화
> RegionFileCache 18

최적화 컬렉션 사용
> DataWatcher 23

Lighting Queue 사용
> Chunk 23

Chunk Tick Selection 최적화
> World 89

더 나은 아이템 확인
> SwiftUtils 11

플레이어 움직임 기반 청크 딜레이
> SpigotConfig 72

화재로 인한 청크 로드 방지
> BlockFire 121

활성 엔티티 범위 최적화
> ActivationRange 91

눈 블럭 Ticking 비활성화
> BlockSnowBlock 26

월드 로드 여부 확인 최적화
> World 254

비동기 월드 저장
> DedicatedServer 174

concurrent 자료형 사용
> MetadataStoreBase 20

### 타일 엔티티
타일 엔티티 최적화
> TileEntity 52 ~ 54

신호기 최적화
> TileEntityBeacon 44

상자 타일 엔티티 최적화
> TileEntityChest 257

엔더상자 최적화
> TileEntityEnderChest 21

햇빛 감지기 최적화
> TileEntityLightDetector 14

타일 엔티티 캐싱 최적화
> WorldTileEntityList

깔때기 최적화
> TileEntity 16

아이템 드롭 연산 최적화
> CraftWorld 485

구현 자료형 변경
> World 78

기능 추가
-----
* 메인 스레드가 응답이 없을 때 모든 플러그인 disable, 데이터 저장 후 버킷 재시작
* spigot.yml 에 버킷 재시작 스크립트 경로, 1번 기능 온/오프
* Tick 당 플레이어당 처리되는 패킷 수 설정 옵션
* tick-next-tick-list-cap 옵션 추가
* 폭발 최적화 옵션
* 청크 로드 스레드 조절 옵션
* 맵 파일 캐시 크기 조절 옵션
* 서버렉을 발생시키지 않게 Queue 의 Light Update 옵션 제공
* 플레이어 움직임 기반 청크 딜레이 옵션
* 인벤토리에 아이템이 확실히 들어가도록 개선
* Spigot config 리로드 명령어 추가
* 월드 자동 저장 기능
* 월드 저장 시작, 종료 메세지 설정 옵션 제공
* 드롭 좌표 연산 최적화 옵션 제공
* FileIOThread 작동 딜레이 온/오프 기능 추가
* 자동 저장 메커니즘 개선
* 손상된 엔티티 자동 삭제 옵션 제공
* 청크 저장 메모리 최적화
* 콘솔에서 한글입력 지원
* 콘솔에서 탭컴플리션(자동완성) 지원
* 네트워크 프레임워크 최신으로 업데이트
* 네트워크 지연 개선

버그 수정
-----
* 주소 변경으로 인한 Timings 오작동 (최신 GSON 라이브러리 추가)
* 채팅으로 트리거의 플레이스홀더를 사용하는 버그
* NBT Exploit
* 프리캠 버그
* 취약 색코드
* 플레이어가 너무 높은곳에 있을때 서버 크래시 방지
* 정해진 월드를 넘어서 청크 생성되는 버그
* 일부 메모리 누수 버그
* 한글 입력이 안되는 버그 수정
* 신속 포션 끊김 버그 수정