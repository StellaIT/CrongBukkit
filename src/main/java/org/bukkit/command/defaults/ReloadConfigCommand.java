package org.bukkit.command.defaults;

import org.bukkit.command.CommandSender;
import org.spigotmc.SpigotConfig;

/**
 * Created by EntryPoint on 2017-01-01.
 */
public class ReloadConfigCommand extends BukkitCommand {
    public ReloadConfigCommand(String name) {
        super(name);
        this.description = "Reload a spigot config";
        this.usageMessage = "/reloadconfig";
        this.setPermission("swiftnode.command.reloadconfig");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] args) {
        SpigotConfig.loadConfig();
        commandSender.sendMessage("Spigot config has reloaded");
        return true;
    }
}
