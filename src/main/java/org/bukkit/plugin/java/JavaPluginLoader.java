package org.bukkit.plugin.java;

/**
 * Created by Junhyeong Lim on 2017-01-29.
 */

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.Validate;
import org.bukkit.Server;
import org.bukkit.Warning;
import org.bukkit.Warning.WarningState;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.plugin.*;
import org.yaml.snakeyaml.error.YAMLException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.regex.Pattern;

public class JavaPluginLoader implements PluginLoader {
    /**
     * @deprecated
     */
    @Deprecated
    protected final Pattern[] fileFilters;
    /**
     * @deprecated
     */
    @Deprecated
    protected final Map<String, Class<?>> classes;
    /**
     * @deprecated
     */
    @Deprecated
    protected final Map<String, PluginClassLoader> loaders;
    final Server server;
    final boolean extended = this.getClass() != JavaPluginLoader.class;
    private final Pattern[] fileFilters0 = new Pattern[]{Pattern.compile("\\.jar$")};
    private final Map<String, Class<?>> classes0;
    private final Map<String, PluginClassLoader> loaders0;
    boolean warn;

    /**
     * @deprecated
     */
    @Deprecated
    public JavaPluginLoader(Server instance) {
        this.fileFilters = this.fileFilters0;
        this.classes0 = new HashMap<>();
        this.classes = this.classes0;
        this.loaders0 = new LinkedHashMap<>();
        this.loaders = this.loaders0;
        Validate.notNull(instance, "Server cannot be null");
        this.server = instance;
        this.warn = instance.getWarningState() != WarningState.OFF;
        if (this.extended && this.warn) {
            this.warn = false;
            instance.getLogger().log(Level.WARNING, "JavaPluginLoader not intended to be extended by " + this.getClass() + ", and may be final in a future version of Bukkit");
        }

    }

    public Plugin loadPlugin(File file) throws InvalidPluginException {
        Validate.notNull(file, "File cannot be null");
        if (!file.exists()) {
            throw new InvalidPluginException(new FileNotFoundException(file.getPath() + " does not exist"));
        } else {
            PluginDescriptionFile description;
            try {
                description = this.getPluginDescription(file);
            } catch (InvalidDescriptionException var14) {
                throw new InvalidPluginException(var14);
            }

            File dataFolder = new File(file.getParentFile(), description.getName());
            File oldDataFolder = this.extended ? this.getDataFolder(file) : this.getDataFolder0(file);
            if (!dataFolder.equals(oldDataFolder)) {
                if (dataFolder.isDirectory() && oldDataFolder.isDirectory()) {
                    this.server.getLogger().log(Level.INFO, String.format("While loading %s (%s) found old-data folder: %s next to the new one: %s", new Object[]{description.getName(), file, oldDataFolder, dataFolder}));
                } else if (oldDataFolder.isDirectory() && !dataFolder.exists()) {
                    if (!oldDataFolder.renameTo(dataFolder)) {
                        throw new InvalidPluginException("Unable to rename old data folder: \'" + oldDataFolder + "\' to: \'" + dataFolder + "\'");
                    }

                    this.server.getLogger().log(Level.INFO, String.format("While loading %s (%s) renamed data folder: \'%s\' to \'%s\'", new Object[]{description.getName(), file, oldDataFolder, dataFolder}));
                }
            }

            if (dataFolder.exists() && !dataFolder.isDirectory()) {
                throw new InvalidPluginException(String.format("Projected datafolder: \'%s\' for %s (%s) exists and is not a directory", new Object[]{dataFolder, description.getName(), file}));
            } else {
                Object depend = description.getDepend();
                if (depend == null) {
                    depend = ImmutableList.of();
                }

                Iterator loader = ((List) depend).iterator();

                String result;
                PluginClassLoader ex;
                do {
                    if (!loader.hasNext()) {
                        loader = null;
                        result = null;

                        PluginClassLoader loader1;
                        JavaPlugin result1;
                        try {
                            URL[] ex1 = new URL[]{file.toURI().toURL()};
                            if (description.getClassLoaderOf() != null) {
                                loader1 = this.loaders0.get(description.getClassLoaderOf());
                                loader1.addURL(ex1[0]);
                            } else {
                                loader1 = new PluginClassLoader(this, ex1, this.getClass().getClassLoader(), null);
                            }

                            Class jarClass = Class.forName(description.getMain(), true, loader1);
                            Class plugin = jarClass.asSubclass(JavaPlugin.class);
                            Constructor constructor = plugin.getConstructor();
                            result1 = (JavaPlugin) constructor.newInstance();
                            result1.initialize(this, this.server, description, dataFolder, file, loader1);
                        } catch (InvocationTargetException var12) {
                            throw new InvalidPluginException(var12.getCause());
                        } catch (Throwable var13) {
                            throw new InvalidPluginException(var13);
                        }

                        this.loaders0.put(description.getName(), loader1);
                        return result1;
                    }

                    result = (String) loader.next();
                    if (this.loaders0 == null) {
                        throw new UnknownDependencyException(result);
                    }

                    ex = this.loaders0.get(result);
                } while (ex != null);

                throw new UnknownDependencyException(result);
            }
        }
    }

    /**
     * @deprecated
     */
    @Deprecated
    public Plugin loadPlugin(File file, boolean ignoreSoftDependencies) throws InvalidPluginException {
        if (this.warn) {
            this.server.getLogger().log(Level.WARNING, "Method \"public Plugin loadPlugin(File, boolean)\" is Deprecated, and may be removed in a future version of Bukkit", new AuthorNagException(""));
            this.warn = false;
        }

        return this.loadPlugin(file);
    }

    /**
     * @deprecated
     */
    @Deprecated
    protected File getDataFolder(File file) {
        if (this.warn) {
            this.server.getLogger().log(Level.WARNING, "Method \"protected File getDataFolder(File)\" is Deprecated, and may be removed in a future version of Bukkit", new AuthorNagException(""));
            this.warn = false;
        }

        return this.getDataFolder0(file);
    }

    private File getDataFolder0(File file) {
        File dataFolder = null;
        String filename = file.getName();
        int index = file.getName().lastIndexOf(".");
        if (index != -1) {
            String name = filename.substring(0, index);
            dataFolder = new File(file.getParentFile(), name);
        } else {
            dataFolder = new File(file.getParentFile(), filename + "_");
        }

        return dataFolder;
    }

    public PluginDescriptionFile getPluginDescription(File file) throws InvalidDescriptionException {
        Validate.notNull(file, "File cannot be null");
        JarFile jar = null;
        InputStream stream = null;

        PluginDescriptionFile var5;
        try {
            jar = new JarFile(file);
            JarEntry ex = jar.getJarEntry("plugin.yml");
            if (ex == null) {
                throw new InvalidDescriptionException(new FileNotFoundException("Jar does not contain plugin.yml"));
            }

            stream = jar.getInputStream(ex);
            var5 = new PluginDescriptionFile(stream);
        } catch (IOException var18) {
            throw new InvalidDescriptionException(var18);
        } catch (YAMLException var19) {
            throw new InvalidDescriptionException(var19);
        } finally {
            if (jar != null) {
                try {
                    jar.close();
                } catch (IOException var17) {
                }
            }

            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException var16) {
                }
            }

        }

        return var5;
    }

    public Pattern[] getPluginFileFilters() {
        return this.fileFilters0.clone();
    }

    /**
     * @deprecated
     */
    @Deprecated
    public Class<?> getClassByName(String name) {
        if (this.warn) {
            this.server.getLogger().log(Level.WARNING, "Method \"public Class<?> getClassByName(String)\" is Deprecated, and may be removed in a future version of Bukkit", new AuthorNagException(""));
            this.warn = false;
        }

        return this.getClassByName0(name);
    }

    Class<?> getClassByName0(String name) {
        Class cachedClass = this.classes0.get(name);
        if (cachedClass != null) {
            return cachedClass;
        } else {
            Iterator i$ = this.loaders0.keySet().iterator();

            while (i$.hasNext()) {
                String current = (String) i$.next();
                PluginClassLoader loader = this.loaders0.get(current);

                try {
                    cachedClass = loader.extended ? loader.findClass(name, false) : loader.findClass0(name, false);
                } catch (ClassNotFoundException var7) {
                }

                if (cachedClass != null) {
                    return cachedClass;
                }
            }

            return null;
        }
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void setClass(String name, Class<?> clazz) {
        if (this.warn) {
            this.server.getLogger().log(Level.WARNING, "Method \"public void setClass(String, Class<?>)\" is Deprecated, and may be removed in a future version of Bukkit", new AuthorNagException(""));
            this.warn = false;
        }

        this.setClass0(name, clazz);
    }

    void setClass0(String name, Class<?> clazz) {
        if (!this.classes0.containsKey(name)) {
            this.classes0.put(name, clazz);
            if (ConfigurationSerializable.class.isAssignableFrom(clazz)) {
                Class serializable = clazz.asSubclass(ConfigurationSerializable.class);
                ConfigurationSerialization.registerClass(serializable);
            }
        }

    }

    /**
     * @deprecated
     */
    @Deprecated
    public void removeClass(String name) {
        if (this.warn) {
            this.server.getLogger().log(Level.WARNING, "Method \"public void removeClass(String)\" is Deprecated, and may be removed in a future version of Bukkit", new AuthorNagException(""));
            this.warn = false;
        }

        this.removeClass0(name);
    }

    private void removeClass0(String name) {
        Class clazz = this.classes0.remove(name);

        try {
            if (clazz != null && ConfigurationSerializable.class.isAssignableFrom(clazz)) {
                Class ex = clazz.asSubclass(ConfigurationSerializable.class);
                ConfigurationSerialization.unregisterClass(ex);
            }
        } catch (NullPointerException var4) {
        }

    }

    public Map<Class<? extends Event>, Set<RegisteredListener>> createRegisteredListeners(Listener listener, final Plugin plugin) {
        Validate.notNull(plugin, "Plugin can not be null");
        Validate.notNull(listener, "Listener can not be null");

        boolean useTimings = server.getPluginManager().useTimings();
        Map<Class<? extends Event>, Set<RegisteredListener>> ret = new HashMap<Class<? extends Event>, Set<RegisteredListener>>();
        Set<Method> methods;
        try {
            Method[] publicMethods = listener.getClass().getMethods();
            methods = new HashSet<Method>(publicMethods.length, Float.MAX_VALUE);
            for (Method method : publicMethods) {
                methods.add(method);
            }
            for (Method method : listener.getClass().getDeclaredMethods()) {
                methods.add(method);
            }
        } catch (NoClassDefFoundError e) {
            plugin.getLogger().severe("Plugin " + plugin.getDescription().getFullName() + " has failed to register events for " + listener.getClass() + " because " + e.getMessage() + " does not exist.");
            return ret;
        }

        for (final Method method : methods) {
            final EventHandler eh = method.getAnnotation(EventHandler.class);
            if (eh == null) continue;
            final Class<?> checkClass;
            if (method.getParameterTypes().length != 1 || !Event.class.isAssignableFrom(checkClass = method.getParameterTypes()[0])) {
                plugin.getLogger().severe(plugin.getDescription().getFullName() + " attempted to register an invalid EventHandler method signature \"" + method.toGenericString() + "\" in " + listener.getClass());
                continue;
            }
            final Class<? extends Event> eventClass = checkClass.asSubclass(Event.class);
            method.setAccessible(true);
            Set<RegisteredListener> eventSet = ret.get(eventClass);
            if (eventSet == null) {
                eventSet = new HashSet<>();
                ret.put(eventClass, eventSet);
            }

            for (Class<?> clazz = eventClass; Event.class.isAssignableFrom(clazz); clazz = clazz.getSuperclass()) {
                // This loop checks for extending deprecated events
                if (clazz.getAnnotation(Deprecated.class) != null) {
                    Warning warning = clazz.getAnnotation(Warning.class);
                    WarningState warningState = server.getWarningState();
                    if (!warningState.printFor(warning)) {
                        break;
                    }
                    plugin.getLogger().log(
                            Level.WARNING,
                            String.format(
                                    "\"%s\" has registered a listener for %s on method \"%s\", but the event is Deprecated." +
                                            " \"%s\"; please notify the authors %s.",
                                    plugin.getDescription().getFullName(),
                                    clazz.getName(),
                                    method.toGenericString(),
                                    (warning != null && warning.reason().length() != 0) ? warning.reason() : "Server performance will be affected",
                                    Arrays.toString(plugin.getDescription().getAuthors().toArray())),
                            warningState == WarningState.ON ? new AuthorNagException(null) : null);
                    break;
                }
            }

            EventExecutor executor = (listener1, event) -> {
                try {
                    if (!eventClass.isAssignableFrom(event.getClass())) {
                        return;
                    }
                    method.invoke(listener1, event);
                } catch (InvocationTargetException ex) {
                    throw new EventException(ex.getCause());
                } catch (Throwable t) {
                    throw new EventException(t);
                }
            };
            if (useTimings) {
                eventSet.add(new TimedRegisteredListener(listener, executor, eh.priority(), plugin, eh.ignoreCancelled()));
            } else {
                eventSet.add(new RegisteredListener(listener, executor, eh.priority(), plugin, eh.ignoreCancelled()));
            }
        }
        return ret;
    }

    public void enablePlugin(Plugin plugin) {
        Validate.isTrue(plugin instanceof JavaPlugin, "Plugin is not associated with this PluginLoader");
        if (!plugin.isEnabled()) {
            plugin.getLogger().info("플러그인 " + plugin.getDescription().getFullName() + " 활성화");
            JavaPlugin jPlugin = (JavaPlugin) plugin;
            String pluginName = jPlugin.getDescription().getName();
            if (!this.loaders0.containsKey(pluginName)) {
                this.loaders0.put(pluginName, (PluginClassLoader) jPlugin.getClassLoader());
            }

            try {
                jPlugin.setEnabled(true);
            } catch (Throwable var5) {
                this.server.getLogger().log(Level.SEVERE, "플러그인 " + plugin.getDescription().getFullName() + " 활성화 중에 에러 발생", var5);
//                this.server.getLogger().log(Level.SEVERE, "해당 플러그인을 비활성화합니다.");
//                disablePlugin(jPlugin);
                return;
            }

            this.server.getPluginManager().callEvent(new PluginEnableEvent(plugin));
        }

    }

    public void disablePlugin(Plugin plugin) {
        Validate.isTrue(plugin instanceof JavaPlugin, "Plugin is not associated with this PluginLoader");
        if (plugin.isEnabled()) {
            String message = String.format("플러그인 %s", plugin.getDescription().getFullName() + " 비활성화");
            plugin.getLogger().info(message);
            this.server.getPluginManager().callEvent(new PluginDisableEvent(plugin));
            JavaPlugin jPlugin = (JavaPlugin) plugin;
            ClassLoader cloader = jPlugin.getClassLoader();

            try {
                jPlugin.setEnabled(false);
            } catch (Throwable var9) {
                this.server.getLogger().log(Level.SEVERE, "플러그인 " + plugin.getDescription().getFullName() + " 비활성화 중에 에러 발생", var9);
            }

            this.loaders0.remove(jPlugin.getDescription().getName());
            if (cloader instanceof PluginClassLoader) {
                PluginClassLoader loader = (PluginClassLoader) cloader;
                Set names = loader.extended ? loader.getClasses() : loader.getClasses0();
                Iterator i$ = names.iterator();

                while (i$.hasNext()) {
                    String name = (String) i$.next();
                    if (this.extended) {
                        this.removeClass(name);
                    } else {
                        this.removeClass0(name);
                    }
                }
            }
        }

    }
}

