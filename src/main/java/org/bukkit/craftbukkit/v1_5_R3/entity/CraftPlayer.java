package org.bukkit.craftbukkit.v1_5_R3.entity;

/**
 * Created by EntryPoint on 2016-12-28.
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.MapMaker;
import net.minecraft.server.v1_5_R3.BanEntry;
import net.minecraft.server.v1_5_R3.*;
import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.Validate;
import org.bukkit.Achievement;
import org.bukkit.*;
import org.bukkit.Effect.Type;
import org.bukkit.Material;
import org.bukkit.Statistic;
import org.bukkit.World;
import org.bukkit.configuration.serialization.DelegateDeserialization;
import org.bukkit.conversations.Conversation;
import org.bukkit.conversations.ConversationAbandonedEvent;
import org.bukkit.conversations.ManuallyAbandonedConversationCanceller;
import org.bukkit.craftbukkit.v1_5_R3.*;
import org.bukkit.craftbukkit.v1_5_R3.conversations.ConversationTracker;
import org.bukkit.craftbukkit.v1_5_R3.map.CraftMapView;
import org.bukkit.craftbukkit.v1_5_R3.map.RenderData;
import org.bukkit.craftbukkit.v1_5_R3.scoreboard.CraftScoreboard;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerRegisterChannelEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.event.player.PlayerUnregisterChannelEvent;
import org.bukkit.inventory.InventoryView.Property;
import org.bukkit.map.MapView;
import org.bukkit.material.MaterialData;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.messaging.StandardMessenger;
import org.bukkit.scoreboard.Scoreboard;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.*;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

@DelegateDeserialization(CraftOfflinePlayer.class)
public class CraftPlayer extends CraftHumanEntity implements Player {
    private final ConversationTracker conversationTracker = new ConversationTracker();
    private final Set<String> channels = new HashSet();
    private final Map<String, Player> hiddenPlayers = (new MapMaker()).softValues().makeMap();
    private final Spigot spigot = new Spigot() {
        public InetSocketAddress getRawAddress() {
            return CraftPlayer.this.getHandle().playerConnection == null ? null : (InetSocketAddress) CraftPlayer.this.getHandle().playerConnection.networkManager.getSocket().getRemoteSocketAddress();
        }

        public void playEffect(Location location, Effect effect, int id, int data, float offsetX, float offsetY, float offsetZ, float speed, int particleCount, int radius) {
            Validate.notNull(location, "Location cannot be null");
            Validate.notNull(effect, "Effect cannot be null");
            Validate.notNull(location.getWorld(), "World cannot be null");
            Object packet;
            if (effect.getType() != Type.PARTICLE) {
                int particleFullName = effect.getId();
                packet = new Packet61WorldEvent(particleFullName, location.getBlockX(), location.getBlockY(), location.getBlockZ(), id, false);
            } else {
                StringBuilder particleFullName1 = new StringBuilder();
                particleFullName1.append(effect.getName());
                if (effect.getData() != null && (effect.getData().equals(Material.class) || effect.getData().equals(MaterialData.class))) {
                    particleFullName1.append('_').append(id);
                }

                if (effect.getData() != null && effect.getData().equals(MaterialData.class)) {
                    particleFullName1.append('_').append(data);
                }

                packet = new Packet63WorldParticles(effect.getName(), (float) location.getX(), (float) location.getY(), (float) location.getZ(), offsetX, offsetY, offsetZ, (float) particleCount, radius);
            }

            if (location.getWorld().equals(CraftPlayer.this.getWorld())) {
                CraftPlayer.this.getHandle().playerConnection.sendPacket((Packet) packet);
            }
        }
    };
    private long firstPlayed = 0L;
    private long lastPlayed = 0L;
    private boolean hasPlayedBefore = false;
    private int hash = 0;

    public CraftPlayer(CraftServer server, EntityPlayer entity) {
        super(server, entity);
        this.firstPlayed = System.currentTimeMillis();
    }

    public boolean isOp() {
        return this.server.getHandle().isOp(this.getName());
    }

    public void setOp(boolean value) {
        if (value != this.isOp()) {
            if (value) {
                this.server.getHandle().addOp(this.getName());
            } else {
                this.server.getHandle().removeOp(this.getName());
            }

            this.perm.recalculatePermissions();
        }
    }

    public boolean isOnline() {
        if (true) {
            return server.getHandle().playerMap.get(getName()) != null;
        }
        Iterator i$ = this.server.getHandle().players.iterator();

        EntityPlayer player;
        do {
            if (!i$.hasNext()) {
                return false;
            }

            Object obj = i$.next();
            player = (EntityPlayer) obj;
        } while (!player.name.equalsIgnoreCase(this.getName()));

        return true;
    }

    public InetSocketAddress getAddress() {
        if (this.getHandle().playerConnection == null) {
            return null;
        } else {
            SocketAddress addr = this.getHandle().playerConnection.networkManager.getSocketAddress();
            return addr instanceof InetSocketAddress ? (InetSocketAddress) addr : null;
        }
    }

    public double getEyeHeight() {
        return this.getEyeHeight(false);
    }

    public double getEyeHeight(boolean ignoreSneaking) {
        return ignoreSneaking ? 1.62D : (this.isSneaking() ? 1.54D : 1.62D);
    }

    public void sendRawMessage(String message) {
        if (this.getHandle().playerConnection != null) {
            this.getHandle().playerConnection.sendPacket(new Packet3Chat(message));
        }
    }

    public void sendMessage(String message) {
        if (!this.conversationTracker.isConversingModaly()) {
            this.sendRawMessage(message);
        }

    }

    public void sendMessage(String[] messages) {
        String[] arr$ = messages;
        int len$ = messages.length;

        for (int i$ = 0; i$ < len$; ++i$) {
            String message = arr$[i$];
            this.sendMessage(message);
        }

    }

    public String getDisplayName() {
        return this.getHandle().displayName;
    }

    public void setDisplayName(String name) {
        this.getHandle().displayName = name;
    }

    public String getPlayerListName() {
        return this.getHandle().listName;
    }

    public void setPlayerListName(String name) {
        String oldName = this.getHandle().listName;
        if (name == null) {
            name = this.getName();
        }

        if (!oldName.equals(name)) {
            if (name.length() > 16) {
                throw new IllegalArgumentException("Player list names can only be a maximum of 16 characters long");
            } else {
                for (int oldpacket = 0; oldpacket < this.server.getHandle().players.size(); ++oldpacket) {
                    if (((EntityPlayer) this.server.getHandle().players.get(oldpacket)).listName.equals(name)) {
                        throw new IllegalArgumentException(name + " is already assigned as a player list name for someone");
                    }
                }

                this.getHandle().listName = name;
                Packet201PlayerInfo var7 = new Packet201PlayerInfo(oldName, false, 9999);
                Packet201PlayerInfo packet = new Packet201PlayerInfo(name, true, this.getHandle().ping);

                for (int i = 0; i < this.server.getHandle().players.size(); ++i) {
                    EntityPlayer entityplayer = (EntityPlayer) this.server.getHandle().players.get(i);
                    if (entityplayer.playerConnection != null && entityplayer.getBukkitEntity().canSee(this)) {
                        entityplayer.playerConnection.sendPacket(var7);
                        entityplayer.playerConnection.sendPacket(packet);
                    }
                }

            }
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof OfflinePlayer)) {
            return false;
        } else {
            OfflinePlayer other = (OfflinePlayer) obj;
            if (this.getName() != null && other.getName() != null) {
                boolean nameEquals = this.getName().equalsIgnoreCase(other.getName());
                boolean idEquals = true;
                if (other instanceof CraftPlayer) {
                    idEquals = this.getEntityId() == ((CraftPlayer) other).getEntityId();
                }

                return nameEquals && idEquals;
            } else {
                return false;
            }
        }
    }

    public void kickPlayer(String message) {
        if (Thread.currentThread() != MinecraftServer.getServer().primaryThread) {
            throw new IllegalStateException("Asynchronous player kick!");
        } else if (this.getHandle().playerConnection != null) {
            this.getHandle().playerConnection.disconnect(message == null ? "" : message);
        }
    }

    public Location getCompassTarget() {
        return this.getHandle().compassTarget;
    }

    public void setCompassTarget(Location loc) {
        if (this.getHandle().playerConnection != null) {
            this.getHandle().playerConnection.sendPacket(new Packet6SpawnPosition(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()));
        }
    }

    public void chat(String msg) {
        if (this.getHandle().playerConnection != null) {
            this.getHandle().playerConnection.chat(msg, false);
        }
    }

    public boolean performCommand(String command) {
        return this.server.dispatchCommand(this, command);
    }

    public void playNote(Location loc, byte instrument, byte note) {
        if (this.getHandle().playerConnection != null) {
            int id = this.getHandle().world.getTypeId(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
            this.getHandle().playerConnection.sendPacket(new Packet54PlayNoteBlock(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ(), id, instrument, note));
        }
    }

    public void playNote(Location loc, Instrument instrument, Note note) {
        if (this.getHandle().playerConnection != null) {
            int id = this.getHandle().world.getTypeId(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
            this.getHandle().playerConnection.sendPacket(new Packet54PlayNoteBlock(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ(), id, instrument.getType(), note.getId()));
        }
    }

    public void playSound(Location loc, Sound sound, float volume, float pitch) {
        if (loc != null && sound != null && this.getHandle().playerConnection != null) {
            double x = (double) loc.getBlockX() + 0.5D;
            double y = (double) loc.getBlockY() + 0.5D;
            double z = (double) loc.getBlockZ() + 0.5D;
            Packet62NamedSoundEffect packet = new Packet62NamedSoundEffect(CraftSound.getSound(sound), x, y, z, volume, pitch);
            this.getHandle().playerConnection.sendPacket(packet);
        }
    }

    public void playEffect(Location location, Effect effect, int data) {
        this.spigot().playEffect(location, effect, data, 0, 0.0F, 0.0F, 0.0F, 1.0F, 1, 64);
    }

    public <T> void playEffect(Location loc, Effect effect, T data) {
        if (data != null) {
            Validate.isTrue(data.getClass().equals(effect.getData()), "Wrong kind of data for this effect!");
        } else {
            Validate.isTrue(effect.getData() == null, "Wrong kind of data for this effect!");
        }

        if (data != null && data.getClass().equals(MaterialData.class)) {
            MaterialData datavalue1 = (MaterialData) data;
            Validate.isTrue(!datavalue1.getItemType().isBlock(), "Material must be block");
            this.spigot().playEffect(loc, effect, datavalue1.getItemType().getId(), datavalue1.getData(), 0.0F, 0.0F, 0.0F, 1.0F, 1, 64);
        } else {
            int datavalue = data == null ? 0 : CraftEffect.getDataValue(effect, data);
            this.playEffect(loc, effect, datavalue);
        }

    }

    public void sendBlockChange(Location loc, Material material, byte data) {
        this.sendBlockChange(loc, material.getId(), data);
    }

    public void sendBlockChange(Location loc, int material, byte data) {
        if (this.getHandle().playerConnection != null) {
            Packet53BlockChange packet = new Packet53BlockChange(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ(), ((CraftWorld) loc.getWorld()).getHandle());
            packet.material = material;
            packet.data = data;
            this.getHandle().playerConnection.sendPacket(packet);
        }
    }

    public boolean sendChunkChange(Location loc, int sx, int sy, int sz, byte[] data) {
        if (this.getHandle().playerConnection == null) {
            return false;
        } else {
            throw new NotImplementedException("Chunk changes do not yet work");
        }
    }

    public void sendMap(MapView map) {
        if (this.getHandle().playerConnection != null) {
            RenderData data = ((CraftMapView) map).render(this);

            for (int x = 0; x < 128; ++x) {
                byte[] bytes = new byte[131];
                bytes[1] = (byte) x;

                for (int packet = 0; packet < 128; ++packet) {
                    bytes[packet + 3] = data.buffer[packet * 128 + x];
                }

                Packet131ItemData var6 = new Packet131ItemData((short) Material.MAP.getId(), map.getId(), bytes);
                this.getHandle().playerConnection.sendPacket(var6);
            }

        }
    }

    public boolean teleport(Location location, TeleportCause cause) {
        EntityPlayer entity = this.getHandle();
        if (this.getHealth() != 0 && !entity.dead) {
            if (entity.playerConnection != null && !entity.playerConnection.disconnected) {
                if (entity.vehicle == null && entity.passenger == null) {
                    Location from = this.getLocation();
                    PlayerTeleportEvent event = new PlayerTeleportEvent(this, from, location, cause);
                    this.server.getPluginManager().callEvent(event);
                    if (event.isCancelled()) {
                        return false;
                    } else {
                        from = event.getFrom();
                        Location to = event.getTo();
                        WorldServer fromWorld = ((CraftWorld) from.getWorld()).getHandle();
                        WorldServer toWorld = ((CraftWorld) to.getWorld()).getHandle();
                        if (this.getHandle().activeContainer != this.getHandle().defaultContainer) {
                            this.getHandle().closeInventory();
                        }

                        if (fromWorld == toWorld) {
                            entity.playerConnection.teleport(to);
                        } else {
                            this.server.getHandle().moveToWorld(entity, toWorld.dimension, true, to, true);
                        }

                        return true;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean isSneaking() {
        return this.getHandle().isSneaking();
    }

    public void setSneaking(boolean sneak) {
        this.getHandle().setSneaking(sneak);
    }

    public boolean isSprinting() {
        return this.getHandle().isSprinting();
    }

    public void setSprinting(boolean sprinting) {
        this.getHandle().setSprinting(sprinting);
    }

    public void loadData() {
        this.server.getHandle().playerFileData.load(this.getHandle());
    }

    public void saveData() {
        this.server.getHandle().playerFileData.save(this.getHandle());
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void updateInventory() {
        this.getHandle().updateInventory(this.getHandle().activeContainer);
    }

    public boolean isSleepingIgnored() {
        return this.getHandle().fauxSleeping;
    }

    public void setSleepingIgnored(boolean isSleeping) {
        this.getHandle().fauxSleeping = isSleeping;
        ((CraftWorld) this.getWorld()).getHandle().checkSleepStatus();
    }

    public void awardAchievement(Achievement achievement) {
        this.sendStatistic(achievement.getId(), 1);
    }

    public void incrementStatistic(Statistic statistic) {
        this.incrementStatistic(statistic, 1);
    }

    public void incrementStatistic(Statistic statistic, int amount) {
        this.sendStatistic(statistic.getId(), amount);
    }

    public void incrementStatistic(Statistic statistic, Material material) {
        this.incrementStatistic(statistic, material, 1);
    }

    public void incrementStatistic(Statistic statistic, Material material, int amount) {
        if (!statistic.isSubstatistic()) {
            throw new IllegalArgumentException("Given statistic is not a substatistic");
        } else if (statistic.isBlock() != material.isBlock()) {
            throw new IllegalArgumentException("Given material is not valid for this substatistic");
        } else {
            int mat = material.getId();
            if (!material.isBlock()) {
                mat -= 255;
            }

            this.sendStatistic(statistic.getId() + mat, amount);
        }
    }

    private void sendStatistic(int id, int amount) {
        if (this.getHandle().playerConnection != null) {
            while (amount > 127) {
                this.sendStatistic(id, 127);
                amount -= 127;
            }

            this.getHandle().playerConnection.sendPacket(new Packet200Statistic(id, amount));
        }
    }

    public void setPlayerTime(long time, boolean relative) {
        this.getHandle().timeOffset = time;
        this.getHandle().relativeTime = relative;
    }

    public long getPlayerTimeOffset() {
        return this.getHandle().timeOffset;
    }

    public long getPlayerTime() {
        return this.getHandle().getPlayerTime();
    }

    public boolean isPlayerTimeRelative() {
        return this.getHandle().relativeTime;
    }

    public void resetPlayerTime() {
        this.setPlayerTime(0L, true);
    }

    public WeatherType getPlayerWeather() {
        return this.getHandle().getPlayerWeather();
    }

    public void setPlayerWeather(WeatherType type) {
        this.getHandle().setPlayerWeather(type, true);
    }

    public void resetPlayerWeather() {
        this.getHandle().resetPlayerWeather();
    }

    public boolean isBanned() {
        return this.server.getHandle().getNameBans().isBanned(this.getName().toLowerCase());
    }

    public void setBanned(boolean value) {
        if (value) {
            BanEntry entry = new BanEntry(this.getName().toLowerCase());
            this.server.getHandle().getNameBans().add(entry);
        } else {
            this.server.getHandle().getNameBans().remove(this.getName().toLowerCase());
        }

        this.server.getHandle().getNameBans().save();
    }

    public boolean isWhitelisted() {
        return this.server.getHandle().getWhitelisted().contains(this.getName().toLowerCase());
    }

    public void setWhitelisted(boolean value) {
        if (value) {
            this.server.getHandle().addWhitelist(this.getName().toLowerCase());
        } else {
            this.server.getHandle().removeWhitelist(this.getName().toLowerCase());
        }

    }

    public GameMode getGameMode() {
        return GameMode.getByValue(this.getHandle().playerInteractManager.getGameMode().a());
    }

    public void setGameMode(GameMode mode) {
        if (this.getHandle().playerConnection != null) {
            if (mode == null) {
                throw new IllegalArgumentException("Mode cannot be null");
            } else {
                if (mode != this.getGameMode()) {
                    PlayerGameModeChangeEvent event = new PlayerGameModeChangeEvent(this, mode);
                    this.server.getPluginManager().callEvent(event);
                    if (event.isCancelled()) {
                        return;
                    }

                    this.getHandle().playerInteractManager.setGameMode(EnumGamemode.a(mode.getValue()));
                    this.getHandle().playerConnection.sendPacket(new Packet70Bed(3, mode.getValue()));
                }

            }
        }
    }

    public void giveExp(int exp) {
        this.getHandle().giveExp(exp);
    }

    public void giveExpLevels(int levels) {
        this.getHandle().levelDown(levels);
    }

    public float getExp() {
        return this.getHandle().exp;
    }

    public void setExp(float exp) {
        this.getHandle().exp = exp;
        this.getHandle().lastSentExp = -1;
    }

    public int getLevel() {
        return this.getHandle().expLevel;
    }

    public void setLevel(int level) {
        this.getHandle().expLevel = level;
        this.getHandle().lastSentExp = -1;
    }

    public int getTotalExperience() {
        return this.getHandle().expTotal;
    }

    public void setTotalExperience(int exp) {
        this.getHandle().expTotal = exp;
    }

    public float getExhaustion() {
        return this.getHandle().getFoodData().exhaustionLevel;
    }

    public void setExhaustion(float value) {
        this.getHandle().getFoodData().exhaustionLevel = value;
    }

    public float getSaturation() {
        return this.getHandle().getFoodData().saturationLevel;
    }

    public void setSaturation(float value) {
        this.getHandle().getFoodData().saturationLevel = value;
    }

    public int getFoodLevel() {
        return this.getHandle().getFoodData().foodLevel;
    }

    public void setFoodLevel(int value) {
        this.getHandle().getFoodData().foodLevel = value;
    }

    public Location getBedSpawnLocation() {
        World world = this.getServer().getWorld(this.getHandle().spawnWorld);
        ChunkCoordinates bed = this.getHandle().getBed();
        if (world != null && bed != null) {
            bed = EntityHuman.getBed(((CraftWorld) world).getHandle(), bed, this.getHandle().isRespawnForced());
            if (bed != null) {
                return new Location(world, (double) bed.x, (double) bed.y, (double) bed.z);
            }
        }

        return null;
    }

    public void setBedSpawnLocation(Location location) {
        this.setBedSpawnLocation(location, false);
    }

    public void setBedSpawnLocation(Location location, boolean override) {
        if (location == null) {
            this.getHandle().setRespawnPosition(null, override);
        } else {
            this.getHandle().setRespawnPosition(new ChunkCoordinates(location.getBlockX(), location.getBlockY(), location.getBlockZ()), override);
            this.getHandle().spawnWorld = location.getWorld().getName();
        }

    }

    public void hidePlayer(Player player) {
        Validate.notNull(player, "hidden player cannot be null");
        if (this.getHandle().playerConnection != null) {
            if (!this.equals(player)) {
                if (!this.hiddenPlayers.containsKey(player.getName())) {
                    this.hiddenPlayers.put(player.getName(), player);
                    EntityTracker tracker = ((WorldServer) this.entity.world).tracker;
                    EntityPlayer other = ((CraftPlayer) player).getHandle();
                    EntityTrackerEntry entry = (EntityTrackerEntry) tracker.trackedEntities.get(other.id);
                    if (entry != null) {
                        entry.clear(this.getHandle());
                    }

                    this.getHandle().playerConnection.sendPacket(new Packet201PlayerInfo(player.getPlayerListName(), false, 9999));
                }
            }
        }
    }

    public void showPlayer(Player player) {
        Validate.notNull(player, "shown player cannot be null");
        if (this.getHandle().playerConnection != null) {
            if (!this.equals(player)) {
                if (this.hiddenPlayers.containsKey(player.getName())) {
                    this.hiddenPlayers.remove(player.getName());
                    EntityTracker tracker = ((WorldServer) this.entity.world).tracker;
                    EntityPlayer other = ((CraftPlayer) player).getHandle();
                    EntityTrackerEntry entry = (EntityTrackerEntry) tracker.trackedEntities.get(other.id);
                    if (entry != null && !entry.trackedPlayers.contains(this.getHandle())) {
                        entry.updatePlayer(this.getHandle());
                    }

                    this.getHandle().playerConnection.sendPacket(new Packet201PlayerInfo(player.getPlayerListName(), true, this.getHandle().ping));
                }
            }
        }
    }

    public boolean canSee(Player player) {
        return !this.hiddenPlayers.containsKey(player.getName());
    }

    public Map<String, Object> serialize() {
        LinkedHashMap result = new LinkedHashMap();
        result.put("name", this.getName());
        return result;
    }

    public Player getPlayer() {
        return this;
    }

    public EntityPlayer getHandle() {
        return (EntityPlayer) this.entity;
    }

    public void setHandle(EntityPlayer entity) {
        super.setHandle(entity);
    }

    public String toString() {
        return "CraftPlayer{name=" + this.getName() + '}';
    }

    public int hashCode() {
        if (this.hash == 0 || this.hash == 485) {
            this.hash = 485 + (this.getName() != null ? this.getName().toLowerCase().hashCode() : 0);
        }

        return this.hash;
    }

    public long getFirstPlayed() {
        return this.firstPlayed;
    }

    public void setFirstPlayed(long firstPlayed) {
        this.firstPlayed = firstPlayed;
    }

    public long getLastPlayed() {
        return this.lastPlayed;
    }

    public boolean hasPlayedBefore() {
        return this.hasPlayedBefore;
    }

    public void readExtraData(NBTTagCompound nbttagcompound) {
        this.hasPlayedBefore = true;
        if (nbttagcompound.hasKey("bukkit")) {
            NBTTagCompound data = nbttagcompound.getCompound("bukkit");
            if (data.hasKey("firstPlayed")) {
                this.firstPlayed = data.getLong("firstPlayed");
                this.lastPlayed = data.getLong("lastPlayed");
            }

            if (data.hasKey("newExp")) {
                EntityPlayer handle = this.getHandle();
                handle.newExp = data.getInt("newExp");
                handle.newTotalExp = data.getInt("newTotalExp");
                handle.newLevel = data.getInt("newLevel");
                handle.expToDrop = data.getInt("expToDrop");
                handle.keepLevel = data.getBoolean("keepLevel");
            }
        }

    }

    public void setExtraData(NBTTagCompound nbttagcompound) {
        if (!nbttagcompound.hasKey("bukkit")) {
            nbttagcompound.setCompound("bukkit", new NBTTagCompound());
        }

        NBTTagCompound data = nbttagcompound.getCompound("bukkit");
        EntityPlayer handle = this.getHandle();
        data.setInt("newExp", handle.newExp);
        data.setInt("newTotalExp", handle.newTotalExp);
        data.setInt("newLevel", handle.newLevel);
        data.setInt("expToDrop", handle.expToDrop);
        data.setBoolean("keepLevel", handle.keepLevel);
        data.setLong("firstPlayed", this.getFirstPlayed());
        data.setLong("lastPlayed", System.currentTimeMillis());
    }

    public boolean beginConversation(Conversation conversation) {
        return this.conversationTracker.beginConversation(conversation);
    }

    public void abandonConversation(Conversation conversation) {
        this.conversationTracker.abandonConversation(conversation, new ConversationAbandonedEvent(conversation, new ManuallyAbandonedConversationCanceller()));
    }

    public void abandonConversation(Conversation conversation, ConversationAbandonedEvent details) {
        this.conversationTracker.abandonConversation(conversation, details);
    }

    public void acceptConversationInput(String input) {
        this.conversationTracker.acceptConversationInput(input);
    }

    public boolean isConversing() {
        return this.conversationTracker.isConversing();
    }

    public void sendPluginMessage(Plugin source, String channel, byte[] message) {
        StandardMessenger.validatePluginMessage(this.server.getMessenger(), source, channel, message);
        if (this.getHandle().playerConnection != null) {
            if (this.channels.contains(channel)) {
                Packet250CustomPayload packet = new Packet250CustomPayload();
                packet.tag = channel;
                packet.length = message.length;
                packet.data = message;
                this.getHandle().playerConnection.sendPacket(packet);
            }

        }
    }

    public void setTexturePack(String url) {
        Validate.notNull(url, "Texture pack URL cannot be null");
        byte[] message = (url + "\u0000" + "16").getBytes();
        Validate.isTrue(message.length <= 32766, "Texture pack URL is too long");
        this.getHandle().playerConnection.sendPacket(new Packet250CustomPayload("MC|TPack", message));
    }

    public void addChannel(String channel) {
        if (this.channels.add(channel)) {
            this.server.getPluginManager().callEvent(new PlayerRegisterChannelEvent(this, channel));
        }

    }

    public void removeChannel(String channel) {
        if (this.channels.remove(channel)) {
            this.server.getPluginManager().callEvent(new PlayerUnregisterChannelEvent(this, channel));
        }

    }

    public Set<String> getListeningPluginChannels() {
        return ImmutableSet.copyOf(this.channels);
    }

    public void sendSupportedChannels() {
        if (this.getHandle().playerConnection != null) {
            Set listening = this.server.getMessenger().getIncomingChannels();
            if (!listening.isEmpty()) {
                Packet250CustomPayload packet = new Packet250CustomPayload();
                packet.tag = "REGISTER";
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                Iterator i$ = listening.iterator();

                while (i$.hasNext()) {
                    String channel = (String) i$.next();

                    try {
                        stream.write(channel.getBytes("UTF8"));
                        stream.write(0);
                    } catch (IOException var7) {
                        Logger.getLogger(CraftPlayer.class.getName()).log(Level.SEVERE, "Could not send Plugin Channel REGISTER to " + this.getName(), var7);
                    }
                }

                packet.data = stream.toByteArray();
                packet.length = packet.data.length;
                this.getHandle().playerConnection.sendPacket(packet);
            }

        }
    }

    public EntityType getType() {
        return EntityType.PLAYER;
    }

    public void setMetadata(String metadataKey, MetadataValue newMetadataValue) {
        this.server.getPlayerMetadata().setMetadata(this, metadataKey, newMetadataValue);
    }

    public List<MetadataValue> getMetadata(String metadataKey) {
        return this.server.getPlayerMetadata().getMetadata(this, metadataKey);
    }

    public boolean hasMetadata(String metadataKey) {
        return this.server.getPlayerMetadata().hasMetadata(this, metadataKey);
    }

    public void removeMetadata(String metadataKey, Plugin owningPlugin) {
        this.server.getPlayerMetadata().removeMetadata(this, metadataKey, owningPlugin);
    }

    public boolean setWindowProperty(Property prop, int value) {
        Container container = this.getHandle().activeContainer;
        if (container.getBukkitView().getType() != prop.getType()) {
            return false;
        } else {
            this.getHandle().setContainerData(container, prop.getId(), value);
            return true;
        }
    }

    public void disconnect(String reason) {
        this.conversationTracker.abandonAllConversations();
        this.perm.clearPermissions();
    }

    public boolean isFlying() {
        return this.getHandle().abilities.isFlying;
    }

    public void setFlying(boolean value) {
        boolean needsUpdate = getHandle().abilities.canFly != value;
        if (!this.getAllowFlight() && value) {
            throw new IllegalArgumentException("Cannot make player fly if getAllowFlight() is false");
        } else {
            this.getHandle().abilities.isFlying = value;
            this.getHandle().updateAbilities();
        }
        if (needsUpdate) getHandle().updateAbilities();
    }

    public boolean getAllowFlight() {
        return this.getHandle().abilities.canFly;
    }

    public void setAllowFlight(boolean value) {
        if (this.isFlying() && !value) {
            this.getHandle().abilities.isFlying = false;
        }

        this.getHandle().abilities.canFly = value;
        this.getHandle().updateAbilities();
    }

    public int getNoDamageTicks() {
        return this.getHandle().invulnerableTicks > 0 ? Math.max(this.getHandle().invulnerableTicks, this.getHandle().noDamageTicks) : this.getHandle().noDamageTicks;
    }

    public float getFlySpeed() {
        return this.getHandle().abilities.flySpeed * 2.0F;
    }

    public void setFlySpeed(float value) {
        this.validateSpeed(value);
        EntityPlayer player = this.getHandle();
        player.abilities.flySpeed = value / 2.0F;
        player.updateAbilities();
    }

    public float getWalkSpeed() {
        return this.getHandle().abilities.walkSpeed * 2.0F;
    }

    public void setWalkSpeed(float value) {
        this.validateSpeed(value);
        EntityPlayer player = this.getHandle();
        player.abilities.walkSpeed = value / 2.0F;
        player.updateAbilities();
    }

    private void validateSpeed(float value) {
        if (value < 0.0F) {
            if (value < -1.0F) {
                throw new IllegalArgumentException(value + " is too low");
            }
        } else if (value > 1.0F) {
            throw new IllegalArgumentException(value + " is too high");
        }

    }

    public void setMaxHealth(int amount) {
        super.setMaxHealth(amount);
        this.getHandle().triggerHealthUpdate();
    }

    public void resetMaxHealth() {
        super.resetMaxHealth();
        this.getHandle().triggerHealthUpdate();
    }

    public CraftScoreboard getScoreboard() {
        return this.server.getScoreboardManager().getPlayerBoard(this);
    }

    public void setScoreboard(Scoreboard scoreboard) {
        Validate.notNull(scoreboard, "Scoreboard cannot be null");
        PlayerConnection playerConnection = this.getHandle().playerConnection;
        if (playerConnection == null) {
            throw new IllegalStateException("Cannot set scoreboard yet");
        } else if (playerConnection.disconnected) {
            throw new IllegalStateException("Cannot set scoreboard for invalid CraftPlayer");
        } else {
            this.server.getScoreboardManager().setPlayerBoard(this, scoreboard);
        }
    }

    public Spigot spigot() {
        return this.spigot;
    }
}
