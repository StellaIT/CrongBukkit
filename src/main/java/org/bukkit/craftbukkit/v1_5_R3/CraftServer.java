package org.bukkit.craftbukkit.v1_5_R3;

import com.avaje.ebean.config.DataSourceConfig;
import com.avaje.ebean.config.ServerConfig;
import com.avaje.ebean.config.dbplatform.SQLitePlatform;
import com.avaje.ebeaninternal.server.lib.sql.TransactionIsolation;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.MapMaker;
import net.minecraft.server.v1_5_R3.BanEntry;
import net.minecraft.server.v1_5_R3.*;
import net.minecraft.server.v1_5_R3.WorldType;
import org.apache.commons.lang.Validate;
import org.bukkit.*;
import org.bukkit.Warning.WarningState;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.command.*;
import org.bukkit.command.CommandException;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.conversations.Conversable;
import org.bukkit.craftbukkit.Main;
import org.bukkit.craftbukkit.libs.jline.console.ConsoleReader;
import org.bukkit.craftbukkit.v1_5_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_5_R3.help.SimpleHelpMap;
import org.bukkit.craftbukkit.v1_5_R3.inventory.*;
import org.bukkit.craftbukkit.v1_5_R3.map.CraftMapView;
import org.bukkit.craftbukkit.v1_5_R3.metadata.EntityMetadataStore;
import org.bukkit.craftbukkit.v1_5_R3.metadata.PlayerMetadataStore;
import org.bukkit.craftbukkit.v1_5_R3.metadata.WorldMetadataStore;
import org.bukkit.craftbukkit.v1_5_R3.potion.CraftPotionBrewer;
import org.bukkit.craftbukkit.v1_5_R3.scheduler.CraftScheduler;
import org.bukkit.craftbukkit.v1_5_R3.scoreboard.CraftScoreboardManager;
import org.bukkit.craftbukkit.v1_5_R3.updater.AutoUpdater;
import org.bukkit.craftbukkit.v1_5_R3.updater.BukkitDLUpdaterService;
import org.bukkit.craftbukkit.v1_5_R3.util.DatFileFilter;
import org.bukkit.craftbukkit.v1_5_R3.util.Versioning;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;
import org.bukkit.event.world.WorldInitEvent;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.event.world.WorldSaveEvent;
import org.bukkit.event.world.WorldUnloadEvent;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.help.HelpMap;
import org.bukkit.inventory.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.Permissible;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.*;
import org.bukkit.plugin.java.JavaPluginLoader;
import org.bukkit.plugin.messaging.Messenger;
import org.bukkit.plugin.messaging.StandardMessenger;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitWorker;
import org.bukkit.util.StringUtil;
import org.bukkit.util.permissions.DefaultPermissions;
import org.spigotmc.SpigotConfig;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.SafeConstructor;
import org.yaml.snakeyaml.error.MarkedYAMLException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class CraftServer implements Server {
    static {
        ConfigurationSerialization.registerClass(CraftOfflinePlayer.class);
        CraftItemFactory.instance();
    }

    protected final MinecraftServer console;
    protected final DedicatedPlayerList playerList;
    private final String serverName = "CraftBukkit";
    private final String serverVersion;
    private final String bukkitVersion = Versioning.getBukkitVersion();
    private final ServicesManager servicesManager = new SimpleServicesManager();
    private final CraftScheduler scheduler = new CraftScheduler();
    private final SimpleCommandMap commandMap = new SimpleCommandMap(this);
    private final SimpleHelpMap helpMap = new SimpleHelpMap(this);
    private final StandardMessenger messenger = new StandardMessenger();
    private final PluginManager pluginManager;
    private final Map<String, World> worlds;
    private final Yaml yaml;
    private final Map<String, OfflinePlayer> offlinePlayers;
    private final AutoUpdater updater;
    private final EntityMetadataStore entityMetadata;
    private final PlayerMetadataStore playerMetadata;
    private final WorldMetadataStore worldMetadata;
    private final CraftServer.BooleanWrapper online;
    public int chunkGCPeriod;
    public int chunkGCLoadThresh;
    public CraftScoreboardManager scoreboardManager;
    private YamlConfiguration configuration;
    private int monsterSpawn;
    private int animalSpawn;
    private int waterAnimalSpawn;
    private int ambientSpawn;
    private File container;
    private WarningState warningState;

    public CraftServer(MinecraftServer console, PlayerList playerList) {
        this.pluginManager = new SimplePluginManager(this, this.commandMap);
        this.worlds = new LinkedHashMap<>();
        this.yaml = new Yaml(new SafeConstructor());
        this.offlinePlayers = (new MapMaker()).softValues().makeMap();
        this.entityMetadata = new EntityMetadataStore();
        this.playerMetadata = new PlayerMetadataStore();
        this.worldMetadata = new WorldMetadataStore();
        this.monsterSpawn = -1;
        this.animalSpawn = -1;
        this.waterAnimalSpawn = -1;
        this.ambientSpawn = -1;
        this.chunkGCPeriod = -1;
        this.chunkGCLoadThresh = 0;
        this.warningState = WarningState.DEFAULT;
        this.online = new CraftServer.BooleanWrapper();
        this.console = console;
        this.playerList = (DedicatedPlayerList) playerList;
        this.serverVersion = CraftServer.class.getPackage().getImplementationVersion();
        this.online.value = console.getPropertyManager().getBoolean("online-mode", true);
        Bukkit.setServer(this);
        Enchantment.DAMAGE_ALL.getClass();
        org.bukkit.enchantments.Enchantment.stopAcceptingRegistrations();
        Potion.setPotionBrewer(new CraftPotionBrewer());
        MobEffectList.BLINDNESS.getClass();
        PotionEffectType.stopAcceptingRegistrations();
        if (!Main.useConsole) {
            this.getLogger().info("Console input is disabled due to --noconsole command argument");
        }

        this.configuration = YamlConfiguration.loadConfiguration(this.getConfigFile());
        this.configuration.options().copyDefaults(true);
        this.configuration.setDefaults(YamlConfiguration.loadConfiguration(this.getClass().getClassLoader().getResourceAsStream("configurations/bukkit.yml")));
        this.saveConfig();
        ((SimplePluginManager) this.pluginManager).useTimings(this.configuration.getBoolean("settings.plugin-profiling"));
        this.monsterSpawn = this.configuration.getInt("spawn-limits.monsters");
        this.animalSpawn = this.configuration.getInt("spawn-limits.animals");
        this.waterAnimalSpawn = this.configuration.getInt("spawn-limits.water-animals");
        this.ambientSpawn = this.configuration.getInt("spawn-limits.ambient");
//        console.autosavePeriod = this.configuration.getInt("ticks-per.autosave");
        this.warningState = WarningState.value(this.configuration.getString("settings.deprecated-verbose"));
        this.chunkGCPeriod = this.configuration.getInt("chunk-gc.period-in-ticks");
        this.chunkGCLoadThresh = this.configuration.getInt("chunk-gc.load-threshold");
        this.updater = new AutoUpdater(new BukkitDLUpdaterService(this.configuration.getString("auto-updater.host")), this.getLogger(), this.configuration.getString("auto-updater.preferred-channel"));
        this.updater.setEnabled(false);
        this.updater.setSuggestChannels(this.configuration.getBoolean("auto-updater.suggest-channels"));
        this.updater.getOnBroken().addAll(this.configuration.getStringList("auto-updater.on-broken"));
        this.updater.getOnUpdate().addAll(this.configuration.getStringList("auto-updater.on-update"));
        this.updater.check(this.serverVersion);
        this.loadPlugins();
        this.enablePlugins(PluginLoadOrder.STARTUP);
    }

    private File getConfigFile() {
        return (File) this.console.options.valueOf("bukkit-settings");
    }

    private void saveConfig() {
        try {
            this.configuration.save(this.getConfigFile());
        } catch (IOException var2) {
            Logger.getLogger(CraftServer.class.getName()).log(Level.SEVERE, "Could not save " + this.getConfigFile(), var2);
        }

    }

    public void loadPlugins() {
        this.pluginManager.registerInterface(JavaPluginLoader.class);
        File pluginFolder = (File) this.console.options.valueOf("plugins");
        if (pluginFolder.exists()) {
            Plugin[] plugins = this.pluginManager.loadPlugins(pluginFolder);
            Plugin[] arr$ = plugins;
            int len$ = plugins.length;

            for (int i$ = 0; i$ < len$; ++i$) {
                Plugin plugin = arr$[i$];

                try {
                    String ex = String.format("플러그인 %s", plugin.getDescription().getFullName() + " 로딩");
                    plugin.getLogger().info(ex);
                    plugin.onLoad();
                } catch (Throwable var8) {
                    Logger.getLogger(CraftServer.class.getName()).log(Level.SEVERE, var8.getMessage() + " initializing " + plugin.getDescription().getFullName() + " (Is it up to date?)", var8);
                }
            }
        } else {
            pluginFolder.mkdir();
        }

    }

    public void enablePlugins(PluginLoadOrder type) {
        if (type == PluginLoadOrder.STARTUP) {
            this.helpMap.clear();
            this.helpMap.initializeGeneralTopics();
        }

        Plugin[] plugins = this.pluginManager.getPlugins();
        Plugin[] arr$ = plugins;
        int len$ = plugins.length;

        for (int i$ = 0; i$ < len$; ++i$) {
            Plugin plugin = arr$[i$];
            if (!plugin.isEnabled() && plugin.getDescription().getLoad() == type) {
                this.loadPlugin(plugin);
            }
        }

        if (type == PluginLoadOrder.POSTWORLD) {
            this.commandMap.registerServerAliases();
            this.loadCustomPermissions();
            DefaultPermissions.registerCorePermissions();
            this.helpMap.initializeCommands();
        }

    }

    public void disablePlugins() {
        this.pluginManager.disablePlugins();
    }

    private void loadPlugin(Plugin plugin) {
        try {
            this.pluginManager.enablePlugin(plugin);
            List ex = plugin.getDescription().getPermissions();
            Iterator i$ = ex.iterator();

            while (i$.hasNext()) {
                Permission perm = (Permission) i$.next();

                try {
                    this.pluginManager.addPermission(perm);
                } catch (IllegalArgumentException var6) {
                    this.getLogger().log(Level.WARNING, "Plugin " + plugin.getDescription().getFullName() + " tried to register permission \'" + perm.getName() + "\' but it\'s already registered", var6);
                }
            }
        } catch (Throwable var7) {
            Logger.getLogger(CraftServer.class.getName()).log(Level.SEVERE, var7.getMessage() + " loading " + plugin.getDescription().getFullName() + " (Is it up to date?)", var7);
        }

    }

    public String getName() {
        return "CraftBukkit";
    }

    public String getVersion() {
        return this.serverVersion + " (MC: " + this.console.getVersion() + ")";
    }

    public String getBukkitVersion() {
        return this.bukkitVersion;
    }

    public Player[] getOnlinePlayers() {
        List online = this.playerList.players;
        Player[] players = new Player[online.size()];

        for (int i = 0; i < players.length; ++i) {
            players[i] = ((EntityPlayer) online.get(i)).playerConnection.getPlayer();
        }

        return players;
    }

    public Player getPlayer(String name) {
        Validate.notNull(name, "Name cannot be null");
        Player found = getPlayerExact(name);
        if (found != null) {
            return found;
        }
        Player[] players = this.getOnlinePlayers();
        String lowerName = name.toLowerCase();
        int delta = 2147483647;
        Player[] arr$ = players;
        int len$ = players.length;

        for (int i$ = 0; i$ < len$; ++i$) {
            Player player = arr$[i$];
            if (player.getName().toLowerCase().startsWith(lowerName)) {
                int curDelta = player.getName().length() - lowerName.length();
                if (curDelta < delta) {
                    found = player;
                    delta = curDelta;
                }

                if (curDelta == 0) {
                    break;
                }
            }
        }

        return found;
    }

    public Player getPlayerExact(String name) {
        Validate.notNull(name, "Name cannot be null");
        String lname = name.toLowerCase();
        Player[] arr$ = this.getOnlinePlayers();
        int len$ = arr$.length;

        for (int i$ = 0; i$ < len$; ++i$) {
            Player player = arr$[i$];
            if (player.getName().equalsIgnoreCase(lname)) {
                return player;
            }
        }

        return null;
    }

    public int broadcastMessage(String message) {
        return this.broadcast(message, "bukkit.broadcast.user");
    }

    public Player getPlayer(EntityPlayer entity) {
        return entity.playerConnection.getPlayer();
    }

    public List<Player> matchPlayer(String partialName) {
        Validate.notNull(partialName, "PartialName cannot be null");
        ArrayList matchedPlayers = new ArrayList();
        Player[] arr$ = this.getOnlinePlayers();
        int len$ = arr$.length;

        for (int i$ = 0; i$ < len$; ++i$) {
            Player iterPlayer = arr$[i$];
            String iterPlayerName = iterPlayer.getName();
            if (partialName.equalsIgnoreCase(iterPlayerName)) {
                matchedPlayers.clear();
                matchedPlayers.add(iterPlayer);
                break;
            }

            if (iterPlayerName.toLowerCase().contains(partialName.toLowerCase())) {
                matchedPlayers.add(iterPlayer);
            }
        }

        return matchedPlayers;
    }

    public int getMaxPlayers() {
        return this.playerList.getMaxPlayers();
    }

    public int getPort() {
        return this.getConfigInt("server-port", 25565);
    }

    public int getViewDistance() {
        return this.getConfigInt("view-distance", 7);
    }

    public String getIp() {
        return this.getConfigString("server-ip", "");
    }

    public String getServerName() {
        return this.getConfigString("server-name", "Unknown Server");
    }

    public String getServerId() {
        return this.getConfigString("server-id", "unnamed");
    }

    public String getWorldType() {
        return this.getConfigString("level-type", "DEFAULT");
    }

    public boolean getGenerateStructures() {
        return this.getConfigBoolean("generate-structures", true);
    }

    public boolean getAllowEnd() {
        return this.configuration.getBoolean("settings.allow-end");
    }

    public boolean getAllowNether() {
        return this.getConfigBoolean("allow-nether", true);
    }

    public boolean getWarnOnOverload() {
        return this.configuration.getBoolean("settings.warn-on-overload");
    }

    public boolean getQueryPlugins() {
        return this.configuration.getBoolean("settings.query-plugins");
    }

    public boolean hasWhitelist() {
        return this.getConfigBoolean("white-list", false);
    }

    private String getConfigString(String variable, String defaultValue) {
        return this.console.getPropertyManager().getString(variable, defaultValue);
    }

    private int getConfigInt(String variable, int defaultValue) {
        return this.console.getPropertyManager().getInt(variable, defaultValue);
    }

    private boolean getConfigBoolean(String variable, boolean defaultValue) {
        return this.console.getPropertyManager().getBoolean(variable, defaultValue);
    }

    public String getUpdateFolder() {
        return this.configuration.getString("settings.update-folder", "update");
    }

    public File getUpdateFolderFile() {
        return new File((File) this.console.options.valueOf("plugins"), this.configuration.getString("settings.update-folder", "update"));
    }

    public int getPingPacketLimit() {
        return this.configuration.getInt("settings.ping-packet-limit", 100);
    }

    public long getConnectionThrottle() {
        return (long) this.configuration.getInt("settings.connection-throttle");
    }

    public int getTicksPerAnimalSpawns() {
        return this.configuration.getInt("ticks-per.animal-spawns");
    }

    public int getTicksPerMonsterSpawns() {
        return this.configuration.getInt("ticks-per.monster-spawns");
    }

    public PluginManager getPluginManager() {
        return this.pluginManager;
    }

    public CraftScheduler getScheduler() {
        return this.scheduler;
    }

    public ServicesManager getServicesManager() {
        return this.servicesManager;
    }

    public List<World> getWorlds() {
        return new ArrayList(this.worlds.values());
    }

    public DedicatedPlayerList getHandle() {
        return this.playerList;
    }

    public boolean dispatchServerCommand(CommandSender sender, ServerCommand serverCommand) {
        if (sender instanceof Conversable) {
            Conversable ex = (Conversable) sender;
            if (ex.isConversing()) {
                ex.acceptConversationInput(serverCommand.command);
                return true;
            }
        }

        try {
            return this.dispatchCommand(sender, serverCommand.command);
        } catch (Exception var4) {
            this.getLogger().log(Level.WARNING, "Unexpected exception while parsing console command \"" + serverCommand.command + '\"', var4);
            return false;
        }
    }

    public boolean dispatchCommand(CommandSender sender, String commandLine) {
        Validate.notNull(sender, "Sender cannot be null");
        Validate.notNull(commandLine, "CommandLine cannot be null");
        if (this.commandMap.dispatch(sender, commandLine)) {
            return true;
        } else {
            sender.sendMessage(SpigotConfig.unknownCommandMessage);
            return false;
        }
    }

    public void reload() {
        this.configuration = YamlConfiguration.loadConfiguration(this.getConfigFile());
        PropertyManager config = new PropertyManager(this.console.options, this.console.getLogger());
        ((DedicatedServer) this.console).propertyManager = config;
        boolean animals = config.getBoolean("spawn-animals", this.console.getSpawnAnimals());
        boolean monsters = config.getBoolean("spawn-monsters", this.console.worlds.get(0).difficulty > 0);
        int difficulty = config.getInt("difficulty", this.console.worlds.get(0).difficulty);
        this.online.value = config.getBoolean("online-mode", this.console.getOnlineMode());
        this.console.setSpawnAnimals(config.getBoolean("spawn-animals", this.console.getSpawnAnimals()));
        this.console.setPvP(config.getBoolean("pvp", this.console.getPvP()));
        this.console.setAllowFlight(config.getBoolean("allow-flight", this.console.getAllowFlight()));
        this.console.setMotd(config.getString("motd", this.console.getMotd()));
        this.monsterSpawn = this.configuration.getInt("spawn-limits.monsters");
        this.animalSpawn = this.configuration.getInt("spawn-limits.animals");
        this.waterAnimalSpawn = this.configuration.getInt("spawn-limits.water-animals");
        this.ambientSpawn = this.configuration.getInt("spawn-limits.ambient");
        this.warningState = WarningState.value(this.configuration.getString("settings.deprecated-verbose"));
//        this.console.autosavePeriod = this.configuration.getInt("ticks-per.autosave");
        this.chunkGCPeriod = this.configuration.getInt("chunk-gc.period-in-ticks");
        this.chunkGCLoadThresh = this.configuration.getInt("chunk-gc.load-threshold");
        this.playerList.getIPBans().load();
        this.playerList.getNameBans().load();
        SpigotConfig.init();

        WorldServer overdueWorkers;
        for (Iterator pollCount = this.console.worlds.iterator(); pollCount.hasNext(); overdueWorkers.spigotConfig.init()) {
            overdueWorkers = (WorldServer) pollCount.next();
            overdueWorkers.difficulty = difficulty;
            overdueWorkers.setSpawnFlags(monsters, animals);
            if (this.getTicksPerAnimalSpawns() < 0) {
                overdueWorkers.ticksPerAnimalSpawns = 400L;
            } else {
                overdueWorkers.ticksPerAnimalSpawns = (long) this.getTicksPerAnimalSpawns();
            }

            if (this.getTicksPerMonsterSpawns() < 0) {
                overdueWorkers.ticksPerMonsterSpawns = 1L;
            } else {
                overdueWorkers.ticksPerMonsterSpawns = (long) this.getTicksPerMonsterSpawns();
            }
        }

        this.pluginManager.clearPlugins();
        this.commandMap.clearCommands();
        this.resetRecipes();
        SpigotConfig.registerCommands();

        for (int var12 = 0; var12 < 50 && this.getScheduler().getActiveWorkers().size() > 0; ++var12) {
            try {
                Thread.sleep(50L);
            } catch (InterruptedException var11) {
            }
        }

        List var13 = this.getScheduler().getActiveWorkers();

        Plugin plugin;
        String author;
        for (Iterator i$ = var13.iterator(); i$.hasNext(); this.getLogger().log(Level.SEVERE, String.format("Nag author: \'%s\' of \'%s\' about the following: %s", new Object[]{author, plugin.getDescription().getName(), "This plugin is not properly shutting down its async tasks when it is being reloaded.  This may cause conflicts with the newly loaded version of the plugin"}))) {
            BukkitWorker worker = (BukkitWorker) i$.next();
            plugin = worker.getOwner();
            author = "<NoAuthorGiven>";
            if (plugin.getDescription().getAuthors().size() > 0) {
                author = plugin.getDescription().getAuthors().get(0);
            }
        }

        this.loadPlugins();
        this.enablePlugins(PluginLoadOrder.STARTUP);
        this.enablePlugins(PluginLoadOrder.POSTWORLD);
    }

    private void loadCustomPermissions() {
        File file = new File(this.configuration.getString("settings.permissions-file"));

        FileInputStream stream;
        try {
            stream = new FileInputStream(file);
        } catch (FileNotFoundException var28) {
            try {
                file.createNewFile();
                return;
            } finally {
                return;
            }
        }

        Map perms = new HashMap();
        label203:
        {
            try {
                perms = (Map) this.yaml.load(stream);
                break label203;
            } catch (MarkedYAMLException var29) {
                this.getLogger().log(Level.WARNING, "Server permissions file " + file + " is not valid YAML: " + var29.toString());
                return;
            } catch (Throwable var30) {
                this.getLogger().log(Level.WARNING, "Server permissions file " + file + " is not valid YAML.", var30);
            } finally {
                try {
                    stream.close();
                } catch (IOException var25) {
                }

            }

            return;
        }

        if (perms == null) {
            this.getLogger().log(Level.INFO, "서버 퍼미션 파일 " + file + " 이 비어있어 무시됩니다.");
        } else {
            List permsList = Permission.loadPermissions(perms, "Permission node \'%s\' in " + file + " is invalid", Permission.DEFAULT_PERMISSION);
            Iterator i$ = permsList.iterator();

            while (i$.hasNext()) {
                Permission perm = (Permission) i$.next();

                try {
                    this.pluginManager.addPermission(perm);
                } catch (IllegalArgumentException var27) {
                    this.getLogger().log(Level.SEVERE, "Permission in " + file + " was already defined", var27);
                }
            }

        }
    }

    public String toString() {
        return "CraftServer{serverName=CraftBukkit,serverVersion=" + this.serverVersion + ",minecraftVersion=" + this.console.getVersion() + '}';
    }

    public World createWorld(String name, Environment environment) {
        return WorldCreator.name(name).environment(environment).createWorld();
    }

    public World createWorld(String name, Environment environment, long seed) {
        return WorldCreator.name(name).environment(environment).seed(seed).createWorld();
    }

    public World createWorld(String name, Environment environment, ChunkGenerator generator) {
        return WorldCreator.name(name).environment(environment).generator(generator).createWorld();
    }

    public World createWorld(String name, Environment environment, long seed, ChunkGenerator generator) {
        return WorldCreator.name(name).environment(environment).seed(seed).generator(generator).createWorld();
    }

    public World createWorld(WorldCreator creator) {
        Validate.notNull(creator, "Creator may not be null");
        String name = creator.name();
        ChunkGenerator generator = creator.generator();
        File folder = new File(this.getWorldContainer(), name);
        World world = this.getWorld(name);
        WorldType type = WorldType.getType(creator.type().getName());
        boolean generateStructures = creator.generateStructures();
        if (world != null) {
            return world;
        } else if (folder.exists() && !folder.isDirectory()) {
            throw new IllegalArgumentException("File exists with the name \'" + name + "\' and isn\'t a folder");
        } else {
            if (generator == null) {
                generator = this.getGenerator(name);
            }

            WorldLoaderServer converter = new WorldLoaderServer(this.getWorldContainer());
            if (converter.isConvertable(name)) {
                this.getLogger().info("Converting world \'" + name + "\'");
                converter.convert(name, new ConvertProgressUpdater(this.console));
            }

            int dimension = 10 + this.console.worlds.size();
            boolean used = false;

            WorldServer internal;
            do {
                Iterator hardcore = this.console.worlds.iterator();

                while (hardcore.hasNext()) {
                    internal = (WorldServer) hardcore.next();
                    used = internal.dimension == dimension;
                    if (used) {
                        ++dimension;
                        break;
                    }
                }
            } while (used);

            boolean var22 = false;
            internal = new WorldServer(this.console, new ServerNBTManager(this.getWorldContainer(), name, true), name, dimension, new WorldSettings(creator.seed(), EnumGamemode.a(this.getDefaultGameMode().getValue()), generateStructures, var22, type), this.console.methodProfiler, this.console.getLogger(), creator.environment(), generator);
            if (!this.worlds.containsKey(name.toLowerCase())) {
                return null;
            } else {
                internal.worldMaps = this.console.worlds.get(0).worldMaps;
                internal.scoreboard = this.getScoreboardManager().getMainScoreboard().getHandle();
                internal.tracker = new EntityTracker(internal);
                internal.addIWorldAccess(new WorldManager(this.console, internal));
                internal.difficulty = 1;
                internal.setSpawnFlags(true, true);
                this.console.worlds.add(internal);
                if (generator != null) {
                    internal.getWorld().getPopulators().addAll(generator.getDefaultPopulators(internal.getWorld()));
                }

                this.pluginManager.callEvent(new WorldInitEvent(internal.getWorld()));
                System.out.print("level 의 영역 시작 준비 " + (this.console.worlds.size() - 1) + " (시드: " + internal.getSeed() + ")");
                if (internal.getWorld().getKeepSpawnInMemory()) {
                    short short1 = 196;
                    long i = System.currentTimeMillis();

                    for (int j = -short1; j <= short1; j += 16) {
                        for (int k = -short1; k <= short1; k += 16) {
                            long l = System.currentTimeMillis();
                            if (l < i) {
                                i = l;
                            }

                            if (l > i + 1000L) {
                                int chunkcoordinates = (short1 * 2 + 1) * (short1 * 2 + 1);
                                int j1 = (j + short1) * (short1 * 2 + 1) + k + 1;
                                System.out.println("스폰 영역 준비 " + name + ", " + j1 * 100 / chunkcoordinates + "%");
                                i = l;
                            }

                            ChunkCoordinates var23 = internal.getSpawn();
                            internal.chunkProviderServer.getChunkAt(var23.x + j >> 4, var23.z + k >> 4);
                        }
                    }
                }

                this.pluginManager.callEvent(new WorldLoadEvent(internal.getWorld()));
                return internal.getWorld();
            }
        }
    }

    public boolean unloadWorld(String name, boolean save) {
        return this.unloadWorld(this.getWorld(name), save);
    }

    public boolean unloadWorld(World world, boolean save) {
        if (world == null) {
            return false;
        } else {
            WorldServer handle = ((CraftWorld) world).getHandle();
            if (!this.console.worlds.contains(handle)) {
                return false;
            } else if (handle.dimension <= 1) {
                return false;
            } else if (handle.players.size() > 0) {
                return false;
            } else {
                WorldUnloadEvent e = new WorldUnloadEvent(handle.getWorld());
                this.pluginManager.callEvent(e);
                if (e.isCancelled()) {
                    return false;
                } else {
                    if (save) {
                        try {
                            handle.save(true, null);
                            handle.saveLevel();
                            WorldSaveEvent parentFolder = new WorldSaveEvent(handle.getWorld());
                            this.getPluginManager().callEvent(parentFolder);
                        } catch (ExceptionWorldConflict var13) {
                            this.getLogger().log(Level.SEVERE, null, var13);
                        }
                    }

                    this.worlds.remove(world.getName().toLowerCase());
                    this.console.worlds.remove(this.console.worlds.indexOf(handle));
                    File parentFolder1 = world.getWorldFolder().getAbsoluteFile();
                    Class var6 = RegionFileCache.class;
                    synchronized (RegionFileCache.class) {
                        Iterator i = RegionFileCache.a.entrySet().iterator();

                        while (true) {
                            while (i.hasNext()) {
                                Entry entry = (Entry) i.next();

                                for (File child = ((File) entry.getKey()).getAbsoluteFile(); child != null; child = child.getParentFile()) {
                                    if (child.equals(parentFolder1)) {
                                        i.remove();

                                        try {
                                            ((RegionFile) entry.getValue()).c();
                                        } catch (IOException var12) {
                                            this.getLogger().log(Level.SEVERE, null, var12);
                                        }
                                        break;
                                    }
                                }
                            }

                            return true;
                        }
                    }
                }
            }
        }
    }

    public MinecraftServer getServer() {
        return this.console;
    }

    public World getWorld(String name) {
        Validate.notNull(name, "Name cannot be null");
        return this.worlds.get(name.toLowerCase());
    }

    public World getWorld(UUID uid) {
        Iterator i$ = this.worlds.values().iterator();

        World world;
        do {
            if (!i$.hasNext()) {
                return null;
            }

            world = (World) i$.next();
        } while (!world.getUID().equals(uid));

        return world;
    }

    public void addWorld(World world) {
        if (this.getWorld(world.getUID()) != null) {
            System.out.println("World " + world.getName() + " is a duplicate of another world and has been prevented from loading. Please delete the uid.dat file from " + world.getName() + "\'s world directory if you want to be able to load the duplicate world.");
        } else {
            this.worlds.put(world.getName().toLowerCase(), world);
        }
    }

    public Logger getLogger() {
        return this.console.getLogger().getLogger();
    }

    public ConsoleReader getReader() {
        return this.console.reader;
    }

    public PluginCommand getPluginCommand(String name) {
        Command command = this.commandMap.getCommand(name);
        return command instanceof PluginCommand ? (PluginCommand) command : null;
    }

    public void savePlayers() {
        this.playerList.savePlayers();
    }

    public void configureDbConfig(ServerConfig config) {
        Validate.notNull(config, "Config cannot be null");
        DataSourceConfig ds = new DataSourceConfig();
        ds.setDriver(this.configuration.getString("database.driver"));
        ds.setUrl(this.configuration.getString("database.url"));
        ds.setUsername(this.configuration.getString("database.username"));
        ds.setPassword(this.configuration.getString("database.password"));
        ds.setIsolationLevel(TransactionIsolation.getLevel(this.configuration.getString("database.isolation")));
        if (ds.getDriver().contains("sqlite")) {
            config.setDatabasePlatform(new SQLitePlatform());
            config.getDatabasePlatform().getDbDdlSyntax().setIdentity("");
        }

        config.setDataSourceConfig(ds);
    }

    public boolean addRecipe(Recipe recipe) {
        Object toAdd;
        if (recipe instanceof CraftRecipe) {
            toAdd = recipe;
        } else if (recipe instanceof ShapedRecipe) {
            toAdd = CraftShapedRecipe.fromBukkitRecipe((ShapedRecipe) recipe);
        } else if (recipe instanceof ShapelessRecipe) {
            toAdd = CraftShapelessRecipe.fromBukkitRecipe((ShapelessRecipe) recipe);
        } else {
            if (!(recipe instanceof FurnaceRecipe)) {
                return false;
            }

            toAdd = CraftFurnaceRecipe.fromBukkitRecipe((FurnaceRecipe) recipe);
        }

        ((CraftRecipe) toAdd).addToCraftingManager();
        CraftingManager.getInstance().sort();
        return true;
    }

    public List<Recipe> getRecipesFor(ItemStack result) {
        Validate.notNull(result, "Result cannot be null");
        ArrayList results = new ArrayList();
        Iterator iter = this.recipeIterator();

        while (true) {
            Recipe recipe;
            ItemStack stack;
            do {
                do {
                    if (!iter.hasNext()) {
                        return results;
                    }

                    recipe = (Recipe) iter.next();
                    stack = recipe.getResult();
                } while (stack.getType() != result.getType());
            } while (result.getDurability() != -1 && result.getDurability() != stack.getDurability());

            results.add(recipe);
        }
    }

    public Iterator<Recipe> recipeIterator() {
        return new RecipeIterator();
    }

    public void clearRecipes() {
        CraftingManager.getInstance().recipes.clear();
        RecipesFurnace.getInstance().recipes.clear();
    }

    public void resetRecipes() {
        CraftingManager.getInstance().recipes = (new CraftingManager()).recipes;
        RecipesFurnace.getInstance().recipes = (new RecipesFurnace()).recipes;
    }

    public Map<String, String[]> getCommandAliases() {
        ConfigurationSection section = this.configuration.getConfigurationSection("aliases");
        LinkedHashMap result = new LinkedHashMap();
        String key;
        Object commands;
        if (section != null) {
            for (Iterator i$ = section.getKeys(false).iterator(); i$.hasNext(); result.put(key, ((List) commands).toArray(new String[((List) commands).size()]))) {
                key = (String) i$.next();
                if (section.isList(key)) {
                    commands = section.getStringList(key);
                } else {
                    commands = ImmutableList.of(section.getString(key));
                }
            }
        }

        return result;
    }

    public void removeBukkitSpawnRadius() {
        this.configuration.set("settings.spawn-radius", null);
        this.saveConfig();
    }

    public int getBukkitSpawnRadius() {
        return this.configuration.getInt("settings.spawn-radius", -1);
    }

    public String getShutdownMessage() {
        return this.configuration.getString("settings.shutdown-message");
    }

    public int getSpawnRadius() {
        return ((DedicatedServer) this.console).propertyManager.getInt("spawn-protection", 16);
    }

    public void setSpawnRadius(int value) {
        this.configuration.set("settings.spawn-radius", Integer.valueOf(value));
        this.saveConfig();
    }

    public boolean getOnlineMode() {
        return this.online.value;
    }

    public boolean getAllowFlight() {
        return this.console.getAllowFlight();
    }

    public boolean isHardcore() {
        return this.console.isHardcore();
    }

    public boolean useExactLoginLocation() {
        return this.configuration.getBoolean("settings.use-exact-login-location");
    }

    public ChunkGenerator getGenerator(String world) {
        ConfigurationSection section = this.configuration.getConfigurationSection("worlds");
        ChunkGenerator result = null;
        if (section != null) {
            section = section.getConfigurationSection(world);
            if (section != null) {
                String name = section.getString("generator");
                if (name != null && !name.equals("")) {
                    String[] split = name.split(":", 2);
                    String id = split.length > 1 ? split[1] : null;
                    Plugin plugin = this.pluginManager.getPlugin(split[0]);
                    if (plugin == null) {
                        this.getLogger().severe("Could not set generator for default world \'" + world + "\': Plugin \'" + split[0] + "\' does not exist");
                    } else if (!plugin.isEnabled()) {
                        this.getLogger().severe("Could not set generator for default world \'" + world + "\': Plugin \'" + plugin.getDescription().getFullName() + "\' is not enabled yet (is it load:STARTUP?)");
                    } else {
                        result = plugin.getDefaultWorldGenerator(world, id);
                        if (result == null) {
                            this.getLogger().severe("Could not set generator for default world \'" + world + "\': Plugin \'" + plugin.getDescription().getFullName() + "\' lacks a default world generator");
                        }
                    }
                }
            }
        }

        return result;
    }

    public CraftMapView getMap(short id) {
        WorldMapCollection collection = this.console.worlds.get(0).worldMaps;
        WorldMap worldmap = (WorldMap) collection.get(WorldMap.class, "map_" + id);
        return worldmap == null ? null : worldmap.mapView;
    }

    public CraftMapView createMap(World world) {
        Validate.notNull(world, "World cannot be null");
        net.minecraft.server.v1_5_R3.ItemStack stack = new net.minecraft.server.v1_5_R3.ItemStack(Item.MAP, 1, -1);
        WorldMap worldmap = Item.MAP.getSavedMap(stack, ((CraftWorld) world).getHandle());
        return worldmap.mapView;
    }

    public void shutdown() {
        this.console.safeShutdown();
    }

    public int broadcast(String message, String permission) {
        int count = 0;
        Set permissibles = this.getPluginManager().getPermissionSubscriptions(permission);
        Iterator i$ = permissibles.iterator();

        while (i$.hasNext()) {
            Permissible permissible = (Permissible) i$.next();
            if (permissible instanceof CommandSender && permissible.hasPermission(permission)) {
                CommandSender user = (CommandSender) permissible;
                user.sendMessage(message);
                ++count;
            }
        }

        return count;
    }

    public OfflinePlayer getOfflinePlayer(String name) {
        return this.getOfflinePlayer(name, false);
    }

    public OfflinePlayer getOfflinePlayer(String name, boolean search) {
        Validate.notNull(name, "Name cannot be null");
        OfflinePlayer result = this.getPlayerExact(name);
        String lname = name.toLowerCase();
        if (result == null) {
            result = this.offlinePlayers.get(lname);
            if (result == null) {
                if (search) {
                    WorldNBTStorage storage = (WorldNBTStorage) this.console.worlds.get(0).getDataManager();
                    String[] arr$ = storage.getPlayerDir().list(new DatFileFilter());
                    int len$ = arr$.length;

                    for (int i$ = 0; i$ < len$; ++i$) {
                        String dat = arr$[i$];
                        String datName = dat.substring(0, dat.length() - 4);
                        if (datName.equalsIgnoreCase(name)) {
                            name = datName;
                            break;
                        }
                    }
                }

                result = new CraftOfflinePlayer(this, name);
                this.offlinePlayers.put(lname, result);
            }
        } else {
            this.offlinePlayers.remove(lname);
        }

        return result;
    }

    public Set<String> getIPBans() {
        return this.playerList.getIPBans().getEntries().keySet();
    }

    public void banIP(String address) {
        Validate.notNull(address, "Address cannot be null.");
        BanEntry entry = new BanEntry(address);
        this.playerList.getIPBans().add(entry);
        this.playerList.getIPBans().save();
    }

    public void unbanIP(String address) {
        this.playerList.getIPBans().remove(address);
        this.playerList.getIPBans().save();
    }

    public Set<OfflinePlayer> getBannedPlayers() {
        HashSet result = new HashSet();
        Iterator i$ = this.playerList.getNameBans().getEntries().keySet().iterator();

        while (i$.hasNext()) {
            Object name = i$.next();
            result.add(this.getOfflinePlayer((String) name));
        }

        return result;
    }

    public void setWhitelist(boolean value) {
        this.playerList.hasWhitelist = value;
        this.console.getPropertyManager().a("white-list", Boolean.valueOf(value));
    }

    public Set<OfflinePlayer> getWhitelistedPlayers() {
        LinkedHashSet result = new LinkedHashSet();
        Iterator i$ = this.playerList.getWhitelisted().iterator();

        while (i$.hasNext()) {
            Object name = i$.next();
            if (((String) name).length() != 0 && !((String) name).startsWith("#")) {
                result.add(this.getOfflinePlayer((String) name));
            }
        }

        return result;
    }

    public Set<OfflinePlayer> getOperators() {
        HashSet result = new HashSet();
        Iterator i$ = this.playerList.getOPs().iterator();

        while (i$.hasNext()) {
            Object name = i$.next();
            result.add(this.getOfflinePlayer((String) name));
        }

        return result;
    }

    public void reloadWhitelist() {
        this.playerList.reloadWhitelist();
    }

    public GameMode getDefaultGameMode() {
        return GameMode.getByValue(this.console.worlds.get(0).getWorldData().getGameType().a());
    }

    public void setDefaultGameMode(GameMode mode) {
        Validate.notNull(mode, "Mode cannot be null");
        Iterator i$ = this.getWorlds().iterator();

        while (i$.hasNext()) {
            World world = (World) i$.next();
            ((CraftWorld) world).getHandle().worldData.setGameType(EnumGamemode.a(mode.getValue()));
        }

    }

    public ConsoleCommandSender getConsoleSender() {
        return this.console.console;
    }

    public EntityMetadataStore getEntityMetadata() {
        return this.entityMetadata;
    }

    public PlayerMetadataStore getPlayerMetadata() {
        return this.playerMetadata;
    }

    public WorldMetadataStore getWorldMetadata() {
        return this.worldMetadata;
    }

    public void detectListNameConflict(EntityPlayer entityPlayer) {
        for (int i = 0; i < this.getHandle().players.size(); ++i) {
            EntityPlayer testEntityPlayer = (EntityPlayer) this.getHandle().players.get(i);
            if (testEntityPlayer != entityPlayer && testEntityPlayer.listName.equals(entityPlayer.listName)) {
                String oldName = entityPlayer.listName;
                int spaceLeft = 16 - oldName.length();
                if (spaceLeft <= 1) {
                    entityPlayer.listName = oldName.subSequence(0, oldName.length() - 2 - spaceLeft) + String.valueOf(System.currentTimeMillis() % 99L);
                } else {
                    entityPlayer.listName = oldName + String.valueOf(System.currentTimeMillis() % 99L);
                }

                return;
            }
        }

    }

    public File getWorldContainer() {
        if (this.getServer().universe != null) {
            return this.getServer().universe;
        } else {
            if (this.container == null) {
                this.container = new File(this.configuration.getString("settings.world-container", "."));
            }

            return this.container;
        }
    }

    public OfflinePlayer[] getOfflinePlayers() {
        WorldNBTStorage storage = (WorldNBTStorage) this.console.worlds.get(0).getDataManager();
        String[] files = storage.getPlayerDir().list(new DatFileFilter());
        HashSet players = new HashSet();
        String[] arr$ = files;
        int len$ = files.length;

        for (int i$ = 0; i$ < len$; ++i$) {
            String file = arr$[i$];
            players.add(this.getOfflinePlayer(file.substring(0, file.length() - 4), false));
        }

        players.addAll(Arrays.asList(this.getOnlinePlayers()));
        return (OfflinePlayer[]) players.toArray(new OfflinePlayer[players.size()]);
    }

    public Messenger getMessenger() {
        return this.messenger;
    }

    public void sendPluginMessage(Plugin source, String channel, byte[] message) {
        StandardMessenger.validatePluginMessage(this.getMessenger(), source, channel, message);
        Player[] arr$ = this.getOnlinePlayers();
        int len$ = arr$.length;

        for (int i$ = 0; i$ < len$; ++i$) {
            Player player = arr$[i$];
            player.sendPluginMessage(source, channel, message);
        }

    }

    public Set<String> getListeningPluginChannels() {
        HashSet result = new HashSet();
        Player[] arr$ = this.getOnlinePlayers();
        int len$ = arr$.length;

        for (int i$ = 0; i$ < len$; ++i$) {
            Player player = arr$[i$];
            result.addAll(player.getListeningPluginChannels());
        }

        return result;
    }

    public void onPlayerJoin(Player player) {
        if (this.updater.isEnabled() && this.updater.getCurrent() != null && player.hasPermission("bukkit.broadcast.admin")) {
            if (this.updater.getCurrent().isBroken() && this.updater.getOnBroken().contains("warn-ops")) {
                player.sendMessage(ChatColor.DARK_RED + "The version of CraftBukkit that this server is running is known to be broken. Please consider updating to the latest version at dl.bukkit.org.");
            } else if (this.updater.isUpdateAvailable() && this.updater.getOnUpdate().contains("warn-ops")) {
                player.sendMessage(ChatColor.DARK_PURPLE + "The version of CraftBukkit that this server is running is out of date. Please consider updating to the latest version at dl.bukkit.org.");
            }
        }

    }

    public Inventory createInventory(InventoryHolder owner, InventoryType type) {
        return new CraftInventoryCustom(owner, type);
    }

    public Inventory createInventory(InventoryHolder owner, int size) throws IllegalArgumentException {
        Validate.isTrue(size % 9 == 0, "Chests must have a size that is a multiple of 9!");
        return new CraftInventoryCustom(owner, size);
    }

    public Inventory createInventory(InventoryHolder owner, int size, String title) throws IllegalArgumentException {
        Validate.isTrue(size % 9 == 0, "Chests must have a size that is a multiple of 9!");
        return new CraftInventoryCustom(owner, size, title);
    }

    public HelpMap getHelpMap() {
        return this.helpMap;
    }

    public SimpleCommandMap getCommandMap() {
        return this.commandMap;
    }

    public int getMonsterSpawnLimit() {
        return this.monsterSpawn;
    }

    public int getAnimalSpawnLimit() {
        return this.animalSpawn;
    }

    public int getWaterAnimalSpawnLimit() {
        return this.waterAnimalSpawn;
    }

    public int getAmbientSpawnLimit() {
        return this.ambientSpawn;
    }

    public boolean isPrimaryThread() {
        return Thread.currentThread().equals(this.console.primaryThread);
    }

    public String getMotd() {
        return this.console.getMotd();
    }

    public WarningState getWarningState() {
        return this.warningState;
    }

    public List<String> tabComplete(ICommandListener sender, String message) {
        if (!(sender instanceof EntityPlayer)) {
            return ImmutableList.of();
        } else {
            CraftPlayer player = ((EntityPlayer) sender).getBukkitEntity();
            return message.startsWith("/") ? this.tabCompleteCommand(player, message) : this.tabCompleteChat(player, message);
        }
    }

    public List<String> tabCompleteCommand(Player player, String message) {
        List completions = null;

        try {
            completions = SpigotConfig.tabComplete ? this.getCommandMap().tabComplete(player, message.substring(1)) : null;
        } catch (CommandException var5) {
            player.sendMessage(ChatColor.RED + "An internal error occurred while attempting to tab-complete this command");
            this.getLogger().log(Level.SEVERE, "Exception when " + player.getName() + " attempted to tab complete " + message, var5);
        }

        return (List) (completions == null ? ImmutableList.of() : completions);
    }

    public List<String> tabCompleteChat(Player player, String message) {
        Player[] players = this.getOnlinePlayers();
        ArrayList completions = new ArrayList();
        PlayerChatTabCompleteEvent event = new PlayerChatTabCompleteEvent(player, message, completions);
        String token = event.getLastToken();
        Player[] it = players;
        int current = players.length;

        for (int i$ = 0; i$ < current; ++i$) {
            Player p = it[i$];
            if (player.canSee(p) && StringUtil.startsWithIgnoreCase(p.getName(), token)) {
                completions.add(p.getName());
            }
        }

        this.pluginManager.callEvent(event);
        Iterator var11 = completions.iterator();

        while (var11.hasNext()) {
            Object var12 = var11.next();
            if (!(var12 instanceof String)) {
                var11.remove();
            }
        }

        Collections.sort(completions, String.CASE_INSENSITIVE_ORDER);
        return completions;
    }

    public CraftItemFactory getItemFactory() {
        return CraftItemFactory.instance();
    }

    public CraftScoreboardManager getScoreboardManager() {
        return this.scoreboardManager;
    }

    private final class BooleanWrapper {
        private boolean value;

        private BooleanWrapper() {
            this.value = true;
        }
    }
}
