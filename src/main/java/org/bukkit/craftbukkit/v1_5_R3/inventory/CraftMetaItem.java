package org.bukkit.craftbukkit.v1_5_R3.inventory;

/**
 * Created by EntryPoint on 2016-12-30.
 */

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import net.minecraft.server.v1_5_R3.*;
import org.apache.commons.lang.Validate;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.DelegateDeserialization;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.craftbukkit.v1_5_R3.Overridden;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.Repairable;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.HashMap;
import java.util.Map.Entry;

@DelegateDeserialization(CraftMetaItem.SerializableMeta.class)
class CraftMetaItem implements ItemMeta, Repairable {
    static final CraftMetaItem.ItemMetaKey NAME = new CraftMetaItem.ItemMetaKey("Name", "display-name");
    static final CraftMetaItem.ItemMetaKey DISPLAY = new CraftMetaItem.ItemMetaKey("display");
    static final CraftMetaItem.ItemMetaKey LORE = new CraftMetaItem.ItemMetaKey("Lore", "lore");
    static final CraftMetaItem.ItemMetaKey ENCHANTMENTS = new CraftMetaItem.ItemMetaKey("ench", "enchants");
    static final CraftMetaItem.ItemMetaKey ENCHANTMENTS_ID = new CraftMetaItem.ItemMetaKey("id");
    static final CraftMetaItem.ItemMetaKey ENCHANTMENTS_LVL = new CraftMetaItem.ItemMetaKey("lvl");
    static final CraftMetaItem.ItemMetaKey REPAIR = new CraftMetaItem.ItemMetaKey("RepairCost", "repair-cost");
    private String displayName;
    private List<String> lore;
    private Map<Enchantment, Integer> enchantments;
    private int repairCost;

    CraftMetaItem(CraftMetaItem meta) {
        if (meta != null) {
            this.displayName = SwiftUtils.limit(meta.displayName, 1024);
            if (meta.hasLore()) {
                this.lore = new ArrayList(meta.lore);
            }

            if (meta.hasEnchants()) {
                this.enchantments = new HashMap(meta.enchantments);
            }

            this.repairCost = meta.repairCost;
        }
    }

    CraftMetaItem(NBTTagCompound tag) {
        if (tag.hasKey(DISPLAY.NBT)) {
            NBTTagCompound display = tag.getCompound(DISPLAY.NBT);
            if (display.hasKey(NAME.NBT)) {
                this.displayName = display.getString(NAME.NBT);
            }

            if (display.hasKey(LORE.NBT)) {
                NBTTagList list = display.getList(LORE.NBT);
                this.lore = new ArrayList(list.size());

                for (int index = 0; index < list.size(); ++index) {
                    String line = SwiftUtils.limit(((NBTTagString) list.get(index)).data, 1024);
                    this.lore.add(line);
                }
            }
        }

        this.enchantments = buildEnchantments(tag, ENCHANTMENTS);
        if (tag.hasKey(REPAIR.NBT)) {
            this.repairCost = tag.getInt(REPAIR.NBT);
        }

    }

    CraftMetaItem(Map<String, Object> map) {
        this.setDisplayName(CraftMetaItem.SerializableMeta.getString(map, NAME.BUKKIT, true));
        Iterable lore = SerializableMeta.getObject(Iterable.class, map, LORE.BUKKIT, true);
        if (lore != null) {
            safelyAdd(lore, this.lore = new ArrayList(), 2147483647);
        }

        this.enchantments = buildEnchantments(map, ENCHANTMENTS);
        Integer repairCost = SerializableMeta.getObject(Integer.class, map, REPAIR.BUKKIT, true);
        if (repairCost != null) {
            this.setRepairCost(repairCost.intValue());
        }

    }

    static Map<Enchantment, Integer> buildEnchantments(NBTTagCompound tag, CraftMetaItem.ItemMetaKey key) {
        if (!tag.hasKey(key.NBT)) {
            return null;
        } else {
            NBTTagList ench = tag.getList(key.NBT);
            HashMap enchantments = new HashMap(ench.size());

            for (int i = 0; i < ench.size(); ++i) {
                int id = '\uffff' & ((NBTTagCompound) ench.get(i)).getShort(ENCHANTMENTS_ID.NBT);
                int level = '\uffff' & ((NBTTagCompound) ench.get(i)).getShort(ENCHANTMENTS_LVL.NBT);
                enchantments.put(Enchantment.getById(id), Integer.valueOf(level));
            }

            return enchantments;
        }
    }

    static Map<Enchantment, Integer> buildEnchantments(Map<String, Object> map, CraftMetaItem.ItemMetaKey key) {
        Map ench = SerializableMeta.getObject(Map.class, map, key.BUKKIT, true);
        if (ench == null) {
            return null;
        } else {
            HashMap enchantments = new HashMap(ench.size());
            Iterator i$ = ench.entrySet().iterator();

            while (i$.hasNext()) {
                Entry entry = (Entry) i$.next();
                Enchantment enchantment = Enchantment.getByName(entry.getKey().toString());
                if (enchantment != null && entry.getValue() instanceof Integer) {
                    enchantments.put(enchantment, entry.getValue());
                }
            }

            return enchantments;
        }
    }

    static NBTTagList createStringList(List<String> list, CraftMetaItem.ItemMetaKey key) {
        if (list != null && !list.isEmpty()) {
            NBTTagList tagList = new NBTTagList(key.NBT);
            Iterator i$ = list.iterator();

            while (i$.hasNext()) {
                String value = (String) i$.next();
                tagList.add(new NBTTagString("", value));
            }

            return tagList;
        } else {
            return null;
        }
    }

    static void applyEnchantments(Map<Enchantment, Integer> enchantments, NBTTagCompound tag, CraftMetaItem.ItemMetaKey key) {
        if (enchantments != null && enchantments.size() != 0) {
            NBTTagList list = new NBTTagList(key.NBT);
            Iterator i$ = enchantments.entrySet().iterator();

            while (i$.hasNext()) {
                Entry entry = (Entry) i$.next();
                NBTTagCompound subtag = new NBTTagCompound();
                subtag.setShort(ENCHANTMENTS_ID.NBT, (short) ((Enchantment) entry.getKey()).getId());
                subtag.setShort(ENCHANTMENTS_LVL.NBT, ((Integer) entry.getValue()).shortValue());
                list.add(subtag);
            }

            tag.set(key.NBT, list);
        }
    }

    static void serializeEnchantments(Map<Enchantment, Integer> enchantments, Builder<String, Object> builder, CraftMetaItem.ItemMetaKey key) {
        if (enchantments != null && !enchantments.isEmpty()) {
            Builder enchants = ImmutableMap.builder();
            Iterator i$ = enchantments.entrySet().iterator();

            while (i$.hasNext()) {
                Entry enchant = (Entry) i$.next();
                enchants.put(((Enchantment) enchant.getKey()).getName(), enchant.getValue());
            }

            builder.put(key.BUKKIT, enchants.build());
        }
    }

    static void safelyAdd(Iterable<?> addFrom, Collection<String> addTo, int maxItemLength) {
        if (addFrom != null) {
            Iterator i$ = addFrom.iterator();

            while (i$.hasNext()) {
                Object object = i$.next();
                if (!(object instanceof String)) {
                    if (object != null) {
                        throw new IllegalArgumentException(addFrom + " cannot contain non-string " + object.getClass().getName());
                    }

                    addTo.add("");
                } else {
                    String page = object.toString();
                    if (page.length() > maxItemLength) {
                        page = page.substring(0, maxItemLength);
                    }

                    addTo.add(page);
                }
            }

        }
    }

    static boolean checkConflictingEnchants(Map<Enchantment, Integer> enchantments, Enchantment ench) {
        if (enchantments != null && !enchantments.isEmpty()) {
            Iterator i$ = enchantments.keySet().iterator();

            Enchantment enchant;
            do {
                if (!i$.hasNext()) {
                    return false;
                }

                enchant = (Enchantment) i$.next();
            } while (!enchant.conflictsWith(ench));

            return true;
        } else {
            return false;
        }
    }

    @Overridden
    void applyToItem(NBTTagCompound itemTag) {
        if (this.hasDisplayName()) {
            this.setDisplayTag(itemTag, NAME.NBT, new NBTTagString(NAME.NBT, this.displayName));
        }

        if (this.hasLore()) {
            this.setDisplayTag(itemTag, LORE.NBT, createStringList(this.lore, LORE));
        }

        applyEnchantments(this.enchantments, itemTag, ENCHANTMENTS);
        if (this.hasRepairCost()) {
            itemTag.setInt(REPAIR.NBT, this.repairCost);
        }

    }

    void setDisplayTag(NBTTagCompound tag, String key, NBTBase value) {
        NBTTagCompound display = tag.getCompound(DISPLAY.NBT);
        if (!tag.hasKey(DISPLAY.NBT)) {
            tag.setCompound(DISPLAY.NBT, display);
        }

        display.set(key, value);
    }

    @Overridden
    boolean applicableTo(Material type) {
        return type != Material.AIR;
    }

    @Overridden
    boolean isEmpty() {
        return !this.hasDisplayName() && !this.hasEnchants() && !this.hasLore();
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public final void setDisplayName(String name) {
        this.displayName = name;
    }

    public boolean hasDisplayName() {
        return !Strings.isNullOrEmpty(this.displayName);
    }

    public boolean hasLore() {
        return this.lore != null && !this.lore.isEmpty();
    }

    public boolean hasRepairCost() {
        return this.repairCost > 0;
    }

    public boolean hasEnchant(Enchantment ench) {
        return this.hasEnchants() && this.enchantments.containsKey(ench);
    }

    public int getEnchantLevel(Enchantment ench) {
        Integer level = this.hasEnchants() ? this.enchantments.get(ench) : null;
        return level == null ? 0 : level.intValue();
    }

    public Map<Enchantment, Integer> getEnchants() {
        return this.hasEnchants() ? ImmutableMap.copyOf(this.enchantments) : ImmutableMap.of();
    }

    public boolean addEnchant(Enchantment ench, int level, boolean ignoreRestrictions) {
        if (this.enchantments == null) {
            this.enchantments = new HashMap(4);
        }

        if (!ignoreRestrictions && (level < ench.getStartLevel() || level > ench.getMaxLevel())) {
            return false;
        } else {
            Integer old = this.enchantments.put(ench, Integer.valueOf(level));
            return old == null || old.intValue() != level;
        }
    }

    public boolean removeEnchant(Enchantment ench) {
        return this.hasEnchants() && this.enchantments.remove(ench) != null;
    }

    public boolean hasEnchants() {
        return this.enchantments != null && !this.enchantments.isEmpty();
    }

    public boolean hasConflictingEnchant(Enchantment ench) {
        return checkConflictingEnchants(this.enchantments, ench);
    }

    public List<String> getLore() {
        return this.lore == null ? null : new ArrayList(this.lore);
    }

    public void setLore(List<String> lore) {
        if (lore == null) {
            this.lore = null;
        } else if (this.lore == null) {
            safelyAdd(lore, this.lore = new ArrayList(lore.size()), 2147483647);
        } else {
            this.lore.clear();
            safelyAdd(lore, this.lore, 2147483647);
        }

    }

    public int getRepairCost() {
        return this.repairCost;
    }

    public void setRepairCost(int cost) {
        this.repairCost = cost;
    }

    public final boolean equals(Object object) {
        return object != null && (object == this || (object instanceof CraftMetaItem && CraftItemFactory.instance().equals(this, (ItemMeta) object)));
    }

    @Overridden
    boolean equalsCommon(CraftMetaItem that) {
        boolean var10000;
        label60:
        {
            label55:
            {
                if (this.hasDisplayName()) {
                    if (!that.hasDisplayName() || !this.displayName.equals(that.displayName)) {
                        break label55;
                    }
                } else if (that.hasDisplayName()) {
                    break label55;
                }

                if (this.hasEnchants()) {
                    if (!that.hasEnchants() || !this.enchantments.equals(that.enchantments)) {
                        break label55;
                    }
                } else if (that.hasEnchants()) {
                    break label55;
                }

                if (this.hasLore()) {
                    if (!that.hasLore() || !this.lore.equals(that.lore)) {
                        break label55;
                    }
                } else if (that.hasLore()) {
                    break label55;
                }

                if (this.hasRepairCost()) {
                    if (that.hasRepairCost() && this.repairCost == that.repairCost) {
                        break label60;
                    }
                } else if (!that.hasRepairCost()) {
                    break label60;
                }
            }

            var10000 = false;
            return var10000;
        }

        var10000 = true;
        return var10000;
    }

    @Overridden
    boolean notUncommon(CraftMetaItem meta) {
        return true;
    }

    public final int hashCode() {
        return this.applyHash();
    }

    @Overridden
    int applyHash() {
        byte hash = 3;
        int hash1 = 61 * hash + (this.hasDisplayName() ? this.displayName.hashCode() : 0);
        hash1 = 61 * hash1 + (this.hasLore() ? this.lore.hashCode() : 0);
        hash1 = 61 * hash1 + (this.hasEnchants() ? this.enchantments.hashCode() : 0);
        hash1 = 61 * hash1 + (this.hasRepairCost() ? this.repairCost : 0);
        return hash1;
    }

    @Overridden
    public CraftMetaItem clone() {
        try {
            CraftMetaItem e = (CraftMetaItem) super.clone();
            if (this.lore != null) {
                e.lore = new ArrayList(this.lore);
            }

            if (this.enchantments != null) {
                e.enchantments = new HashMap(this.enchantments);
            }

            return e;
        } catch (CloneNotSupportedException var2) {
            throw new Error(var2);
        }
    }

    public final Map<String, Object> serialize() {
        Builder map = ImmutableMap.builder();
        map.put("meta-type", CraftMetaItem.SerializableMeta.classMap.get(this.getClass()));
        this.serialize(map);
        return map.build();
    }

    @Overridden
    Builder<String, Object> serialize(Builder<String, Object> builder) {
        if (this.hasDisplayName()) {
            builder.put(NAME.BUKKIT, this.displayName);
        }

        if (this.hasLore()) {
            builder.put(LORE.BUKKIT, ImmutableList.copyOf(this.lore));
        }

        serializeEnchantments(this.enchantments, builder, ENCHANTMENTS);
        if (this.hasRepairCost()) {
            builder.put(REPAIR.BUKKIT, Integer.valueOf(this.repairCost));
        }

        return builder;
    }

    public final String toString() {
        return SerializableMeta.classMap.get(this.getClass()) + "_META:" + this.serialize();
    }

    @SerializableAs("ItemMeta")
    public static class SerializableMeta implements ConfigurationSerializable {
        static final String TYPE_FIELD = "meta-type";
        static final ImmutableMap<Class<? extends CraftMetaItem>, String> classMap = ImmutableMap.<Class<? extends CraftMetaItem>, String>builder()
                .put(CraftMetaBook.class, "BOOK")
                .put(CraftMetaSkull.class, "SKULL")
                .put(CraftMetaLeatherArmor.class, "LEATHER_ARMOR")
                .put(CraftMetaMap.class, "MAP")
                .put(CraftMetaPotion.class, "POTION").put(CraftMetaEnchantedBook.class, "ENCHANTED")
                .put(CraftMetaFirework.class, "FIREWORK").put(CraftMetaCharge.class, "FIREWORK_EFFECT")
                .put(CraftMetaItem.class, "UNSPECIFIC").build();
        static final ImmutableMap<String, Constructor<? extends CraftMetaItem>> constructorMap;

        static {
            Builder classConstructorBuilder = ImmutableMap.builder();
            Iterator i$ = classMap.entrySet().iterator();

            while (i$.hasNext()) {
                Entry mapping = (Entry) i$.next();

                try {
                    classConstructorBuilder.put(mapping.getValue(), ((Class) mapping.getKey()).getDeclaredConstructor(Map.class));
                } catch (NoSuchMethodException var4) {
                    throw new AssertionError(var4);
                }
            }

            constructorMap = classConstructorBuilder.build();
        }

        private SerializableMeta() {
        }

        public static ItemMeta deserialize(Map<String, Object> map) throws Throwable {
            Validate.notNull(map, "Cannot deserialize null map");
            String type = getString(map, "meta-type", false);
            Constructor constructor = constructorMap.get(type);
            if (constructor == null) {
                throw new IllegalArgumentException(type + " is not a valid " + "meta-type");
            } else {
                try {
                    return (ItemMeta) constructor.newInstance(map);
                } catch (InstantiationException var4) {
                    throw new AssertionError(var4);
                } catch (IllegalAccessException var5) {
                    throw new AssertionError(var5);
                } catch (InvocationTargetException var6) {
                    throw var6.getCause();
                }
            }
        }

        static String getString(Map<?, ?> map, Object field, boolean nullable) {
            return getObject(String.class, map, field, nullable);
        }

        static boolean getBoolean(Map<?, ?> map, Object field) {
            Boolean value = getObject(Boolean.class, map, field, true);
            return value != null && value.booleanValue();
        }

        static <T> T getObject(Class<T> clazz, Map<?, ?> map, Object field, boolean nullable) {
            Object object = map.get(field);
            if (clazz.isInstance(object)) {
                return clazz.cast(object);
            } else if (object == null) {
                if (!nullable) {
                    throw new NoSuchElementException(map + " does not contain " + field);
                } else {
                    return null;
                }
            } else {
                throw new IllegalArgumentException(field + "(" + object + ") is not a valid " + clazz);
            }
        }

        public Map<String, Object> serialize() {
            throw new AssertionError();
        }
    }

    static class ItemMetaKey {
        final String BUKKIT;
        final String NBT;

        ItemMetaKey(String both) {
            this(both, both);
        }

        ItemMetaKey(String nbt, String bukkit) {
            this.NBT = nbt;
            this.BUKKIT = bukkit;
        }

        @Retention(RetentionPolicy.SOURCE)
        @Target({ElementType.FIELD})
        @interface Specific {
            CraftMetaItem.ItemMetaKey.Specific.To value();

            enum To {
                BUKKIT,
                NBT;

                To() {
                }
            }
        }
    }
}
