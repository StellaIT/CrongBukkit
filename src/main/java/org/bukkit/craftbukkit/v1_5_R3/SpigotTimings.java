package org.bukkit.craftbukkit.v1_5_R3;

/**
 * Created by EntryPoint on 2016-12-30.
 */

import net.minecraft.server.v1_5_R3.Entity;
import net.minecraft.server.v1_5_R3.TileEntity;
import net.minecraft.server.v1_5_R3.World;
import org.bukkit.scheduler.BukkitTask;
import org.spigotmc.CustomTimingsHandler;

import java.util.HashMap;

public class SpigotTimings {
    public static final CustomTimingsHandler serverTickTimer = new CustomTimingsHandler("** Full Server Tick");
    public static final CustomTimingsHandler playerListTimer = new CustomTimingsHandler("Player List");
    public static final CustomTimingsHandler connectionTimer = new CustomTimingsHandler("Player Tick");
    public static final CustomTimingsHandler tickablesTimer = new CustomTimingsHandler("Tickables");
    public static final CustomTimingsHandler schedulerTimer = new CustomTimingsHandler("Scheduler");
    public static final CustomTimingsHandler chunkIOTickTimer = new CustomTimingsHandler("ChunkIOTick");
    public static final CustomTimingsHandler syncChunkLoadTimer = new CustomTimingsHandler("syncChunkLoad");
    public static final CustomTimingsHandler entityMoveTimer = new CustomTimingsHandler("** entityMove");
    public static final CustomTimingsHandler tickEntityTimer = new CustomTimingsHandler("** tickEntity");
    public static final CustomTimingsHandler activatedEntityTimer = new CustomTimingsHandler("** activatedTickEntity");
    public static final CustomTimingsHandler tickTileEntityTimer = new CustomTimingsHandler("** tickTileEntity");
    public static final CustomTimingsHandler timerEntityBaseTick = new CustomTimingsHandler("** livingEntityBaseTick");
    public static final CustomTimingsHandler timerEntityAI = new CustomTimingsHandler("** livingEntityAI");
    public static final CustomTimingsHandler timerEntityAICollision = new CustomTimingsHandler("** livingEntityAICollision");
    public static final CustomTimingsHandler timerEntityAIMove = new CustomTimingsHandler("** livingEntityAIMove");
    public static final CustomTimingsHandler timerEntityTickRest = new CustomTimingsHandler("** livingEntityTickRest");
    public static final CustomTimingsHandler playerCommandTimer = new CustomTimingsHandler("** playerCommand");
    public static final CustomTimingsHandler entityActivationCheckTimer = new CustomTimingsHandler("entityActivationCheck");
    public static final CustomTimingsHandler checkIfActiveTimer = new CustomTimingsHandler("** checkIfActive");
    public static final HashMap<String, CustomTimingsHandler> entityTypeTimingMap = new HashMap();
    public static final HashMap<String, CustomTimingsHandler> tileEntityTypeTimingMap = new HashMap();
    public static final HashMap<String, CustomTimingsHandler> pluginTaskTimingMap = new HashMap();

    public SpigotTimings() {
    }

    public static CustomTimingsHandler getPluginTaskTimings(BukkitTask task, long period) {
        String plugin = task.getOwner().getDescription().getFullName();
        String name = "Task: " + plugin + " Id:";
        if (period > 0L) {
            name = name + "(interval:" + period + ")";
        } else {
            name = name + "(Single)";
        }

        CustomTimingsHandler result = pluginTaskTimingMap.get(name);
        if (result == null) {
            result = new CustomTimingsHandler(name);
            pluginTaskTimingMap.put(name, result);
        }

        return result;
    }

    public static CustomTimingsHandler getEntityTimings(Entity entity) {
        String entityType = entity.getClass().getSimpleName();
        CustomTimingsHandler result = entityTypeTimingMap.get(entityType);
        if (result == null) {
            result = new CustomTimingsHandler("** tickEntity - " + entityType, activatedEntityTimer);
            entityTypeTimingMap.put(entityType, result);
        }

        return result;
    }

    public static CustomTimingsHandler getTileEntityTimings(TileEntity entity) {
        String entityType = entity.getClass().getSimpleName();
        CustomTimingsHandler result = tileEntityTypeTimingMap.get(entityType);
        if (result == null) {
            result = new CustomTimingsHandler("** tickTileEntity - " + entityType, tickTileEntityTimer);
            tileEntityTypeTimingMap.put(entityType, result);
        }

        return result;
    }

    public static class WorldTimingsHandler {
        public final CustomTimingsHandler mobSpawn;
        public final CustomTimingsHandler doChunkUnload;
        public final CustomTimingsHandler doPortalForcer;
        public final CustomTimingsHandler doTickPending;
        public final CustomTimingsHandler doTickTiles;
        public final CustomTimingsHandler doVillages;
        public final CustomTimingsHandler doChunkMap;
        public final CustomTimingsHandler doChunkGC;
        public final CustomTimingsHandler doSounds;
        public final CustomTimingsHandler entityTick;
        public final CustomTimingsHandler tileEntityTick;
        public final CustomTimingsHandler tileEntityPending;
        public final CustomTimingsHandler tracker;
        public final CustomTimingsHandler lightingQueueTimer;

        public WorldTimingsHandler(World server) {
            String name = server.worldData.getName() + " - ";
            this.mobSpawn = new CustomTimingsHandler(name + "mobSpawn");
            this.doChunkUnload = new CustomTimingsHandler(name + "doChunkUnload");
            this.doTickPending = new CustomTimingsHandler(name + "doTickPending");
            this.doTickTiles = new CustomTimingsHandler(name + "doTickTiles");
            this.doVillages = new CustomTimingsHandler(name + "doVillages");
            this.doChunkMap = new CustomTimingsHandler(name + "doChunkMap");
            this.doSounds = new CustomTimingsHandler(name + "doSounds");
            this.doChunkGC = new CustomTimingsHandler(name + "doChunkGC");
            this.doPortalForcer = new CustomTimingsHandler(name + "doPortalForcer");
            this.entityTick = new CustomTimingsHandler(name + "entityTick");
            this.tileEntityTick = new CustomTimingsHandler(name + "tileEntityTick");
            this.tileEntityPending = new CustomTimingsHandler(name + "tileEntityPending");
            this.tracker = new CustomTimingsHandler(name + "tracker");
            this.lightingQueueTimer = new CustomTimingsHandler(name + "Lighting Queue");
        }
    }
}