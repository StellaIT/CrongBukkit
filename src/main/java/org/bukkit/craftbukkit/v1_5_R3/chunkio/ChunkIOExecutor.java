package org.bukkit.craftbukkit.v1_5_R3.chunkio;

/**
 * Created by EntryPoint on 2016-12-29.
 */

import net.minecraft.server.v1_5_R3.*;
import org.bukkit.craftbukkit.v1_5_R3.util.AsynchronousExecutor;
import org.bukkit.craftbukkit.v1_5_R3.util.LongHash;
import org.spigotmc.SpigotConfig;

public class ChunkIOExecutor {
    static final int BASE_THREADS = SpigotConfig.minChunkLoadThreads;
    static final int PLAYERS_PER_THREAD = 50;
    private static final AsynchronousExecutor<QueuedChunk, Chunk, Runnable, RuntimeException> instance = new AsynchronousExecutor(new ChunkIOProvider(), 1);

    public ChunkIOExecutor() {
    }

    public static void waitForChunkLoad(World world, int x, int z) {
        instance.get(new QueuedChunk(LongHash.toLong(x, z), null, world, null));
    }

    public static void queueChunkLoad(World world, ChunkRegionLoader loader, ChunkProviderServer provider, int x, int z, Runnable runnable) {
        instance.add(new QueuedChunk(LongHash.toLong(x, z), loader, world, provider), runnable);
    }

    public static void adjustPoolSize(int players) {
        int size = Math.max(1, (int) Math.ceil((double) (players / 50)));
        instance.setActiveThreads(size);
    }

    public static void tick() {
        instance.finishActive();
    }

    public static Chunk syncChunkLoad(World world, ChunkRegionLoader loader, ChunkProviderServer provider, int x, int z) {
        return SwiftUtils.ensureMain("Async Chunk Load", () -> instance.getSkipQueue(new QueuedChunk(LongHash.toLong(x, z), loader, world, provider)));
    }
}
