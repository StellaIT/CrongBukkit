package org.bukkit.craftbukkit.v1_5_R3.command;

import org.bukkit.craftbukkit.libs.jline.console.completer.Completer;
import org.bukkit.craftbukkit.v1_5_R3.CraftServer;
import org.bukkit.craftbukkit.v1_5_R3.util.Waitable;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;

/**
 * Created by Junhyeong Lim on 2017-06-23.
 */
public class ConsoleCommandCompleter implements Completer {
    private final CraftServer server;

    public ConsoleCommandCompleter(CraftServer server) {
        this.server = server;
    }

    @Override
    public int complete(final String buffer, int cursor, List<CharSequence> candidates) {
        Waitable<List<String>> waitable = new Waitable<List<String>>() {
            @Override
            protected List<String> evaluate() {
                return server.getCommandMap().tabComplete(
                        server.getConsoleSender(), buffer);
            }
        };
        server.getServer().processQueue.add(waitable);
        try {
            List<String> list = waitable.get();
            if (list != null) {
                candidates.addAll(list);
                int lastSpace = buffer.lastIndexOf(32);
                if (lastSpace == -1) {
                    cursor = cursor - buffer.length();
                }
                return cursor - (buffer.length() - lastSpace - 1);
            }
        } catch (ExecutionException e) {
            this.server.getLogger().log(Level.WARNING, "Unhandled exception when tab completing", e);
        } catch (InterruptedException v0) {
            Thread.currentThread().interrupt();
        }
        return cursor;
    }
}
