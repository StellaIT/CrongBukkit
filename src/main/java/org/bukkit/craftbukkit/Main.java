package org.bukkit.craftbukkit;

import net.minecraft.server.v1_5_R3.MinecraftServer;
import org.bukkit.craftbukkit.libs.jline.UnsupportedTerminal;
import org.bukkit.craftbukkit.libs.joptsimple.OptionException;
import org.bukkit.craftbukkit.libs.joptsimple.OptionParser;
import org.bukkit.craftbukkit.libs.joptsimple.OptionSet;
import org.bukkit.craftbukkit.v1_5_R3.CraftServer;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Junhyeong Lim on 2017-07-06.
 */
public class Main {
    public static boolean useJline = true;
    public static boolean useConsole = true;

    public static void main(String[] args) throws InterruptedException {
        OptionParser parser = new OptionParser() {
            {
                this.acceptsAll(Arrays.asList("?", "help"), "Show the help");
                this.acceptsAll(Arrays.asList("c", "config"), "Properties file to use").withRequiredArg().ofType(File.class).defaultsTo(new File("server.properties"), new File[0]).describedAs("Properties file");
                this.acceptsAll(Arrays.asList("P", "plugins"), "Plugin directory to use").withRequiredArg().ofType(File.class).defaultsTo(new File("plugins"), new File[0]).describedAs("Plugin directory");
                this.acceptsAll(Arrays.asList("h", "host", "server-ip"), "Host to listen on").withRequiredArg().ofType(String.class).describedAs("Hostname or IP");
                this.acceptsAll(Arrays.asList("W", "world-dir", "universe", "world-container"), "World container").withRequiredArg().ofType(File.class).describedAs("Directory containing worlds");
                this.acceptsAll(Arrays.asList("w", "world", "level-name"), "World name").withRequiredArg().ofType(String.class).describedAs("World name");
                this.acceptsAll(Arrays.asList("p", "port", "server-port"), "Port to listen on").withRequiredArg().ofType(Integer.class).describedAs("Port");
                this.acceptsAll(Arrays.asList("o", "online-mode"), "Whether to use online authentication").withRequiredArg().ofType(Boolean.class).describedAs("Authentication");
                this.acceptsAll(Arrays.asList("s", "size", "max-players"), "Maximum amount of players").withRequiredArg().ofType(Integer.class).describedAs("Server size");
                this.acceptsAll(Arrays.asList("d", "date-format"), "Format of the date to display in the console (for log entries)").withRequiredArg().ofType(SimpleDateFormat.class).describedAs("Log date format");
                this.acceptsAll(Arrays.asList("log-pattern"), "Specfies the log filename pattern").withRequiredArg().ofType(String.class).defaultsTo("server.log", new String[0]).describedAs("Log filename");
                this.acceptsAll(Arrays.asList("log-limit"), "Limits the maximum size of the log file (0 = unlimited)").withRequiredArg().ofType(Integer.class).defaultsTo(Integer.valueOf(0), new Integer[0]).describedAs("Max log size");
                this.acceptsAll(Arrays.asList("log-count"), "Specified how many log files to cycle through").withRequiredArg().ofType(Integer.class).defaultsTo(Integer.valueOf(1), new Integer[0]).describedAs("Log count");
                this.acceptsAll(Arrays.asList("log-append"), "Whether to append to the log file").withRequiredArg().ofType(Boolean.class).defaultsTo(Boolean.valueOf(true), new Boolean[0]).describedAs("Log append");
                this.acceptsAll(Arrays.asList("log-strip-color"), "Strips color codes from log file");
                this.acceptsAll(Arrays.asList("b", "bukkit-settings"), "File for bukkit settings").withRequiredArg().ofType(File.class).defaultsTo(new File("bukkit.yml"), new File[0]).describedAs("Yml file");
                this.acceptsAll(Arrays.asList("nojline"), "Disables jline and emulates the vanilla console");
                this.acceptsAll(Arrays.asList("noconsole"), "Disables the console");
                this.acceptsAll(Arrays.asList("v", "version"), "Show the CraftBukkit Version");
                this.acceptsAll(Arrays.asList("demo"), "Demo mode");
            }
        };
        OptionSet options = null;

        try {
            options = parser.parse(args);
        } catch (OptionException var7) {
            Logger.getLogger(org.bukkit.craftbukkit.Main.class.getName()).log(Level.SEVERE, var7.getLocalizedMessage());
        }

        if (options != null && !options.has("?")) {
            if (options.has("v")) {
                System.out.println(CraftServer.class.getPackage().getImplementationVersion());
            } else {
                try {
                    String jline_UnsupportedTerminal = new String(new char[]{'j', 'l', 'i', 'n', 'e', '.', 'U', 'n', 's', 'u', 'p', 'p', 'o', 'r', 't', 'e', 'd', 'T', 'e', 'r', 'm', 'i', 'n', 'a', 'l'});
                    String jline_terminal = new String(new char[]{'j', 'l', 'i', 'n', 'e', '.', 't', 'e', 'r', 'm', 'i', 'n', 'a', 'l'});
                    useJline = !jline_UnsupportedTerminal.equals(System.getProperty(jline_terminal));
                    if (options.has("nojline")) {
                        System.setProperty("user.language", "en");
                        useJline = false;
                    }

                    if (!useJline) {
                        System.setProperty("org.bukkit.craftbukkit.libs.jline.terminal", UnsupportedTerminal.class.getName());
                    }

                    if (options.has("noconsole")) {
                        useConsole = false;
                    }

                    MinecraftServer.main(options);
                } catch (Throwable var5) {
                    var5.printStackTrace();
                }
            }
        } else {
            try {
                parser.printHelpOn(System.out);
            } catch (IOException var6) {
                Logger.getLogger(org.bukkit.craftbukkit.Main.class.getName()).log(Level.SEVERE, null, var6);
            }
        }

    }

    private static List<String> asList(String... params) {
        return Arrays.asList(params);
    }
}
