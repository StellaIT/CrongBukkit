package org.bukkit.metadata;

/**
 * Created by Junhyeong Lim on 2017-02-15.
 */

import org.apache.commons.lang.Validate;
import org.bukkit.plugin.Plugin;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public abstract class MetadataStoreBase<T> {
    private Map<String, Map<Plugin, MetadataValue>> metadataMap = new ConcurrentHashMap<>();

    public void setMetadata(T subject, String metadataKey, MetadataValue newMetadataValue) {
        Plugin owningPlugin = newMetadataValue.getOwningPlugin();
        Validate.notNull(newMetadataValue, "Value cannot be null");
        Validate.notNull(owningPlugin, "Plugin cannot be null");
        String key = this.disambiguate(subject, metadataKey);
        Map<Plugin, MetadataValue> entry = this.metadataMap.computeIfAbsent(key, k -> new WeakHashMap<>(1));

        entry.put(owningPlugin, newMetadataValue);
    }

    public List<MetadataValue> getMetadata(T subject, String metadataKey) {
        String key = this.disambiguate(subject, metadataKey);
        if (this.metadataMap.containsKey(key)) {
            Collection<MetadataValue> values = this.metadataMap.get(key).values();
            return Collections.unmodifiableList(new ArrayList<>(values));
        } else {
            return Collections.emptyList();
        }
    }

    public boolean hasMetadata(T subject, String metadataKey) {
        String key = this.disambiguate(subject, metadataKey);
        return this.metadataMap.containsKey(key);
    }

    public void removeMetadata(T subject, String metadataKey, Plugin owningPlugin) {
        Validate.notNull(owningPlugin, "Plugin cannot be null");
        String key = this.disambiguate(subject, metadataKey);
        Map entry = this.metadataMap.get(key);
        if (entry != null) {
            entry.remove(owningPlugin);
            if (entry.isEmpty()) {
                this.metadataMap.remove(key);
            }

        }
    }

    public void invalidateAll(Plugin owningPlugin) {
        Validate.notNull(owningPlugin, "Plugin cannot be null");
        Iterator i$ = this.metadataMap.values().iterator();

        while (i$.hasNext()) {
            Map values = (Map) i$.next();
            if (values.containsKey(owningPlugin)) {
                ((MetadataValue) values.get(owningPlugin)).invalidate();
            }
        }

    }

    protected abstract String disambiguate(T var1, String var2);
}
