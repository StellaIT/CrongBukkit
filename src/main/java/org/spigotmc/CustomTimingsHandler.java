package org.spigotmc;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredListener;
import org.bukkit.plugin.TimedRegisteredListener;

import java.io.PrintStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Created by Junhyeong Lim on 2017-02-15.
 */
public class CustomTimingsHandler {
    private static final Collection<CustomTimingsHandler> ALL_HANDLERS = new HashSet();
    private static CustomTimingsHandler[] BAKED_HANDLERS;
    private final String name;
    private final CustomTimingsHandler parent;
    private long count;
    private long start;
    private long timingDepth;
    private long totalTime;
    private long curTickTotal;
    private long violations;

    public CustomTimingsHandler(String name) {
        this(name, null);
    }

    public CustomTimingsHandler(String name, CustomTimingsHandler parent) {
        this.count = 0L;
        this.start = 0L;
        this.timingDepth = 0L;
        this.totalTime = 0L;
        this.curTickTotal = 0L;
        this.violations = 0L;
        this.name = name;
        this.parent = parent;
        ALL_HANDLERS.add(this);
        BAKED_HANDLERS = ALL_HANDLERS.toArray(new CustomTimingsHandler[ALL_HANDLERS.size()]);
    }

    public static void printTimings(PrintStream printStream) {
        printStream.println("Minecraft");
        CustomTimingsHandler[] entities = BAKED_HANDLERS;
        int livingEntities = entities.length;

        for (int i$ = 0; i$ < livingEntities; ++i$) {
            CustomTimingsHandler world = entities[i$];
            long time = world.totalTime;
            long count = world.count;
            if (count != 0L) {
                long avg = time / count;
                printStream.println("    " + world.name + " Time: " + time + " Count: " + count + " Avg: " + avg + " Violations: " + world.violations);
            }
        }

        printStream.println("# Version " + Bukkit.getVersion());
        int var11 = 0;
        livingEntities = 0;

        World var13;
        for (Iterator var12 = Bukkit.getWorlds().iterator(); var12.hasNext(); livingEntities += var13.getLivingEntities().size()) {
            var13 = (World) var12.next();
            var11 += var13.getEntities().size();
        }

        printStream.println("# Entities " + var11);
        printStream.println("# LivingEntities " + livingEntities);
    }

    public static void reload() {
        if (Bukkit.getPluginManager().useTimings()) {
            CustomTimingsHandler[] arr$ = BAKED_HANDLERS;
            int len$ = arr$.length;

            for (int i$ = 0; i$ < len$; ++i$) {
                CustomTimingsHandler timings = arr$[i$];
                timings.reset();
            }
        }

    }

    public static void tick() {
        if (Bukkit.getPluginManager().useTimings()) {
            CustomTimingsHandler[] arr$ = BAKED_HANDLERS;
            int len$ = arr$.length;

            int i$;
            for (i$ = 0; i$ < len$; ++i$) {
                CustomTimingsHandler plugin = arr$[i$];
                if (plugin.curTickTotal > 50000000L) {
                    plugin.violations = (long) ((double) plugin.violations + Math.ceil((double) (plugin.curTickTotal / 50000000L)));
                }

                plugin.curTickTotal = 0L;
            }

            Plugin[] var7 = Bukkit.getPluginManager().getPlugins();
            len$ = var7.length;

            for (i$ = 0; i$ < len$; ++i$) {
                Plugin var8 = var7[i$];
                Iterator i$1 = HandlerList.getRegisteredListeners(var8).iterator();

                while (i$1.hasNext()) {
                    RegisteredListener listener = (RegisteredListener) i$1.next();
                    if (listener instanceof TimedRegisteredListener) {
                        TimedRegisteredListener timings = (TimedRegisteredListener) listener;
                        if (timings.curTickTotal > 50000000L) {
                            timings.violations = (long) ((double) timings.violations + Math.ceil((double) (timings.curTickTotal / 50000000L)));
                        }

                        timings.curTickTotal = 0L;
                    }
                }
            }
        }

    }

    public void startTiming() {
        if (Bukkit.getPluginManager().useTimings() && ++this.timingDepth == 1L) {
            this.start = System.nanoTime();
            if (this.parent != null && ++this.parent.timingDepth == 1L) {
                this.parent.start = this.start;
            }
        }

    }

    public void stopTiming() {
        if (Bukkit.getPluginManager().useTimings()) {
            if (--this.timingDepth != 0L || this.start == 0L) {
                return;
            }

            long diff = System.nanoTime() - this.start;
            this.totalTime += diff;
            this.curTickTotal += diff;
            ++this.count;
            this.start = 0L;
            if (this.parent != null) {
                this.parent.stopTiming();
            }
        }

    }

    public void reset() {
        this.count = 0L;
        this.violations = 0L;
        this.curTickTotal = 0L;
        this.totalTime = 0L;
    }
}
