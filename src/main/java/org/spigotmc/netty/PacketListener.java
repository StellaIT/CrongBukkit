package org.spigotmc.netty;

import com.google.common.base.Preconditions;
import net.minecraft.server.v1_5_R3.Connection;
import net.minecraft.server.v1_5_R3.INetworkManager;
import net.minecraft.server.v1_5_R3.Packet;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

/**
 * Created by Junhyeong Lim on 2017-07-06.
 */
public class PacketListener {
    private static final Map<PacketListener, Plugin> listeners = new HashMap();
    private static PacketListener[] baked = new PacketListener[0];

    public PacketListener() {
    }

    public static synchronized void register(PacketListener listener, Plugin plugin) {
        Preconditions.checkNotNull(listener, "listener");
        Preconditions.checkNotNull(plugin, "plugin");
        Preconditions.checkState(!listeners.containsKey(listener), "listener already registered");
        int size = listeners.size();
        Preconditions.checkState(baked.length == size);
        listeners.put(listener, plugin);
        baked = (PacketListener[]) Arrays.copyOf(baked, size + 1);
        baked[size] = listener;
    }

    static Packet callReceived(INetworkManager networkManager, Connection connection, Packet packet) {
        PacketListener[] arr$ = baked;
        int len$ = arr$.length;

        for (int i$ = 0; i$ < len$; ++i$) {
            PacketListener listener = arr$[i$];

            try {
                packet = listener.packetReceived(networkManager, connection, packet);
            } catch (Throwable var8) {
                Bukkit.getServer().getLogger().log(Level.SEVERE, "Error whilst firing receive hook for packet", var8);
            }
        }

        return packet;
    }

    static Packet callQueued(INetworkManager networkManager, Connection connection, Packet packet) {
        PacketListener[] arr$ = baked;
        int len$ = arr$.length;

        for (int i$ = 0; i$ < len$; ++i$) {
            PacketListener listener = arr$[i$];

            try {
                packet = listener.packetQueued(networkManager, connection, packet);
            } catch (Throwable var8) {
                Bukkit.getServer().getLogger().log(Level.SEVERE, "Error whilst firing queued hook for packet", var8);
            }
        }

        return packet;
    }

    public Packet packetReceived(INetworkManager networkManager, Connection connection, Packet packet) {
        return packet;
    }

    public Packet packetQueued(INetworkManager networkManager, Connection connection, Packet packet) {
        return packet;
    }
}
