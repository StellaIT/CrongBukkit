package org.spigotmc.netty;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.timeout.ReadTimeoutHandler;
import net.minecraft.server.v1_5_R3.MinecraftServer;
import net.minecraft.server.v1_5_R3.PendingConnection;
import net.minecraft.server.v1_5_R3.ServerConnection;
import org.bukkit.Bukkit;
import org.spigotmc.SpigotConfig;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.util.*;
import java.util.logging.Level;

/**
 * Created by Junhyeong Lim on 2017-07-06.
 */
public class NettyServerConnection extends ServerConnection {
    private static EventLoopGroup group;
    private final ChannelFuture socket;
    private final Map<InetAddress, Long> throttle = new HashMap<>();
    private final List<PendingConnection> pending = Collections.synchronizedList(new ArrayList<>());

    public NettyServerConnection(MinecraftServer ms, InetAddress host, int port) {
        super(ms);
        if (group == null) {
            group = new NioEventLoopGroup(SpigotConfig.nettyThreads, (new ThreadFactoryBuilder()).setNameFormat("Netty IO Thread - %1$d").build());
        }

        this.socket = new ServerBootstrap()
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer() {
                    public void initChannel(Channel ch) throws Exception {
                        if (NettyServerConnection.this.throttle(((InetSocketAddress) ch.remoteAddress()).getAddress())) {
                            ch.close();
                        } else {
                            try {
                                ch.config().setOption(ChannelOption.IP_TOS, 24);
                            } catch (ChannelException var3) {
                                // Ignore
                            }

                            NettyNetworkManager networkManager = new NettyNetworkManager();
                            ch.pipeline()
                                    .addLast("timer", new ReadTimeoutHandler(30))
                                    .addLast("decoder", new PacketDecoder())
                                    .addLast("manager", networkManager);
                        }
                    }
                })
                .childOption(ChannelOption.TCP_NODELAY, Boolean.TRUE)
                .group(group)
                .localAddress(host, port)
                .bind()
                .syncUninterruptibly();
    }

    public static Cipher getCipher(int opMode, Key key) {
        try {
            Cipher cip = Cipher.getInstance("AES/CFB8/NoPadding");
            cip.init(opMode, key, new IvParameterSpec(key.getEncoded()));
            return cip;
        } catch (GeneralSecurityException var3) {
            throw new RuntimeException(var3);
        }
    }

    public void unThrottle(InetAddress address) {
        if (address != null) {
            Map var2 = this.throttle;
            synchronized (this.throttle) {
                this.throttle.remove(address);
            }
        }

    }

    public boolean throttle(InetAddress address) {
        long currentTime = System.currentTimeMillis();
        Map var4 = this.throttle;
        synchronized (this.throttle) {
            Long value = this.throttle.get(address);
            if (value != null && !address.isLoopbackAddress() && currentTime - value.longValue() < this.d().server.getConnectionThrottle()) {
                this.throttle.put(address, currentTime);
                return true;
            } else {
                this.throttle.put(address, currentTime);
                return false;
            }
        }
    }

    public void a() {
        this.socket.channel().close().syncUninterruptibly();
    }

    public void b() {
        super.b();

        for (int i = 0; i < this.pending.size(); ++i) {
            PendingConnection connection = this.pending.get(i);

            try {
                connection.c();
            } catch (Exception var4) {
                connection.disconnect("Internal server error");
                Bukkit.getServer().getLogger().log(Level.WARNING, "Failed to handle packet: " + var4, var4);
            }

            if (connection.b) {
                this.pending.remove(i--);
            }
        }

    }

    public void register(PendingConnection conn) {
        this.pending.add(conn);
    }
}
