package org.spigotmc.netty;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;
import net.minecraft.server.v1_5_R3.MinecraftServer;
import net.minecraft.server.v1_5_R3.Packet;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.List;

/**
 * Created by Junhyeong Lim on 2017-07-06.
 */
public class PacketDecoder extends ReplayingDecoder<ReadState> {
    private DataInputStream input;
    private Packet packet;

    public PacketDecoder() {
        super(ReadState.HEADER);
    }

    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out)
            throws Exception {
        if (this.input == null) {
            this.input = new DataInputStream(new ByteBufInputStream(in));
        }
        switch (state()) {
            case HEADER:
                short packetId = in.readUnsignedByte();
                this.packet = Packet.a(MinecraftServer.getServer().getLogger(), packetId);
                if (this.packet == null) {
                    throw new IOException("Bad packet id " + packetId);
                }
                checkpoint(ReadState.DATA);
            case DATA:
                try {
                    this.packet.a(this.input);
                } catch (EOFException ex) {
                    return;
                }
                checkpoint(ReadState.HEADER);
                out.add(this.packet);
                this.packet = null;
                break;
            default:
                throw new IllegalStateException();
        }
    }
}
