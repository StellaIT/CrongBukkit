package org.spigotmc.netty;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.socket.SocketChannel;
import net.minecraft.server.v1_5_R3.*;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import java.net.Socket;
import java.net.SocketAddress;
import java.security.PrivateKey;
import java.util.AbstractList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Junhyeong Lim on 2017-07-06.
 */
public class NettyNetworkManager extends ChannelInboundHandlerAdapter implements INetworkManager {
    private static final ExecutorService threadPool = Executors.newCachedThreadPool((new ThreadFactoryBuilder()).setNameFormat("Async Packet Handler - %1$d").build());
    private static final MinecraftServer server = MinecraftServer.getServer();
    private static final PrivateKey key;
    private static final NettyServerConnection serverConnection;

    static {
        key = server.F().getPrivate();
        serverConnection = (NettyServerConnection) server.ae();
    }

    private final Queue<Packet> syncPackets = new ConcurrentLinkedQueue<>();
    private final List<Packet> highPriorityQueue = new AbstractList<Packet>() {
        public void add(int index, Packet element) {
        }

        public Packet get(int index) {
            throw new UnsupportedOperationException();
        }

        public int size() {
            return 0;
        }
    };
    Connection connection;
    private volatile boolean connected;
    private Channel channel;
    private SocketAddress address;
    private SecretKey secret;
    private String dcReason;
    private Object[] dcArgs;
    private Socket socketAdaptor;
    private long writtenBytes;
    private PacketWriter writer;

    public NettyNetworkManager() {
    }

    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        this.channel = ctx.channel();
        this.address = this.channel.remoteAddress();
        this.socketAdaptor = NettySocketAdaptor.adapt((SocketChannel) this.channel);
        this.connection = new PendingConnection(server, this);
        this.writer = new PacketWriter();
        this.connected = true;
        serverConnection.register((PendingConnection) this.connection);
    }

    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        this.writer.release();
        this.a("disconnect.endOfStream", new Object[0]);
    }

    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        this.a("disconnect.genericReason", new Object[]{"Internal exception: " + cause});
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object obj) throws Exception {
        Packet msg = (Packet) obj;
        if (this.connected) {
            if (msg instanceof Packet252KeyResponse) {
                this.secret = ((Packet252KeyResponse) msg).a(key);
                Cipher decrypt = NettyServerConnection.getCipher(2, this.secret);
                this.channel.pipeline().addBefore("decoder", "decrypt", new CipherDecoder(decrypt));
            }

            if (msg.a_()) {
                threadPool.submit(new Runnable() {
                    public void run() {
                        Packet packet = PacketListener.callReceived(NettyNetworkManager.this, NettyNetworkManager.this.connection, msg);
                        if (packet != null) {
                            packet.handle(NettyNetworkManager.this.connection);
                        }
                    }
                });
            } else {
                this.syncPackets.add(msg);
            }
        }
    }

    public Socket getSocket() {
        return this.socketAdaptor;
    }

    public void a(Connection nh) {
        this.connection = nh;
    }

    public void queue(final Packet packet) {
        if (this.connected) {
            if (this.channel.eventLoop().inEventLoop()) {
                this.queue0(packet);
            } else {
                this.channel.eventLoop().execute(new Runnable() {
                    public void run() {
                        NettyNetworkManager.this.queue0(packet);
                    }
                });
            }
        }

    }

    private void queue0(Packet packet) {
        packet = PacketListener.callQueued(this, this.connection, packet);
        if (packet != null) {
            this.highPriorityQueue.add(packet);

            this.writer.write(this.channel, this, packet);
            if (packet instanceof Packet252KeyResponse) {
                Cipher encrypt = NettyServerConnection.getCipher(1, this.secret);
                this.channel.pipeline().addBefore("decoder", "encrypt", new CipherEncoder(encrypt));
            }
        }

    }

    public void a() {
    }

    public void b() {
        for (int i = 1000; !this.syncPackets.isEmpty() && i >= 0; --i) {
            label37:
            {
                if (this.connection instanceof PendingConnection) {
                    if (!((PendingConnection) this.connection).b) {
                        break label37;
                    }
                } else if (!((PlayerConnection) this.connection).disconnected) {
                    break label37;
                }

                this.syncPackets.clear();
                break;
            }

            Packet packet = PacketListener.callReceived(this, this.connection, (Packet) this.syncPackets.poll());
            if (packet != null) {
                packet.handle(this.connection);
            }
        }

        if (!this.connected && (this.dcReason != null || this.dcArgs != null)) {
            this.connection.a(this.dcReason, this.dcArgs);
        }

    }

    public SocketAddress getSocketAddress() {
        return this.address;
    }

    public void setSocketAddress(SocketAddress address) {
        this.address = address;
    }

    public void d() {
        if (this.connected) {
            this.connected = false;
            this.channel.close();
        }

    }

    public int e() {
        return 0;
    }

    public void a(String reason, Object... arguments) {
        if (this.connected) {
            this.dcReason = reason;
            this.dcArgs = arguments;
            this.d();
        }

    }

    public long getWrittenBytes() {
        return this.writtenBytes;
    }

    public void addWrittenBytes(int written) {
        this.writtenBytes += (long) written;
    }
}
