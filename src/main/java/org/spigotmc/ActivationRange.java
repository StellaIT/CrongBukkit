package org.spigotmc;

/**
 * Created by EntryPoint on 2016-12-29.
 */

import net.minecraft.server.v1_5_R3.*;
import org.bukkit.craftbukkit.v1_5_R3.SpigotTimings;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ActivationRange {
    static AxisAlignedBB maxBB = AxisAlignedBB.a(0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D);
    static AxisAlignedBB miscBB = AxisAlignedBB.a(0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D);
    static AxisAlignedBB animalBB = AxisAlignedBB.a(0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D);
    static AxisAlignedBB monsterBB = AxisAlignedBB.a(0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D);

    public ActivationRange() {
    }

    public static byte initializeEntityActivationType(Entity entity) {
        return (byte) (!(entity instanceof EntityMonster) && !(entity instanceof EntitySlime) ? (!(entity instanceof EntityCreature) && !(entity instanceof EntityAmbient) ? 3 : 2) : 1);
    }

    public static boolean initializeEntityActivationState(Entity entity, SpigotWorldConfig config) {
        return entity.activationType == 3 && config.miscActivationRange == 0 || entity.activationType == 2 && config.animalActivationRange == 0 || entity.activationType == 1 && config.monsterActivationRange == 0 || entity instanceof EntityHuman || entity instanceof EntityProjectile || entity instanceof EntityEnderDragon || entity instanceof EntityComplexPart || entity instanceof EntityWither || entity instanceof EntityFireball || entity instanceof EntityWeather || entity instanceof EntityTNTPrimed || entity instanceof EntityEnderCrystal || entity instanceof EntityFireworks;
    }

    public static void growBB(AxisAlignedBB target, AxisAlignedBB source, int x, int y, int z) {
        target.a = source.a - (double) x;
        target.b = source.b - (double) y;
        target.c = source.c - (double) z;
        target.d = source.d + (double) x;
        target.e = source.e + (double) y;
        target.f = source.f + (double) z;
    }

    public static void activateEntities(World world) {
        SpigotTimings.entityActivationCheckTimer.startTiming();
        int miscActivationRange = world.spigotConfig.miscActivationRange;
        int animalActivationRange = world.spigotConfig.animalActivationRange;
        int monsterActivationRange = world.spigotConfig.monsterActivationRange;
        int maxRange = Math.max(monsterActivationRange, animalActivationRange);
        maxRange = Math.max(maxRange, miscActivationRange);
        maxRange = Math.min((world.spigotConfig.viewDistance << 4) - 8, maxRange);
        Iterator i$ = (new ArrayList(world.players)).iterator();

        Chunk chunk;
        while (i$.hasNext()) {
            Entity player = (Entity) i$.next();
            player.activatedTick = (long) MinecraftServer.currentTick;
            growBB(maxBB, player.boundingBox, maxRange, 256, maxRange);
            growBB(miscBB, player.boundingBox, miscActivationRange, 256, miscActivationRange);
            growBB(animalBB, player.boundingBox, animalActivationRange, 256, animalActivationRange);
            growBB(monsterBB, player.boundingBox, monsterActivationRange, 256, monsterActivationRange);
            int i = MathHelper.floor(maxBB.a / 16.0D);
            int j = MathHelper.floor(maxBB.d / 16.0D);
            int k = MathHelper.floor(maxBB.c / 16.0D);
            int l = MathHelper.floor(maxBB.f / 16.0D);

            for (int i1 = i; i1 <= j; ++i1) {
                for (int j1 = k; j1 <= l; ++j1) {
                    if ((chunk = SwiftUtils.getLoadedChunkWithoutMarkingActive(world, i1, j1)) != null) {
                        activateChunkEntities(chunk);
                    }
                }
            }
        }

        SpigotTimings.entityActivationCheckTimer.stopTiming();
    }

    private static void activateChunkEntities(Chunk chunk) {
        List[] arr$ = chunk.entitySlices;
        int len$ = arr$.length;

        for (int i$ = 0; i$ < len$; ++i$) {
            List slice = arr$[i$];
            Iterator i$1 = slice.iterator();

            while (i$1.hasNext()) {
                Entity entity = (Entity) i$1.next();
                if ((long) MinecraftServer.currentTick > entity.activatedTick) {
                    if (entity.defaultActivationState) {
                        entity.activatedTick = (long) MinecraftServer.currentTick;
                    } else {
                        switch (entity.activationType) {
                            case 1:
                                if (monsterBB.a(entity.boundingBox)) {
                                    entity.activatedTick = (long) MinecraftServer.currentTick;
                                }
                                break;
                            case 2:
                                if (animalBB.a(entity.boundingBox)) {
                                    entity.activatedTick = (long) MinecraftServer.currentTick;
                                }
                                break;
                            case 3:
                            default:
                                if (miscBB.a(entity.boundingBox)) {
                                    entity.activatedTick = (long) MinecraftServer.currentTick;
                                }
                        }
                    }
                }
            }
        }

    }

    public static boolean checkEntityImmunities(Entity entity) {
        if (!entity.inWater && entity.fireTicks <= 0) {
            if (!(entity instanceof EntityArrow)) {
                if (!entity.onGround || entity.passenger != null || entity.vehicle != null) {
                    return true;
                }
            } else if (!((EntityArrow) entity).inGround) {
                return true;
            }

            if (entity instanceof EntityLiving) {
                EntityLiving living = (EntityLiving) entity;
                if (living.attackTicks > 0 || living.hurtTicks > 0 || living.effects.size() > 0) {
                    return true;
                }

                if (entity instanceof EntityCreature && ((EntityCreature) entity).target != null) {
                    return true;
                }

                if (entity instanceof EntityVillager && ((EntityVillager) entity).n()) {
                    return true;
                }

                if (entity instanceof EntityAnimal) {
                    EntityAnimal animal = (EntityAnimal) entity;
                    if (animal.isBaby() || animal.r()) {
                        return true;
                    }

                    if (entity instanceof EntitySheep && ((EntitySheep) entity).isSheared()) {
                        return true;
                    }
                }
            }

            return false;
        } else {
            return true;
        }
    }

    public static boolean checkIfActive(Entity entity) {
        SpigotTimings.checkIfActiveTimer.startTiming();
        boolean isActive = entity.activatedTick >= (long) MinecraftServer.currentTick || entity.defaultActivationState;
        if (!isActive) {
            if (((long) MinecraftServer.currentTick - entity.activatedTick - 1L) % 20L == 0L) {
                if (checkEntityImmunities(entity)) {
                    entity.activatedTick = (long) (MinecraftServer.currentTick + 20);
                }

                isActive = true;
            }
        } else if (!entity.defaultActivationState && entity.ticksLived % 4 == 0 && !checkEntityImmunities(entity)) {
            isActive = false;
        }

        int x = MathHelper.floor(entity.locX);
        int z = MathHelper.floor(entity.locZ);
        if (isActive && !entity.world.areChunksLoaded(x, 0, z, 16)) {
            isActive = false;
        }

        SpigotTimings.checkIfActiveTimer.stopTiming();
        return isActive;
    }
}
