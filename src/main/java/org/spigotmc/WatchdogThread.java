package org.spigotmc;

import net.minecraft.server.v1_5_R3.SwiftCrashReport;
import net.minecraft.server.v1_5_R3.SwiftLogger;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;

import java.lang.management.ManagementFactory;
import java.lang.management.MonitorInfo;
import java.lang.management.ThreadInfo;
import java.util.logging.Level;

public class WatchdogThread extends Thread {
    private static WatchdogThread instance;
    private final long timeoutTime;
    private final boolean restart;
    private volatile long lastTick;
    private volatile boolean stopping;

    private WatchdogThread(long timeoutTime, boolean restart) {
        super("Spigot Watchdog Thread");
        this.timeoutTime = timeoutTime;
        this.restart = restart;
    }

    public static void doStart(int timeoutTime, boolean restart) {
        if (instance == null) {
            instance = new WatchdogThread((long) timeoutTime * 1000L, restart);
            instance.start();
        }

    }

    public static void tick() {
        instance.lastTick = System.currentTimeMillis();
    }

    public static void doStop() {
        if (instance != null) {
            instance.stopping = true;
        }

    }

    public void run() {
        while (!this.stopping) {
            if (this.lastTick != 0L && System.currentTimeMillis() > this.lastTick + this.timeoutTime) {
                SwiftLogger ex = new SwiftLogger(Bukkit.getServer().getLogger());
                ex.log(Level.SEVERE, "서버에 응답이 없습니다!");
                ex.log(Level.SEVERE, "밑의 내용은 에러 로그가 아닌 당시에 작동하던 스레드의 로그입니다.");
                ex.log(Level.SEVERE, "밑의 전체 내용을 포함해 https://github.com/apexnetwork/SwiftBukkit-Issues 에 제보해주세요.");
                ex.log(Level.SEVERE, "버킷 버전: " + Bukkit.getServer().getVersion());
                ex.log(Level.SEVERE, "현재 스레드 상태:");
                ThreadInfo[] threads = ManagementFactory.getThreadMXBean().dumpAllThreads(true, true);
                ThreadInfo[] arr$ = threads;
                int len$ = threads.length;

                for (int i$ = 0; i$ < len$; ++i$) {
                    ThreadInfo thread = arr$[i$];
                    if (thread.getThreadState() != State.WAITING) {
                        ex.log(Level.SEVERE, "------------------------------");
                        ex.log(Level.SEVERE, "현재 스레드: " + thread.getThreadName());
                        ex.log(Level.SEVERE, "\tPID: " + thread.getThreadId() + " | Suspended: " + thread.isSuspended() + " | Native: " + thread.isInNative() + " | State: " + thread.getThreadState());
                        int line;
                        if (thread.getLockedMonitors().length != 0) {
                            ex.log(Level.SEVERE, "\tThread is waiting on monitor(s):");
                            MonitorInfo[] stack = thread.getLockedMonitors();
                            line = stack.length;

                            for (int i$1 = 0; i$1 < line; ++i$1) {
                                MonitorInfo monitor = stack[i$1];
                                ex.log(Level.SEVERE, "\t\tLocked on:" + monitor.getLockedStackFrame());
                            }
                        }

                        ex.log(Level.SEVERE, "\tStack:");
                        StackTraceElement[] var12 = thread.getStackTrace();

                        for (line = 0; line < var12.length; ++line) {
                            ex.log(Level.SEVERE, "\t\t" + var12[line].toString());
                        }
                    }
                }

                // Crash report
                SwiftCrashReport report = new SwiftCrashReport(ex.toString(), "메인 스레드 응답 중지");
                if (report.write()) {
                    ex.log(Level.SEVERE, "본 크래쉬 로그가 다음 경로에 저장됩니다 : " + report.getLastWritedFile());
                } else {
                    ex.log(Level.SEVERE, "디스크에 크래쉬 로그를 쓸 수 없습니다.");
                }

                // Auto save
                ex.log(Level.SEVERE, "------------------------------");
                if (SpigotConfig.autoRestart) {
                    // Save data
                    ex.log(Level.SEVERE, "데이터가 저장됩니다.");
                    Bukkit.savePlayers();
                    for (World world : Bukkit.getWorlds()) {
                        world.save();
                    }
                    // Plugin disable
                    for (Plugin plugin : Bukkit.getPluginManager().getPlugins()) {
                        try {
                            plugin.onDisable();
                            ex.log(Level.INFO, "플러그인 " + plugin.getName() + " 비활성화");
                        } catch (Throwable th) {
                            ex.log(Level.WARNING, "플러그인 " + plugin.getName() + " 을(를) 비활성화하는 중에 에러가 발생했습니다." + th.toString());
                        }
                    }
                }

                // Restart
                if (this.restart) {
                    ex.log(Level.SEVERE, "서버가 재시작됩니다.");
                    RestartCommand.restart();
                } else {
                    ex.log(Level.SEVERE, "서버 다운 시 재시작 옵션이 비활성화이므로 종료됩니다. 옵션을 활성화하려면 spigot.yml 에서 auto-restart 옵션을 수정하세요.");
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.exit(0);
                }
                break;
            }

            try {
                sleep(10000L);
            } catch (InterruptedException var11) {
                this.interrupt();
            }
        }

    }
}
