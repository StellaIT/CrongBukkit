package org.spigotmc;

import com.google.common.base.Throwables;
import net.minecraft.server.v1_5_R3.MinecraftServer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_5_R3.command.TicksPerSecondCommand;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class SpigotConfig {
    private static final File CONFIG_FILE = new File("spigot.yml");
    private static final String HEADER = "This is the main configuration file for Spigot.\nAs you can see, there\'s tons to configure. Some options may impact gameplay, so use\nwith caution, and make sure you know what each option does before configuring.\nFor a reference for any variable inside this file, check out the Spigot wiki at\nhttp://www.spigotmc.org/wiki/spigot-configuration/\n\nIf you need help with the configuration or have any questions related to Spigot,\njoin us at the IRC or drop by our forums and leave a post.\n\nIRC: #spigot @ irc.esper.net ( http://webchat.esper.net/?channel=spigot )\nForums: http://www.spigotmc.org/forum/\n";
    public static boolean preventProxies;
    public static int timeoutTime = 60;
    public static String restartScript = "./start.ps1";
    public static List<SpigotConfig.Listener> listeners = new ArrayList();
    public static int nettyThreads;
    public static List<String> bungeeAddresses = Arrays.asList("127.0.0.1");
    public static boolean bungee = true;
    public static List<String> spamExclusions;
    public static boolean logCommands;
    public static boolean tabComplete;
    public static String whitelistMessage;
    public static String unknownCommandMessage;
    public static String serverFullMessage;
    public static List<Pattern> logFilters;

    public static boolean autoRestart;
    public static int maxPacketsPerPlayer;
    public static int tickNextTickListCap;
    public static boolean optimizeExplosions;
    public static int minChunkLoadThreads;
    public static int regionFileCacheSize;
    public static boolean queueLightUpdates;
    public static int delayChunkUnloadsBy;
    public static int worldSavePeriod;
    public static String worldSaveStartMessage;
    public static String worldSaveEndMessage;
    public static boolean onlyOnceCalculateWhenDrop;
    public static boolean enableFileIOThreadSleep;
    public static boolean removeCorruptTEs;
    public static int playerAutoSaveRate;

    static YamlConfiguration config;
    static int version;
    static Map<String, Command> commands;
    private static Metrics metrics;

    public static void init() {
        loadConfig();
        config.options().header("This is the main configuration file for Spigot.\nAs you can see, there\'s tons to configure. Some options may impact gameplay, so use\nwith caution, and make sure you know what each option does before configuring.\nFor a reference for any variable inside this file, check out the Spigot wiki at\nhttp://www.spigotmc.org/wiki/spigot-configuration/\n\nIf you need help with the configuration or have any questions related to Spigot,\njoin us at the IRC or drop by our forums and leave a post.\n\nIRC: #spigot @ irc.esper.net ( http://webchat.esper.net/?channel=spigot )\nForums: http://www.spigotmc.org/forum/\n");
        config.options().copyDefaults(true);
        commands = new HashMap<>();
        version = getInt("config-version", 1);
        readConfig(SpigotConfig.class, null);
    }

    public static void loadConfig() {
        config = YamlConfiguration.loadConfiguration(CONFIG_FILE);
        // Swiftnode
        autoRestart = getBoolean("swiftnode.auto-restart", true);
        maxPacketsPerPlayer = getInt("swiftnode.max-packets-per-player", 1000);
        tickNextTickListCap = getInt("swiftnode.tick-next-tick-list-cap", 10000);
        optimizeExplosions = getBoolean("swiftnode.optimize-explosions", true);
        minChunkLoadThreads = Math.min(6, getInt("swiftnode.min-chunk-load-threads", 2));
        regionFileCacheSize = getInt("swiftnode.region-file-cache-size", 256);
        queueLightUpdates = getBoolean("swiftnode.queue-light-updates", true);
        delayChunkUnloadsBy = getInt("swiftnode.delay-chunk-unloads-by", 5) * 1000;
        worldSavePeriod = getInt("swiftnode.world-save-period", 300) * 1000;
        worldSaveStartMessage = getString("swiftnode.world-save-start-message", "월드 저장이 시작됩니다");
        worldSaveEndMessage = getString("swiftnode.world-save-end-message", "월드가 저장되었습니다.");
        onlyOnceCalculateWhenDrop = getBoolean("swiftnode.only-once-calculate-when-drop", true);
        enableFileIOThreadSleep = getBoolean("swiftnode.enable-file-io-thread-sleep", true);
        removeCorruptTEs = getBoolean("remove-corrupt-tile-entities", true);
        playerAutoSaveRate = getInt("swiftnode.player-auto-save-rate", -1);
        //
    }

    public static void registerCommands() {
        Iterator ex = commands.entrySet().iterator();

        while (ex.hasNext()) {
            Entry entry = (Entry) ex.next();
            MinecraftServer.getServer().server.getCommandMap().register((String) entry.getKey(), "Spigot", (Command) entry.getValue());
        }

        if (metrics == null) {
            metrics = new Metrics();
        }

    }

    static void readConfig(Class<?> clazz, Object instance) {
        Method[] ex = clazz.getDeclaredMethods();
        int len$ = ex.length;

        for (int i$ = 0; i$ < len$; ++i$) {
            Method method = ex[i$];
            if (Modifier.isPrivate(method.getModifiers()) && method.getParameterTypes().length == 0 && method.getReturnType() == Void.TYPE) {
                try {
                    method.setAccessible(true);
                    method.invoke(instance);
                } catch (InvocationTargetException var8) {
                    Throwables.propagate(var8.getCause());
                } catch (Exception var9) {
                    Bukkit.getLogger().log(Level.SEVERE, "Error invoking " + method, var9);
                }
            }
        }

        try {
            config.save(CONFIG_FILE);
        } catch (IOException var7) {
            Bukkit.getLogger().log(Level.SEVERE, "Could not save " + CONFIG_FILE, var7);
        }

    }

    private static boolean getBoolean(String path, boolean def) {
        config.addDefault(path, Boolean.valueOf(def));
        return config.getBoolean(path, config.getBoolean(path));
    }

    private static int getInt(String path, int def) {
        config.addDefault(path, Integer.valueOf(def));
        return config.getInt(path, config.getInt(path));
    }

    private static <T> List getList(String path, T def) {
        config.addDefault(path, def);
        return config.getList(path, config.getList(path));
    }

    private static String getString(String path, String def) {
        config.addDefault(path, def);
        return config.getString(path, config.getString(path));
    }

    private static void preventProxies() {
        preventProxies = getBoolean("settings.prevent-proxies", false);
    }

    private static void tpsCommand() {
        commands.put("tps", new TicksPerSecondCommand("tps"));
    }

    private static void watchdog() {
        restartScript = getString("swiftnode.restart-script", restartScript);
        timeoutTime = getInt("settings.timeout-time", timeoutTime);
        commands.put("restart", new RestartCommand("restart"));
        WatchdogThread.doStart(timeoutTime, autoRestart);
    }

    private static void listeners() {
        HashMap def = new HashMap();
        def.put("host", "default");
        def.put("port", "default");
        def.put("netty", Boolean.valueOf(true));
        config.addDefault("listeners", Collections.singletonList(def));
        Iterator i$ = config.getList("listeners").iterator();

        while (i$.hasNext()) {
            Map info = (Map) i$.next();
            String host = (String) info.get("host");
            if (!"default".equals(host)) {
                throw new IllegalArgumentException("Can only bind listener to default! Configure it in server.properties");
            }

            host = Bukkit.getIp();
            if (info.get("port") instanceof Integer) {
                throw new IllegalArgumentException("Can only bind port to default! Configure it in server.properties");
            }

            int port = Bukkit.getPort();
            boolean netty = ((Boolean) info.get("netty")).booleanValue();
            listeners.add(new SpigotConfig.Listener(host, port, netty, Bukkit.getConnectionThrottle()));
        }

        if (listeners.size() != 1) {
            throw new IllegalArgumentException("May only have one listener!");
        } else {
            nettyThreads = getInt("settings.netty-threads", 3);
        }
    }

    private static void bungee() {
        bungeeAddresses = getList("settings.bungeecord-addresses", bungeeAddresses);
        bungee = getBoolean("settings.bungeecord", true);
    }

    private static void spamExclusions() {
        spamExclusions = getList("commands.spam-exclusions", Arrays.asList("/skill"));
    }

    private static void logCommands() {
        logCommands = getBoolean("commands.log", true);
    }

    private static void tabComplete() {
        tabComplete = getBoolean("commands.tab-complete", true);
    }

    private static String transform(String s) {
        return ChatColor.translateAlternateColorCodes('&', s).replaceAll("\\n", "\n");
    }

    private static void messages() {
        whitelistMessage = transform(getString("messages.whitelist", "You are not whitelisted on this server!"));
        unknownCommandMessage = transform(getString("messages.unknown-command", "Unknown command. Type \"help\" for help."));
        serverFullMessage = transform(getString("messages.server-full", "The server is full!"));
    }

    private static void filters() {
        List def = Arrays.asList("^(.*)(/login)(.*)$");
        logFilters = new ArrayList();
        Iterator i$ = getList("settings.log-filters", def).iterator();

        while (i$.hasNext()) {
            String regex = (String) i$.next();

            try {
                logFilters.add(Pattern.compile(regex));
            } catch (PatternSyntaxException var4) {
                Bukkit.getLogger().log(Level.WARNING, "Supplied filter " + regex + " is invalid, ignoring!", var4);
            }
        }

        Bukkit.getLogger().setFilter(new LogFilter());
    }

    public static class Listener {
        public String host;
        public int port;
        public boolean netty;
        public long connectionThrottle;

        public Listener(String host, int port, boolean netty, long connectionThrottle) {
            this.host = host;
            this.port = port;
            this.netty = netty;
            this.connectionThrottle = connectionThrottle;
        }
    }
}
