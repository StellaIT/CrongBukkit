package cloud.swiftnode.bukkit;

import org.bukkit.Bukkit;

import java.io.IOException;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.util.List;

/**
 * Created by Junhyeong Lim on 2017-07-12.
 */
public class PrimaryThreadRestrictProxySelector extends ProxySelector {
    private final ProxySelector delegate;

    public PrimaryThreadRestrictProxySelector(ProxySelector delegate) {
        this.delegate = delegate;
    }

    @Override
    public List<Proxy> select(URI uri) {
        checkSocketAndMainThread();
        return delegate.select(uri);
    }

    @Override
    public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
        delegate.connectFailed(uri, sa, ioe);
    }

    private void checkSocketAndMainThread() {
        if (Bukkit.isPrimaryThread()) {
            throw new RuntimeException("버킷 메인스레드에서 네트워크 사용이 감지되었습니다.");
        }
    }
}
