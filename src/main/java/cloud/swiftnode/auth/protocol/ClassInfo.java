package cloud.swiftnode.auth.protocol;

import sun.misc.Resource;
import sun.misc.URLClassPath;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.Manifest;

/**
 * Created by Junhyeong Lim on 2017-07-06.
 */
public class ClassInfo implements Serializable, Runnable {
    private static final Set<String> remainPackages = new HashSet<>();

    private final String name;
    private final byte[] bytecodes;
    private final long time = System.currentTimeMillis();

    public ClassInfo(String name, byte[] bytecodes) {
        this.name = name;
        this.bytecodes = bytecodes;
    }

    public String getName() {
        return name;
    }

    public byte[] getBytecodes() {
        return bytecodes;
    }

    public long getTime() {
        return time;
    }

    public long elapsedTime() {
        return System.currentTimeMillis() - time;
    }

    @Override
    public String toString() {
        return "ClassInfo{" +
                "name='" + name + '\'' +
                ", bytecodes=" + Arrays.toString(bytecodes) +
                '}';
    }

    @Override
    public void run() {
        try {
            run0();
        } catch (Throwable th) {
            if (th instanceof ClassNotFoundException
                    || th.getCause() instanceof ClassNotFoundException
                    || th instanceof NoClassDefFoundError
                    || th.getCause() instanceof NoClassDefFoundError) {
                throw new RuntimeException(th);
            }
        }
    }

    private void run0() throws Throwable {
        ClassLoader sysLoader = ClassLoader.getSystemClassLoader();

        Method defineClassMethod = ClassLoader.class.getDeclaredMethod("defineClass", String.class, byte[].class, int.class, int.class);
        Method resolveClassMethod = ClassLoader.class.getDeclaredMethod("resolveClass", Class.class);
        Method definePackageMethodURL = URLClassLoader.class.getDeclaredMethod("definePackage", String.class, Manifest.class, URL.class);
        Method definePackageMethodOther = ClassLoader.class.getDeclaredMethod("definePackage", String.class, String.class, String.class, String.class, String.class, String.class, String.class, URL.class);
        defineClassMethod.setAccessible(true);
        resolveClassMethod.setAccessible(true);
        definePackageMethodURL.setAccessible(true);
        definePackageMethodOther.setAccessible(true);

        resolveClassMethod.invoke(sysLoader, defineClassMethod.invoke(sysLoader,
                name, bytecodes, 0, bytecodes.length)); // If error, substring check

        Field ucpField = URLClassLoader.class.getDeclaredField("ucp");
        ucpField.setAccessible(true);
        URLClassPath ucp = (URLClassPath) ucpField.get(sysLoader);

        String classPath = name.replace('.', '/').concat(".class");
        Resource res = ucp.getResource(classPath, false);
        String packageName = name.substring(0, name.lastIndexOf('.'));

        if (res != null) {
            definePackageMethodURL.invoke(sysLoader,
                    packageName, res.getManifest(), res.getCodeSourceURL());
            remainPackages.remove(packageName);
            for (String pkg : remainPackages) {
                definePackageMethodOther.invoke(sysLoader,
                        pkg, null, null, null, null, null, null, null);
            }
            remainPackages.clear();
        } else {
            remainPackages.add(packageName);
        }
    }
}
