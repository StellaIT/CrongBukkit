package net.minecraft.server.v1_5_R3;

/**
 * Created by EntryPoint on 2017-01-04.
 */

import java.io.*;
import java.util.Iterator;

public class DedicatedPlayerList extends PlayerList {
    private File d;
    private File e;

    public DedicatedPlayerList(DedicatedServer var1) {
        super(var1);
        this.d = var1.e("ops.txt");
        this.e = var1.e("white-list.txt");
        this.c = var1.a("view-distance", 7);
        this.maxPlayers = var1.a("max-players", 20);
        this.setHasWhitelist(var1.a("white-list", false));
        if (!var1.I()) {
            this.getNameBans().setEnabled(true);
            this.getIPBans().setEnabled(true);
        }

        this.getNameBans().load();
        this.getNameBans().save();
        this.getIPBans().load();
        this.getIPBans().save();
        this.t();
        this.v();
        this.u();
        if (!this.e.exists()) {
            this.w();
        }

    }

    public void setHasWhitelist(boolean var1) {
        super.setHasWhitelist(var1);
        this.getServer().a("white-list", Boolean.valueOf(var1));
        this.getServer().a();
    }

    public void addOp(String var1) {
        super.addOp(var1);
        this.u();
    }

    public void removeOp(String var1) {
        super.removeOp(var1);
        this.u();
    }

    public void removeWhitelist(String var1) {
        super.removeWhitelist(var1);
        this.w();
    }

    public void addWhitelist(String var1) {
        super.addWhitelist(var1);
        this.w();
    }

    public void reloadWhitelist() {
        this.v();
    }

    private void t() {
        try {
            this.getOPs().clear();
            BufferedReader var1 = new BufferedReader(new FileReader(this.d));
            String var2 = "";

            while ((var2 = var1.readLine()) != null) {
                this.getOPs().add(var2.trim().toLowerCase());
            }

            var1.close();
        } catch (Exception var3) {
            this.getServer().getLogger().warning("Failed to load operators list: " + var3);
        }

    }

    private void u() {
        try {
            PrintWriter var1 = new PrintWriter(new FileWriter(this.d, false));
            Iterator var2 = this.getOPs().iterator();

            while (var2.hasNext()) {
                String var3 = (String) var2.next();
                var1.println(var3);
            }

            var1.close();
        } catch (Exception var4) {
            this.getServer().getLogger().warning("Failed to save operators list: " + var4);
        }

    }

    private void v() {
        try {
            this.getWhitelisted().clear();
            BufferedReader var1 = new BufferedReader(new FileReader(this.e));
            String var2 = "";

            while ((var2 = var1.readLine()) != null) {
                this.getWhitelisted().add(var2.trim().toLowerCase());
            }

            var1.close();
        } catch (Exception var3) {
            this.getServer().getLogger().warning("Failed to load white-list: " + var3);
        }

    }

    private void w() {
        try {
            PrintWriter var1 = new PrintWriter(new FileWriter(this.e, false));
            Iterator var2 = this.getWhitelisted().iterator();

            while (var2.hasNext()) {
                String var3 = (String) var2.next();
                var1.println(var3);
            }

            var1.close();
        } catch (Exception var4) {
            this.getServer().getLogger().warning("Failed to save white-list: " + var4);
        }

    }

    public boolean isWhitelisted(String var1) {
        var1 = var1.trim().toLowerCase();
        return !this.getHasWhitelist() || this.isOp(var1) || this.getWhitelisted().contains(var1);
    }

    public DedicatedServer getServer() {
        return (DedicatedServer) super.getServer();
    }
}
