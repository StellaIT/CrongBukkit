package net.minecraft.server.v1_5_R3;

/**
 * Created by EntryPoint on 2016-12-29.
 */

import com.google.common.collect.ImmutableList;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.WeatherType;
import org.bukkit.craftbukkit.v1_5_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_5_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_5_R3.event.CraftEventFactory;
import org.bukkit.craftbukkit.v1_5_R3.inventory.CraftItemStack;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.*;

public class EntityPlayer extends EntityHuman implements ICrafting {
    public final List<ChunkCoordIntPair> chunkCoordIntPairQueue = new LinkedList<>();
    public final Deque<Integer> removeQueue = new ArrayDeque<>();
    public PlayerConnection playerConnection;
    public MinecraftServer server;
    public PlayerInteractManager playerInteractManager;
    public double d;
    public double e;
    public int lastSentExp = -99999999;
    public int invulnerableTicks = 60;
    public boolean h;
    public int ping;
    public boolean viewingCredits = false;
    public String displayName;
    public String listName;
    public Location compassTarget;
    public int newExp = 0;
    public int newLevel = 0;
    public int newTotalExp = 0;
    public boolean keepLevel = false;
    public long timeOffset = 0L;
    public boolean relativeTime = true;
    public WeatherType weather = null;
    public long lastSave = MinecraftServer.currentTick;
    private LocaleLanguage locale = new LocaleLanguage("en_US");
    private int cm = -99999999;
    private int cn = -99999999;
    private boolean co = true;
    private int cr = 0;
    private int cs = 0;
    private boolean ct = true;
    private int containerCounter = 0;

    public EntityPlayer(MinecraftServer minecraftserver, World world, String s, PlayerInteractManager playerinteractmanager) {
        super(world);
        playerinteractmanager.player = this;
        this.playerInteractManager = playerinteractmanager;
        this.cr = minecraftserver.getPlayerList().o();
        ChunkCoordinates chunkcoordinates = world.getSpawn();
        int i = chunkcoordinates.x;
        int j = chunkcoordinates.z;
        int k = chunkcoordinates.y;
        if (!world.worldProvider.f && world.getWorldData().getGameType() != EnumGamemode.ADVENTURE) {
            int l = Math.max(5, minecraftserver.getSpawnProtection() - 6);
            i += this.random.nextInt(l * 2) - l;
            j += this.random.nextInt(l * 2) - l;
            k = world.i(i, j);
        }

        this.server = minecraftserver;
        this.Y = 0.0F;
        this.name = s;
        this.height = 0.0F;
        this.setPositionRotation((double) i + 0.5D, (double) k, (double) j + 0.5D, 0.0F, 0.0F);

        while (!world.getCubes(this, this.boundingBox).isEmpty()) {
            this.setPosition(this.locX, this.locY + 1.0D, this.locZ);
        }

        this.displayName = this.name;
        this.listName = this.name;
        this.canPickUpLoot = true;
    }

    public void a(NBTTagCompound nbttagcompound) {
        super.a(nbttagcompound);
        if (nbttagcompound.hasKey("playerGameType")) {
            if (MinecraftServer.getServer().getForceGamemode()) {
                this.playerInteractManager.setGameMode(MinecraftServer.getServer().getGamemode());
            } else {
                this.playerInteractManager.setGameMode(EnumGamemode.a(nbttagcompound.getInt("playerGameType")));
            }
        }

        if (this.locY > 300) this.locY = 257;
        this.getBukkitEntity().readExtraData(nbttagcompound);
    }

    public void b(NBTTagCompound nbttagcompound) {
        super.b(nbttagcompound);
        nbttagcompound.setInt("playerGameType", this.playerInteractManager.getGameMode().a());
        this.getBukkitEntity().setExtraData(nbttagcompound);
    }

    public void spawnIn(World world) {
        super.spawnIn(world);
        if (world == null) {
            this.dead = false;
            ChunkCoordinates position = null;
            if (this.spawnWorld != null && !this.spawnWorld.equals("")) {
                CraftWorld cworld = (CraftWorld) Bukkit.getServer().getWorld(this.spawnWorld);
                if (cworld != null && this.getBed() != null) {
                    world = cworld.getHandle();
                    position = EntityHuman.getBed(cworld.getHandle(), this.getBed(), false);
                }
            }

            if (world == null || position == null) {
                world = ((CraftWorld) Bukkit.getServer().getWorlds().get(0)).getHandle();
                position = world.getSpawn();
            }

            this.world = world;
            this.setPosition((double) position.x + 0.5D, (double) position.y, (double) position.z + 0.5D);
        }

        this.dimension = ((WorldServer) this.world).dimension;
        this.playerInteractManager.a((WorldServer) world);
    }

    public void levelDown(int i) {
        super.levelDown(i);
        this.lastSentExp = -1;
    }

    public void syncInventory() {
        this.activeContainer.addSlotListener(this);
    }

    protected void e_() {
        this.height = 0.0F;
    }

    public float getHeadHeight() {
        return 1.62F;
    }

    public void l_() {
        this.playerInteractManager.a();
        --this.invulnerableTicks;
        this.activeContainer.b();
        if (!this.activeContainer.a(this)) {
            this.closeInventory();
            this.activeContainer = this.defaultContainer;
        }

        while (!this.removeQueue.isEmpty()) {
            int arraylist = Math.min(this.removeQueue.size(), 127);
            int[] iterator1 = new int[arraylist];
//            Iterator iterator = this.removeQueue.iterator();
            int iterator2 = 0;
            /*
            while (arraylist1.hasNext() && iterator2 < arraylist) {
                iterator1[iterator2++] = ((Integer) arraylist1.next()).intValue();
                arraylist1.remove();
            }
            */

            Integer integer;
            while (iterator2 < arraylist && (integer = this.removeQueue.poll()) != null) {
                iterator1[iterator2++] = integer;
            }

            this.playerConnection.sendPacket(new Packet29DestroyEntity(iterator1));
        }

        if (!this.chunkCoordIntPairQueue.isEmpty()) {
            ArrayList var6 = new ArrayList();
            Iterator var7 = this.chunkCoordIntPairQueue.iterator();
            ArrayList var8 = new ArrayList();

            Chunk chunk;
            while (var7.hasNext() && var6.size() < 5) {
                ChunkCoordIntPair var9 = (ChunkCoordIntPair) var7.next();
                var7.remove();
                if (var9 != null && this.world.isLoaded(var9.x << 4, 0, var9.z << 4)) {
                    chunk = this.world.getChunkAt(var9.x, var9.z);
                    var6.add(chunk);
                    var8.addAll(chunk.tileEntities.values());
                }
            }

            if (!var6.isEmpty()) {
                this.playerConnection.sendPacket(new Packet56MapChunkBulk(var6));
                Iterator var10 = var8.iterator();

                while (var10.hasNext()) {
                    TileEntity var11 = (TileEntity) var10.next();
                    this.b(var11);
                }

                var10 = var6.iterator();

                while (var10.hasNext()) {
                    chunk = (Chunk) var10.next();
                    this.o().getTracker().a(this, chunk);
                }
            }
        }

    }

    public void setHealth(int i) {
        super.setHealth(i);
        this.world.getServer().getScoreboardManager().updateAllScoresForList(IScoreboardCriteria.f, this.getLocalizedName(), ImmutableList.of(this));
    }

    public void g() {
        try {
            super.l_();

            for (int throwable = 0; throwable < this.inventory.getSize(); ++throwable) {
                ItemStack var5 = this.inventory.getItem(throwable);
                if (var5 != null && Item.byId[var5.id].f() && this.playerConnection.lowPriorityCount() <= 5) {
                    Packet var6 = ((ItemWorldMapBase) Item.byId[var5.id]).c(var5, this.world, this);
                    if (var6 != null) {
                        this.playerConnection.sendPacket(var6);
                    }
                }
            }

            if (this.getHealth() != this.cm || this.cn != this.foodData.a() || this.foodData.e() == 0.0F != this.co) {
                this.playerConnection.sendPacket(new Packet8UpdateHealth(this.getScaledHealth(), this.foodData.a(), this.foodData.e()));
                this.cm = this.getHealth();
                this.cn = this.foodData.a();
                this.co = this.foodData.e() == 0.0F;
            }

            if (this.expTotal != this.lastSentExp) {
                this.lastSentExp = this.expTotal;
                this.playerConnection.sendPacket(new Packet43SetExperience(this.exp, this.expTotal, this.expLevel));
            }

            if (this.oldLevel == -1) {
                this.oldLevel = this.expLevel;
            }

            if (this.oldLevel != this.expLevel) {
                CraftEventFactory.callPlayerLevelChangeEvent(this.world.getServer().getPlayer(this), this.oldLevel, this.expLevel);
                this.oldLevel = this.expLevel;
            }

        } catch (Throwable var4) {
            CrashReport crashreport = CrashReport.a(var4, "Ticking player");
            CrashReportSystemDetails crashreportsystemdetails = crashreport.a("Player being ticked");
            this.a(crashreportsystemdetails);
            throw new ReportedException(crashreport);
        }
    }

    public void die(DamageSource damagesource) {
        if (!this.dead) {
            ArrayList loot = new ArrayList();
            boolean keepInventory = this.world.getGameRules().getBoolean("keepInventory");
            if (!keepInventory) {
                int event;
                for (event = 0; event < this.inventory.items.length; ++event) {
                    if (this.inventory.items[event] != null) {
                        loot.add(CraftItemStack.asCraftMirror(this.inventory.items[event]));
                    }
                }

                for (event = 0; event < this.inventory.armor.length; ++event) {
                    if (this.inventory.armor[event] != null) {
                        loot.add(CraftItemStack.asCraftMirror(this.inventory.armor[event]));
                    }
                }
            }

            PlayerDeathEvent var9 = CraftEventFactory.callPlayerDeathEvent(this, loot, this.bt.b());
            String deathMessage = var9.getDeathMessage();
            if (deathMessage != null && deathMessage.length() > 0) {
                this.server.getPlayerList().k(var9.getDeathMessage());
            }

            if (!keepInventory) {
                int collection;
                for (collection = 0; collection < this.inventory.items.length; ++collection) {
                    this.inventory.items[collection] = null;
                }

                for (collection = 0; collection < this.inventory.armor.length; ++collection) {
                    this.inventory.armor[collection] = null;
                }
            }

            this.closeInventory();
            Collection var10 = this.world.getServer().getScoreboardManager().getScoreboardScores(IScoreboardCriteria.c, this.getLocalizedName(), new ArrayList());
            Iterator iterator = var10.iterator();

            while (iterator.hasNext()) {
                ScoreboardScore entityliving = (ScoreboardScore) iterator.next();
                entityliving.incrementScore();
            }

            EntityLiving var11 = this.bN();
            if (var11 != null) {
                var11.c(this, this.aM);
            }

        }
    }

    public boolean damageEntity(DamageSource damagesource, int i) {
        if (this.isInvulnerable()) {
            return false;
        } else {
            boolean flag = this.server.T() && this.world.pvpMode && "fall".equals(damagesource.translationIndex);
            if (!flag && this.invulnerableTicks > 0 && damagesource != DamageSource.OUT_OF_WORLD) {
                return false;
            } else {
                if (damagesource instanceof EntityDamageSource) {
                    Entity entity = damagesource.getEntity();
                    if (entity instanceof EntityHuman && !this.a((EntityHuman) entity)) {
                        return false;
                    }

                    if (entity instanceof EntityArrow) {
                        EntityArrow entityarrow = (EntityArrow) entity;
                        if (entityarrow.shooter instanceof EntityHuman && !this.a((EntityHuman) entityarrow.shooter)) {
                            return false;
                        }
                    }
                }

                return super.damageEntity(damagesource, i);
            }
        }
    }

    public boolean a(EntityHuman entityhuman) {
        return this.world.pvpMode && super.a(entityhuman);
    }

    public void c(int i) {
        if (this.dimension == 1 && i == 1) {
            this.a(AchievementList.C);
            this.world.kill(this);
            this.viewingCredits = true;
            this.playerConnection.sendPacket(new Packet70Bed(4, 0));
        } else {
            if (this.dimension == 1 && i == 0) {
                this.a(AchievementList.B);
            } else {
                this.a(AchievementList.x);
            }

            TeleportCause cause = this.dimension != 1 && i != 1 ? TeleportCause.NETHER_PORTAL : TeleportCause.END_PORTAL;
            this.server.getPlayerList().changeDimension(this, i, cause);
            this.lastSentExp = -1;
            this.cm = -1;
            this.cn = -1;
        }

    }

    private void b(TileEntity tileentity) {
        if (tileentity != null) {
            Packet packet = tileentity.getUpdatePacket();
            if (packet != null) {
                this.playerConnection.sendPacket(packet);
            }
        }

    }

    public void receive(Entity entity, int i) {
        super.receive(entity, i);
        this.activeContainer.b();
    }

    public EnumBedResult a(int i, int j, int k) {
        EnumBedResult enumbedresult = super.a(i, j, k);
        if (enumbedresult == EnumBedResult.OK) {
            Packet17EntityLocationAction packet17entitylocationaction = new Packet17EntityLocationAction(this, 0, i, j, k);
            this.o().getTracker().a(this, packet17entitylocationaction);
            this.playerConnection.a(this.locX, this.locY, this.locZ, this.yaw, this.pitch);
            this.playerConnection.sendPacket(packet17entitylocationaction);
        }

        return enumbedresult;
    }

    public void a(boolean flag, boolean flag1, boolean flag2) {
        if (!this.fauxSleeping || this.sleeping) {
            if (this.isSleeping()) {
                this.o().getTracker().sendPacketToEntity(this, new Packet18ArmAnimation(this, 3));
            }

            super.a(flag, flag1, flag2);
            if (this.playerConnection != null) {
                this.playerConnection.a(this.locX, this.locY, this.locZ, this.yaw, this.pitch);
            }

        }
    }

    public void mount(Entity entity) {
        this.setPassengerOf(entity);
    }

    public void setPassengerOf(Entity entity) {
        super.setPassengerOf(entity);
        this.playerConnection.sendPacket(new Packet39AttachEntity(this, this.vehicle));
        this.playerConnection.a(this.locX, this.locY, this.locZ, this.yaw, this.pitch);
    }

    protected void a(double d0, boolean flag) {
    }

    public void b(double d0, boolean flag) {
        super.a(d0, flag);
    }

    public int nextContainerCounter() {
        this.containerCounter = this.containerCounter % 100 + 1;
        return this.containerCounter;
    }

    public void startCrafting(int i, int j, int k) {
        Container container = CraftEventFactory.callInventoryOpenEvent(this, new ContainerWorkbench(this.inventory, this.world, i, j, k));
        if (container != null) {
            this.nextContainerCounter();
            this.playerConnection.sendPacket(new Packet100OpenWindow(this.containerCounter, 1, "Crafting", 9, true));
            this.activeContainer = container;
            this.activeContainer.windowId = this.containerCounter;
            this.activeContainer.addSlotListener(this);
        }
    }

    public void startEnchanting(int i, int j, int k, String s) {
        Container container = CraftEventFactory.callInventoryOpenEvent(this, new ContainerEnchantTable(this.inventory, this.world, i, j, k));
        if (container != null) {
            this.nextContainerCounter();
            this.playerConnection.sendPacket(new Packet100OpenWindow(this.containerCounter, 4, s == null ? "" : s, 9, s != null));
            this.activeContainer = container;
            this.activeContainer.windowId = this.containerCounter;
            this.activeContainer.addSlotListener(this);
        }
    }

    public void openAnvil(int i, int j, int k) {
        Container container = CraftEventFactory.callInventoryOpenEvent(this, new ContainerAnvil(this.inventory, this.world, i, j, k, this));
        if (container != null) {
            this.nextContainerCounter();
            this.playerConnection.sendPacket(new Packet100OpenWindow(this.containerCounter, 8, "Repairing", 9, true));
            this.activeContainer = container;
            this.activeContainer.windowId = this.containerCounter;
            this.activeContainer.addSlotListener(this);
        }
    }

    public void openContainer(IInventory iinventory) {
        if (this.activeContainer != this.defaultContainer) {
            this.closeInventory();
        }

        Container container = CraftEventFactory.callInventoryOpenEvent(this, new ContainerChest(this.inventory, iinventory));
        if (container != null) {
            this.nextContainerCounter();
            this.playerConnection.sendPacket(new Packet100OpenWindow(this.containerCounter, 0, iinventory.getName(), iinventory.getSize(), iinventory.c()));
            this.activeContainer = container;
            this.activeContainer.windowId = this.containerCounter;
            this.activeContainer.addSlotListener(this);
        }
    }

    public void openHopper(TileEntityHopper tileentityhopper) {
        Container container = CraftEventFactory.callInventoryOpenEvent(this, new ContainerHopper(this.inventory, tileentityhopper));
        if (container != null) {
            this.nextContainerCounter();
            this.playerConnection.sendPacket(new Packet100OpenWindow(this.containerCounter, 9, tileentityhopper.getName(), tileentityhopper.getSize(), tileentityhopper.c()));
            this.activeContainer = container;
            this.activeContainer.windowId = this.containerCounter;
            this.activeContainer.addSlotListener(this);
        }
    }

    public void openMinecartHopper(EntityMinecartHopper entityminecarthopper) {
        Container container = CraftEventFactory.callInventoryOpenEvent(this, new ContainerHopper(this.inventory, entityminecarthopper));
        if (container != null) {
            this.nextContainerCounter();
            this.playerConnection.sendPacket(new Packet100OpenWindow(this.containerCounter, 9, entityminecarthopper.getName(), entityminecarthopper.getSize(), entityminecarthopper.c()));
            this.activeContainer = container;
            this.activeContainer.windowId = this.containerCounter;
            this.activeContainer.addSlotListener(this);
        }
    }

    public void openFurnace(TileEntityFurnace tileentityfurnace) {
        Container container = CraftEventFactory.callInventoryOpenEvent(this, new ContainerFurnace(this.inventory, tileentityfurnace));
        if (container != null) {
            this.nextContainerCounter();
            this.playerConnection.sendPacket(new Packet100OpenWindow(this.containerCounter, 2, tileentityfurnace.getName(), tileentityfurnace.getSize(), tileentityfurnace.c()));
            this.activeContainer = container;
            this.activeContainer.windowId = this.containerCounter;
            this.activeContainer.addSlotListener(this);
        }
    }

    public void openDispenser(TileEntityDispenser tileentitydispenser) {
        Container container = CraftEventFactory.callInventoryOpenEvent(this, new ContainerDispenser(this.inventory, tileentitydispenser));
        if (container != null) {
            this.nextContainerCounter();
            this.playerConnection.sendPacket(new Packet100OpenWindow(this.containerCounter, tileentitydispenser instanceof TileEntityDropper ? 10 : 3, tileentitydispenser.getName(), tileentitydispenser.getSize(), tileentitydispenser.c()));
            this.activeContainer = container;
            this.activeContainer.windowId = this.containerCounter;
            this.activeContainer.addSlotListener(this);
        }
    }

    public void openBrewingStand(TileEntityBrewingStand tileentitybrewingstand) {
        Container container = CraftEventFactory.callInventoryOpenEvent(this, new ContainerBrewingStand(this.inventory, tileentitybrewingstand));
        if (container != null) {
            this.nextContainerCounter();
            this.playerConnection.sendPacket(new Packet100OpenWindow(this.containerCounter, 5, tileentitybrewingstand.getName(), tileentitybrewingstand.getSize(), tileentitybrewingstand.c()));
            this.activeContainer = container;
            this.activeContainer.windowId = this.containerCounter;
            this.activeContainer.addSlotListener(this);
        }
    }

    public void openBeacon(TileEntityBeacon tileentitybeacon) {
        Container container = CraftEventFactory.callInventoryOpenEvent(this, new ContainerBeacon(this.inventory, tileentitybeacon));
        if (container != null) {
            this.nextContainerCounter();
            this.playerConnection.sendPacket(new Packet100OpenWindow(this.containerCounter, 7, tileentitybeacon.getName(), tileentitybeacon.getSize(), tileentitybeacon.c()));
            this.activeContainer = container;
            this.activeContainer.windowId = this.containerCounter;
            this.activeContainer.addSlotListener(this);
        }
    }

    public void openTrade(IMerchant imerchant, String s) {
        Container container = CraftEventFactory.callInventoryOpenEvent(this, new ContainerMerchant(this.inventory, imerchant, this.world));
        if (container != null) {
            this.nextContainerCounter();
            this.activeContainer = container;
            this.activeContainer.windowId = this.containerCounter;
            this.activeContainer.addSlotListener(this);
            InventoryMerchant inventorymerchant = ((ContainerMerchant) this.activeContainer).getMerchantInventory();
            this.playerConnection.sendPacket(new Packet100OpenWindow(this.containerCounter, 6, s == null ? "" : s, inventorymerchant.getSize(), s != null));
            MerchantRecipeList merchantrecipelist = imerchant.getOffers(this);
            if (merchantrecipelist != null) {
                try {
                    ByteArrayOutputStream ioexception = new ByteArrayOutputStream();
                    DataOutputStream dataoutputstream = new DataOutputStream(ioexception);
                    dataoutputstream.writeInt(this.containerCounter);
                    merchantrecipelist.a(dataoutputstream);
                    this.playerConnection.sendPacket(new Packet250CustomPayload("MC|TrList", ioexception.toByteArray()));
                } catch (IOException var8) {
                    var8.printStackTrace();
                }
            }

        }
    }

    public void a(Container container, int i, ItemStack itemstack) {
        if (!(container.getSlot(i) instanceof SlotResult) && !this.h) {
            this.playerConnection.sendPacket(new Packet103SetSlot(container.windowId, i, itemstack));
        }

    }

    public void updateInventory(Container container) {
        this.a(container, container.a());
    }

    public void a(Container container, List list) {
        this.playerConnection.sendPacket(new Packet104WindowItems(container.windowId, list));
        this.playerConnection.sendPacket(new Packet103SetSlot(-1, -1, this.inventory.getCarried()));
        if (EnumSet.of(InventoryType.CRAFTING, InventoryType.WORKBENCH).contains(container.getBukkitView().getType())) {
            this.playerConnection.sendPacket(new Packet103SetSlot(container.windowId, 0, container.getSlot(0).getItem()));
        }

    }

    public void setContainerData(Container container, int i, int j) {
        this.playerConnection.sendPacket(new Packet105CraftProgressBar(container.windowId, i, j));
    }

    public void closeInventory() {
        CraftEventFactory.handleInventoryCloseEvent(this);
        this.playerConnection.sendPacket(new Packet101CloseWindow(this.activeContainer.windowId));
        this.j();
    }

    public void broadcastCarriedItem() {
        if (!this.h) {
            this.playerConnection.sendPacket(new Packet103SetSlot(-1, -1, this.inventory.getCarried()));
        }

    }

    public void j() {
        this.activeContainer.b(this);
        this.activeContainer = this.defaultContainer;
    }

    public void a(Statistic statistic, int i) {
        if (statistic != null && !statistic.f) {
            while (i > 100) {
                this.playerConnection.sendPacket(new Packet200Statistic(statistic.e, 100));
                i -= 100;
            }

            this.playerConnection.sendPacket(new Packet200Statistic(statistic.e, i));
        }

    }

    public void k() {
        if (this.passenger != null) {
            this.passenger.mount(this);
        }

        if (this.sleeping) {
            this.a(true, false, false);
        }

    }

    public void triggerHealthUpdate() {
        this.cm = -99999999;
        this.lastSentExp = -1;
    }

    public void b(String s) {
        LocaleLanguage localelanguage = LocaleLanguage.a();
        String s1 = localelanguage.a(s);
        this.playerConnection.sendPacket(new Packet3Chat(s1));
    }

    protected void m() {
        this.playerConnection.sendPacket(new Packet38EntityStatus(this.id, (byte) 9));
        super.m();
    }

    public void a(ItemStack itemstack, int i) {
        super.a(itemstack, i);
        if (itemstack != null && itemstack.getItem() != null && itemstack.getItem().b_(itemstack) == EnumAnimation.EAT) {
            this.o().getTracker().sendPacketToEntity(this, new Packet18ArmAnimation(this, 5));
        }

    }

    public void copyTo(EntityHuman entityhuman, boolean flag) {
        super.copyTo(entityhuman, flag);
        this.lastSentExp = -1;
        this.cm = -1;
        this.cn = -1;
//        this.removeQueue.addAll(((EntityPlayer)entityhuman).removeQueue);
        if (this.removeQueue != ((EntityPlayer) entityhuman).removeQueue) {
            this.removeQueue.addAll(((EntityPlayer) entityhuman).removeQueue);
        }
    }

    protected void a(MobEffect mobeffect) {
        super.a(mobeffect);
        this.playerConnection.sendPacket(new Packet41MobEffect(this.id, mobeffect));
    }

    protected void b(MobEffect mobeffect) {
        super.b(mobeffect);
        this.playerConnection.sendPacket(new Packet41MobEffect(this.id, mobeffect));
    }

    protected void c(MobEffect mobeffect) {
        super.c(mobeffect);
        this.playerConnection.sendPacket(new Packet42RemoveMobEffect(this.id, mobeffect));
    }

    public void enderTeleportTo(double d0, double d1, double d2) {
        this.playerConnection.a(d0, d1, d2, this.yaw, this.pitch);
    }

    public void b(Entity entity) {
        this.o().getTracker().sendPacketToEntity(this, new Packet18ArmAnimation(entity, 6));
    }

    public void c(Entity entity) {
        this.o().getTracker().sendPacketToEntity(this, new Packet18ArmAnimation(entity, 7));
    }

    public void updateAbilities() {
        if (this.playerConnection != null) {
            this.playerConnection.sendPacket(new Packet202Abilities(this.abilities));
        }

    }

    public WorldServer o() {
        return (WorldServer) this.world;
    }

    public void a(EnumGamemode enumgamemode) {
        this.playerInteractManager.setGameMode(enumgamemode);
        this.playerConnection.sendPacket(new Packet70Bed(3, enumgamemode.a()));
    }

    public void sendMessage(String s) {
        this.playerConnection.sendPacket(new Packet3Chat(s));
    }

    public boolean a(int i, String s) {
        return "seed".equals(s) && !this.server.T() || (!(!"tell".equals(s) && !"help".equals(s) && !"me".equals(s)) || this.server.getPlayerList().isOp(this.name));
    }

    public String p() {
        String s = this.playerConnection.networkManager.getSocketAddress().toString();
        s = s.substring(s.indexOf("/") + 1);
        s = s.substring(0, s.indexOf(":"));
        return s;
    }

    public void a(Packet204LocaleAndViewDistance packet204localeandviewdistance) {
        if (this.locale.b().containsKey(packet204localeandviewdistance.d())) {
            this.locale.a(packet204localeandviewdistance.d(), false);
        }

        int i = 256 >> packet204localeandviewdistance.f();
        if (i > 3 && i < 15) {
            this.cr = i;
        }

        this.cs = packet204localeandviewdistance.g();
        this.ct = packet204localeandviewdistance.h();
        if (this.server.I() && this.server.H().equals(this.name)) {
            this.server.c(packet204localeandviewdistance.i());
        }

        this.b(1, !packet204localeandviewdistance.j());
    }

    public LocaleLanguage getLocale() {
        return this.locale;
    }

    public int getChatFlags() {
        return this.cs;
    }

    public void a(String s, int i) {
        String s1 = s + "\u0000" + i;
        this.playerConnection.sendPacket(new Packet250CustomPayload("MC|TPack", s1.getBytes()));
    }

    public ChunkCoordinates b() {
        return new ChunkCoordinates(MathHelper.floor(this.locX), MathHelper.floor(this.locY + 0.5D), MathHelper.floor(this.locZ));
    }

    public long getPlayerTime() {
        return this.relativeTime ? this.world.getDayTime() + this.timeOffset : this.world.getDayTime() - this.world.getDayTime() % 24000L + this.timeOffset;
    }

    public WeatherType getPlayerWeather() {
        return this.weather;
    }

    public void setPlayerWeather(WeatherType type, boolean plugin) {
        if (plugin || this.weather == null) {
            if (plugin) {
                this.weather = type;
            }

            this.playerConnection.sendPacket(new Packet70Bed(type == WeatherType.DOWNFALL ? 1 : 2, 0));
        }
    }

    public void resetPlayerWeather() {
        this.weather = null;
        this.setPlayerWeather(this.o().getWorldData().hasStorm() ? WeatherType.DOWNFALL : WeatherType.CLEAR, false);
    }

    public String toString() {
        return super.toString() + "(" + this.name + " at " + this.locX + "," + this.locY + "," + this.locZ + ")";
    }

    public void reset() {
        float exp = 0.0F;
        boolean keepInventory = this.world.getGameRules().getBoolean("keepInventory");
        if (this.keepLevel || keepInventory) {
            exp = this.exp;
            this.newTotalExp = this.expTotal;
            this.newLevel = this.expLevel;
        }

        this.health = this.maxHealth;
        this.fireTicks = 0;
        this.fallDistance = 0.0F;
        this.foodData = new FoodMetaData();
        this.expLevel = this.newLevel;
        this.expTotal = this.newTotalExp;
        this.exp = 0.0F;
        this.deathTicks = 0;
        this.effects.clear();
        this.updateEffects = true;
        this.activeContainer = this.defaultContainer;
        this.killer = null;
        this.lastDamager = null;
        this.bt = new CombatTracker(this);
        this.lastSentExp = -1;
        if (!this.keepLevel && !keepInventory) {
            this.giveExp(this.newExp);
        } else {
            this.exp = exp;
        }

        this.keepLevel = false;
    }

    public CraftPlayer getBukkitEntity() {
        return (CraftPlayer) super.getBukkitEntity();
    }
}
