package net.minecraft.server.v1_5_R3;

/**
 * Created by EntryPoint on 2016-12-29.
 */

import org.spigotmc.SpigotConfig;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class RegionFileCache {
    public static final Map<File, RegionFile> a = new LinkedHashMap<>(SpigotConfig.regionFileCacheSize, 0.75f, true);

    public static synchronized RegionFile a(File file1, int i, int j) {
        File file2 = new File(file1, "region");
        File file3 = new File(file2, "r." + (i >> 5) + "." + (j >> 5) + ".mca");
        RegionFile regionfile = a.get(file3);
        if (regionfile != null) {
            return regionfile;
        } else {
            if (!file2.exists()) {
                file2.mkdirs();
            }

//            if (a.size() >= 256) {
//                a();
//            }

            if (RegionFileCache.a.size() >= SpigotConfig.regionFileCacheSize) {
                trimCache();
            }

            RegionFile regionfile1 = new RegionFile(file3);
            a.put(file3, regionfile1);
            return regionfile1;
        }
    }

    private static synchronized void trimCache() {
        Iterator<Map.Entry<File, RegionFile>> itr = RegionFileCache.a.entrySet().iterator();
        int count = RegionFileCache.a.size() - SpigotConfig.regionFileCacheSize;
        while (count-- >= 0 && itr.hasNext()) {
            try {
                itr.next().getValue().c();
            } catch (IOException ioexception) {
                ioexception.printStackTrace();
            }
            itr.remove();
        }
    }

    public static synchronized void a() {

        for (RegionFile regionfile : a.values()) {
            try {
                if (regionfile != null) {
                    regionfile.c();
                }
            } catch (IOException var3) {
                var3.printStackTrace();
            }
        }

        a.clear();
    }

    public static DataInputStream c(File file1, int i, int j) {
        RegionFile regionfile = a(file1, i, j);
        return regionfile.a(i & 31, j & 31);
    }

    public static DataOutputStream d(File file1, int i, int j) {
        RegionFile regionfile = a(file1, i, j);
        return regionfile.b(i & 31, j & 31);
    }
}
