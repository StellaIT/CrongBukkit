package net.minecraft.server.v1_5_R3;

/**
 * Created by EntryPoint on 2016-12-29.
 */

import gnu.trove.iterator.TLongShortIterator;
import org.bukkit.BlockChangeDelegate;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.WeatherType;
import org.bukkit.World.Environment;
import org.bukkit.block.BlockState;
import org.bukkit.craftbukkit.v1_5_R3.CraftTravelAgent;
import org.bukkit.craftbukkit.v1_5_R3.generator.CustomChunkGenerator;
import org.bukkit.craftbukkit.v1_5_R3.generator.NetherChunkGenerator;
import org.bukkit.craftbukkit.v1_5_R3.generator.NormalChunkGenerator;
import org.bukkit.craftbukkit.v1_5_R3.generator.SkyLandsChunkGenerator;
import org.bukkit.craftbukkit.v1_5_R3.util.LongHash;
import org.bukkit.craftbukkit.v1_5_R3.util.LongObjectHashMap;
import org.bukkit.entity.LightningStrike;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.weather.LightningStrikeEvent;
import org.bukkit.event.weather.ThunderChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.generator.ChunkGenerator;
import org.spigotmc.SpigotConfig;

import java.util.*;

public class WorldServer extends World implements BlockChangeDelegate {
    private static final StructurePieceTreasure[] S;

    static {
        S = new StructurePieceTreasure[]{new StructurePieceTreasure(Item.STICK.id, 0, 1, 3, 10), new StructurePieceTreasure(Block.WOOD.id, 0, 1, 3, 10), new StructurePieceTreasure(Block.LOG.id, 0, 1, 3, 10), new StructurePieceTreasure(Item.STONE_AXE.id, 0, 1, 1, 3), new StructurePieceTreasure(Item.WOOD_AXE.id, 0, 1, 1, 5), new StructurePieceTreasure(Item.STONE_PICKAXE.id, 0, 1, 1, 3), new StructurePieceTreasure(Item.WOOD_PICKAXE.id, 0, 1, 1, 5), new StructurePieceTreasure(Item.APPLE.id, 0, 2, 3, 5), new StructurePieceTreasure(Item.BREAD.id, 0, 2, 3, 3)};
    }

    public final int dimension;
    private final MinecraftServer server;
    private final PlayerChunkMap manager;
    private final PortalTravelAgent P;
    public EntityTracker tracker;
    public ChunkProviderServer chunkProviderServer;
    public boolean savingDisabled;
    private LongObjectHashMap<Set<NextTickListEntry>> tickEntriesByChunk;
    private TreeSet<NextTickListEntry> tickEntryQueue;
    private boolean N;
    private int emptyTime = 0;
    private NoteDataList[] Q = new NoteDataList[]{new NoteDataList(null), new NoteDataList(null)};
    private int R = 0;
    private ArrayList<NextTickListEntry> pendingTickEntries = new ArrayList();
    private int nextPendingTickEntry;
    private IntHashMap entitiesById;

    public WorldServer(MinecraftServer minecraftserver, IDataManager idatamanager, String s, int i, WorldSettings worldsettings, MethodProfiler methodprofiler, IConsoleLogManager iconsolelogmanager, Environment env, ChunkGenerator gen) {
        super(idatamanager, s, worldsettings, WorldProvider.byDimension(env.getId()), methodprofiler, iconsolelogmanager, gen, env);
        this.dimension = i;
        this.pvpMode = minecraftserver.getPvP();
        this.server = minecraftserver;
        this.tracker = new EntityTracker(this);
        this.manager = new PlayerChunkMap(this, this.spigotConfig.viewDistance);
        if (this.entitiesById == null) {
            this.entitiesById = new IntHashMap();
        }

        if (this.tickEntriesByChunk == null) {
            this.tickEntriesByChunk = new LongObjectHashMap();
        }

        if (this.tickEntryQueue == null) {
            this.tickEntryQueue = new TreeSet();
        }

        this.P = new CraftTravelAgent(this);
        this.scoreboard = new ScoreboardServer(minecraftserver);
        ScoreboardSaveData scoreboardsavedata = (ScoreboardSaveData) this.worldMaps.get(ScoreboardSaveData.class, "scoreboard");
        if (scoreboardsavedata == null) {
            scoreboardsavedata = new ScoreboardSaveData();
            this.worldMaps.a("scoreboard", scoreboardsavedata);
        }

        scoreboardsavedata.a(this.scoreboard);
        ((ScoreboardServer) this.scoreboard).a(scoreboardsavedata);
    }

    public TileEntity getTileEntity(int i, int j, int k) {
        TileEntity result = super.getTileEntity(i, j, k);
        int type = this.getTypeId(i, j, k);
        if (type == Block.CHEST.id) {
            if (!(result instanceof TileEntityChest)) {
                result = this.fixTileEntity(i, j, k, type, result);
            }
        } else if (type == Block.FURNACE.id) {
            if (!(result instanceof TileEntityFurnace)) {
                result = this.fixTileEntity(i, j, k, type, result);
            }
        } else if (type == Block.DROPPER.id) {
            if (!(result instanceof TileEntityDropper)) {
                result = this.fixTileEntity(i, j, k, type, result);
            }
        } else if (type == Block.DISPENSER.id) {
            if (!(result instanceof TileEntityDispenser)) {
                result = this.fixTileEntity(i, j, k, type, result);
            }
        } else if (type == Block.JUKEBOX.id) {
            if (!(result instanceof TileEntityRecordPlayer)) {
                result = this.fixTileEntity(i, j, k, type, result);
            }
        } else if (type == Block.NOTE_BLOCK.id) {
            if (!(result instanceof TileEntityNote)) {
                result = this.fixTileEntity(i, j, k, type, result);
            }
        } else if (type == Block.MOB_SPAWNER.id) {
            if (!(result instanceof TileEntityMobSpawner)) {
                result = this.fixTileEntity(i, j, k, type, result);
            }
        } else if (type != Block.SIGN_POST.id && type != Block.WALL_SIGN.id) {
            if (type == Block.ENDER_CHEST.id) {
                if (!(result instanceof TileEntityEnderChest)) {
                    result = this.fixTileEntity(i, j, k, type, result);
                }
            } else if (type == Block.BREWING_STAND.id) {
                if (!(result instanceof TileEntityBrewingStand)) {
                    result = this.fixTileEntity(i, j, k, type, result);
                }
            } else if (type == Block.BEACON.id) {
                if (!(result instanceof TileEntityBeacon)) {
                    result = this.fixTileEntity(i, j, k, type, result);
                }
            } else if (type == Block.HOPPER.id && !(result instanceof TileEntityHopper)) {
                result = this.fixTileEntity(i, j, k, type, result);
            }
        } else if (!(result instanceof TileEntitySign)) {
            result = this.fixTileEntity(i, j, k, type, result);
        }

        return result;
    }

    private TileEntity fixTileEntity(int x, int y, int z, int type, TileEntity found) {
        this.getServer().getLogger().severe("Block at " + x + "," + y + "," + z + " is " + Material.getMaterial(type).toString() + " but has " + found + ". " + "Bukkit will attempt to fix this, but there may be additional damage that we cannot recover.");
        if (Block.byId[type] instanceof BlockContainer) {
            TileEntity replacement = ((BlockContainer) Block.byId[type]).b(this);
            replacement.world = this;
            this.setTileEntity(x, y, z, replacement);
            return replacement;
        } else {
            this.getServer().getLogger().severe("Don\'t know how to fix for this type... Can\'t do anything! :(");
            return found;
        }
    }

    private boolean canSpawn(int x, int z) {
        return this.generator != null ? this.generator.canSpawn(this.getWorld(), x, z) : this.worldProvider.canSpawn(x, z);
    }

    public void doTick() {
        super.doTick();
        if (this.getWorldData().isHardcore() && this.difficulty < 3) {
            this.difficulty = 3;
        }

        this.worldProvider.d.b();
        if (this.everyoneDeeplySleeping()) {
            boolean flag = false;
            if (this.allowMonsters && this.difficulty >= 1) {
            }

            if (!flag) {
                long i = this.worldData.getDayTime() + 24000L;
                this.worldData.setDayTime(i - i % 24000L);
                this.d();
            }
        }

        this.methodProfiler.a("mobSpawner");
        long time = this.worldData.getTime();
        if (this.getGameRules().getBoolean("doMobSpawning") && (this.allowMonsters || this.allowAnimals) && this instanceof WorldServer && this.players.size() > 0) {
            this.timings.mobSpawn.startTiming();
            SpawnerCreature.spawnEntities(this, this.allowMonsters && this.ticksPerMonsterSpawns != 0L && time % this.ticksPerMonsterSpawns == 0L, this.allowAnimals && this.ticksPerAnimalSpawns != 0L && time % this.ticksPerAnimalSpawns == 0L, this.worldData.getTime() % 400L == 0L);
            this.timings.mobSpawn.stopTiming();
        }

        this.timings.doChunkUnload.startTiming();
        this.methodProfiler.c("chunkSource");
        this.chunkProvider.unloadChunks();
        int j = this.a(1.0F);
        if (j != this.j) {
            this.j = j;
        }

        this.worldData.setTime(this.worldData.getTime() + 1L);
        this.worldData.setDayTime(this.worldData.getDayTime() + 1L);
        this.timings.doChunkUnload.stopTiming();
        this.methodProfiler.c("tickPending");
        this.timings.doTickPending.startTiming();
        this.a(false);
        this.timings.doTickPending.stopTiming();
        this.methodProfiler.c("tickTiles");
        this.timings.doTickTiles.startTiming();
        this.g();
        this.timings.doTickTiles.stopTiming();
        this.methodProfiler.c("chunkMap");
        this.timings.doChunkMap.startTiming();
        this.manager.flush();
        this.timings.doChunkMap.stopTiming();
        this.methodProfiler.c("village");
        this.timings.doVillages.startTiming();
        this.villages.tick();
        this.siegeManager.a();
        this.timings.doVillages.stopTiming();
        this.methodProfiler.c("portalForcer");
        this.timings.doPortalForcer.startTiming();
        this.P.a(this.getTime());
        this.timings.doPortalForcer.stopTiming();
        this.methodProfiler.b();
        this.timings.doSounds.startTiming();
        this.Z();
        this.timings.doSounds.stopTiming();
        this.timings.doChunkGC.startTiming();
        this.getWorld().processChunkGC();
        this.timings.doChunkGC.stopTiming();
    }

    public BiomeMeta a(EnumCreatureType enumcreaturetype, int i, int j, int k) {
        List list = this.K().getMobsFor(enumcreaturetype, i, j, k);
        return list != null && !list.isEmpty() ? (BiomeMeta) WeightedRandom.a(this.random, list) : null;
    }

    public void everyoneSleeping() {
        this.N = !this.players.isEmpty();
        Iterator iterator = this.players.iterator();

        while (iterator.hasNext()) {
            EntityHuman entityhuman = (EntityHuman) iterator.next();
            if (!entityhuman.isSleeping() && !entityhuman.fauxSleeping) {
                this.N = false;
                break;
            }
        }

    }

    protected void d() {
        this.N = false;
        Iterator iterator = this.players.iterator();

        while (iterator.hasNext()) {
            EntityHuman entityhuman = (EntityHuman) iterator.next();
            if (entityhuman.isSleeping()) {
                entityhuman.a(false, false, true);
            }
        }

        this.Y();
    }

    private void Y() {
        WeatherChangeEvent weather = new WeatherChangeEvent(this.getWorld(), false);
        this.getServer().getPluginManager().callEvent(weather);
        ThunderChangeEvent thunder = new ThunderChangeEvent(this.getWorld(), false);
        this.getServer().getPluginManager().callEvent(thunder);
        if (!weather.isCancelled()) {
            this.worldData.setWeatherDuration(0);
            this.worldData.setStorm(false);
        }

        if (!thunder.isCancelled()) {
            this.worldData.setThunderDuration(0);
            this.worldData.setThundering(false);
        }

    }

    public boolean everyoneDeeplySleeping() {
        if (this.N && !this.isStatic) {
            Iterator iterator = this.players.iterator();
            boolean foundActualSleepers = false;

            EntityHuman entityhuman;
            do {
                if (!iterator.hasNext()) {
                    return foundActualSleepers;
                }

                entityhuman = (EntityHuman) iterator.next();
                if (entityhuman.isDeeplySleeping()) {
                    foundActualSleepers = true;
                }
            } while (entityhuman.isDeeplySleeping() || entityhuman.fauxSleeping);

            return false;
        } else {
            return false;
        }
    }

    protected void g() {
        super.g();
        int i = 0;
        int j = 0;
        TLongShortIterator iter = this.chunkTickList.iterator();

        while (true) {
            while (iter.hasNext()) {
                iter.advance();
                long chunkCoord = iter.key();
                int chunkX = World.keyToX(chunkCoord);
                int chunkZ = World.keyToZ(chunkCoord);
                if (this.isChunkLoaded(chunkX, chunkZ) && !this.chunkProviderServer.unloadQueue.contains(chunkX, chunkZ)) {
                    int k = chunkX * 16;
                    int l = chunkZ * 16;
                    this.methodProfiler.a("getChunk");
                    Chunk chunk = this.getChunkAt(chunkX, chunkZ);
                    this.a(k, l, chunk);
                    this.methodProfiler.c("tickChunk");
                    chunk.k();
                    this.methodProfiler.c("thunder");
                    int i1;
                    int j1;
                    int k1;
                    int l1;
                    if (this.random.nextInt(100000) == 0 && this.P() && this.O()) {
                        this.k = this.k * 3 + 1013904223;
                        i1 = this.k >> 2;
                        j1 = k + (i1 & 15);
                        k1 = l + (i1 >> 8 & 15);
                        l1 = this.h(j1, k1);
                        if (this.F(j1, l1, k1)) {
                            this.strikeLightning(new EntityLightning(this, (double) j1, (double) l1, (double) k1));
                        }
                    }

                    this.methodProfiler.c("iceandsnow");
                    int i2;
                    if (this.random.nextInt(16) == 0) {
                        this.k = this.k * 3 + 1013904223;
                        i1 = this.k >> 2;
                        j1 = i1 & 15;
                        k1 = i1 >> 8 & 15;
                        l1 = this.h(j1 + k, k1 + l);
                        BlockState achunksection;
                        BlockFormEvent chunksection;
                        if (this.y(j1 + k, l1 - 1, k1 + l)) {
                            achunksection = this.getWorld().getBlockAt(j1 + k, l1 - 1, k1 + l).getState();
                            achunksection.setTypeId(Block.ICE.id);
                            chunksection = new BlockFormEvent(achunksection.getBlock(), achunksection);
                            this.getServer().getPluginManager().callEvent(chunksection);
                            if (!chunksection.isCancelled()) {
                                achunksection.update(true);
                            }
                        }

                        if (this.P() && this.z(j1 + k, l1, k1 + l)) {
                            achunksection = this.getWorld().getBlockAt(j1 + k, l1, k1 + l).getState();
                            achunksection.setTypeId(Block.SNOW.id);
                            chunksection = new BlockFormEvent(achunksection.getBlock(), achunksection);
                            this.getServer().getPluginManager().callEvent(chunksection);
                            if (!chunksection.isCancelled()) {
                                achunksection.update(true);
                            }
                        }

                        if (this.P()) {
                            BiomeBase var24 = this.getBiome(j1 + k, k1 + l);
                            if (var24.d()) {
                                i2 = this.getTypeId(j1 + k, l1 - 1, k1 + l);
                                if (i2 != 0) {
                                    Block.byId[i2].g(this, j1 + k, l1 - 1, k1 + l);
                                }
                            }
                        }
                    }

                    this.methodProfiler.c("tickTiles");
                    ChunkSection[] var25 = chunk.i();
                    j1 = var25.length;

                    for (k1 = 0; k1 < j1; ++k1) {
                        ChunkSection var26 = var25[k1];
                        if (var26 != null && var26.shouldTick()) {
                            for (int j2 = 0; j2 < 3; ++j2) {
                                this.k = this.k * 3 + 1013904223;
                                i2 = this.k >> 2;
                                int k2 = i2 & 15;
                                int l2 = i2 >> 8 & 15;
                                int i3 = i2 >> 16 & 15;
                                int j3 = var26.getTypeId(k2, i3, l2);
                                ++j;
                                Block block = Block.byId[j3];
                                if (block != null && block.isTicking()) {
                                    ++i;
                                    this.growthOdds = iter.value() < 1 ? this.modifiedOdds : 100.0F;
                                    block.a(this, k2 + k, i3 + var26.getYPosition(), l2 + l, this.random);
                                }
                            }
                        }
                    }

                    this.methodProfiler.b();
                } else {
                    iter.remove();
                }
            }

            return;
        }
    }

    public boolean a(int i, int j, int k, int l) {
        int te_cnt = this.pendingTickEntries.size();

        for (int idx = this.nextPendingTickEntry; idx < te_cnt; ++idx) {
            NextTickListEntry ent = this.pendingTickEntries.get(idx);
            if (ent.a == i && ent.b == j && ent.c == k && Block.b(ent.d, l)) {
                return true;
            }
        }

        return false;
    }

    public void a(int i, int j, int k, int l, int i1) {
        this.a(i, j, k, l, i1, 0);
    }

    public void a(int i, int j, int k, int l, int i1, int j1) {
        NextTickListEntry nextticklistentry = new NextTickListEntry(i, j, k, l);
        byte b0 = 0;
        if (this.d && l > 0) {
            if (Block.byId[l].l()) {
                if (this.e(nextticklistentry.a - b0, nextticklistentry.b - b0, nextticklistentry.c - b0, nextticklistentry.a + b0, nextticklistentry.b + b0, nextticklistentry.c + b0)) {
                    int k1 = this.getTypeId(nextticklistentry.a, nextticklistentry.b, nextticklistentry.c);
                    if (k1 == nextticklistentry.d && k1 > 0) {
                        Block.byId[k1].a(this, nextticklistentry.a, nextticklistentry.b, nextticklistentry.c, this.random);
                    }
                }

                return;
            }

            i1 = 1;
        }

        if (this.e(i - b0, j - b0, k - b0, i + b0, j + b0, k + b0)) {
            if (l > 0) {
                nextticklistentry.a((long) i1 + this.worldData.getTime());
                nextticklistentry.a(j1);
            }

            this.addNextTickIfNeeded(nextticklistentry);
        }

    }

    public void b(int i, int j, int k, int l, int i1, int j1) {
        NextTickListEntry nextticklistentry = new NextTickListEntry(i, j, k, l);
        nextticklistentry.a(j1);
        if (l > 0) {
            nextticklistentry.a((long) i1 + this.worldData.getTime());
        }

        this.addNextTickIfNeeded(nextticklistentry);
    }

    public void tickEntities() {
        this.i();
        super.tickEntities();
    }

    public void i() {
        this.emptyTime = 0;
    }

    public boolean a(boolean flag) {
        int i = this.tickEntryQueue.size();
        this.nextPendingTickEntry = 0;
        /*
        if(i > 1000) {
            if(i > 20000) {
                i /= 20;
            } else {
                i = 1000;
            }
        }
        */
        if (i > SpigotConfig.tickNextTickListCap) {
            i = SpigotConfig.tickNextTickListCap;
        }

        this.methodProfiler.a("cleaning");

        int j;
        NextTickListEntry nextticklistentry;
        for (j = 0; j < i; ++j) {
            nextticklistentry = this.tickEntryQueue.first();
            if (!flag && nextticklistentry.e > this.worldData.getTime()) {
                break;
            }

            this.removeNextTickIfNeeded(nextticklistentry);
            this.pendingTickEntries.add(nextticklistentry);
        }

        this.methodProfiler.b();
        this.methodProfiler.a("ticking");
        j = 0;

        for (int te_cnt = this.pendingTickEntries.size(); j < te_cnt; ++j) {
            nextticklistentry = this.pendingTickEntries.get(j);
            this.nextPendingTickEntry = j + 1;
            byte b0 = 0;
            if (this.e(nextticklistentry.a - b0, nextticklistentry.b - b0, nextticklistentry.c - b0, nextticklistentry.a + b0, nextticklistentry.b + b0, nextticklistentry.c + b0)) {
                int k = this.getTypeId(nextticklistentry.a, nextticklistentry.b, nextticklistentry.c);
                if (k > 0 && Block.b(k, nextticklistentry.d)) {
                    try {
                        Block.byId[k].a(this, nextticklistentry.a, nextticklistentry.b, nextticklistentry.c, this.random);
                    } catch (Throwable var14) {
                        CrashReport crashreport = CrashReport.a(var14, "Exception while ticking a block");
                        CrashReportSystemDetails crashreportsystemdetails = crashreport.a("Block being ticked");

                        int l;
                        try {
                            l = this.getData(nextticklistentry.a, nextticklistentry.b, nextticklistentry.c);
                        } catch (Throwable var13) {
                            l = -1;
                        }

                        CrashReportSystemDetails.a(crashreportsystemdetails, nextticklistentry.a, nextticklistentry.b, nextticklistentry.c, k, l);
                        throw new ReportedException(crashreport);
                    }
                }
            } else {
                this.a(nextticklistentry.a, nextticklistentry.b, nextticklistentry.c, nextticklistentry.d, 0);
            }
        }

        this.methodProfiler.b();
        this.pendingTickEntries.clear();
        this.nextPendingTickEntry = 0;
        return !this.tickEntryQueue.isEmpty();
    }

    public List a(Chunk chunk, boolean flag) {
        return this.getNextTickEntriesForChunk(chunk, flag);
    }

    public void entityJoinedWorld(Entity entity, boolean flag) {
        if (!this.server.getSpawnNPCs() && entity instanceof NPC) {
            entity.die();
        }

        if (!(entity.passenger instanceof EntityHuman)) {
            super.entityJoinedWorld(entity, flag);
        }

    }

    public void vehicleEnteredWorld(Entity entity, boolean flag) {
        super.entityJoinedWorld(entity, flag);
    }

    protected IChunkProvider j() {
        IChunkLoader ichunkloader = this.dataManager.createChunkLoader(this.worldProvider);
        Object gen;
        if (this.generator != null) {
            gen = new CustomChunkGenerator(this, this.getSeed(), this.generator);
        } else if (this.worldProvider instanceof WorldProviderHell) {
            gen = new NetherChunkGenerator(this, this.getSeed());
        } else if (this.worldProvider instanceof WorldProviderTheEnd) {
            gen = new SkyLandsChunkGenerator(this, this.getSeed());
        } else {
            gen = new NormalChunkGenerator(this, this.getSeed());
        }

        this.chunkProviderServer = new ChunkProviderServer(this, ichunkloader, (IChunkProvider) gen);
        return this.chunkProviderServer;
    }

    public List getTileEntities(int i, int j, int k, int l, int i1, int j1) {
        ArrayList arraylist = new ArrayList();

        for (int chunkX = i >> 4; chunkX <= l - 1 >> 4; ++chunkX) {
            for (int chunkZ = k >> 4; chunkZ <= j1 - 1 >> 4; ++chunkZ) {
                Chunk chunk = this.getChunkAt(chunkX, chunkZ);
                if (chunk != null) {
                    Iterator i$ = chunk.tileEntities.values().iterator();

                    while (i$.hasNext()) {
                        Object te = i$.next();
                        TileEntity tileentity = (TileEntity) te;
                        if (tileentity.x >= i && tileentity.y >= j && tileentity.z >= k && tileentity.x < l && tileentity.y < i1 && tileentity.z < j1) {
                            arraylist.add(tileentity);
                        }
                    }
                }
            }
        }

        return arraylist;
    }

    public boolean a(EntityHuman entityhuman, int i, int j, int k) {
        return !this.server.a(this, i, j, k, entityhuman);
    }

    protected void a(WorldSettings worldsettings) {
        if (this.entitiesById == null) {
            this.entitiesById = new IntHashMap();
        }

        if (this.tickEntriesByChunk == null) {
            this.tickEntriesByChunk = new LongObjectHashMap();
        }

        if (this.tickEntryQueue == null) {
            this.tickEntryQueue = new TreeSet();
        }

        this.b(worldsettings);
        super.a(worldsettings);
    }

    protected void b(WorldSettings worldsettings) {
        if (!this.worldProvider.e()) {
            this.worldData.setSpawn(0, this.worldProvider.getSeaLevel(), 0);
        } else {
            this.isLoading = true;
            WorldChunkManager worldchunkmanager = this.worldProvider.d;
            List list = worldchunkmanager.a();
            Random random = new Random(this.getSeed());
            ChunkPosition chunkposition = worldchunkmanager.a(0, 0, 256, list, random);
            int i = 0;
            int j = this.worldProvider.getSeaLevel();
            int k = 0;
            if (this.generator != null) {
                Random l = new Random(this.getSeed());
                Location spawn = this.generator.getFixedSpawnLocation(this.getWorld(), l);
                if (spawn != null) {
                    if (spawn.getWorld() != this.getWorld()) {
                        throw new IllegalStateException("Cannot set spawn point for " + this.worldData.getName() + " to be in another world (" + spawn.getWorld().getName() + ")");
                    }

                    this.worldData.setSpawn(spawn.getBlockX(), spawn.getBlockY(), spawn.getBlockZ());
                    this.isLoading = false;
                    return;
                }
            }

            if (chunkposition != null) {
                i = chunkposition.x;
                k = chunkposition.z;
            } else {
                this.getLogger().warning("Unable to find spawn biome");
            }

            int var11 = 0;

            while (!this.canSpawn(i, k)) {
                i += random.nextInt(64) - random.nextInt(64);
                k += random.nextInt(64) - random.nextInt(64);
                ++var11;
                if (var11 == 1000) {
                    break;
                }
            }

            this.worldData.setSpawn(i, j, k);
            this.isLoading = false;
            if (worldsettings.c()) {
                this.k();
            }
        }

    }

    protected void k() {
        WorldGenBonusChest worldgenbonuschest = new WorldGenBonusChest(S, 10);

        for (int i = 0; i < 10; ++i) {
            int j = this.worldData.c() + this.random.nextInt(6) - this.random.nextInt(6);
            int k = this.worldData.e() + this.random.nextInt(6) - this.random.nextInt(6);
            int l = this.i(j, k) + 1;
            if (worldgenbonuschest.a(this, this.random, j, l, k)) {
                break;
            }
        }

    }

    public ChunkCoordinates getDimensionSpawn() {
        return this.worldProvider.h();
    }

    public void save(boolean flag, IProgressUpdate iprogressupdate) throws ExceptionWorldConflict {
        if (this.chunkProvider.canSave()) {
            if (iprogressupdate != null) {
                iprogressupdate.a("Saving level");
            }

            this.a();
            if (iprogressupdate != null) {
                iprogressupdate.c("Saving chunks");
            }

            this.chunkProvider.saveChunks(flag, iprogressupdate);
        }

    }

    public void flushSave() {
        if (this.chunkProvider.canSave()) {
            this.chunkProvider.b();
        }

    }

    protected void a() throws ExceptionWorldConflict {
        this.F();
        this.dataManager.saveWorldData(this.worldData, this.server.getPlayerList().q());
        this.worldMaps.a();
    }

    protected void a(Entity entity) {
        super.a(entity);
        this.entitiesById.a(entity.id, entity);
        Entity[] aentity = entity.an();
        if (aentity != null) {
            for (int i = 0; i < aentity.length; ++i) {
                this.entitiesById.a(aentity[i].id, aentity[i]);
            }
        }

    }

    protected void b(Entity entity) {
        super.b(entity);
        this.entitiesById.d(entity.id);
        Entity[] aentity = entity.an();
        if (aentity != null) {
            for (int i = 0; i < aentity.length; ++i) {
                this.entitiesById.d(aentity[i].id);
            }
        }

    }

    public Entity getEntity(int i) {
        return (Entity) this.entitiesById.get(i);
    }

    public boolean strikeLightning(Entity entity) {
        LightningStrikeEvent lightning = new LightningStrikeEvent(this.getWorld(), (LightningStrike) entity.getBukkitEntity());
        this.getServer().getPluginManager().callEvent(lightning);
        if (lightning.isCancelled()) {
            return false;
        } else if (super.strikeLightning(entity)) {
            this.server.getPlayerList().sendPacketNearby(entity.locX, entity.locY, entity.locZ, 512.0D, this.dimension, new Packet71Weather(entity));
            return true;
        } else {
            return false;
        }
    }

    public void broadcastEntityEffect(Entity entity, byte b0) {
        Packet38EntityStatus packet38entitystatus = new Packet38EntityStatus(entity.id, b0);
        this.getTracker().sendPacketToEntity(entity, packet38entitystatus);
    }

    public Explosion createExplosion(Entity entity, double d0, double d1, double d2, float f, boolean flag, boolean flag1) {
        Explosion explosion = super.createExplosion(entity, d0, d1, d2, f, flag, flag1);
        if (explosion.wasCanceled) {
            return explosion;
        } else {
            if (!flag1) {
                explosion.blocks.clear();
            }

            Iterator iterator = this.players.iterator();

            while (iterator.hasNext()) {
                EntityHuman entityhuman = (EntityHuman) iterator.next();
                if (entityhuman.e(d0, d1, d2) < 4096.0D) {
                    ((EntityPlayer) entityhuman).playerConnection.sendPacket(new Packet60Explosion(d0, d1, d2, f, explosion.blocks, (Vec3D) explosion.b().get(entityhuman)));
                }
            }

            return explosion;
        }
    }

    public void playNote(int i, int j, int k, int l, int i1, int j1) {
        NoteBlockData noteblockdata = new NoteBlockData(i, j, k, l, i1, j1);
        Iterator iterator = this.Q[this.R].iterator();

        while (iterator.hasNext()) {
            NoteBlockData noteblockdata1 = (NoteBlockData) iterator.next();
            if (noteblockdata1.equals(noteblockdata)) {
                return;
            }
        }

        this.Q[this.R].add(noteblockdata);
    }

    private void Z() {
        while (!this.Q[this.R].isEmpty()) {
            int i = this.R;
            this.R ^= 1;
            Iterator iterator = this.Q[i].iterator();

            while (iterator.hasNext()) {
                NoteBlockData noteblockdata = (NoteBlockData) iterator.next();
                if (this.a(noteblockdata)) {
                    this.server.getPlayerList().sendPacketNearby((double) noteblockdata.a(), (double) noteblockdata.b(), (double) noteblockdata.c(), 64.0D, this.dimension, new Packet54PlayNoteBlock(noteblockdata.a(), noteblockdata.b(), noteblockdata.c(), noteblockdata.f(), noteblockdata.d(), noteblockdata.e()));
                }
            }

            this.Q[i].clear();
        }

    }

    private boolean a(NoteBlockData noteblockdata) {
        int i = this.getTypeId(noteblockdata.a(), noteblockdata.b(), noteblockdata.c());
        return i == noteblockdata.f() && Block.byId[i].b(this, noteblockdata.a(), noteblockdata.b(), noteblockdata.c(), noteblockdata.d(), noteblockdata.e());
    }

    public void saveLevel() {
        this.dataManager.a();
    }

    protected void o() {
        boolean flag = this.P();
        super.o();
        if (flag != this.P()) {
            for (int i = 0; i < this.players.size(); ++i) {
                if (((EntityPlayer) this.players.get(i)).world == this) {
                    ((EntityPlayer) this.players.get(i)).setPlayerWeather(!flag ? WeatherType.DOWNFALL : WeatherType.CLEAR, false);
                }
            }
        }

    }

    public MinecraftServer getMinecraftServer() {
        return this.server;
    }

    public EntityTracker getTracker() {
        return this.tracker;
    }

    public PlayerChunkMap getPlayerChunkMap() {
        return this.manager;
    }

    public PortalTravelAgent t() {
        return this.P;
    }

    public boolean setRawTypeId(int x, int y, int z, int typeId) {
        return this.setTypeIdAndData(x, y, z, typeId, 0, 4);
    }

    public boolean setRawTypeIdAndData(int x, int y, int z, int typeId, int data) {
        return this.setTypeIdAndData(x, y, z, typeId, data, 4);
    }

    public boolean setTypeId(int x, int y, int z, int typeId) {
        return this.setTypeIdAndData(x, y, z, typeId, 0, 3);
    }

    public boolean setTypeIdAndData(int x, int y, int z, int typeId, int data) {
        return this.setTypeIdAndData(x, y, z, typeId, data, 3);
    }

    private void addNextTickIfNeeded(NextTickListEntry ent) {
        long coord = LongHash.toLong(ent.a >> 4, ent.c >> 4);
        Set<NextTickListEntry> chunkset = this.tickEntriesByChunk.get(coord);
        if (chunkset == null) {
            chunkset = new HashSet<>();
            this.tickEntriesByChunk.put(coord, chunkset);
        } else if (chunkset.contains(ent)) {
            return;
        }

        chunkset.add(ent);
        this.tickEntryQueue.add(ent);
    }

    private void removeNextTickIfNeeded(NextTickListEntry ent) {
        long coord = LongHash.toLong(ent.a >> 4, ent.c >> 4);
        Set chunkset = this.tickEntriesByChunk.get(coord);
        if (chunkset != null) {
            chunkset.remove(ent);
            if (chunkset.isEmpty()) {
                this.tickEntriesByChunk.remove(coord);
            }
        }

        this.tickEntryQueue.remove(ent);
    }

    private List<NextTickListEntry> getNextTickEntriesForChunk(Chunk chunk, boolean remove) {
        long coord = LongHash.toLong(chunk.x, chunk.z);
        Set chunkset = this.tickEntriesByChunk.get(coord);
        ArrayList list = null;
        if (chunkset != null) {
            list = new ArrayList(chunkset);
            if (remove) {
                this.tickEntriesByChunk.remove(coord);
                this.tickEntryQueue.removeAll(list);
                chunkset.clear();
            }
        }

        if (this.nextPendingTickEntry < this.pendingTickEntries.size()) {
            int xmin = chunk.x << 4;
            int xmax = xmin + 16;
            int zmin = chunk.z << 4;
            int zmax = zmin + 16;
            int te_cnt = this.pendingTickEntries.size();

            for (int i = this.nextPendingTickEntry; i < te_cnt; ++i) {
                NextTickListEntry ent = this.pendingTickEntries.get(i);
                if (ent.a >= xmin && ent.a < xmax && ent.c >= zmin && ent.c < zmax) {
                    if (list == null) {
                        list = new ArrayList();
                    }

                    list.add(ent);
                }
            }
        }

        return list;
    }
}
