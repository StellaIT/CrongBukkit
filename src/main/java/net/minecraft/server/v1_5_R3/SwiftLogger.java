package net.minecraft.server.v1_5_R3;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Junhyeong Lim on 2017-01-29.
 */
public class SwiftLogger {
    private Logger parent;
    private StringBuilder log;

    public SwiftLogger(Logger parent) {
        this.parent = parent;
        this.log = new StringBuilder();
    }


    public void log(Level level, String msg) {
        log.append(msg).append("\n");
        parent.log(level, msg);
    }

    @Override
    public String toString() {
        return log.toString();
    }
}
