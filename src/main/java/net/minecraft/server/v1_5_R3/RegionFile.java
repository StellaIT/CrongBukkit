package net.minecraft.server.v1_5_R3;

/**
 * Created by EntryPoint on 2016-12-29.
 */

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;

public class RegionFile {
    private static final byte[] a = new byte[4096];
    private final File b;
    private final int[] d = new int[1024];
    private final int[] e = new int[1024];
    private RandomAccessFile c;
    private ArrayList f;
    private int g;
    private long h = 0L;

    public RegionFile(File file1) {
        this.b = file1;
        this.g = 0;

        try {
            if (file1.exists()) {
                this.h = file1.lastModified();
            }

            this.c = new RandomAccessFile(file1, "rw");
            int ioexception;
            if (this.c.length() < 4096L) {
                for (ioexception = 0; ioexception < 1024; ++ioexception) {
                    this.c.writeInt(0);
                }

                for (ioexception = 0; ioexception < 1024; ++ioexception) {
                    this.c.writeInt(0);
                }

                this.g += 8192;
            }

            if ((this.c.length() & 4095L) != 0L) {
                for (ioexception = 0; (long) ioexception < (this.c.length() & 4095L); ++ioexception) {
                    this.c.write(0);
                }
            }

            ioexception = (int) this.c.length() / 4096;
            this.f = new ArrayList(ioexception);

            int j;
            for (j = 0; j < ioexception; ++j) {
                this.f.add(Boolean.TRUE);
            }

            this.f.set(0, Boolean.FALSE);
            this.f.set(1, Boolean.FALSE);
            this.c.seek(0L);

            int k;
            ByteBuffer header = ByteBuffer.allocate(8192);
            while (header.hasRemaining()) {
                if (this.c.getChannel().read(header) == -1) throw new EOFException();
            }
            header.clear();
            IntBuffer headerAsInts = header.asIntBuffer();
            for (j = 0; j < 1024; ++j) {
                k = headerAsInts.get();
                this.d[j] = k;
                if (k != 0 && (k >> 8) + (k & 255) <= this.f.size()) {
                    for (int l = 0; l < (k & 255); ++l) {
                        this.f.set((k >> 8) + l, Boolean.FALSE);
                    }
                }
            }

            for (j = 0; j < 1024; ++j) {
                k = headerAsInts.get();
                this.e[j] = k;
            }
        } catch (IOException var6) {
            var6.printStackTrace();
        }

    }

    public synchronized boolean chunkExists(int i, int j) {
        if (this.d(i, j)) {
            return false;
        } else {
            try {
                int ioexception = this.e(i, j);
                if (ioexception == 0) {
                    return false;
                } else {
                    int l = ioexception >> 8;
                    int i1 = ioexception & 255;
                    if (l + i1 > this.f.size()) {
                        return false;
                    } else {
                        this.c.seek((long) (l * 4096));
                        int j1 = this.c.readInt();
                        if (j1 <= 4096 * i1 && j1 > 0) {
                            byte b0 = this.c.readByte();
                            return b0 == 1 || b0 == 2;
                        } else {
                            return false;
                        }
                    }
                }
            } catch (IOException var8) {
                return false;
            }
        }
    }

    public synchronized DataInputStream a(int i, int j) {
        if (this.d(i, j)) {
            return null;
        } else {
            try {
                int ioexception = this.e(i, j);
                if (ioexception == 0) {
                    return null;
                } else {
                    int l = ioexception >> 8;
                    int i1 = ioexception & 255;
                    if (l + i1 > this.f.size()) {
                        return null;
                    } else {
                        this.c.seek((long) (l * 4096));
                        int j1 = this.c.readInt();
                        if (j1 > 4096 * i1) {
                            return null;
                        } else if (j1 <= 0) {
                            return null;
                        } else {
                            byte b0 = this.c.readByte();
                            byte[] abyte;
                            if (b0 == 1) {
                                abyte = new byte[j1 - 1];
                                this.c.read(abyte);
                                return new DataInputStream(new BufferedInputStream(new GZIPInputStream(new ByteArrayInputStream(abyte))));
                            } else if (b0 == 2) {
                                abyte = new byte[j1 - 1];
                                this.c.read(abyte);
                                return new DataInputStream(new BufferedInputStream(new InflaterInputStream(new ByteArrayInputStream(abyte))));
                            } else {
                                return null;
                            }
                        }
                    }
                }
            } catch (IOException var9) {
                return null;
            }
        }
    }

    public DataOutputStream b(int i, int j) {
        return this.d(i, j) ? null :
                new DataOutputStream(
                        new BufferedOutputStream(
                                new DeflaterOutputStream(
                                        new ChunkBuffer(this, i, j))));
    }

    protected synchronized void a(int i, int j, byte[] abyte, int k) {
        try {
            int ioexception = this.e(i, j);
            int i1 = ioexception >> 8;
            int j1 = ioexception & 255;
            int k1 = (k + 5) / 4096 + 1;
            if (k1 >= 256) {
                return;
            }

            if (i1 != 0 && j1 == k1) {
                this.a(i1, abyte, k);
            } else {
                int l1;
                for (l1 = 0; l1 < j1; ++l1) {
                    this.f.set(i1 + l1, Boolean.TRUE);
                }

                l1 = this.f.indexOf(Boolean.TRUE);
                int i2 = 0;
                int j2;
                if (l1 != -1) {
                    for (j2 = l1; j2 < this.f.size(); ++j2) {
                        if (i2 != 0) {
                            if ((Boolean) this.f.get(j2)) {
                                ++i2;
                            } else {
                                i2 = 0;
                            }
                        } else if ((Boolean) this.f.get(j2)) {
                            l1 = j2;
                            i2 = 1;
                        }

                        if (i2 >= k1) {
                            break;
                        }
                    }
                }

                if (i2 >= k1) {
                    i1 = l1;
                    this.a(i, j, l1 << 8 | k1);

                    for (j2 = 0; j2 < k1; ++j2) {
                        this.f.set(i1 + j2, Boolean.FALSE);
                    }

                    this.a(i1, abyte, k);
                } else {
                    this.c.seek(this.c.length());
                    i1 = this.f.size();

                    for (j2 = 0; j2 < k1; ++j2) {
                        this.c.write(a);
                        this.f.add(Boolean.FALSE);
                    }

                    this.g += 4096 * k1;
                    this.a(i1, abyte, k);
                    this.a(i, j, i1 << 8 | k1);
                }
            }

            this.b(i, j, (int) (System.currentTimeMillis() / 1000L));
        } catch (IOException var12) {
            var12.printStackTrace();
        }

    }

    private void a(int i, byte[] abyte, int j) throws IOException {
        this.c.seek((long) (i * 4096));
        this.c.writeInt(j + 1);
        this.c.writeByte(2);
        this.c.write(abyte, 0, j);
    }

    private boolean d(int i, int j) {
        return i < 0 || i >= 32 || j < 0 || j >= 32;
    }

    private int e(int i, int j) {
        return this.d[i + j * 32];
    }

    public boolean c(int i, int j) {
        return this.e(i, j) != 0;
    }

    private void a(int i, int j, int k) throws IOException {
        this.d[i + j * 32] = k;
        this.c.seek((long) ((i + j * 32) * 4));
        this.c.writeInt(k);
    }

    private void b(int i, int j, int k) throws IOException {
        this.e[i + j * 32] = k;
        this.c.seek((long) (4096 + (i + j * 32) * 4));
        this.c.writeInt(k);
    }

    public void c() throws IOException {
        if (this.c != null) {
            this.c.close();
        }

    }
}
