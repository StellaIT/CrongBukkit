package net.minecraft.server.v1_5_R3;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class PlayerChunkMap {
    private final WorldServer world;
    private final List managedPlayers = new ArrayList();
    private final LongHashMap c = new LongHashMap();
    private final Queue d = new ConcurrentLinkedQueue();
    private final int e;
    private final int[][] f = new int[][]{{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
    private boolean wasNotEmpty;

    public PlayerChunkMap(WorldServer worldserver, int i) {
        if (i > 15) {
            throw new IllegalArgumentException("Too big view radius!");
        } else if (i < 1) {
            throw new IllegalArgumentException("Too small view radius!");
        } else {
            this.e = i;
            this.world = worldserver;
        }
    }

    public static int getFurthestViewableBlock(int i) {
        return i * 16 - 16;
    }

    static WorldServer a(PlayerChunkMap playerchunkmap) {
        return playerchunkmap.world;
    }

    static LongHashMap b(PlayerChunkMap playerchunkmap) {
        return playerchunkmap.c;
    }

    static Queue c(PlayerChunkMap playermanager) {
        return playermanager.d;
    }

    public WorldServer a() {
        return this.world;
    }

    public void flush() {
        Iterator iterator = this.d.iterator();

        while (iterator.hasNext()) {
            PlayerChunk worldprovider = (PlayerChunk) iterator.next();
            worldprovider.a();
            iterator.remove();
        }

        if (this.managedPlayers.isEmpty()) {
            if (!this.wasNotEmpty) {
                return;
            }

            WorldProvider worldprovider1 = this.world.worldProvider;
            if (!worldprovider1.e()) {
                this.world.chunkProviderServer.a();
            }

            this.wasNotEmpty = false;
        } else {
            this.wasNotEmpty = true;
        }

    }

    private PlayerChunk a(int i, int j, boolean flag) {
        long k = (long) i + 2147483647L | (long) j + 2147483647L << 32;
        // TODO: 에러날것같음
        PlayerChunk playerchunk = (PlayerChunk) this.c.getEntry(k);
        if (playerchunk == null && flag) {
            playerchunk = new PlayerChunk(this, i, j);
            this.c.put(k, playerchunk);
        }

        return playerchunk;
    }

    public final boolean isChunkInUse(int x, int z) {
        PlayerChunk pi = this.a(x, z, false);
        return pi != null && PlayerChunk.b(pi).size() > 0;
    }

    public void flagDirty(int i, int j, int k) {
        int l = i >> 4;
        int i1 = k >> 4;
        PlayerChunk playerchunk = this.a(l, i1, false);
        if (playerchunk != null) {
            playerchunk.a(i & 15, j, k & 15);
        }

    }

    public void addPlayer(EntityPlayer entityplayer) {
        int i = (int) entityplayer.locX >> 4;
        int j = (int) entityplayer.locZ >> 4;
        entityplayer.d = entityplayer.locX;
        entityplayer.e = entityplayer.locZ;
        LinkedList chunkList = new LinkedList();

        for (int i$ = i - this.e; i$ <= i + this.e; ++i$) {
            for (int pair = j - this.e; pair <= j + this.e; ++pair) {
                chunkList.add(new ChunkCoordIntPair(i$, pair));
            }
        }

        Collections.sort(chunkList, new PlayerChunkMap.ChunkCoordComparator(entityplayer));
        Iterator var7 = chunkList.iterator();

        while (var7.hasNext()) {
            ChunkCoordIntPair var8 = (ChunkCoordIntPair) var7.next();
            this.a(var8.x, var8.z, true).a(entityplayer);
        }

        this.managedPlayers.add(entityplayer);
        this.b(entityplayer);
    }

    public void b(EntityPlayer entityplayer) {
        ArrayList arraylist = new ArrayList(entityplayer.chunkCoordIntPairQueue);
        int i = 0;
        int j = this.e;
        int k = (int) entityplayer.locX >> 4;
        int l = (int) entityplayer.locZ >> 4;
        int i1 = 0;
        int j1 = 0;
        ChunkCoordIntPair chunkcoordintpair = PlayerChunk.a(this.a(k, l, true));
        entityplayer.chunkCoordIntPairQueue.clear();
        if (arraylist.contains(chunkcoordintpair)) {
            entityplayer.chunkCoordIntPairQueue.add(chunkcoordintpair);
        }

        int k1;
        for (k1 = 1; k1 <= j * 2; ++k1) {
            for (int l1 = 0; l1 < 2; ++l1) {
                int[] aint = this.f[i++ % 4];

                for (int i2 = 0; i2 < k1; ++i2) {
                    i1 += aint[0];
                    j1 += aint[1];
                    chunkcoordintpair = PlayerChunk.a(this.a(k + i1, l + j1, true));
                    if (arraylist.contains(chunkcoordintpair)) {
                        entityplayer.chunkCoordIntPairQueue.add(chunkcoordintpair);
                    }
                }
            }
        }

        i %= 4;

        for (k1 = 0; k1 < j * 2; ++k1) {
            i1 += this.f[i][0];
            j1 += this.f[i][1];
            chunkcoordintpair = PlayerChunk.a(this.a(k + i1, l + j1, true));
            if (arraylist.contains(chunkcoordintpair)) {
                entityplayer.chunkCoordIntPairQueue.add(chunkcoordintpair);
            }
        }

    }

    public void removePlayer(EntityPlayer entityplayer) {
        int i = (int) entityplayer.d >> 4;
        int j = (int) entityplayer.e >> 4;

        for (int k = i - this.e; k <= i + this.e; ++k) {
            for (int l = j - this.e; l <= j + this.e; ++l) {
                PlayerChunk playerchunk = this.a(k, l, false);
                if (playerchunk != null) {
                    playerchunk.b(entityplayer);
                }
            }
        }

        this.managedPlayers.remove(entityplayer);
    }

    private boolean a(int i, int j, int k, int l, int i1) {
        int j1 = i - k;
        int k1 = j - l;
        return (j1 >= -i1 && j1 <= i1) && (k1 >= -i1 && k1 <= i1);
    }

    public void movePlayer(EntityPlayer entityplayer) {
        int i = (int) entityplayer.locX >> 4;
        int j = (int) entityplayer.locZ >> 4;
        double d0 = entityplayer.d - entityplayer.locX;
        double d1 = entityplayer.e - entityplayer.locZ;
        double d2 = d0 * d0 + d1 * d1;
        if (d2 >= 64.0D) {
            int k = (int) entityplayer.d >> 4;
            int l = (int) entityplayer.e >> 4;
            int i1 = this.e;
            int j1 = i - k;
            int k1 = j - l;
            LinkedList chunksToLoad = new LinkedList();
            if (j1 != 0 || k1 != 0) {
                for (int i$ = i - i1; i$ <= i + i1; ++i$) {
                    for (int pair = j - i1; pair <= j + i1; ++pair) {
                        if (!this.a(i$, pair, k, l, i1)) {
                            chunksToLoad.add(new ChunkCoordIntPair(i$, pair));
                        }

                        if (!this.a(i$ - j1, pair - k1, i, j, i1)) {
                            PlayerChunk playerchunk = this.a(i$ - j1, pair - k1, false);
                            if (playerchunk != null) {
                                playerchunk.b(entityplayer);
                            }
                        }
                    }
                }

                this.b(entityplayer);
                entityplayer.d = entityplayer.locX;
                entityplayer.e = entityplayer.locZ;
                Collections.sort(chunksToLoad, new PlayerChunkMap.ChunkCoordComparator(entityplayer));
                Iterator var19 = chunksToLoad.iterator();

                while (var19.hasNext()) {
                    ChunkCoordIntPair var20 = (ChunkCoordIntPair) var19.next();
                    this.a(var20.x, var20.z, true).a(entityplayer);
                }

                if (i1 > 1 || i1 < -1 || j1 > 1 || j1 < -1) {
                    Collections.sort(entityplayer.chunkCoordIntPairQueue, new PlayerChunkMap.ChunkCoordComparator(entityplayer));
                }
            }
        }

    }

    public boolean a(EntityPlayer entityplayer, int i, int j) {
        PlayerChunk playerchunk = this.a(i, j, false);
        return playerchunk != null && (PlayerChunk.b(playerchunk).contains(entityplayer) && !entityplayer.chunkCoordIntPairQueue.contains(PlayerChunk.a(playerchunk)));
    }

    private static class ChunkCoordComparator implements Comparator<ChunkCoordIntPair> {
        private int x;
        private int z;

        public ChunkCoordComparator(EntityPlayer entityplayer) {
            this.x = (int) entityplayer.locX >> 4;
            this.z = (int) entityplayer.locZ >> 4;
        }

        public int compare(ChunkCoordIntPair a, ChunkCoordIntPair b) {
            if (a.equals(b)) {
                return 0;
            } else {
                int ax = a.x - this.x;
                int az = a.z - this.z;
                int bx = b.x - this.x;
                int bz = b.z - this.z;
                int result = (ax - bx) * (ax + bx) + (az - bz) * (az + bz);
                return result != 0 ? result : (ax < 0 ? (bx < 0 ? bz - az : -1) : (bx < 0 ? 1 : az - bz));
            }
        }
    }
}
