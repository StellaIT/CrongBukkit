package net.minecraft.server.v1_5_R3;

import org.bukkit.craftbukkit.v1_5_R3.util.LongHash;
import org.bukkit.craftbukkit.v1_5_R3.util.Waitable;

import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;

/**
 * Created by EntryPoint on 2016-12-30.
 */
public class SwiftUtils {
    public static Chunk getLoadedChunkWithoutMarkingActive(World world, int x, int z) {
        return ((ChunkProviderServer) world.chunkProvider).chunks.get(LongHash.toLong(x, z));
    }

    public static String limit(String str, int limit) {
        if (str != null && str.length() > limit) {
            str = str.substring(0, limit);
        }
        return str;
    }

    public static <T> T ensureMain(String reason, final Supplier<T> run) {
        if (Thread.currentThread() != MinecraftServer.getServer().primaryThread) {
            (new IllegalStateException("Asynchronous " + reason + "! Blocking thread until it returns ")).printStackTrace();
            Waitable wait = new Waitable() {
                protected T evaluate() {
                    return run.get();
                }
            };
            MinecraftServer.getServer().processQueue.add(wait);

            try {
                return (T) wait.get();
            } catch (ExecutionException | InterruptedException var4) {
                var4.printStackTrace();
                return null;
            }
        } else {
            return run.get();
        }
    }
}
