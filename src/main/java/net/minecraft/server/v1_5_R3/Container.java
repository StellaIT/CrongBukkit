package net.minecraft.server.v1_5_R3;

/**
 * Created by EntryPoint on 2016-12-31.
 */

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_5_R3.entity.CraftHumanEntity;
import org.bukkit.craftbukkit.v1_5_R3.inventory.CraftInventory;
import org.bukkit.craftbukkit.v1_5_R3.inventory.CraftItemStack;
import org.bukkit.event.Event.Result;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.InventoryView;

import java.util.*;
import java.util.HashMap;
import java.util.Map.Entry;

public abstract class Container {
    private final Set h = new HashSet();
    public List b = new ArrayList();
    public List<Slot> c = new ArrayList<>();
    public int windowId = 0;
    public int g = 0;
    public boolean checkReachable = true;
    protected List listeners = new ArrayList();
    private short a = 0;
    private int f = -1;
    private Set i = new HashSet();

    public Container() {
    }

    public static int b(int i) {
        return i >> 2 & 3;
    }

    public static int c(int i) {
        return i & 3;
    }

    public static boolean d(int i) {
        return i == 0 || i == 1;
    }

    public static boolean a(Slot slot, ItemStack itemstack, boolean flag) {
        boolean flag1 = slot == null || !slot.d();
        if (slot != null && slot.d() && itemstack != null && itemstack.doMaterialsMatch(slot.getItem()) && ItemStack.equals(slot.getItem(), itemstack)) {
            int i = flag ? 0 : itemstack.count;
            flag1 |= slot.getItem().count + i <= itemstack.getMaxStackSize();
        }

        return flag1;
    }

    public static void a(Set set, int i, ItemStack itemstack, int j) {
        switch (i) {
            case 0:
                itemstack.count = MathHelper.d((float) itemstack.count / (float) set.size());
                break;
            case 1:
                itemstack.count = 1;
        }

        itemstack.count += j;
    }

    public static int b(IInventory iinventory) {
        if (iinventory == null) {
            return 0;
        } else {
            int i = 0;
            float f = 0.0F;

            for (int j = 0; j < iinventory.getSize(); ++j) {
                ItemStack itemstack = iinventory.getItem(j);
                if (itemstack != null) {
                    f += (float) itemstack.count / (float) Math.min(iinventory.getMaxStackSize(), itemstack.getMaxStackSize());
                    ++i;
                }
            }

            f /= (float) iinventory.getSize();
            return MathHelper.d(f * 14.0F) + (i > 0 ? 1 : 0);
        }
    }

    public abstract InventoryView getBukkitView();

    public void transferTo(Container other, CraftHumanEntity player) {
        InventoryView source = this.getBukkitView();
        InventoryView destination = other.getBukkitView();
        ((CraftInventory) source.getTopInventory()).getInventory().onClose(player);
        ((CraftInventory) source.getBottomInventory()).getInventory().onClose(player);
        ((CraftInventory) destination.getTopInventory()).getInventory().onOpen(player);
        ((CraftInventory) destination.getBottomInventory()).getInventory().onOpen(player);
    }

    protected Slot a(Slot slot) {
        slot.g = this.c.size();
        this.c.add(slot);
        this.b.add(null);
        return slot;
    }

    public void addSlotListener(ICrafting icrafting) {
        if (this.listeners.contains(icrafting)) {
            throw new IllegalArgumentException("Listener already listening");
        } else {
            this.listeners.add(icrafting);
            icrafting.a(this, this.a());
            this.b();
        }
    }

    public List a() {
        ArrayList arraylist = new ArrayList();

        for (int i = 0; i < this.c.size(); ++i) {
            arraylist.add(this.c.get(i).getItem());
        }

        return arraylist;
    }

    public void b() {
        for (int i = 0; i < this.c.size(); ++i) {
            ItemStack itemstack = this.c.get(i).getItem();
            ItemStack itemstack1 = (ItemStack) this.b.get(i);
            if (!ItemStack.matches(itemstack1, itemstack)) {
                itemstack1 = itemstack == null ? null : itemstack.cloneItemStack();
                this.b.set(i, itemstack1);

                for (int j = 0; j < this.listeners.size(); ++j) {
                    ((ICrafting) this.listeners.get(j)).a(this, i, itemstack1);
                }
            }
        }

    }

    public boolean a(EntityHuman entityhuman, int i) {
        return false;
    }

    public Slot a(IInventory iinventory, int i) {
        for (int j = 0; j < this.c.size(); ++j) {
            Slot slot = this.c.get(j);
            if (slot.a(iinventory, i)) {
                return slot;
            }
        }

        return null;
    }

    public Slot getSlot(int i) {
        return this.c.get(i);
    }

    public ItemStack b(EntityHuman entityhuman, int i) {
        Slot slot = this.c.get(i);
        return slot != null ? slot.getItem() : null;
    }

    public ItemStack clickItem(int i, int j, int k, EntityHuman entityhuman) {
        ItemStack itemstack = null;
        PlayerInventory playerinventory = entityhuman.inventory;
        ItemStack itemstack1;
        int l;
        if (k == 5) {
            int slot2 = this.g;
            this.g = c(j);
            if ((slot2 != 1 || this.g != 2) && slot2 != this.g) {
                this.d();
            } else if (playerinventory.getCarried() == null) {
                this.d();
            } else if (this.g == 0) {
                this.f = b(j);
                if (d(this.f)) {
                    this.g = 1;
                    this.h.clear();
                } else {
                    this.d();
                }
            } else if (this.g == 1) {
                Slot k1 = i < this.c.size() ? this.c.get(i) : null;
                if (k1 != null && a(k1, playerinventory.getCarried(), true) && k1.isAllowed(playerinventory.getCarried()) && playerinventory.getCarried().count > this.h.size() && this.b(k1)) {
                    this.h.add(k1);
                }
            } else if (this.g == 2) {
                if (!this.h.isEmpty()) {
                    itemstack1 = playerinventory.getCarried().cloneItemStack();
                    l = playerinventory.getCarried().count;
                    Iterator var21 = this.h.iterator();
                    HashMap itemstack3 = new HashMap();

                    while (var21.hasNext()) {
                        Slot l1 = (Slot) var21.next();
                        if (l1 != null && a(l1, playerinventory.getCarried(), true) && l1.isAllowed(playerinventory.getCarried()) && playerinventory.getCarried().count >= this.h.size() && this.b(l1)) {
                            ItemStack i2 = itemstack1.cloneItemStack();
                            int slot3 = l1.d() ? l1.getItem().count : 0;
                            a(this.h, this.f, i2, slot3);
                            if (i2.count > i2.getMaxStackSize()) {
                                i2.count = i2.getMaxStackSize();
                            }

                            if (i2.count > l1.a()) {
                                i2.count = l1.a();
                            }

                            l -= i2.count - slot3;
                            itemstack3.put(Integer.valueOf(l1.g), i2);
                        }
                    }

                    InventoryView var24 = this.getBukkitView();
                    CraftItemStack var27 = CraftItemStack.asCraftMirror(itemstack1);
                    var27.setAmount(l);
                    HashMap var30 = new HashMap();
                    Iterator j2 = itemstack3.entrySet().iterator();

                    while (j2.hasNext()) {
                        Entry itemstack5 = (Entry) j2.next();
                        var30.put(itemstack5.getKey(), CraftItemStack.asBukkitCopy((ItemStack) itemstack5.getValue()));
                    }

                    ItemStack var32 = playerinventory.getCarried();
                    playerinventory.setCarried(CraftItemStack.asNMSCopy(var27));
                    InventoryDragEvent var34 = new InventoryDragEvent(var24, var27.getType() != Material.AIR ? var27 : null, CraftItemStack.asBukkitCopy(var32), this.f == 1, var30);
                    entityhuman.world.getServer().getPluginManager().callEvent(var34);
                    boolean needsUpdate = var34.getResult() != Result.DEFAULT;
                    if (var34.getResult() != Result.DENY) {
                        Iterator i$ = itemstack3.entrySet().iterator();

                        while (i$.hasNext()) {
                            Entry dslot = (Entry) i$.next();
                            var24.setItem(((Integer) dslot.getKey()).intValue(), CraftItemStack.asBukkitCopy((ItemStack) dslot.getValue()));
                        }

                        if (playerinventory.getCarried() != null) {
                            playerinventory.setCarried(CraftItemStack.asNMSCopy(var34.getCursor()));
                            needsUpdate = true;
                        }
                    } else {
                        playerinventory.setCarried(var32);
                    }

                    if (needsUpdate && entityhuman instanceof EntityPlayer) {
                        ((EntityPlayer) entityhuman).updateInventory(this);
                    }
                }

                this.d();
            } else {
                this.d();
            }
        } else if (this.g != 0) {
            this.d();
        } else {
            Slot var20;
            int var22;
            ItemStack var23;
            if ((k == 0 || k == 1) && (j == 0 || j == 1)) {
                ItemStack var29;
                if (i == -999) {
                    if (playerinventory.getCarried() != null && i == -999) {
                        if (j == 0) {
                            entityhuman.drop(playerinventory.getCarried());
                            playerinventory.setCarried(null);
                        }

                        if (j == 1) {
                            var29 = playerinventory.getCarried();
                            if (var29.count > 0) {
                                entityhuman.drop(var29.a(1));
                            }

                            if (var29.count == 0) {
                                playerinventory.setCarried(null);
                            }
                        }
                    }
                } else if (k == 1) {
                    if (i < 0) {
                        return null;
                    }

                    var20 = this.c.get(i);
                    if (var20 != null && var20.a(entityhuman)) {
                        itemstack1 = this.b(entityhuman, i);
                        if (itemstack1 != null) {
                            l = itemstack1.id;
                            itemstack = itemstack1.cloneItemStack();
                            if (var20 != null && var20.getItem() != null && var20.getItem().id == l) {
                                this.a(i, j, true, entityhuman);
                            }
                        }
                    }
                } else {
                    if (i < 0) {
                        return null;
                    }

                    var20 = this.c.get(i);
                    if (var20 != null) {
                        itemstack1 = var20.getItem();
                        var29 = playerinventory.getCarried();
                        if (itemstack1 != null) {
                            itemstack = itemstack1.cloneItemStack();
                        }

                        if (itemstack1 == null) {
                            if (var29 != null && var20.isAllowed(var29)) {
                                var22 = j == 0 ? var29.count : 1;
                                if (var22 > var20.a()) {
                                    var22 = var20.a();
                                }

                                if (var29.count >= var22) {
                                    var20.set(var29.a(var22));
                                }

                                if (var29.count == 0) {
                                    playerinventory.setCarried(null);
                                }
                            }
                        } else if (var20.a(entityhuman)) {
                            if (var29 == null) {
                                var22 = j == 0 ? itemstack1.count : (itemstack1.count + 1) / 2;
                                var23 = var20.a(var22);
                                playerinventory.setCarried(var23);
                                if (itemstack1.count == 0) {
                                    var20.set(null);
                                }

                                var20.a(entityhuman, playerinventory.getCarried());
                            } else if (var20.isAllowed(var29)) {
                                if (itemstack1.id == var29.id && itemstack1.getData() == var29.getData() && ItemStack.equals(itemstack1, var29)) {
                                    var22 = j == 0 ? var29.count : 1;
                                    if (var22 > var20.a() - itemstack1.count) {
                                        var22 = var20.a() - itemstack1.count;
                                    }

                                    if (var22 > var29.getMaxStackSize() - itemstack1.count) {
                                        var22 = var29.getMaxStackSize() - itemstack1.count;
                                    }

                                    var29.a(var22);
                                    if (var29.count == 0) {
                                        playerinventory.setCarried(null);
                                    }

                                    itemstack1.count += var22;
                                } else if (var29.count <= var20.a()) {
                                    var20.set(var29);
                                    playerinventory.setCarried(itemstack1);
                                }
                            } else if (itemstack1.id == var29.id && var29.getMaxStackSize() > 1 && (!itemstack1.usesData() || itemstack1.getData() == var29.getData()) && ItemStack.equals(itemstack1, var29)) {
                                var22 = itemstack1.count;
                                if (var22 > 0 && var22 + var29.count <= var29.getMaxStackSize()) {
                                    var29.count += var22;
                                    itemstack1 = var20.a(var22);
                                    if (itemstack1.count == 0) {
                                        var20.set(null);
                                    }

                                    var20.a(entityhuman, playerinventory.getCarried());
                                }
                            }
                        }

                        var20.e();
                    }
                }
            } else if (k == 2 && j >= 0 && j < 9) {
                var20 = this.c.get(i);
                if (var20.a(entityhuman)) {
                    itemstack1 = playerinventory.getItem(j);
                    boolean var26 = itemstack1 == null || var20.inventory == playerinventory && var20.isAllowed(itemstack1);
                    var22 = -1;
                    if (!var26) {
                        var22 = playerinventory.j();
                        var26 |= var22 > -1;
                    }

                    if (var20.d() && var26) {
                        var23 = var20.getItem();
                        playerinventory.setItem(j, var23.cloneItemStack());
                        if ((var20.inventory != playerinventory || !var20.isAllowed(itemstack1)) && itemstack1 != null) {
                            if (var22 > -1) {
                                playerinventory.pickup(itemstack1);
                                var20.a(var23.count);
                                var20.set(null);
                                var20.a(entityhuman, var23);
                            }
                        } else {
                            var20.a(var23.count);
                            var20.set(itemstack1);
                            var20.a(entityhuman, var23);
                        }
                    } else if (!var20.d() && itemstack1 != null && var20.isAllowed(itemstack1)) {
                        playerinventory.setItem(j, null);
                        var20.set(itemstack1);
                    }
                }
            } else if (k == 3 && entityhuman.abilities.canInstantlyBuild && playerinventory.getCarried() == null && i >= 0) {
                var20 = this.c.get(i);
                if (var20 != null && var20.d()) {
                    itemstack1 = var20.getItem().cloneItemStack();
                    itemstack1.count = itemstack1.getMaxStackSize();
                    playerinventory.setCarried(itemstack1);
                }
            } else if (k == 4 && playerinventory.getCarried() == null && i >= 0) {
                var20 = this.c.get(i);
                if (var20 != null && var20.d() && var20.a(entityhuman)) {
                    itemstack1 = var20.a(j == 0 ? 1 : var20.getItem().count);
                    var20.a(entityhuman, itemstack1);
                    entityhuman.drop(itemstack1);
                }
            } else if (k == 6 && i >= 0) {
                var20 = this.c.get(i);
                itemstack1 = playerinventory.getCarried();
                if (itemstack1 != null && (var20 == null || !var20.d() || !var20.a(entityhuman))) {
                    l = j == 0 ? 0 : this.c.size() - 1;
                    var22 = j == 0 ? 1 : -1;

                    for (int var25 = 0; var25 < 2; ++var25) {
                        for (int var28 = l; var28 >= 0 && var28 < this.c.size() && itemstack1.count < itemstack1.getMaxStackSize(); var28 += var22) {
                            Slot var31 = this.c.get(var28);
                            if (var31.d() && a(var31, itemstack1, true) && var31.a(entityhuman) && this.a(itemstack1, var31) && (var25 != 0 || var31.getItem().count != var31.getItem().getMaxStackSize())) {
                                int var33 = Math.min(itemstack1.getMaxStackSize() - itemstack1.count, var31.getItem().count);
                                ItemStack var35 = var31.a(var33);
                                itemstack1.count += var33;
                                if (var35.count <= 0) {
                                    var31.set(null);
                                }

                                var31.a(entityhuman, var35);
                            }
                        }
                    }
                }

                this.b();
            }
        }

        return itemstack;
    }

    public boolean a(ItemStack itemstack, Slot slot) {
        return true;
    }

    protected void a(int i, int j, boolean flag, EntityHuman entityhuman) {
        this.clickItem(i, j, 1, entityhuman);
    }

    public void b(EntityHuman entityhuman) {
        PlayerInventory playerinventory = entityhuman.inventory;
        if (playerinventory.getCarried() != null) {
            entityhuman.drop(playerinventory.getCarried());
            playerinventory.setCarried(null);
        }

    }

    public void a(IInventory iinventory) {
        this.b();
    }

    public void setItem(int i, ItemStack itemstack) {
        this.getSlot(i).set(itemstack);
    }

    public boolean c(EntityHuman entityhuman) {
        return !this.i.contains(entityhuman);
    }

    public void a(EntityHuman entityhuman, boolean flag) {
        if (flag) {
            this.i.remove(entityhuman);
        } else {
            this.i.add(entityhuman);
        }

    }

    public abstract boolean a(EntityHuman var1);

    protected boolean a(ItemStack itemstack, int i, int j, boolean flag) {
        boolean flag1 = false;
        int k = i;
        if (flag) {
            k = j - 1;
        }

        Slot slot;
        ItemStack itemstack1;
        if (itemstack.isStackable()) {
            while (itemstack.count > 0 && (!flag && k < j || flag && k >= i)) {
                slot = this.c.get(k);
                itemstack1 = slot.getItem();
                if (itemstack1 != null && itemstack1.id == itemstack.id && (!itemstack.usesData() || itemstack.getData() == itemstack1.getData()) && ItemStack.equals(itemstack, itemstack1)) {
                    int l = itemstack1.count + itemstack.count;
                    if (l <= itemstack.getMaxStackSize()) {
                        itemstack.count = 0;
                        itemstack1.count = l;
                        slot.e();
                        flag1 = true;
                    } else if (itemstack1.count < itemstack.getMaxStackSize()) {
                        itemstack.count -= itemstack.getMaxStackSize() - itemstack1.count;
                        itemstack1.count = itemstack.getMaxStackSize();
                        slot.e();
                        flag1 = true;
                    }
                }

                if (flag) {
                    --k;
                } else {
                    ++k;
                }
            }
        }

        if (itemstack.count > 0) {
            if (flag) {
                k = j - 1;
            } else {
                k = i;
            }

            while (!flag && k < j || flag && k >= i) {
                slot = this.c.get(k);
                itemstack1 = slot.getItem();
                if (itemstack1 == null) {
                    slot.set(itemstack.cloneItemStack());
                    slot.e();
                    itemstack.count = 0;
                    flag1 = true;
                    break;
                }

                if (flag) {
                    --k;
                } else {
                    ++k;
                }
            }
        }

        return flag1;
    }

    protected void d() {
        this.g = 0;
        this.h.clear();
    }

    public boolean b(Slot slot) {
        return true;
    }
}
