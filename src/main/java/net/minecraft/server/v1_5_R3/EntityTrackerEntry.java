package net.minecraft.server.v1_5_R3;

/**
 * Created by EntryPoint on 2016-12-29.
 */

import org.bukkit.craftbukkit.v1_5_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerVelocityEvent;
import org.bukkit.util.Vector;

import java.util.*;

public class EntityTrackerEntry {
    public Entity tracker;
    public int b;
    public int c;
    public int xLoc;
    public int yLoc;
    public int zLoc;
    public int yRot;
    public int xRot;
    public int i;
    public double j;
    public double k;
    public double l;
    public int m = 0;
    public boolean n = false;
    public Set trackedPlayers = new HashSet();
    private double p;
    private double q;
    private double r;
    private boolean s = false;
    private boolean isMoving;
    private int u = 0;
    private Entity v;
    private boolean w = false;

    public EntityTrackerEntry(Entity entity, int i, int j, boolean flag) {
        this.tracker = entity;
        this.b = i;
        this.c = j;
        this.isMoving = flag;
        this.xLoc = MathHelper.floor(entity.locX * 32.0D);
        this.yLoc = MathHelper.floor(entity.locY * 32.0D);
        this.zLoc = MathHelper.floor(entity.locZ * 32.0D);
        this.yRot = MathHelper.d(entity.yaw * 256.0F / 360.0F);
        this.xRot = MathHelper.d(entity.pitch * 256.0F / 360.0F);
        this.i = MathHelper.d(entity.getHeadRotation() * 256.0F / 360.0F);
    }

    public boolean equals(Object object) {
        return object instanceof EntityTrackerEntry && ((EntityTrackerEntry) object).tracker.id == this.tracker.id;
    }

    public int hashCode() {
        return this.tracker.id;
    }

    public void track(List list) {
        this.n = false;
        if (!this.s || this.tracker.e(this.p, this.q, this.r) > 16.0D) {
            this.p = this.tracker.locX;
            this.q = this.tracker.locY;
            this.r = this.tracker.locZ;
            this.s = true;
            this.n = true;
            this.scanPlayers(list);
        }

        if (this.v != this.tracker.vehicle) {
            this.v = this.tracker.vehicle;
            this.broadcast(new Packet39AttachEntity(this.tracker, this.tracker.vehicle));
        }

        if (this.tracker instanceof EntityItemFrame) {
            EntityItemFrame cancelled = (EntityItemFrame) this.tracker;
            ItemStack player = cancelled.i();
            if (this.m % 10 == 0 && player != null && player.getItem() instanceof ItemWorldMap) {
                WorldMap velocity = Item.MAP.getSavedMap(player, this.tracker.world);
                Iterator event = this.trackedPlayers.iterator();

                while (event.hasNext()) {
                    EntityHuman i1 = (EntityHuman) event.next();
                    EntityPlayer j1 = (EntityPlayer) i1;
                    velocity.a(j1, player);
                    if (j1.playerConnection.lowPriorityCount() <= 5) {
                        Packet k1 = Item.MAP.c(player, this.tracker.world, j1);
                        if (k1 != null) {
                            j1.playerConnection.sendPacket(k1);
                        }
                    }
                }
            }

            DataWatcher var28 = this.tracker.getDataWatcher();
            if (var28.a()) {
                this.broadcastIncludingSelf(new Packet40EntityMetadata(this.tracker.id, var28, false));
            }
        } else if (this.m % this.c == 0 || this.tracker.an || this.tracker.getDataWatcher().a()) {
            int var24;
            int var25;
            if (this.tracker.vehicle == null) {
                ++this.u;
                var24 = this.tracker.at.a(this.tracker.locX);
                var25 = MathHelper.floor(this.tracker.locY * 32.0D);
                int var30 = this.tracker.at.a(this.tracker.locZ);
                int var33 = MathHelper.d(this.tracker.yaw * 256.0F / 360.0F);
                int var35 = MathHelper.d(this.tracker.pitch * 256.0F / 360.0F);
                int var36 = var24 - this.xLoc;
                int var37 = var25 - this.yLoc;
                int l1 = var30 - this.zLoc;
                Object object = null;
                boolean flag = Math.abs(var36) >= 4 || Math.abs(var37) >= 4 || Math.abs(l1) >= 4 || this.m % 60 == 0;
                boolean flag1 = Math.abs(var33 - this.yRot) >= 4 || Math.abs(var35 - this.xRot) >= 4;
                if (flag) {
                    this.xLoc = var24;
                    this.yLoc = var25;
                    this.zLoc = var30;
                }

                if (flag1) {
                    this.yRot = var33;
                    this.xRot = var35;
                }

                if (this.m > 0 || this.tracker instanceof EntityArrow) {
                    if (var36 >= -128 && var36 < 128 && var37 >= -128 && var37 < 128 && l1 >= -128 && l1 < 128 && this.u <= 400 && !this.w) {
                        if (flag && flag1) {
                            object = new Packet33RelEntityMoveLook(this.tracker.id, (byte) var36, (byte) var37, (byte) l1, (byte) var33, (byte) var35);
                        } else if (flag) {
                            object = new Packet31RelEntityMove(this.tracker.id, (byte) var36, (byte) var37, (byte) l1);
                        } else if (flag1) {
                            object = new Packet32EntityLook(this.tracker.id, (byte) var33, (byte) var35);
                        }
                    } else {
                        this.u = 0;
                        if (this.tracker instanceof EntityPlayer) {
                            this.scanPlayers(new ArrayList(this.trackedPlayers));
                        }

                        object = new Packet34EntityTeleport(this.tracker.id, var24, var25, var30, (byte) var33, (byte) var35);
                    }
                }

                if (this.isMoving) {
                    double d0 = this.tracker.motX - this.j;
                    double d1 = this.tracker.motY - this.k;
                    double d2 = this.tracker.motZ - this.l;
                    double d3 = 0.02D;
                    double d4 = d0 * d0 + d1 * d1 + d2 * d2;
                    if (d4 > d3 * d3 || d4 > 0.0D && this.tracker.motX == 0.0D && this.tracker.motY == 0.0D && this.tracker.motZ == 0.0D) {
                        this.j = this.tracker.motX;
                        this.k = this.tracker.motY;
                        this.l = this.tracker.motZ;
                        this.broadcast(new Packet28EntityVelocity(this.tracker.id, this.j, this.k, this.l));
                    }
                }

                if (object != null) {
                    this.broadcast((Packet) object);
                }

                DataWatcher datawatcher1 = this.tracker.getDataWatcher();
                if (datawatcher1.a()) {
                    this.broadcastIncludingSelf(new Packet40EntityMetadata(this.tracker.id, datawatcher1, false));
                }

                this.w = false;
            } else {
                var24 = MathHelper.d(this.tracker.yaw * 256.0F / 360.0F);
                var25 = MathHelper.d(this.tracker.pitch * 256.0F / 360.0F);
                boolean var29 = Math.abs(var24 - this.yRot) >= 4 || Math.abs(var25 - this.xRot) >= 4;
                if (var29) {
                    this.broadcast(new Packet32EntityLook(this.tracker.id, (byte) var24, (byte) var25));
                    this.yRot = var24;
                    this.xRot = var25;
                }

                this.xLoc = this.tracker.at.a(this.tracker.locX);
                this.yLoc = MathHelper.floor(this.tracker.locY * 32.0D);
                this.zLoc = this.tracker.at.a(this.tracker.locZ);
                DataWatcher var31 = this.tracker.getDataWatcher();
                if (var31.a()) {
                    this.broadcastIncludingSelf(new Packet40EntityMetadata(this.tracker.id, var31, false));
                }

                this.w = true;
            }

            var24 = MathHelper.d(this.tracker.getHeadRotation() * 256.0F / 360.0F);
            if (Math.abs(var24 - this.i) >= 4) {
                this.broadcast(new Packet35EntityHeadRotation(this.tracker.id, (byte) var24));
                this.i = var24;
            }

            this.tracker.an = false;
        }

        ++this.m;
        if (this.tracker.velocityChanged) {
            boolean var26 = false;
            if (this.tracker instanceof EntityPlayer) {
                Player var27 = (Player) this.tracker.getBukkitEntity();
                Vector var32 = var27.getVelocity();
                PlayerVelocityEvent var34 = new PlayerVelocityEvent(var27, var32);
                this.tracker.world.getServer().getPluginManager().callEvent(var34);
                if (var34.isCancelled()) {
                    var26 = true;
                } else if (!var32.equals(var34.getVelocity())) {
                    var27.setVelocity(var32);
                }
            }

            if (!var26) {
                this.broadcastIncludingSelf(new Packet28EntityVelocity(this.tracker));
            }

            this.tracker.velocityChanged = false;
        }

    }

    public void broadcast(Packet packet) {
        Iterator iterator = this.trackedPlayers.iterator();

        while (iterator.hasNext()) {
            EntityPlayer entityplayer = (EntityPlayer) iterator.next();
            entityplayer.playerConnection.sendPacket(packet);
        }

    }

    public void broadcastIncludingSelf(Packet packet) {
        this.broadcast(packet);
        if (this.tracker instanceof EntityPlayer) {
            ((EntityPlayer) this.tracker).playerConnection.sendPacket(packet);
        }

    }

    public void a() {
        Iterator iterator = this.trackedPlayers.iterator();

        while (iterator.hasNext()) {
            EntityPlayer entityplayer = (EntityPlayer) iterator.next();
            entityplayer.removeQueue.add(Integer.valueOf(this.tracker.id));
        }

    }

    public void a(EntityPlayer entityplayer) {
        if (this.trackedPlayers.contains(entityplayer)) {
            entityplayer.removeQueue.add(Integer.valueOf(this.tracker.id));
            this.trackedPlayers.remove(entityplayer);
        }

    }

    public void updatePlayer(EntityPlayer entityplayer) {
        if (Thread.currentThread() != MinecraftServer.getServer().primaryThread) {
            throw new IllegalStateException("Asynchronous player tracker update!");
        } else {
            if (entityplayer != this.tracker) {
                double d0 = entityplayer.locX - (double) (this.xLoc / 32);
                double d1 = entityplayer.locZ - (double) (this.zLoc / 32);
                if (d0 >= (double) (-this.b) && d0 <= (double) this.b && d1 >= (double) (-this.b) && d1 <= (double) this.b) {
                    if (!this.trackedPlayers.contains(entityplayer) && (this.d(entityplayer) || this.tracker.p)) {
                        if (this.tracker instanceof EntityPlayer) {
                            CraftPlayer packet = ((EntityPlayer) this.tracker).getBukkitEntity();
                            if (!entityplayer.getBukkitEntity().canSee(packet)) {
                                return;
                            }
                        }

                        entityplayer.removeQueue.remove(this.tracker.id);
                        this.trackedPlayers.add(entityplayer);
                        Packet var10 = this.b();
                        entityplayer.playerConnection.sendPacket(var10);
                        if (!this.tracker.getDataWatcher().d()) {
                            entityplayer.playerConnection.sendPacket(new Packet40EntityMetadata(this.tracker.id, this.tracker.getDataWatcher(), true));
                        }

                        this.j = this.tracker.motX;
                        this.k = this.tracker.motY;
                        this.l = this.tracker.motZ;
                        if (this.isMoving && !(var10 instanceof Packet24MobSpawn)) {
                            entityplayer.playerConnection.sendPacket(new Packet28EntityVelocity(this.tracker.id, this.tracker.motX, this.tracker.motY, this.tracker.motZ));
                        }

                        if (this.tracker.vehicle != null && this.tracker.id > this.tracker.vehicle.id) {
                            entityplayer.playerConnection.sendPacket(new Packet39AttachEntity(this.tracker, this.tracker.vehicle));
                        } else if (this.tracker.passenger != null && this.tracker.id > this.tracker.passenger.id) {
                            entityplayer.playerConnection.sendPacket(new Packet39AttachEntity(this.tracker.passenger, this.tracker));
                        }

                        if (this.tracker instanceof EntityLiving) {
                            for (int entityliving = 0; entityliving < 5; ++entityliving) {
                                ItemStack iterator = ((EntityLiving) this.tracker).getEquipment(entityliving);
                                if (iterator != null) {
                                    entityplayer.playerConnection.sendPacket(new Packet5EntityEquipment(this.tracker.id, entityliving, iterator));
                                }
                            }
                        }

                        if (this.tracker instanceof EntityHuman) {
                            EntityHuman var11 = (EntityHuman) this.tracker;
                            if (var11.isSleeping()) {
                                entityplayer.playerConnection.sendPacket(new Packet17EntityLocationAction(this.tracker, 0, MathHelper.floor(this.tracker.locX), MathHelper.floor(this.tracker.locY), MathHelper.floor(this.tracker.locZ)));
                            }
                        }

                        this.i = MathHelper.d(this.tracker.getHeadRotation() * 256.0F / 360.0F);
                        this.broadcast(new Packet35EntityHeadRotation(this.tracker.id, (byte) this.i));
                        if (this.tracker instanceof EntityLiving) {
                            EntityLiving var12 = (EntityLiving) this.tracker;
                            Iterator var13 = var12.getEffects().iterator();

                            while (var13.hasNext()) {
                                MobEffect mobeffect = (MobEffect) var13.next();
                                entityplayer.playerConnection.sendPacket(new Packet41MobEffect(this.tracker.id, mobeffect));
                            }
                        }
                    }
                } else if (this.trackedPlayers.contains(entityplayer)) {
                    this.trackedPlayers.remove(entityplayer);
                    entityplayer.removeQueue.add(this.tracker.id);
                }
            }

        }
    }

    private boolean d(EntityPlayer entityplayer) {
        return entityplayer.o().getPlayerChunkMap().a(entityplayer, this.tracker.aj, this.tracker.al);
    }

    public void scanPlayers(List list) {
        for (Object aList : list) {
            this.updatePlayer((EntityPlayer) aList);
        }

    }

    private Packet b() {
        if (this.tracker.dead) {
            return null;
        } else if (this.tracker instanceof EntityItem) {
            return new Packet23VehicleSpawn(this.tracker, 2, 1);
        } else if (this.tracker instanceof EntityPlayer) {
            return new Packet20NamedEntitySpawn((EntityHuman) this.tracker);
        } else if (this.tracker instanceof EntityMinecartAbstract) {
            EntityMinecartAbstract packet23vehiclespawn3 = (EntityMinecartAbstract) this.tracker;
            return new Packet23VehicleSpawn(this.tracker, 10, packet23vehiclespawn3.getType());
        } else if (this.tracker instanceof EntityBoat) {
            return new Packet23VehicleSpawn(this.tracker, 1);
        } else if (!(this.tracker instanceof IAnimal) && !(this.tracker instanceof EntityEnderDragon)) {
            if (this.tracker instanceof EntityFishingHook) {
                EntityHuman packet23vehiclespawn2 = ((EntityFishingHook) this.tracker).owner;
                return new Packet23VehicleSpawn(this.tracker, 90, packet23vehiclespawn2 != null ? packet23vehiclespawn2.id : this.tracker.id);
            } else if (this.tracker instanceof EntityArrow) {
                Entity packet23vehiclespawn1 = ((EntityArrow) this.tracker).shooter;
                return new Packet23VehicleSpawn(this.tracker, 60, packet23vehiclespawn1 != null ? packet23vehiclespawn1.id : this.tracker.id);
            } else if (this.tracker instanceof EntitySnowball) {
                return new Packet23VehicleSpawn(this.tracker, 61);
            } else if (this.tracker instanceof EntityPotion) {
                return new Packet23VehicleSpawn(this.tracker, 73, ((EntityPotion) this.tracker).getPotionValue());
            } else if (this.tracker instanceof EntityThrownExpBottle) {
                return new Packet23VehicleSpawn(this.tracker, 75);
            } else if (this.tracker instanceof EntityEnderPearl) {
                return new Packet23VehicleSpawn(this.tracker, 65);
            } else if (this.tracker instanceof EntityEnderSignal) {
                return new Packet23VehicleSpawn(this.tracker, 72);
            } else if (this.tracker instanceof EntityFireworks) {
                return new Packet23VehicleSpawn(this.tracker, 76);
            } else {
                Packet23VehicleSpawn packet23vehiclespawn;
                if (this.tracker instanceof EntityFireball) {
                    EntityFireball entityitemframe2 = (EntityFireball) this.tracker;
                    packet23vehiclespawn = null;
                    byte b0 = 63;
                    if (this.tracker instanceof EntitySmallFireball) {
                        b0 = 64;
                    } else if (this.tracker instanceof EntityWitherSkull) {
                        b0 = 66;
                    }

                    if (entityitemframe2.shooter != null) {
                        packet23vehiclespawn = new Packet23VehicleSpawn(this.tracker, b0, ((EntityFireball) this.tracker).shooter.id);
                    } else {
                        packet23vehiclespawn = new Packet23VehicleSpawn(this.tracker, b0, 0);
                    }

                    packet23vehiclespawn.e = (int) (entityitemframe2.dirX * 8000.0D);
                    packet23vehiclespawn.f = (int) (entityitemframe2.dirY * 8000.0D);
                    packet23vehiclespawn.g = (int) (entityitemframe2.dirZ * 8000.0D);
                    return packet23vehiclespawn;
                } else if (this.tracker instanceof EntityEgg) {
                    return new Packet23VehicleSpawn(this.tracker, 62);
                } else if (this.tracker instanceof EntityTNTPrimed) {
                    return new Packet23VehicleSpawn(this.tracker, 50);
                } else if (this.tracker instanceof EntityEnderCrystal) {
                    return new Packet23VehicleSpawn(this.tracker, 51);
                } else if (this.tracker instanceof EntityFallingBlock) {
                    EntityFallingBlock entityitemframe1 = (EntityFallingBlock) this.tracker;
                    return new Packet23VehicleSpawn(this.tracker, 70, entityitemframe1.id | entityitemframe1.data << 16);
                } else if (this.tracker instanceof EntityPainting) {
                    return new Packet25EntityPainting((EntityPainting) this.tracker);
                } else if (this.tracker instanceof EntityItemFrame) {
                    EntityItemFrame entityitemframe = (EntityItemFrame) this.tracker;
                    packet23vehiclespawn = new Packet23VehicleSpawn(this.tracker, 71, entityitemframe.direction);
                    packet23vehiclespawn.b = MathHelper.d((float) (entityitemframe.x * 32));
                    packet23vehiclespawn.c = MathHelper.d((float) (entityitemframe.y * 32));
                    packet23vehiclespawn.d = MathHelper.d((float) (entityitemframe.z * 32));
                    return packet23vehiclespawn;
                } else if (this.tracker instanceof EntityExperienceOrb) {
                    return new Packet26AddExpOrb((EntityExperienceOrb) this.tracker);
                } else {
                    throw new IllegalArgumentException("Don\'t know how to add " + this.tracker.getClass() + "!");
                }
            }
        } else {
            this.i = MathHelper.d(this.tracker.getHeadRotation() * 256.0F / 360.0F);
            return new Packet24MobSpawn((EntityLiving) this.tracker);
        }
    }

    public void clear(EntityPlayer entityplayer) {
        if (Thread.currentThread() != MinecraftServer.getServer().primaryThread) {
            throw new IllegalStateException("Asynchronous player tracker clear!");
        } else {
            if (this.trackedPlayers.contains(entityplayer)) {
                this.trackedPlayers.remove(entityplayer);
                entityplayer.removeQueue.add(Integer.valueOf(this.tracker.id));
            }

        }
    }
}
