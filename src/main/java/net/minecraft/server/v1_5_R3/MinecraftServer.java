package net.minecraft.server.v1_5_R3;

import com.google.common.io.Files;
import org.bukkit.World.Environment;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.RemoteConsoleCommandSender;
import org.bukkit.craftbukkit.Main;
import org.bukkit.craftbukkit.libs.jline.console.ConsoleReader;
import org.bukkit.craftbukkit.libs.joptsimple.OptionSet;
import org.bukkit.craftbukkit.v1_5_R3.CraftServer;
import org.bukkit.craftbukkit.v1_5_R3.SpigotTimings;
import org.bukkit.craftbukkit.v1_5_R3.chunkio.ChunkIOExecutor;
import org.bukkit.craftbukkit.v1_5_R3.scoreboard.CraftScoreboardManager;
import org.bukkit.craftbukkit.v1_5_R3.util.ServerShutdownThread;
import org.bukkit.craftbukkit.v1_5_R3.util.Waitable;
import org.bukkit.event.server.RemoteServerCommandEvent;
import org.bukkit.event.world.WorldInitEvent;
import org.bukkit.event.world.WorldSaveEvent;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginLoadOrder;
import org.fusesource.jansi.AnsiConsole;
import org.spigotmc.CustomTimingsHandler;
import org.spigotmc.WatchdogThread;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.security.KeyPair;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class MinecraftServer implements ICommandListener, Runnable, IMojangStatistics {
    private static final int TPS = 20;
    private static final int TICK_TIME = 50000000;
    public static int currentTick = 0;
    public static double currentTPS = 0.0D;
    private static MinecraftServer k = null;
    private static long catchupTime = 0L;

    public final MethodProfiler methodProfiler = new MethodProfiler();
    public final long[] e = new long[100];
    public final long[] f = new long[100];
    public final long[] g = new long[100];
    public final long[] h = new long[100];
    public final long[] i = new long[100];
    public final Thread primaryThread;
    private final MojangStatisticsGenerator m = new MojangStatisticsGenerator("server", this);
    private final List<IUpdatePlayerListBox> o = new ArrayList<>();
    private final ICommandHandler p;
    public Convertable convertable;
    public File universe;
    public WorldServer[] worldServer;
    public String c;
    public int d;
    public long[][] j;
    public List<WorldServer> worlds = new ArrayList<>();
    public CraftServer server;
    public OptionSet options;
    public ConsoleCommandSender console;
    public RemoteConsoleCommandSender remoteConsole;
    public ConsoleReader reader;
    public Queue<Runnable> processQueue = new CachedSizeConcurrentLinkedQueue<>();
    public int ticks = 0;
    public boolean serverAutoSave = false;
    //    public int autosavePeriod;
    private String serverIp;
    private int r = -1;
    private PlayerList s;
    private boolean isRunning = true;
    private boolean isStopped = false;
    private boolean onlineMode;
    private boolean spawnAnimals;
    private boolean spawnNPCs;
    private boolean pvpMode;
    private boolean allowFlight;
    private String motd;
    private int C;
    private long D;
    private long E;
    private long F;
    private long G;
    private KeyPair H;
    private String I;
    private String J;
    private boolean demoMode;
    private boolean M;
    private boolean N;
    private String O = "";
    private boolean P = false;
    private long Q;
    private String R;
    private boolean S;
    private boolean T = false;

    public MinecraftServer(OptionSet options) {
        k = this;
        this.p = new CommandDispatcher();
        this.an();
        this.options = options;

        try {
            this.reader = new ConsoleReader(System.in, System.out);
            this.reader.setExpandEvents(false);
        } catch (Exception var5) {
            try {
                System.setProperty("org.bukkit.craftbukkit.libs.jline.terminal", "org.bukkit.craftbukkit.libs.jline.UnsupportedTerminal");
                System.setProperty("user.language", "en");
                Main.useJline = false;
                this.reader = new ConsoleReader(System.in, System.out);
                this.reader.setExpandEvents(false);
            } catch (IOException var4) {
                Logger.getLogger(MinecraftServer.class.getName()).log(Level.SEVERE, null, var4);
            }
        }
        Runtime.getRuntime().addShutdownHook(new ServerShutdownThread(this));
        this.primaryThread = new ThreadServerApplication(this, "Server thread");
    }

    public static void main(OptionSet options) {
        if (Main.useJline) {
            AnsiConsole.systemInstall();
        }

        StatisticList.a();
        IConsoleLogManager iconsolelogmanager = null;

        try {
            DedicatedServer exception = new DedicatedServer(options);
            iconsolelogmanager = exception.getLogger();
            if (options.has("port")) {
                int port = ((Integer) options.valueOf("port"));
                if (port > 0) {
                    exception.setPort(port);
                }
            }

            if (options.has("universe")) {
                exception.universe = (File) options.valueOf("universe");
            }

            if (options.has("world")) {
                exception.l((String) options.valueOf("world"));
            }

            exception.primaryThread.start();
        } catch (Exception var4) {
            if (iconsolelogmanager != null) {
                iconsolelogmanager.severe("Failed to start the minecraft server", var4);
            } else {
                Logger.getAnonymousLogger().log(Level.SEVERE, "Failed to start the minecraft server", var4);
            }
        }

    }

    public static MinecraftServer getServer() {
        return k;
    }

    public static PlayerList a(MinecraftServer minecraftserver) {
        return minecraftserver.s;
    }

    public abstract PropertyManager getPropertyManager();

    private void an() {
        DispenserRegistry.a();
    }

    protected abstract boolean init() throws UnknownHostException;

    protected void b(String s) {
        if (this.getConvertable().isConvertable(s)) {
            this.getLogger().info("Converting map!");
            this.c("menu.convertingLevel");
            this.getConvertable().convert(s, new ConvertProgressUpdater(this));
        }

    }

    protected synchronized void c(String s) {
        this.R = s;
    }

    protected void a(String s, String s1, long i, WorldType worldtype, String s2) {
        this.b(s);
        this.c("menu.loadingLevel");
        this.worldServer = new WorldServer[3];
        IDataManager idatamanager = this.convertable.a(s, true);
        WorldData worlddata = idatamanager.getWorldData();
        byte worldCount = 3;

        for (int j = 0; j < worldCount; ++j) {
            byte dimension = 0;
            if (j == 1) {
                if (!this.getAllowNether()) {
                    continue;
                }

                dimension = -1;
            }

            if (j == 2) {
                if (!this.server.getAllowEnd()) {
                    continue;
                }

                dimension = 1;
            }

            String worldType = Environment.getEnvironment(dimension).toString().toLowerCase();
            String name = dimension == 0 ? s : s + "_" + worldType;
            ChunkGenerator gen = this.server.getGenerator(name);
            WorldSettings worldsettings = new WorldSettings(i, this.getGamemode(), this.getGenerateStructures(), this.isHardcore(), worldtype);
            worldsettings.a(s2);
            WorldServer world;
            if (j == 0) {
                if (this.M()) {
                    world = new DemoWorldServer(this, new ServerNBTManager(this.server.getWorldContainer(), s1, true), s1, dimension, this.methodProfiler, this.getLogger());
                } else {
                    world = new WorldServer(this, new ServerNBTManager(this.server.getWorldContainer(), s1, true), s1, dimension, worldsettings, this.methodProfiler, this.getLogger(), Environment.getEnvironment(dimension), gen);
                }
            } else {
                String dim = "DIM" + dimension;
                File newWorld = new File(new File(name), dim);
                File oldWorld = new File(new File(s), dim);
                if (!newWorld.isDirectory() && oldWorld.isDirectory()) {
                    IConsoleLogManager log = this.getLogger();
                    log.info("---- Migration of old " + worldType + " folder required ----");
                    log.info("Unfortunately due to the way that Minecraft implemented multiworld support in 1.6, Bukkit requires that you move your " + worldType + " folder to a new location in order to operate correctly.");
                    log.info("We will move this folder for you, but it will mean that you need to move it back should you wish to stop using Bukkit in the future.");
                    log.info("Attempting to move " + oldWorld + " to " + newWorld + "...");
                    if (newWorld.exists()) {
                        log.severe("A file or folder already exists at " + newWorld + "!");
                        log.info("---- Migration of old " + worldType + " folder failed ----");
                    } else if (newWorld.getParentFile().mkdirs()) {
                        if (oldWorld.renameTo(newWorld)) {
                            log.info("Success! To restore " + worldType + " in the future, simply move " + newWorld + " to " + oldWorld);

                            try {
                                Files.copy(new File(new File(s), "level.dat"), new File(new File(name), "level.dat"));
                            } catch (IOException var22) {
                                log.severe("Unable to migrate world data.");
                            }

                            log.info("---- Migration of old " + worldType + " folder complete ----");
                        } else {
                            log.severe("Could not move folder " + oldWorld + " to " + newWorld + "!");
                            log.info("---- Migration of old " + worldType + " folder failed ----");
                        }
                    } else {
                        log.severe("Could not create path for " + newWorld + "!");
                        log.info("---- Migration of old " + worldType + " folder failed ----");
                    }
                }

                this.c(name);
                world = new SecondaryWorldServer(this, new ServerNBTManager(this.server.getWorldContainer(), name, true), name, dimension, worldsettings, this.worlds.get(0), this.methodProfiler, this.getLogger(), Environment.getEnvironment(dimension), gen);
            }

            if (gen != null) {
                world.getWorld().getPopulators().addAll(gen.getDefaultPopulators(world.getWorld()));
            }

            this.server.scoreboardManager = new CraftScoreboardManager(this, world.getScoreboard());
            this.server.getPluginManager().callEvent(new WorldInitEvent(world.getWorld()));
            world.addIWorldAccess(new WorldManager(this, world));
            if (!this.I()) {
                world.getWorldData().setGameType(this.getGamemode());
            }

            this.worlds.add(world);
            this.s.setPlayerFileData(this.worlds.toArray(new WorldServer[this.worlds.size()]));
        }

        this.c(this.getDifficulty());
        this.e();
    }

    protected void e() {
        long i = System.currentTimeMillis();
        this.c("menu.generatingTerrain");
        boolean b0 = false;

        for (int j = 0; j < this.worlds.size(); ++j) {
            WorldServer worldserver = this.worlds.get(j);
            this.getLogger().info("월드의 " + j + " 영역 시작 준비중 (시드: " + worldserver.getSeed() + ")");
            if (worldserver.getWorld().getKeepSpawnInMemory()) {
                ChunkCoordinates chunkcoordinates = worldserver.getSpawn();

                for (int k = -192; k <= 192 && this.isRunning(); k += 16) {
                    for (int l = -192; l <= 192 && this.isRunning(); l += 16) {
                        long i1 = System.currentTimeMillis();
                        if (i1 < i) {
                            i = i1;
                        }

                        if (i1 > i + 1000L) {
                            int j1 = 148225;
                            int k1 = (k + 192) * 385 + l + 1;
                            this.a_("스폰 영역 준비중", k1 * 100 / j1);
                            i = i1;
                        }

                        worldserver.chunkProviderServer.getChunkAt(chunkcoordinates.x + k >> 4, chunkcoordinates.z + l >> 4);
                    }
                }
            }
        }

        this.j();
    }

    public abstract boolean getGenerateStructures();

    public abstract EnumGamemode getGamemode();

    public abstract int getDifficulty();

    public abstract boolean isHardcore();

    protected void a_(String s, int i) {
        this.c = s;
        this.d = i;
        this.getLogger().info(s + ": " + i + "%");
    }

    protected void j() {
        this.c = null;
        this.d = 0;
        this.server.enablePlugins(PluginLoadOrder.POSTWORLD);
    }

    protected void saveChunks(boolean flag) throws ExceptionWorldConflict {
        if (!this.N) {
            for (int j = 0; j < this.worlds.size(); ++j) {
                WorldServer worldserver = this.worlds.get(j);
                if (worldserver != null) {
                    if (!flag) {
                        this.getLogger().info("Saving chunks for level \'" + worldserver.getWorldData().getName() + "\'/" + worldserver.worldProvider.getName());
                    }

                    worldserver.save(true, null);
                    worldserver.saveLevel();
                    if (flag)
                        this.server.getPluginManager().callEvent(
                                new WorldSaveEvent(worldserver.getWorld()));
                }
            }
        }

    }

    public void stop() throws ExceptionWorldConflict {
        if (!this.N) {
            this.getLogger().info("서버가 중지됩니다.");
            if (this.server != null) {
                this.server.disablePlugins();
            }

            if (this.ae() != null) {
                this.ae().a();
            }

            if (this.s != null) {
                this.getLogger().info("플레이어 저장 중..");
                this.s.savePlayers();
                this.s.r();
            }
            this.getLogger().info("월드 저장 중..");
            this.saveChunks(false);
            if (this.m != null && this.m.d()) {
                this.m.e();
            }
        }
    }

    public String getServerIp() {
        return this.serverIp;
    }

    public void d(String s) {
        this.serverIp = s;
    }

    public boolean isRunning() {
        return this.isRunning;
    }

    public void safeShutdown() {
        this.isRunning = false;
    }

    public void run() {
        try {
            if (this.init()) {
                for (long lastTick = 0L; this.isRunning; this.P = true) {
                    long curTime = System.nanoTime();
                    long wait = 50000000L - (curTime - lastTick) - catchupTime;
                    if (wait > 0L) {
                        Thread.sleep(wait / 1000000L);
                        catchupTime = 0L;
                    } else {
                        catchupTime = Math.min(1000000000L, Math.abs(wait));
                        currentTPS = currentTPS * 0.95D + 1.0E9D / (double) (curTime - lastTick) * 0.05D;
                        lastTick = curTime;
                        ++currentTick;
                        SpigotTimings.serverTickTimer.startTiming();
                        this.q();
                        SpigotTimings.serverTickTimer.stopTiming();
                        CustomTimingsHandler.tick();
                        WatchdogThread.tick();
                    }
                }
            } else {
                this.a((CrashReport) null);
            }
        } catch (Throwable var77) {
            var77.printStackTrace();
            this.getLogger().severe("예상치 못한 예외가 발생했습니다 " + var77.getClass().getSimpleName(), var77);
            CrashReport crashreport;
            if (var77 instanceof ReportedException) {
                crashreport = this.b(((ReportedException) var77).a());
            } else {
                crashreport = this.b(new CrashReport("서버 Tick 중의 예외 ", var77));
            }

            File file1 = new File(new File(this.o(), "crash-reports"), "crash-" + (new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss")).format(new Date()) + "-server.txt");
            if (crashreport.a(file1, this.getLogger())) {
                this.getLogger().severe("본 크래쉬 로그가 다음 경로에 저장됩니다 : " + file1.getAbsolutePath());
            } else {
                this.getLogger().severe("디스크에 크래쉬 로그를 쓸 수 없습니다.");
            }

            this.a(crashreport);
        } finally {
            try {
                WatchdogThread.doStop();
                this.stop();
                this.isStopped = true;
            } catch (Throwable var75) {
                var75.printStackTrace();
            } finally {
                try {
                    this.reader.getTerminal().restore();
                } catch (Exception var74) {
                }

                this.p();
            }

        }

    }

    protected File o() {
        return new File(".");
    }

    protected void a(CrashReport crashreport) {
    }

    protected void p() {
    }

    protected void q() throws ExceptionWorldConflict {
        long i = System.nanoTime();
        AxisAlignedBB.a().a();
        ++this.ticks;
        if (this.S) {
            this.S = false;
            this.methodProfiler.a = true;
            this.methodProfiler.a();
        }

        this.methodProfiler.a("root");
        this.r();
//        if (this.autosavePeriod > 0 && this.ticks % this.autosavePeriod == 0) {
//            this.methodProfiler.a("save");
//            this.s.savePlayers();
//            this.saveChunks(true);
//            this.methodProfiler.b();
//        }

        this.methodProfiler.a("tallying");
        this.i[this.ticks % 100] = System.nanoTime() - i;
        this.e[this.ticks % 100] = Packet.q - this.D;
        this.D = Packet.q;
        this.f[this.ticks % 100] = Packet.r - this.E;
        this.E = Packet.r;
        this.g[this.ticks % 100] = Packet.o - this.F;
        this.F = Packet.o;
        this.h[this.ticks % 100] = Packet.p - this.G;
        this.G = Packet.p;
        this.methodProfiler.b();
        this.methodProfiler.a("snooper");
        if (!this.m.d() && this.ticks > 100) {
            this.m.a();
        }

        if (this.ticks % 6000 == 0) {
            this.m.b();
        }

        this.methodProfiler.b();
        this.methodProfiler.b();
        SwiftLightingQueue.processQueue(i);
    }

    public void r() {
        this.methodProfiler.a("levels");
        SpigotTimings.schedulerTimer.startTiming();
        this.server.getScheduler().mainThreadHeartbeat(this.ticks);

        while (!this.processQueue.isEmpty()) {
            this.processQueue.remove().run();
        }

        SpigotTimings.schedulerTimer.stopTiming();
        SpigotTimings.chunkIOTickTimer.startTiming();
        ChunkIOExecutor.tick();
        SpigotTimings.chunkIOTickTimer.stopTiming();
        int i;
        if (this.ticks % 20 == 0) {
            for (i = 0; i < this.getPlayerList().players.size(); ++i) {
                EntityPlayer entityplayer = (EntityPlayer) this.getPlayerList().players.get(i);
                entityplayer.playerConnection.sendPacket(new Packet4UpdateTime(entityplayer.world.getTime(), entityplayer.getPlayerTime()));
            }
        }

        for (i = 0; i < this.worlds.size(); ++i) {
            long j = System.nanoTime();
            WorldServer worldserver = this.worlds.get(i);
            this.methodProfiler.a(worldserver.getWorldData().getName());
            this.methodProfiler.a("pools");
            worldserver.getVec3DPool().a();
            this.methodProfiler.b();
            this.methodProfiler.a("tick");

            CrashReport crashreport;
            try {
                worldserver.doTick();
            } catch (Throwable var9) {
                crashreport = CrashReport.a(var9, "월드 Ticking 중 예외");
                worldserver.a(crashreport);
                throw new ReportedException(crashreport);
            }

            try {
                worldserver.tickEntities();
            } catch (Throwable var8) {
                crashreport = CrashReport.a(var8, "월드 엔티티 Ticking 중 예외");
                worldserver.a(crashreport);
                throw new ReportedException(crashreport);
            }

            this.methodProfiler.b();
            this.methodProfiler.a("tracker");
            worldserver.timings.tracker.startTiming();
            worldserver.getTracker().updatePlayers();
            worldserver.timings.tracker.stopTiming();
            this.methodProfiler.b();
            this.methodProfiler.b();
        }

        this.methodProfiler.c("connection");
        SpigotTimings.connectionTimer.startTiming();
        this.ae().b();
        SpigotTimings.connectionTimer.stopTiming();
        this.methodProfiler.c("players");
        SpigotTimings.playerListTimer.startTiming();
        this.s.tick();
        SpigotTimings.playerListTimer.stopTiming();
        this.methodProfiler.c("tickables");
        SpigotTimings.tickablesTimer.startTiming();

        for (i = 0; i < this.o.size(); ++i) {
            this.o.get(i).a();
        }

        SpigotTimings.tickablesTimer.stopTiming();
        this.methodProfiler.b();
    }

    public boolean getAllowNether() {
        return true;
    }

    public void a(IUpdatePlayerListBox iupdateplayerlistbox) {
        this.o.add(iupdateplayerlistbox);
    }

    public void t() {
    }

    public File e(String s) {
        return new File(this.o(), s);
    }

    public void info(String s) {
        this.getLogger().info(s);
    }

    public void warning(String s) {
        this.getLogger().warning(s);
    }

    public WorldServer getWorldServer(int i) {
        Iterator i$ = this.worlds.iterator();

        WorldServer world;
        do {
            if (!i$.hasNext()) {
                return this.worlds.get(0);
            }

            world = (WorldServer) i$.next();
        } while (world.dimension != i);

        return world;
    }

    public String u() {
        return this.serverIp;
    }

    public int v() {
        return this.r;
    }

    public String w() {
        return this.motd;
    }

    public String getVersion() {
        return "1.5.2";
    }

    public int y() {
        return this.s.getPlayerCount();
    }

    public int z() {
        return this.s.getMaxPlayers();
    }

    public String[] getPlayers() {
        return this.s.d();
    }

    public String getPlugins() {
        StringBuilder result = new StringBuilder();
        Plugin[] plugins = this.server.getPluginManager().getPlugins();
        result.append(this.server.getName());
        result.append(" on Bukkit ");
        result.append(this.server.getBukkitVersion());
        if (plugins.length > 0 && this.server.getQueryPlugins()) {
            result.append(": ");

            for (int i = 0; i < plugins.length; ++i) {
                if (i > 0) {
                    result.append("; ");
                }

                result.append(plugins[i].getDescription().getName());
                result.append(" ");
                result.append(plugins[i].getDescription().getVersion().replaceAll(";", ","));
            }
        }

        return result.toString();
    }

    public String h(final String s) {
        Waitable waitable = new Waitable() {
            protected String evaluate() {
                RemoteControlCommandListener.instance.c();
                RemoteServerCommandEvent event = new RemoteServerCommandEvent(MinecraftServer.this.remoteConsole, s);
                MinecraftServer.this.server.getPluginManager().callEvent(event);
                ServerCommand servercommand = new ServerCommand(event.getCommand(), RemoteControlCommandListener.instance);
                MinecraftServer.this.server.dispatchServerCommand(MinecraftServer.this.remoteConsole, servercommand);
                return RemoteControlCommandListener.instance.d();
            }
        };
        this.processQueue.add(waitable);

        try {
            return (String) waitable.get();
        } catch (ExecutionException var4) {
            throw new RuntimeException("rcon 명령어 처리 중 예외 " + s, var4.getCause());
        } catch (InterruptedException var5) {
            Thread.currentThread().interrupt();
            throw new RuntimeException("rcon 명령어 처리 중단 " + s, var5);
        }
    }

    public boolean isDebugging() {
        return this.getPropertyManager().getBoolean("debug", false);
    }

    public void i(String s) {
        this.getLogger().severe(s);
    }

    public void j(String s) {
        if (this.isDebugging()) {
            this.getLogger().info(s);
        }

    }

    public String getServerModName() {
        return "craftbukkit";
    }

    public CrashReport b(CrashReport crashreport) {
        crashreport.g().a("Profiler Position", new CrashReportProfilerPosition(this));
        if (this.worlds != null && this.worlds.size() > 0 && this.worlds.get(0) != null) {
            crashreport.g().a("Vec3 Pool Size", new CrashReportVec3DPoolSize(this));
        }

        if (this.s != null) {
            crashreport.g().a("Player Count", new CrashReportPlayerCount(this));
        }

        return crashreport;
    }

    public List a(ICommandListener icommandlistener, String s) {
        return this.server.tabComplete(icommandlistener, s);
    }

    public String getName() {
        return "Server";
    }

    public void sendMessage(String s) {
        this.getLogger().info(StripColor.a(s));
    }

    public boolean a(int i, String s) {
        return true;
    }

    public String a(String s, Object... aobject) {
        return LocaleLanguage.a().a(s, aobject);
    }

    public ICommandHandler getCommandHandler() {
        return this.p;
    }

    public KeyPair F() {
        return this.H;
    }

    public int G() {
        return this.r;
    }

    public void setPort(int i) {
        this.r = i;
    }

    public String H() {
        return this.I;
    }

    public void k(String s) {
        this.I = s;
    }

    public boolean I() {
        return this.I != null;
    }

    public String J() {
        return this.J;
    }

    public void l(String s) {
        this.J = s;
    }

    public void a(KeyPair keypair) {
        this.H = keypair;
    }

    public void c(int i) {
        for (int j = 0; j < this.worlds.size(); ++j) {
            WorldServer worldserver = this.worlds.get(j);
            if (worldserver != null) {
                if (worldserver.getWorldData().isHardcore()) {
                    worldserver.difficulty = 3;
                    worldserver.setSpawnFlags(true, true);
                } else if (this.I()) {
                    worldserver.difficulty = i;
                    worldserver.setSpawnFlags(worldserver.difficulty > 0, true);
                } else {
                    worldserver.difficulty = i;
                    worldserver.setSpawnFlags(this.getSpawnMonsters(), this.spawnAnimals);
                }
            }
        }

    }

    protected boolean getSpawnMonsters() {
        return true;
    }

    public boolean M() {
        return this.demoMode;
    }

    public void b(boolean flag) {
        this.demoMode = flag;
    }

    public void c(boolean flag) {
        this.M = flag;
    }

    public Convertable getConvertable() {
        return this.convertable;
    }

    public void P() {
        this.N = true;
        this.getConvertable().d();

        for (int i = 0; i < this.worlds.size(); ++i) {
            WorldServer worldserver = this.worlds.get(i);
            if (worldserver != null) {
                worldserver.saveLevel();
            }
        }

        this.getConvertable().e(this.worlds.get(0).getDataManager().g());
        this.safeShutdown();
    }

    public String getTexturePack() {
        return this.O;
    }

    public void setTexturePack(String s) {
        this.O = s;
    }

    public void a(MojangStatisticsGenerator mojangstatisticsgenerator) {
        mojangstatisticsgenerator.a("whitelist_enabled", Boolean.valueOf(false));
        mojangstatisticsgenerator.a("whitelist_count", Integer.valueOf(0));
        mojangstatisticsgenerator.a("players_current", Integer.valueOf(this.y()));
        mojangstatisticsgenerator.a("players_max", Integer.valueOf(this.z()));
        mojangstatisticsgenerator.a("players_seen", Integer.valueOf(this.s.getSeenPlayers().length));
        mojangstatisticsgenerator.a("uses_auth", Boolean.valueOf(this.onlineMode));
        mojangstatisticsgenerator.a("gui_state", this.ag() ? "enabled" : "disabled");
        mojangstatisticsgenerator.a("avg_tick_ms", Integer.valueOf((int) (MathHelper.a(this.i) * 1.0E-6D)));
        mojangstatisticsgenerator.a("avg_sent_packet_count", Integer.valueOf((int) MathHelper.a(this.e)));
        mojangstatisticsgenerator.a("avg_sent_packet_size", Integer.valueOf((int) MathHelper.a(this.f)));
        mojangstatisticsgenerator.a("avg_rec_packet_count", Integer.valueOf((int) MathHelper.a(this.g)));
        mojangstatisticsgenerator.a("avg_rec_packet_size", Integer.valueOf((int) MathHelper.a(this.h)));
        int i = 0;

        for (int j = 0; j < this.worlds.size(); ++j) {
            WorldServer worldserver = this.worlds.get(j);
            WorldData worlddata = worldserver.getWorldData();
            mojangstatisticsgenerator.a("world[" + i + "][dimension]", Integer.valueOf(worldserver.worldProvider.dimension));
            mojangstatisticsgenerator.a("world[" + i + "][mode]", worlddata.getGameType());
            mojangstatisticsgenerator.a("world[" + i + "][difficulty]", Integer.valueOf(worldserver.difficulty));
            mojangstatisticsgenerator.a("world[" + i + "][hardcore]", Boolean.valueOf(worlddata.isHardcore()));
            mojangstatisticsgenerator.a("world[" + i + "][generator_name]", worlddata.getType().name());
            mojangstatisticsgenerator.a("world[" + i + "][generator_version]", Integer.valueOf(worlddata.getType().getVersion()));
            mojangstatisticsgenerator.a("world[" + i + "][height]", Integer.valueOf(this.C));
            mojangstatisticsgenerator.a("world[" + i + "][chunks_loaded]", Integer.valueOf(worldserver.K().getLoadedChunks()));
            ++i;
        }

        mojangstatisticsgenerator.a("worlds", Integer.valueOf(i));
    }

    public void b(MojangStatisticsGenerator mojangstatisticsgenerator) {
        mojangstatisticsgenerator.a("singleplayer", Boolean.valueOf(this.I()));
        mojangstatisticsgenerator.a("server_brand", this.getServerModName());
        mojangstatisticsgenerator.a("gui_supported", GraphicsEnvironment.isHeadless() ? "headless" : "supported");
        mojangstatisticsgenerator.a("dedicated", Boolean.valueOf(this.T()));
    }

    public boolean getSnooperEnabled() {
        return true;
    }

    public int S() {
        return 16;
    }

    public abstract boolean T();

    public boolean getOnlineMode() {
        return this.server.getOnlineMode();
    }

    public void setOnlineMode(boolean flag) {
        this.onlineMode = flag;
    }

    public boolean getSpawnAnimals() {
        return this.spawnAnimals;
    }

    public void setSpawnAnimals(boolean flag) {
        this.spawnAnimals = flag;
    }

    public boolean getSpawnNPCs() {
        return this.spawnNPCs;
    }

    public void setSpawnNPCs(boolean flag) {
        this.spawnNPCs = flag;
    }

    public boolean getPvP() {
        return this.pvpMode;
    }

    public void setPvP(boolean flag) {
        this.pvpMode = flag;
    }

    public boolean getAllowFlight() {
        return this.allowFlight;
    }

    public void setAllowFlight(boolean flag) {
        this.allowFlight = flag;
    }

    public abstract boolean getEnableCommandBlock();

    public String getMotd() {
        return this.motd;
    }

    public void setMotd(String s) {
        this.motd = s;
    }

    public int getMaxBuildHeight() {
        return this.C;
    }

    public void d(int i) {
        this.C = i;
    }

    public boolean isStopped() {
        return this.isStopped;
    }

    public PlayerList getPlayerList() {
        return this.s;
    }

    public void a(PlayerList playerlist) {
        this.s = playerlist;
    }

    public void a(EnumGamemode enumgamemode) {
        for (int i = 0; i < this.worlds.size(); ++i) {
            getServer().worlds.get(i).getWorldData().setGameType(enumgamemode);
        }

    }

    public abstract ServerConnection ae();

    public boolean ag() {
        return false;
    }

    public abstract String a(EnumGamemode var1, boolean var2);

    public int ah() {
        return this.ticks;
    }

    public void ai() {
        this.S = true;
    }

    public ChunkCoordinates b() {
        return new ChunkCoordinates(0, 0, 0);
    }

    public int getSpawnProtection() {
        return 16;
    }

    public boolean a(World world, int i, int j, int k, EntityHuman entityhuman) {
        return false;
    }

    public abstract IConsoleLogManager getLogger();

    public boolean getForceGamemode() {
        return this.T;
    }

    public void setForceGamemode(boolean flag) {
        this.T = flag;
    }
}
