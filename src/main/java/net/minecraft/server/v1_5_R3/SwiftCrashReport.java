package net.minecraft.server.v1_5_R3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Junhyeong Lim on 2017-01-29.
 */
public class SwiftCrashReport {
    private String msg;
    private String desc;
    private File lastWrited;

    public SwiftCrashReport(String msg, String desc) {
        this.msg = msg;
        this.desc = desc;
    }

    public static File defaultFile() {
        return new File("swift-reports",
                "crash-" + new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss").format(new Date()) + "-server.txt");
    }

    public String prettyString() {
        StringBuilder builder = new StringBuilder();
        builder.append("---- 스위프트버킷 크래쉬 리포트 ----\n\n")
                .append("설명: ").append(desc).append("\n")
                .append("시간: ").append(new SimpleDateFormat().format(new Date())).append("\n\n")
                .append("내용: \n\n")
                .append(msg)
                .append("\n\n")
                .append("----------- 파일의 끝 -----------");
        return builder.toString();
    }

    public boolean write(File file) {
        file.getParentFile().mkdir();
        try {
            file.createNewFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(lastWrited = file));
            writer.write(prettyString());
            writer.close();
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public boolean write() {
        return write(defaultFile());
    }

    public File getLastWritedFile() {
        return lastWrited;
    }
}
