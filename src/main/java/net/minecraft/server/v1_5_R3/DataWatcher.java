package net.minecraft.server.v1_5_R3;

/**
 * Created by EntryPoint on 2016-12-30.
 */

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.*;
import java.util.HashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class DataWatcher {

    private static final Map<Class, Integer> b = new HashMap<>();

    static {
        b.put(Byte.class, 0);
        b.put(Short.class, 1);
        b.put(Integer.class, 2);
        b.put(Float.class, 3);
        b.put(String.class, 4);
        b.put(ItemStack.class, 5);
        b.put(ChunkCoordinates.class, 6);
    }

    private final Map<Integer, WatchableObject> c = new Int2ObjectOpenHashMap<>();
    private boolean a = true;
    private boolean d;
    private ReadWriteLock e = new ReentrantReadWriteLock();

    public DataWatcher() {
    }

    public static void a(List<WatchableObject> list, DataOutputStream dataoutputstream) {
        if (list != null) {
            Iterator<WatchableObject> iterator = list.iterator();

            while (iterator.hasNext()) {
                WatchableObject watchableobject = iterator.next();

                a(dataoutputstream, watchableobject);
            }
        }
        try {
            dataoutputstream.writeByte(127);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void a(DataOutputStream dataoutputstream, WatchableObject watchableobject) {
        int i = (watchableobject.c() << 5 | watchableobject.a() & 31) & 255;

        try {
            dataoutputstream.writeByte(i);
            switch (watchableobject.c()) {
                case 0:
                    dataoutputstream.writeByte((Byte) watchableobject.b());
                    break;

                case 1:
                    dataoutputstream.writeShort((Short) watchableobject.b());
                    break;

                case 2:
                    dataoutputstream.writeInt((Integer) watchableobject.b());
                    break;

                case 3:
                    dataoutputstream.writeFloat((Float) watchableobject.b());
                    break;

                case 4:
                    Packet.a((String) watchableobject.b(), dataoutputstream);
                    break;

                case 5:
                    ItemStack itemstack = (ItemStack) watchableobject.b();

                    Packet.a(itemstack, dataoutputstream);
                    break;

                case 6:
                    ChunkCoordinates chunkcoordinates = (ChunkCoordinates) watchableobject.b();

                    dataoutputstream.writeInt(chunkcoordinates.x);
                    dataoutputstream.writeInt(chunkcoordinates.y);
                    dataoutputstream.writeInt(chunkcoordinates.z);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static List<WatchableObject> a(DataInputStream datainputstream) {
        ArrayList<WatchableObject> arraylist = null;
        try {
            for (byte b0 = datainputstream.readByte(); b0 != 127; b0 = datainputstream.readByte()) {
                if (arraylist == null) {
                    arraylist = new ArrayList<>();
                }

                int i = (b0 & 224) >> 5;
                int j = b0 & 31;
                WatchableObject watchableobject = null;

                switch (i) {
                    case 0:
                        watchableobject = new WatchableObject(i, j, datainputstream.readByte());
                        break;

                    case 1:
                        watchableobject = new WatchableObject(i, j, datainputstream.readShort());
                        break;

                    case 2:
                        watchableobject = new WatchableObject(i, j, datainputstream.readInt());
                        break;

                    case 3:
                        watchableobject = new WatchableObject(i, j, datainputstream.readFloat());
                        break;

                    case 4:
                        watchableobject = new WatchableObject(i, j, Packet.a(datainputstream, 64));
                        break;

                    case 5:
                        watchableobject = new WatchableObject(i, j, Packet.c(datainputstream));
                        break;

                    case 6:
                        int k = datainputstream.readInt();
                        int l = datainputstream.readInt();
                        int i1 = datainputstream.readInt();

                        watchableobject = new WatchableObject(i, j, new ChunkCoordinates(k, l, i1));
                }

                arraylist.add(watchableobject);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return arraylist;
    }

    public void a(int i, Object object) {
        Integer integer = b.get(object.getClass());

        if (integer == null) {
            throw new IllegalArgumentException("Unknown data type: " + object.getClass());
        } else if (i > 31) {
            throw new IllegalArgumentException("Data value id is too big with " + i + "! (Max is " + 31 + ")");
        } else if (this.c.containsKey(i)) {
            throw new IllegalArgumentException("Duplicate id value for " + i + "!");
        } else {
            WatchableObject watchableobject = new WatchableObject(integer, i, object);

            this.e.writeLock().lock();
            this.c.put(i, watchableobject);
            this.e.writeLock().unlock();
            this.a = false;
        }
    }

    public void a(int i, int j) {
        WatchableObject watchableobject = new WatchableObject(j, i, null);

        this.e.writeLock().lock();
        this.c.put(i, watchableobject);
        this.e.writeLock().unlock();
        this.a = false;
    }

    public byte getByte(int i) {
        return (Byte) this.i(i).b();
    }

    public short getShort(int i) {
        return (Short) this.i(i).b();
    }

    public int getInt(int i) {
        return (Integer) this.i(i).b();
    }

    public String getString(int i) {
        return (String) this.i(i).b();
    }

    public ItemStack getItemStack(int i) {
        return (ItemStack) this.i(i).b();
    }

    private WatchableObject i(int i) {
        this.e.readLock().lock();

        WatchableObject watchableobject;

        try {
            watchableobject = this.c.get(i);
        } catch (Throwable throwable) {
            CrashReport crashreport = CrashReport.a(throwable, "Getting synched entity data");
            CrashReportSystemDetails crashreportsystemdetails = crashreport.a("Synched entity data");

            crashreportsystemdetails.a("Data ID", i);
            throw new ReportedException(crashreport);
        }

        this.e.readLock().unlock();
        return watchableobject;
    }

    public void watch(int i, Object object) {
        WatchableObject watchableobject = this.i(i);

        if (!object.equals(watchableobject.b())) {
            watchableobject.a(object);
            watchableobject.a(true);
            this.d = true;
        }
    }

    public void h(int i) {
        WatchableObject.a(this.i(i), true);
        this.d = true;
    }

    public boolean a() {
        return this.d;
    }

    public List<WatchableObject> b() {
        ArrayList<WatchableObject> arraylist = null;

        if (this.d) {
            this.e.readLock().lock();
            Iterator<WatchableObject> iterator = this.c.values().iterator();

            while (iterator.hasNext()) {
                WatchableObject watchableobject = iterator.next();

                if (watchableobject.d()) {
                    watchableobject.a(false);
                    if (arraylist == null) {
                        arraylist = new ArrayList<>();
                    }

                    arraylist.add(watchableobject);
                }
            }

            this.e.readLock().unlock();
        }

        this.d = false;
        return arraylist;
    }

    public void a(DataOutputStream dataoutputstream) {
        this.e.readLock().lock();
        Iterator<WatchableObject> iterator = this.c.values().iterator();

        while (iterator.hasNext()) {
            WatchableObject watchableobject = iterator.next();

            a(dataoutputstream, watchableobject);
        }

        this.e.readLock().unlock();
        try {
            dataoutputstream.writeByte(127);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<WatchableObject> c() {
        ArrayList<WatchableObject> arraylist = null;

        this.e.readLock().lock();

        WatchableObject watchableobject;

        for (Iterator<WatchableObject> iterator = this.c.values().iterator(); iterator.hasNext(); arraylist.add(watchableobject)) {
            watchableobject = iterator.next();
            if (arraylist == null) {
                arraylist = new ArrayList<>();
            }
        }

        this.e.readLock().unlock();
        return arraylist;
    }

    public boolean d() {
        return this.a;
    }
}
