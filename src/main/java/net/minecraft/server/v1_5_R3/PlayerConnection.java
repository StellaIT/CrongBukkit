package net.minecraft.server.v1_5_R3;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandException;
import org.bukkit.craftbukkit.v1_5_R3.CraftServer;
import org.bukkit.craftbukkit.v1_5_R3.SpigotTimings;
import org.bukkit.craftbukkit.v1_5_R3.TextWrapper;
import org.bukkit.craftbukkit.v1_5_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_5_R3.event.CraftEventFactory;
import org.bukkit.craftbukkit.v1_5_R3.inventory.CraftInventoryView;
import org.bukkit.craftbukkit.v1_5_R3.inventory.CraftItemStack;
import org.bukkit.craftbukkit.v1_5_R3.util.LazyPlayerSet;
import org.bukkit.craftbukkit.v1_5_R3.util.Waitable;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.inventory.*;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.*;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.Recipe;
import org.spigotmc.SpigotConfig;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PlayerConnection extends Connection {
    private static final AtomicIntegerFieldUpdater chatSpamField = AtomicIntegerFieldUpdater.newUpdater(PlayerConnection.class, "chatThrottle");
    private static final int PLACE_DISTANCE_SQUARED = 36;
    private static final HashSet<Integer> invalidItems = new HashSet(Arrays.asList(new Integer[]{Integer.valueOf(8), Integer.valueOf(9), Integer.valueOf(10), Integer.valueOf(11), Integer.valueOf(26), Integer.valueOf(34), Integer.valueOf(36), Integer.valueOf(43), Integer.valueOf(51), Integer.valueOf(52), Integer.valueOf(55), Integer.valueOf(59), Integer.valueOf(60), Integer.valueOf(62), Integer.valueOf(63), Integer.valueOf(64), Integer.valueOf(68), Integer.valueOf(71), Integer.valueOf(74), Integer.valueOf(75), Integer.valueOf(83), Integer.valueOf(90), Integer.valueOf(92), Integer.valueOf(93), Integer.valueOf(94), Integer.valueOf(95), Integer.valueOf(104), Integer.valueOf(105), Integer.valueOf(115), Integer.valueOf(117), Integer.valueOf(118), Integer.valueOf(119), Integer.valueOf(125), Integer.valueOf(127), Integer.valueOf(132), Integer.valueOf(137), Integer.valueOf(140), Integer.valueOf(141), Integer.valueOf(142), Integer.valueOf(144)}));
    private static Random j = new Random();
    public final INetworkManager networkManager;
    private final MinecraftServer minecraftServer;
    private final CraftServer server;
    public boolean disconnected = false;
    public EntityPlayer player;
    public boolean checkMovement = true;
    Long lastPacket;
    private int e;
    private int f;
    private boolean g;
    private int h;
    private long i;
    private long k;
    private volatile int chatThrottle = 0;
    private int x = 0;
    private double y;
    private double z;
    private double p;
    private IntHashMap r = new IntHashMap();
    private int lastTick;
    private int lastDropTick;
    private int dropCount;
    private double lastPosX;
    private double lastPosY;
    private double lastPosZ;
    private float lastPitch;
    private float lastYaw;
    private boolean justTeleported;
    private int lastMaterial;

    public PlayerConnection(MinecraftServer minecraftserver, INetworkManager inetworkmanager, EntityPlayer entityplayer) {
        this.lastTick = MinecraftServer.currentTick;
        this.lastDropTick = MinecraftServer.currentTick;
        this.dropCount = 0;
        this.lastPosX = 1.7976931348623157E308D;
        this.lastPosY = 1.7976931348623157E308D;
        this.lastPosZ = 1.7976931348623157E308D;
        this.lastPitch = 3.4028235E38F;
        this.lastYaw = 3.4028235E38F;
        this.justTeleported = false;
        this.minecraftServer = minecraftserver;
        this.networkManager = inetworkmanager;
        inetworkmanager.a(this);
        this.player = entityplayer;
        entityplayer.playerConnection = this;
        this.server = minecraftserver.server;
    }

    public CraftPlayer getPlayer() {
        return this.player == null ? null : this.player.getBukkitEntity();
    }

    public void d() {
        this.g = false;
        ++this.e;
        this.minecraftServer.methodProfiler.a("packetflow");
        this.networkManager.b();
        this.minecraftServer.methodProfiler.c("keepAlive");
        if ((long) this.e - this.k > 20L) {
            this.k = (long) this.e;
            this.i = System.nanoTime() / 1000000L;
            this.h = j.nextInt();
            this.sendPacket(new Packet0KeepAlive(this.h));
        }

        int spam;
        do {
            spam = this.chatThrottle;
        } while (this.chatThrottle > 0 && !chatSpamField.compareAndSet(this, spam, spam - 1));

        if (this.x > 0) {
            --this.x;
        }

        this.minecraftServer.methodProfiler.c("playerTick");
        this.minecraftServer.methodProfiler.b();
    }

    public void disconnect(String s) {
        if (!this.disconnected) {
            String leaveMessage = EnumChatFormat.YELLOW + this.player.name + " 이 서버에서 나갔습니다.";
            PlayerKickEvent event = new PlayerKickEvent(this.server.getPlayer(this.player), s, leaveMessage);
            if (this.server.getServer().isRunning()) {
                this.server.getPluginManager().callEvent(event);
            }

            if (event.isCancelled()) {
                return;
            }

            s = event.getReason();
            this.player.k();
            this.sendPacket(new Packet255KickDisconnect(s));
            this.networkManager.d();
            leaveMessage = event.getLeaveMessage();
            if (leaveMessage != null && leaveMessage.length() > 0) {
                this.minecraftServer.getPlayerList().sendAll(new Packet3Chat(leaveMessage));
            }

            this.minecraftServer.getPlayerList().disconnect(this.player);
            this.disconnected = true;
        }

    }

    public void a(Packet10Flying packet10flying) {
        WorldServer worldserver = this.minecraftServer.getWorldServer(this.player.dimension);
        this.g = true;
        if (!this.player.viewingCredits) {
            double d0;
            if (!this.checkMovement) {
                d0 = packet10flying.y - this.z;
                if (packet10flying.x == this.y && d0 * d0 < 0.01D && packet10flying.z == this.p) {
                    this.checkMovement = true;
                }
            }

            CraftPlayer player = this.getPlayer();
            Location from = new Location(player.getWorld(), this.lastPosX, this.lastPosY, this.lastPosZ, this.lastYaw, this.lastPitch);
            Location to = player.getLocation().clone();
            if (packet10flying.hasPos && (!packet10flying.hasPos || packet10flying.y != -999.0D || packet10flying.stance != -999.0D)) {
                to.setX(packet10flying.x);
                to.setY(packet10flying.y);
                to.setZ(packet10flying.z);
            }

            if (packet10flying.hasLook) {
                to.setYaw(packet10flying.yaw);
                to.setPitch(packet10flying.pitch);
            }

            double delta = Math.pow(this.lastPosX - to.getX(), 2.0D) + Math.pow(this.lastPosY - to.getY(), 2.0D) + Math.pow(this.lastPosZ - to.getZ(), 2.0D);
            float deltaAngle = Math.abs(this.lastYaw - to.getYaw()) + Math.abs(this.lastPitch - to.getPitch());
            if ((delta > 0.00390625D || deltaAngle > 10.0F) && this.checkMovement && !this.player.dead) {
                this.lastPosX = to.getX();
                this.lastPosY = to.getY();
                this.lastPosZ = to.getZ();
                this.lastYaw = to.getYaw();
                this.lastPitch = to.getPitch();
                if (from.getX() != 1.7976931348623157E308D) {
                    PlayerMoveEvent event = new PlayerMoveEvent(player, from, to);
                    this.server.getPluginManager().callEvent(event);
                    if (event.isCancelled()) {
                        this.player.playerConnection.sendPacket(new Packet13PlayerLookMove(from.getX(), from.getY() + 1.6200000047683716D, from.getY(), from.getZ(), from.getYaw(), from.getPitch(), false));
                        return;
                    }

                    if (!to.equals(event.getTo()) && !event.isCancelled()) {
                        this.player.getBukkitEntity().teleport(event.getTo(), TeleportCause.UNKNOWN);
                        return;
                    }

                    if (!from.equals(this.getPlayer().getLocation()) && this.justTeleported) {
                        this.justTeleported = false;
                        return;
                    }
                }
            }

            if (!Double.isNaN(packet10flying.x) && !Double.isNaN(packet10flying.y) && !Double.isNaN(packet10flying.z) && !Double.isNaN(packet10flying.stance)) {
                if (this.checkMovement && !this.player.dead) {
                    float f2;
                    float f3;
                    double d1;
                    double d2;
                    double d3;
                    double d6;
                    double d4;
                    if (this.player.vehicle != null) {
                        f2 = this.player.yaw;
                        f3 = this.player.pitch;
                        this.player.vehicle.U();
                        d1 = this.player.locX;
                        d2 = this.player.locY;
                        d3 = this.player.locZ;
                        d6 = 0.0D;
                        d4 = 0.0D;
                        if (packet10flying.hasLook) {
                            f2 = packet10flying.yaw;
                            f3 = packet10flying.pitch;
                        }

                        if (packet10flying.hasPos && packet10flying.y == -999.0D && packet10flying.stance == -999.0D) {
                            if (Math.abs(packet10flying.x) > 1.0D || Math.abs(packet10flying.z) > 1.0D) {
                                System.err.println(this.player.name + " was caught trying to crash the server with an invalid position.");
                                this.disconnect("Nope!");
                                return;
                            }

                            d6 = packet10flying.x;
                            d4 = packet10flying.z;
                        }

                        this.player.onGround = packet10flying.g;
                        this.player.g();
                        this.player.move(d6, 0.0D, d4);
                        this.player.setLocation(d1, d2, d3, f2, f3);
                        this.player.motX = d6;
                        this.player.motZ = d4;
                        if (this.player.vehicle != null) {
                            worldserver.vehicleEnteredWorld(this.player.vehicle, true);
                        }

                        if (this.player.vehicle != null) {
                            this.player.vehicle.U();
                        }

                        this.minecraftServer.getPlayerList().d(this.player);
                        this.y = this.player.locX;
                        this.z = this.player.locY;
                        this.p = this.player.locZ;
                        worldserver.playerJoinedWorld(this.player);
                    } else if (this.player.isSleeping()) {
                        this.player.g();
                        this.player.setLocation(this.y, this.z, this.p, this.player.yaw, this.player.pitch);
                        worldserver.playerJoinedWorld(this.player);
                    } else {
                        d0 = this.player.locY;
                        this.y = this.player.locX;
                        this.z = this.player.locY;
                        this.p = this.player.locZ;
                        d1 = this.player.locX;
                        d2 = this.player.locY;
                        d3 = this.player.locZ;
                        f2 = this.player.yaw;
                        f3 = this.player.pitch;
                        if (packet10flying.hasPos && packet10flying.y == -999.0D && packet10flying.stance == -999.0D) {
                            packet10flying.hasPos = false;
                        }

                        if (packet10flying.hasPos) {
                            d1 = packet10flying.x;
                            d2 = packet10flying.y;
                            d3 = packet10flying.z;
                            d4 = packet10flying.stance - packet10flying.y;
                            if (!this.player.isSleeping() && (d4 > 1.65D || d4 < 0.1D)) {
                                this.disconnect("Illegal stance");
                                this.minecraftServer.getLogger().warning(this.player.name + " had an illegal stance: " + d4);
                                return;
                            }

                            if (Math.abs(packet10flying.x) > 3.2E7D || Math.abs(packet10flying.z) > 3.2E7D) {
                                this.a(this.y, this.z, this.p, this.player.yaw, this.player.pitch);
                                return;
                            }
                        }

                        if (packet10flying.hasLook) {
                            f2 = packet10flying.yaw;
                            f3 = packet10flying.pitch;
                        }

                        this.player.g();
                        this.player.X = 0.0F;
                        this.player.setLocation(this.y, this.z, this.p, f2, f3);
                        if (this.checkMovement) {
                            d4 = d1 - this.player.locX;
                            d6 = d2 - this.player.locY;
                            double d7 = d3 - this.player.locZ;
                            double d8 = Math.max(Math.abs(d4), Math.abs(this.player.motX));
                            double d9 = Math.max(Math.abs(d6), Math.abs(this.player.motY));
                            double d10 = Math.max(Math.abs(d7), Math.abs(this.player.motZ));
                            double d11 = d8 * d8 + d9 * d9 + d10 * d10;
                            if (d11 <= 100.0D || !this.checkMovement || this.minecraftServer.I() && this.minecraftServer.H().equals(this.player.name)) {
                                float f4 = 0.0625F;
                                boolean flag = worldserver.getCubes(this.player, this.player.boundingBox.clone().shrink((double) f4, (double) f4, (double) f4)).isEmpty();
                                if (this.player.onGround && !packet10flying.g && d6 > 0.0D) {
                                    this.player.j(0.2F);
                                }

                                this.player.move(d4, d6, d7);
                                this.player.onGround = packet10flying.g;
                                this.player.checkMovement(d4, d6, d7);
                                double d12 = d6;
                                d4 = d1 - this.player.locX;
                                d6 = d2 - this.player.locY;
                                if (d6 > -0.5D || d6 < 0.5D) {
                                    d6 = 0.0D;
                                }

                                d7 = d3 - this.player.locZ;
                                d11 = d4 * d4 + d6 * d6 + d7 * d7;
                                boolean flag1 = false;
                                if (d11 > 0.0625D && !this.player.isSleeping() && !this.player.playerInteractManager.isCreative()) {
                                    flag1 = true;
                                    this.minecraftServer.getLogger().warning(this.player.name + " 이 이상하게(잘못) 움직였습니다!");
                                }

                                this.player.setLocation(d1, d2, d3, f2, f3);
                                boolean flag2 = worldserver.getCubes(this.player, this.player.boundingBox.clone().shrink((double) f4, (double) f4, (double) f4)).isEmpty();
                                if (flag && (flag1 || !flag2) && !this.player.isSleeping()) {
                                    this.a(this.y, this.z, this.p, f2, f3);
                                } else {
                                    AxisAlignedBB axisalignedbb = this.player.boundingBox.clone().grow((double) f4, (double) f4, (double) f4).a(0.0D, -0.55D, 0.0D);
                                    if (!this.minecraftServer.getAllowFlight() && !this.player.abilities.canFly && !worldserver.c(axisalignedbb)) {
                                        if (d12 >= -0.03125D) {
                                            ++this.f;
                                            if (this.f > 80) {
                                                this.minecraftServer.getLogger().warning(this.player.name + " was kicked for floating too long!");
                                                this.disconnect("Flying is not enabled on this server");
                                                return;
                                            }
                                        }
                                    } else {
                                        this.f = 0;
                                    }

                                    this.player.onGround = packet10flying.g;
                                    this.minecraftServer.getPlayerList().d(this.player);
                                    if (!this.player.playerInteractManager.isCreative()) {
                                        this.player.b(this.player.locY - d0, packet10flying.g);
                                    }
                                }
                            } else {
                                this.minecraftServer.getLogger().warning(this.player.name + " moved too quickly! " + d4 + "," + d6 + "," + d7 + " (" + d8 + ", " + d9 + ", " + d10 + ")");
                                this.a(this.y, this.z, this.p, this.player.yaw, this.player.pitch);
                            }
                        }
                    }
                }
            } else {
                player.teleport(player.getWorld().getSpawnLocation(), TeleportCause.UNKNOWN);
                System.err.println(player.getName() + " was caught trying to crash the server with an invalid position.");
                player.kickPlayer("Nope!");
            }
        }
    }

    public void a(double d0, double d1, double d2, float f, float f1) {
        CraftPlayer player = this.getPlayer();
        Location from = player.getLocation();
        Location to = new Location(this.getPlayer().getWorld(), d0, d1, d2, f, f1);
        PlayerTeleportEvent event = new PlayerTeleportEvent(player, from, to, TeleportCause.UNKNOWN);
        this.server.getPluginManager().callEvent(event);
        from = event.getFrom();
        to = event.isCancelled() ? from : event.getTo();
        this.teleport(to);
    }

    public void teleport(Location dest) {
        double d0 = dest.getX();
        double d1 = dest.getY();
        double d2 = dest.getZ();
        float f = dest.getYaw();
        float f1 = dest.getPitch();
        if (Float.isNaN(f)) {
            f = 0.0F;
        }

        if (Float.isNaN(f1)) {
            f1 = 0.0F;
        }

        this.lastPosX = d0;
        this.lastPosY = d1;
        this.lastPosZ = d2;
        this.lastYaw = f;
        this.lastPitch = f1;
        this.justTeleported = true;
        this.checkMovement = false;
        this.y = d0;
        this.z = d1;
        this.p = d2;
        this.player.setLocation(d0, d1, d2, f, f1);
        this.player.playerConnection.sendPacket(new Packet13PlayerLookMove(d0, d1 + 1.6200000047683716D, d1, d2, f, f1, false));
    }

    public void a(Packet14BlockDig packet14blockdig) {
        if (!this.player.dead) {
            WorldServer worldserver = this.minecraftServer.getWorldServer(this.player.dimension);
            if (packet14blockdig.e == 4) {
                if (this.lastDropTick != MinecraftServer.currentTick) {
                    this.dropCount = 0;
                    this.lastDropTick = MinecraftServer.currentTick;
                } else {
                    ++this.dropCount;
                    if (this.dropCount >= 20) {
                        this.minecraftServer.getLogger().warning(this.player.name + " dropped their items too quickly!");
                        this.disconnect("You dropped your items too quickly (Hacking?)");
                        return;
                    }
                }

                this.player.a(false);
            } else if (packet14blockdig.e == 3) {
                this.player.a(true);
            } else if (packet14blockdig.e == 5) {
                this.player.bZ();
            } else {
                boolean flag = false;
                if (packet14blockdig.e == 0) {
                    flag = true;
                }

                if (packet14blockdig.e == 1) {
                    flag = true;
                }

                if (packet14blockdig.e == 2) {
                    flag = true;
                }

                int i = packet14blockdig.a;
                int j = packet14blockdig.b;
                int k = packet14blockdig.c;
                if (flag) {
                    double d0 = this.player.locX - ((double) i + 0.5D);
                    double d1 = this.player.locY - ((double) j + 0.5D) + 1.5D;
                    double d2 = this.player.locZ - ((double) k + 0.5D);
                    double d3 = d0 * d0 + d1 * d1 + d2 * d2;
                    if (d3 > 36.0D) {
                        return;
                    }

                    if (j >= this.minecraftServer.getMaxBuildHeight()) {
                        return;
                    }
                }

                if (packet14blockdig.e == 0) {
                    if (!this.minecraftServer.a(worldserver, i, j, k, this.player)) {
                        this.player.playerInteractManager.dig(i, j, k, packet14blockdig.face);
                    } else {
                        CraftEventFactory.callPlayerInteractEvent(this.player, Action.LEFT_CLICK_BLOCK, i, j, k, packet14blockdig.face, this.player.inventory.getItemInHand());
                        this.player.playerConnection.sendPacket(new Packet53BlockChange(i, j, k, worldserver));
                        TileEntity tileentity = worldserver.getTileEntity(i, j, k);
                        if (tileentity != null) {
                            this.player.playerConnection.sendPacket(tileentity.getUpdatePacket());
                        }
                    }
                } else if (packet14blockdig.e == 2) {
                    this.player.playerInteractManager.a(i, j, k);
                    if (worldserver.getTypeId(i, j, k) != 0) {
                        this.player.playerConnection.sendPacket(new Packet53BlockChange(i, j, k, worldserver));
                    }
                } else if (packet14blockdig.e == 1) {
                    this.player.playerInteractManager.c(i, j, k);
                    if (worldserver.getTypeId(i, j, k) != 0) {
                        this.player.playerConnection.sendPacket(new Packet53BlockChange(i, j, k, worldserver));
                    }
                }
            }

        }
    }

    public void a(Packet15Place packet15place) {
        WorldServer worldserver = this.minecraftServer.getWorldServer(this.player.dimension);
        if (!this.player.dead) {
            if (packet15place.getFace() == 255) {
                if (packet15place.getItemStack() != null && packet15place.getItemStack().id == this.lastMaterial && this.lastPacket != null && packet15place.timestamp - this.lastPacket.longValue() < 100L) {
                    this.lastPacket = null;
                    return;
                }
            } else {
                this.lastMaterial = packet15place.getItemStack() == null ? -1 : packet15place.getItemStack().id;
                this.lastPacket = Long.valueOf(packet15place.timestamp);
            }

            boolean always = false;
            ItemStack itemstack = this.player.inventory.getItemInHand();
            boolean flag = false;
            int i = packet15place.d();
            int j = packet15place.f();
            int k = packet15place.g();
            int l = packet15place.getFace();
            if (packet15place.getFace() == 255) {
                if (itemstack == null) {
                    return;
                }

                int slot = itemstack.count;
                PlayerInteractEvent event = CraftEventFactory.callPlayerInteractEvent(this.player, Action.RIGHT_CLICK_AIR, itemstack);
                if (event.useItemInHand() != Result.DENY) {
                    this.player.playerInteractManager.useItem(this.player, this.player.world, itemstack);
                }

                always = itemstack.count != slot;
            } else if (packet15place.f() >= this.minecraftServer.getMaxBuildHeight() - 1 && (packet15place.getFace() == 1 || packet15place.f() >= this.minecraftServer.getMaxBuildHeight())) {
                this.player.playerConnection.sendPacket(new Packet3Chat("" + EnumChatFormat.GRAY + "Height limit for building is " + this.minecraftServer.getMaxBuildHeight()));
                flag = true;
            } else {
                Location var12 = this.getPlayer().getEyeLocation();
                if (Math.pow(var12.getX() - (double) i, 2.0D) + Math.pow(var12.getY() - (double) j, 2.0D) + Math.pow(var12.getZ() - (double) k, 2.0D) > 36.0D) {
                    return;
                }

                this.player.playerInteractManager.interact(this.player, worldserver, itemstack, i, j, k, l, packet15place.j(), packet15place.k(), packet15place.l());
                flag = true;
            }

            if (flag) {
                this.player.playerConnection.sendPacket(new Packet53BlockChange(i, j, k, worldserver));
                if (l == 0) {
                    --j;
                }
                if (l == 1) {
                    ++j;
                }

                if (l == 2) {
                    --k;
                }

                if (l == 3) {
                    ++k;
                }

                if (l == 4) {
                    --i;
                }

                if (l == 5) {
                    ++i;
                }

                this.player.playerConnection.sendPacket(new Packet53BlockChange(i, j, k, worldserver));
            }

            itemstack = this.player.inventory.getItemInHand();
            if (itemstack != null && itemstack.count == 0) {
                this.player.inventory.items[this.player.inventory.itemInHandIndex] = null;
                itemstack = null;
            }

            if (itemstack == null || itemstack.n() == 0) {
                this.player.h = true;
                this.player.inventory.items[this.player.inventory.itemInHandIndex] = ItemStack.b(this.player.inventory.items[this.player.inventory.itemInHandIndex]);
                Slot var13 = this.player.activeContainer.a(this.player.inventory, this.player.inventory.itemInHandIndex);
                this.player.activeContainer.b();
                this.player.h = false;
                if (!ItemStack.matches(this.player.inventory.getItemInHand(), packet15place.getItemStack()) || always) {
                    this.sendPacket(new Packet103SetSlot(this.player.activeContainer.windowId, var13.g, this.player.inventory.getItemInHand()));
                }
            }

        }
    }

    public void a(String s, Object[] aobject) {
        if (!this.disconnected) {
            this.minecraftServer.getLogger().info(this.player.name + " lost connection: " + s);
            String quitMessage = this.minecraftServer.getPlayerList().disconnect(this.player);
            if (quitMessage != null && quitMessage.length() > 0) {
                this.minecraftServer.getPlayerList().sendAll(new Packet3Chat(quitMessage));
            }

            this.disconnected = true;
            if (this.minecraftServer.I() && this.player.name.equals(this.minecraftServer.H())) {
                this.minecraftServer.getLogger().info("Stopping singleplayer server as player logged out");
                this.minecraftServer.safeShutdown();
            }

        }
    }

    public void onUnhandledPacket(Packet packet) {
        if (!this.disconnected) {
            this.minecraftServer.getLogger().warning(this.getClass() + " wasn\'t prepared to deal with a " + packet.getClass());
            this.disconnect("Protocol error, unexpected packet");
        }
    }

    public void sendPacket(Packet packet) {
        // Block &k in sign
        if (packet != null) {
            if (packet instanceof Packet130UpdateSign) {
                Packet130UpdateSign signPacket = (Packet130UpdateSign) packet;
                int i = 0;
                for (String line : signPacket.lines) {
                    signPacket.lines[i++] = line.replaceAll("&k", "").replaceAll("§k", "");
                }
            }
        }
        //
        if (!(packet instanceof Packet3Chat)) {
            if (packet != null) {
                if (packet instanceof Packet6SpawnPosition) {
                    Packet6SpawnPosition throwable1 = (Packet6SpawnPosition) packet;
                    this.player.compassTarget = new Location(this.getPlayer().getWorld(), (double) throwable1.x, (double) throwable1.y, (double) throwable1.z);
                }

                try {
                    this.networkManager.queue(packet);
                } catch (Throwable var7) {
                    CrashReport crashreport1 = CrashReport.a(var7, "Sending packet");
                    CrashReportSystemDetails crashreportsystemdetails1 = crashreport1.a("Packet being sent");
                    crashreportsystemdetails1.a("Packet ID", new CrashReportConnectionPacketID(this, packet));
                    crashreportsystemdetails1.a("Packet class", new CrashReportConnectionPacketClass(this, packet));
                    throw new ReportedException(crashreport1);
                }
            }
        } else {
            Packet3Chat throwable = (Packet3Chat) packet;
            int crashreport = this.player.getChatFlags();
            if (crashreport != 2) {
                if (crashreport != 1 || throwable.isServer()) {
                    String crashreportsystemdetails = throwable.message;
                    Iterator i$ = TextWrapper.wrapText(crashreportsystemdetails).iterator();

                    while (i$.hasNext()) {
                        String line = (String) i$.next();
                        this.networkManager.queue(new Packet3Chat(line));
                    }

                }
            }
        }
    }

    public void a(Packet16BlockItemSwitch packet16blockitemswitch) {
        if (!this.player.dead) {
            if (packet16blockitemswitch.itemInHandIndex >= 0 && packet16blockitemswitch.itemInHandIndex < PlayerInventory.getHotbarSize()) {
                PlayerItemHeldEvent event = new PlayerItemHeldEvent(this.getPlayer(), this.player.inventory.itemInHandIndex, packet16blockitemswitch.itemInHandIndex);
                this.server.getPluginManager().callEvent(event);
                if (event.isCancelled()) {
                    this.sendPacket(new Packet16BlockItemSwitch(this.player.inventory.itemInHandIndex));
                    return;
                }

                this.player.inventory.itemInHandIndex = packet16blockitemswitch.itemInHandIndex;
            } else {
                this.minecraftServer.getLogger().warning(this.player.name + " tried to set an invalid carried item");
                this.disconnect("Nope!");
            }

        }
    }

    public void a(Packet3Chat packet3chat) {
        if (this.player.getChatFlags() == 2) {
            this.sendPacket(new Packet3Chat("Cannot send chat message."));
        } else {
            String s = packet3chat.message;
            if (!getPlayer().isOp()) {
                // Replace the trigger placeholder
                s = s.replaceAll("<.+>", "");
                // Replace &k color code
                s = s.replaceAll("§k", "").replaceAll("&k", "");
                // Replace unreg command
//            if (s.startsWith("/회원탈퇴") || s.startsWith("/unreg")) {
//                return;
//            }
            }
            Waitable waitable;
            if (s.length() > 100) {
                if (packet3chat.a_()) {
                    waitable = new Waitable() {
                        protected Object evaluate() {
                            PlayerConnection.this.disconnect("너무 긴 내용을 채팅");
                            return null;
                        }
                    };
                    this.minecraftServer.processQueue.add(waitable);

                    try {
                        waitable.get();
                    } catch (InterruptedException var10) {
                        Thread.currentThread().interrupt();
                    } catch (ExecutionException var11) {
                        throw new RuntimeException(var11);
                    }
                } else {
                    this.disconnect("채팅한 메세지가 너무 깁니다.");
                }
            } else {
                s = s.trim();

                for (int var12 = 0; var12 < s.length(); ++var12) {
                    if (!SharedConstants.isAllowedChatCharacter(s.charAt(var12))) {
                        if (packet3chat.a_()) {
                            Waitable e = new Waitable() {
                                protected Object evaluate() {
                                    PlayerConnection.this.disconnect("허용하지 않는 문자를 입력");
                                    return null;
                                }
                            };
                            this.minecraftServer.processQueue.add(e);

                            try {
                                e.get();
                            } catch (InterruptedException var6) {
                                Thread.currentThread().interrupt();
                            } catch (ExecutionException var7) {
                                throw new RuntimeException(var7);
                            }
                        } else {
                            this.disconnect("채팅 내용에 허용하지 않는 문자가 있습니다.");
                        }

                        return;
                    }
                }

                if (this.player.getChatFlags() == 1 && !s.startsWith("/")) {
                    this.sendPacket(new Packet3Chat("Cannot send chat message."));
                    return;
                }

                this.chat(s, packet3chat.a_());
                if (!SpigotConfig.spamExclusions.contains(s) && chatSpamField.addAndGet(this, 20) > 200 && !this.minecraftServer.getPlayerList().isOp(this.player.name)) {
                    if (packet3chat.a_()) {
                        waitable = new Waitable() {
                            protected Object evaluate() {
                                PlayerConnection.this.disconnect("disconnect.spam");
                                return null;
                            }
                        };
                        this.minecraftServer.processQueue.add(waitable);

                        try {
                            waitable.get();
                        } catch (InterruptedException var8) {
                            Thread.currentThread().interrupt();
                        } catch (ExecutionException var9) {
                            throw new RuntimeException(var9);
                        }
                    } else {
                        this.disconnect("disconnect.spam");
                    }
                }
            }
        }

    }

    public void chat(String s, boolean async) {
        if (!this.player.dead) {
            if (s.length() == 0) {
                this.minecraftServer.getLogger().warning(this.player.name + " 가 빈 메세지를 전송했습니다.");
                return;
            }

            if (this.getPlayer().isConversing()) {
                this.getPlayer().acceptConversationInput(s);
                return;
            }

            if (s.startsWith("/")) {
                this.handleCommand(s);
                return;
            }

            CraftPlayer player = this.getPlayer();
            AsyncPlayerChatEvent event = new AsyncPlayerChatEvent(async, player, s, new LazyPlayerSet());
            this.server.getPluginManager().callEvent(event);
            if (PlayerChatEvent.getHandlerList().getRegisteredListeners().length != 0) {
                final PlayerChatEvent i$ = new PlayerChatEvent(player, event.getMessage(), event.getFormat(), event.getRecipients());
                i$.setCancelled(event.isCancelled());
                Waitable recipient = new Waitable() {
                    protected Object evaluate() {
                        Bukkit.getPluginManager().callEvent(i$);
                        if (i$.isCancelled()) {
                            return null;
                        } else {
                            String message = String.format(i$.getFormat(), i$.getPlayer().getDisplayName(), i$.getMessage());
                            PlayerConnection.this.minecraftServer.console.sendMessage(message);
                            Iterator i$x;
                            if (((LazyPlayerSet) i$.getRecipients()).isLazy()) {
                                i$x = PlayerConnection.this.minecraftServer.getPlayerList().players.iterator();

                                while (i$x.hasNext()) {
                                    Object player = i$x.next();
                                    ((EntityPlayer) player).sendMessage(message);
                                }
                            } else {
                                i$x = i$.getRecipients().iterator();

                                while (i$x.hasNext()) {
                                    Player player1 = (Player) i$x.next();
                                    player1.sendMessage(message);
                                }
                            }

                            return null;
                        }
                    }
                };
                if (async) {
                    this.minecraftServer.processQueue.add(recipient);
                } else {
                    recipient.run();
                }

                try {
                    recipient.get();
                } catch (InterruptedException var8) {
                    Thread.currentThread().interrupt();
                } catch (ExecutionException var9) {
                    throw new RuntimeException("채팅 이벤트 처리 중에 예외", var9.getCause());
                }
            } else {
                if (event.isCancelled()) {
                    return;
                }

                s = String.format(event.getFormat(), event.getPlayer().getDisplayName(), event.getMessage());
                this.minecraftServer.console.sendMessage(s);
                Iterator i$1;
                if (((LazyPlayerSet) event.getRecipients()).isLazy()) {
                    i$1 = this.minecraftServer.getPlayerList().players.iterator();

                    while (i$1.hasNext()) {
                        Object recipient1 = i$1.next();
                        ((EntityPlayer) recipient1).sendMessage(s);
                    }
                } else {
                    i$1 = event.getRecipients().iterator();

                    while (i$1.hasNext()) {
                        Player recipient2 = (Player) i$1.next();
                        recipient2.sendMessage(s);
                    }
                }
            }
        }

    }

    private void handleCommand(String s) {
        SpigotTimings.playerCommandTimer.startTiming();
        CraftPlayer player = this.getPlayer();
        PlayerCommandPreprocessEvent event = new PlayerCommandPreprocessEvent(player, s, new LazyPlayerSet());
        this.server.getPluginManager().callEvent(event);
        if (event.isCancelled()) {
            SpigotTimings.playerCommandTimer.stopTiming();
        } else {
            try {
                if (SpigotConfig.logCommands) {
                    this.minecraftServer.getLogger().info(event.getPlayer().getName() + " 가 서버 명령어를 입력: " + event.getMessage());
                }

                if (this.server.dispatchCommand(event.getPlayer(), event.getMessage().substring(1))) {
                    SpigotTimings.playerCommandTimer.stopTiming();
                    return;
                }
            } catch (CommandException var5) {
                player.sendMessage(ChatColor.RED + "명령어 처리 중에 문제가 발생했습니다.");
                Logger.getLogger(PlayerConnection.class.getName()).log(Level.SEVERE, null, var5);
                SpigotTimings.playerCommandTimer.stopTiming();
                return;
            }

            SpigotTimings.playerCommandTimer.stopTiming();
        }
    }

    public void a(Packet18ArmAnimation packet18armanimation) {
        if (!this.player.dead) {
            if (packet18armanimation.b == 1) {
                float f = 1.0F;
                float f1 = this.player.lastPitch + (this.player.pitch - this.player.lastPitch) * f;
                float f2 = this.player.lastYaw + (this.player.yaw - this.player.lastYaw) * f;
                double d0 = this.player.lastX + (this.player.locX - this.player.lastX) * (double) f;
                double d1 = this.player.lastY + (this.player.locY - this.player.lastY) * (double) f + 1.62D - (double) this.player.height;
                double d2 = this.player.lastZ + (this.player.locZ - this.player.lastZ) * (double) f;
                Vec3D vec3d = this.player.world.getVec3DPool().create(d0, d1, d2);
                float f3 = MathHelper.cos(-f2 * 0.017453292F - 3.1415927F);
                float f4 = MathHelper.sin(-f2 * 0.017453292F - 3.1415927F);
                float f5 = -MathHelper.cos(-f1 * 0.017453292F);
                float f6 = MathHelper.sin(-f1 * 0.017453292F);
                float f7 = f4 * f5;
                float f8 = f3 * f5;
                double d3 = 5.0D;
                Vec3D vec3d1 = vec3d.add((double) f7 * d3, (double) f6 * d3, (double) f8 * d3);
                MovingObjectPosition movingobjectposition = this.player.world.rayTrace(vec3d, vec3d1, true);
                if (movingobjectposition == null || movingobjectposition.type != EnumMovingObjectType.TILE) {
                    CraftEventFactory.callPlayerInteractEvent(this.player, Action.LEFT_CLICK_AIR, this.player.inventory.getItemInHand());
                }

                PlayerAnimationEvent event = new PlayerAnimationEvent(this.getPlayer());
                this.server.getPluginManager().callEvent(event);
                if (event.isCancelled()) {
                    return;
                }

                this.player.bK();
            }

        }
    }

    public void a(Packet19EntityAction packet19entityaction) {
        if (!this.player.dead) {
            if (packet19entityaction.animation == 1 || packet19entityaction.animation == 2) {
                PlayerToggleSneakEvent event = new PlayerToggleSneakEvent(this.getPlayer(), packet19entityaction.animation == 1);
                this.server.getPluginManager().callEvent(event);
                if (event.isCancelled()) {
                    return;
                }
            }

            if (packet19entityaction.animation == 4 || packet19entityaction.animation == 5) {
                PlayerToggleSprintEvent event1 = new PlayerToggleSprintEvent(this.getPlayer(), packet19entityaction.animation == 4);
                this.server.getPluginManager().callEvent(event1);
                if (event1.isCancelled()) {
                    return;
                }
            }

            if (packet19entityaction.animation == 1) {
                this.player.setSneaking(true);
            } else if (packet19entityaction.animation == 2) {
                this.player.setSneaking(false);
            } else if (packet19entityaction.animation == 4) {
                this.player.setSprinting(true);
            } else if (packet19entityaction.animation == 5) {
                this.player.setSprinting(false);
            } else if (packet19entityaction.animation == 3) {
                this.player.a(false, true, true);
            }

        }
    }

    public void a(Packet255KickDisconnect packet255kickdisconnect) {
        this.networkManager.a("disconnect.quitting");
    }

    public int lowPriorityCount() {
        return this.networkManager.e();
    }

    public void a(Packet7UseEntity packet7useentity) {
        if (!this.player.dead) {
            WorldServer worldserver = this.minecraftServer.getWorldServer(this.player.dimension);
            Entity entity = worldserver.getEntity(packet7useentity.target);
            if (entity == this.player) {
                this.disconnect("Cannot interact with self!");
            } else {
                if (entity != null) {
                    boolean flag = this.player.n(entity);
                    double d0 = 36.0D;
                    if (!flag) {
                        d0 = 9.0D;
                    }

                    if (this.player.e(entity) < d0) {
                        ItemStack itemInHand = this.player.inventory.getItemInHand();
                        if (packet7useentity.action == 0) {
                            PlayerInteractEntityEvent type = new PlayerInteractEntityEvent(this.getPlayer(), entity.getBukkitEntity());
                            this.server.getPluginManager().callEvent(type);
                            if (type.isCancelled()) {
                                return;
                            }

                            this.player.p(entity);
                            if (itemInHand != null && itemInHand.count <= -1) {
                                this.player.updateInventory(this.player.activeContainer);
                            }
                        } else if (packet7useentity.action == 1) {
                            if (entity instanceof EntityItem || entity instanceof EntityExperienceOrb || entity instanceof EntityArrow) {
                                String type1 = entity.getClass().getSimpleName();
                                this.disconnect("Attacking an " + type1 + " is not permitted");
                                System.out.println("Player " + this.player.name + " tried to attack an " + type1 + ", so I have disconnected them for exploiting.");
                                return;
                            }

                            this.player.attack(entity);
                            if (itemInHand != null && itemInHand.count <= -1) {
                                this.player.updateInventory(this.player.activeContainer);
                            }
                        }
                    }
                }

            }
        }
    }

    public void a(Packet205ClientCommand packet205clientcommand) {
        if (packet205clientcommand.a == 1) {
            if (this.player.viewingCredits) {
                this.minecraftServer.getPlayerList().changeDimension(this.player, 0, TeleportCause.END_PORTAL);
            } else if (this.player.o().getWorldData().isHardcore()) {
                if (this.minecraftServer.I() && this.player.name.equals(this.minecraftServer.H())) {
                    this.player.playerConnection.disconnect("You have died. Game over, man, it\'s game over!");
                    this.minecraftServer.P();
                } else {
                    BanEntry banentry = new BanEntry(this.player.name);
                    banentry.setReason("Death in Hardcore");
                    this.minecraftServer.getPlayerList().getNameBans().add(banentry);
                    this.player.playerConnection.disconnect("You have died. Game over, man, it\'s game over!");
                }
            } else {
                if (this.player.getHealth() > 0) {
                    return;
                }

                this.player = this.minecraftServer.getPlayerList().moveToWorld(this.player, 0, false);
            }
        }

    }

    public boolean b() {
        return true;
    }

    public void a(Packet9Respawn packet9respawn) {
    }

    public void handleContainerClose(Packet101CloseWindow packet101closewindow) {
        if (!this.player.dead) {
            CraftEventFactory.handleInventoryCloseEvent(this.player);
            this.player.j();
        }
    }

    public void a(Packet102WindowClick packet102windowclick) {
        if (!this.player.dead && this.player.activeContainer.a(this.player)) {
            if (this.player.activeContainer.windowId == packet102windowclick.a && this.player.activeContainer.c(this.player)) {
                if (packet102windowclick.slot < -1 && packet102windowclick.slot != -999) {
                    return;
                }

                InventoryView inventory = this.player.activeContainer.getBukkitView();
                SlotType type = CraftInventoryView.getSlotType(inventory, packet102windowclick.slot);
                InventoryClickEvent event;
                ClickType click = ClickType.UNKNOWN;
                InventoryAction action = InventoryAction.UNKNOWN;
                ItemStack itemstack = null;
                if (packet102windowclick.slot == -1) {
                    type = SlotType.OUTSIDE;
                    click = packet102windowclick.button == 0 ? ClickType.WINDOW_BORDER_LEFT : ClickType.WINDOW_BORDER_RIGHT;
                    action = InventoryAction.NOTHING;
                } else {
                    ItemStack i;
                    int firstEmptySlot;
                    Slot var12;
                    if (packet102windowclick.shift == 0) {
                        if (packet102windowclick.button == 0) {
                            click = ClickType.LEFT;
                        } else if (packet102windowclick.button == 1) {
                            click = ClickType.RIGHT;
                        }

                        if (packet102windowclick.button == 0 || packet102windowclick.button == 1) {
                            action = InventoryAction.NOTHING;
                            if (packet102windowclick.slot == -999) {
                                if (this.player.inventory.getCarried() != null) {
                                    action = packet102windowclick.button == 0 ? InventoryAction.DROP_ALL_CURSOR : InventoryAction.DROP_ONE_CURSOR;
                                }
                            } else {
                                var12 = this.player.activeContainer.getSlot(packet102windowclick.slot);
                                if (var12 != null) {
                                    i = var12.getItem();
                                    ItemStack var16 = this.player.inventory.getCarried();
                                    if (i == null) {
                                        if (var16 != null) {
                                            action = packet102windowclick.button == 0 ? InventoryAction.PLACE_ALL : InventoryAction.PLACE_ONE;
                                        }
                                    } else if (var12.a(this.player)) {
                                        if (var16 == null) {
                                            action = packet102windowclick.button == 0 ? InventoryAction.PICKUP_ALL : InventoryAction.PICKUP_HALF;
                                        } else if (var12.isAllowed(var16)) {
                                            if (i.doMaterialsMatch(var16) && ItemStack.equals(i, var16)) {
                                                firstEmptySlot = packet102windowclick.button == 0 ? var16.count : 1;
                                                firstEmptySlot = Math.min(firstEmptySlot, i.getMaxStackSize() - i.count);
                                                firstEmptySlot = Math.min(firstEmptySlot, var12.inventory.getMaxStackSize() - i.count);
                                                if (firstEmptySlot == 1) {
                                                    action = InventoryAction.PLACE_ONE;
                                                } else if (firstEmptySlot == var16.count) {
                                                    action = InventoryAction.PLACE_ALL;
                                                } else if (firstEmptySlot < 0) {
                                                    action = firstEmptySlot != -1 ? InventoryAction.PICKUP_SOME : InventoryAction.PICKUP_ONE;
                                                } else if (firstEmptySlot != 0) {
                                                    action = InventoryAction.PLACE_SOME;
                                                }
                                            } else if (var16.count <= var12.a()) {
                                                action = InventoryAction.SWAP_WITH_CURSOR;
                                            }
                                        } else if (var16.id == i.id && (!var16.usesData() || var16.getData() == i.getData()) && ItemStack.equals(var16, i) && i.count >= 0 && i.count + var16.count <= var16.getMaxStackSize()) {
                                            action = InventoryAction.PICKUP_ALL;
                                        }
                                    }
                                }
                            }
                        }
                    } else if (packet102windowclick.shift == 1) {
                        if (packet102windowclick.button == 0) {
                            click = ClickType.SHIFT_LEFT;
                        } else if (packet102windowclick.button == 1) {
                            click = ClickType.SHIFT_RIGHT;
                        }

                        if (packet102windowclick.button == 0 || packet102windowclick.button == 1) {
                            if (packet102windowclick.slot < 0) {
                                action = InventoryAction.NOTHING;
                            } else {
                                var12 = this.player.activeContainer.getSlot(packet102windowclick.slot);
                                if (var12 != null && var12.a(this.player) && var12.d()) {
                                    action = InventoryAction.MOVE_TO_OTHER_INVENTORY;
                                } else {
                                    action = InventoryAction.NOTHING;
                                }
                            }
                        }
                    } else if (packet102windowclick.shift == 2) {
                        if (packet102windowclick.button >= 0 && packet102windowclick.button < 9) {
                            click = ClickType.NUMBER_KEY;
                            var12 = this.player.activeContainer.getSlot(packet102windowclick.slot);
                            if (var12.a(this.player)) {
                                i = this.player.inventory.getItem(packet102windowclick.button);
                                boolean canCleanSwap = i == null || var12.inventory == this.player.inventory && var12.isAllowed(i);
                                if (var12.d()) {
                                    if (canCleanSwap) {
                                        action = InventoryAction.HOTBAR_SWAP;
                                    } else {
                                        firstEmptySlot = this.player.inventory.j();
                                        if (firstEmptySlot > -1) {
                                            action = InventoryAction.HOTBAR_MOVE_AND_READD;
                                        } else {
                                            action = InventoryAction.NOTHING;
                                        }
                                    }
                                } else if (!var12.d() && i != null && var12.isAllowed(i)) {
                                    action = InventoryAction.HOTBAR_SWAP;
                                } else {
                                    action = InventoryAction.NOTHING;
                                }
                            } else {
                                action = InventoryAction.NOTHING;
                            }

                            event = new InventoryClickEvent(inventory, type, packet102windowclick.slot, click, action, packet102windowclick.button);
                        }
                    } else if (packet102windowclick.shift == 3) {
                        if (packet102windowclick.button == 2) {
                            click = ClickType.MIDDLE;
                            if (packet102windowclick.slot == -999) {
                                action = InventoryAction.NOTHING;
                            } else {
                                var12 = this.player.activeContainer.getSlot(packet102windowclick.slot);
                                if (var12 != null && var12.d() && this.player.abilities.canInstantlyBuild && this.player.inventory.getCarried() == null) {
                                    action = InventoryAction.CLONE_STACK;
                                } else {
                                    action = InventoryAction.NOTHING;
                                }
                            }
                        } else {
                            click = ClickType.UNKNOWN;
                            action = InventoryAction.UNKNOWN;
                        }
                    } else if (packet102windowclick.shift == 4) {
                        if (packet102windowclick.slot >= 0) {
                            if (packet102windowclick.button == 0) {
                                click = ClickType.DROP;
                                var12 = this.player.activeContainer.getSlot(packet102windowclick.slot);
                                if (var12 != null && var12.d() && var12.a(this.player) && var12.getItem() != null && var12.getItem().id != 0) {
                                    action = InventoryAction.DROP_ONE_SLOT;
                                } else {
                                    action = InventoryAction.NOTHING;
                                }
                            } else if (packet102windowclick.button == 1) {
                                click = ClickType.CONTROL_DROP;
                                var12 = this.player.activeContainer.getSlot(packet102windowclick.slot);
                                if (var12 != null && var12.d() && var12.a(this.player) && var12.getItem() != null && var12.getItem().id != 0) {
                                    action = InventoryAction.DROP_ALL_SLOT;
                                } else {
                                    action = InventoryAction.NOTHING;
                                }
                            }
                        } else {
                            click = ClickType.LEFT;
                            if (packet102windowclick.button == 1) {
                                click = ClickType.RIGHT;
                            }

                            action = InventoryAction.NOTHING;
                        }
                    } else if (packet102windowclick.shift == 5) {
                        itemstack = this.player.activeContainer.clickItem(packet102windowclick.slot, packet102windowclick.button, 5, this.player);
                    } else if (packet102windowclick.shift == 6) {
                        click = ClickType.DOUBLE_CLICK;
                        action = InventoryAction.NOTHING;
                        if (packet102windowclick.slot >= 0 && this.player.inventory.getCarried() != null) {
                            ItemStack arraylist = this.player.inventory.getCarried();
                            action = InventoryAction.NOTHING;
                            if (inventory.getTopInventory().contains(arraylist.id) || inventory.getBottomInventory().contains(arraylist.id)) {
                                action = InventoryAction.COLLECT_TO_CURSOR;
                            }
                        }
                    }
                }

                if (packet102windowclick.shift != 5) {
                    if (click == ClickType.NUMBER_KEY) {
                        event = new InventoryClickEvent(inventory, type, packet102windowclick.slot, click, action, packet102windowclick.button);
                    } else {
                        event = new InventoryClickEvent(inventory, type, packet102windowclick.slot, click, action);
                    }

                    Inventory var15 = inventory.getTopInventory();
                    if (packet102windowclick.slot == 0 && var15 instanceof CraftingInventory) {
                        Recipe var13 = ((CraftingInventory) var15).getRecipe();
                        if (var13 != null) {
                            if (click == ClickType.NUMBER_KEY) {
                                event = new CraftItemEvent(var13, inventory, type, packet102windowclick.slot, click, action, packet102windowclick.button);
                            } else {
                                event = new CraftItemEvent(var13, inventory, type, packet102windowclick.slot, click, action);
                            }
                        }
                    }

                    this.server.getPluginManager().callEvent(event);
                    switch (event.getResult()) {
                        case ALLOW:
                        case DEFAULT:
                            itemstack = this.player.activeContainer.clickItem(packet102windowclick.slot, packet102windowclick.button, packet102windowclick.shift, this.player);
                            break;
                        case DENY:
                            switch (action) {
                                case PICKUP_ALL:
                                case MOVE_TO_OTHER_INVENTORY:
                                case HOTBAR_MOVE_AND_READD:
                                case HOTBAR_SWAP:
                                case COLLECT_TO_CURSOR:
                                case UNKNOWN:
                                    this.player.updateInventory(this.player.activeContainer);
                                    break;
                                case PICKUP_SOME:
                                case PICKUP_HALF:
                                case PICKUP_ONE:
                                case PLACE_ALL:
                                case PLACE_SOME:
                                case PLACE_ONE:
                                case SWAP_WITH_CURSOR:
                                    this.player.playerConnection.sendPacket(new Packet103SetSlot(-1, -1, this.player.inventory.getCarried()));
                                    this.player.playerConnection.sendPacket(new Packet103SetSlot(this.player.activeContainer.windowId, packet102windowclick.slot, this.player.activeContainer.getSlot(packet102windowclick.slot).getItem()));
                                    break;
                                case DROP_ALL_SLOT:
                                case DROP_ONE_SLOT:
                                    this.player.playerConnection.sendPacket(new Packet103SetSlot(this.player.activeContainer.windowId, packet102windowclick.slot, this.player.activeContainer.getSlot(packet102windowclick.slot).getItem()));
                                    break;
                                case DROP_ALL_CURSOR:
                                case DROP_ONE_CURSOR:
                                case CLONE_STACK:
                                    this.player.playerConnection.sendPacket(new Packet103SetSlot(-1, -1, this.player.inventory.getCarried()));
                                    break;
                            }
                            return;
                    }
                }

                if (ItemStack.matches(packet102windowclick.item, itemstack)) {
                    this.player.playerConnection.sendPacket(new Packet106Transaction(packet102windowclick.a, packet102windowclick.d, true));
                    this.player.h = true;
                    this.player.activeContainer.b();
                    this.player.broadcastCarriedItem();
                    this.player.h = false;
                } else {
                    this.r.a(this.player.activeContainer.windowId, Short.valueOf(packet102windowclick.d));
                    this.player.playerConnection.sendPacket(new Packet106Transaction(packet102windowclick.a, packet102windowclick.d, false));
                    this.player.activeContainer.a(this.player, false);
                    ArrayList var17 = new ArrayList();

                    for (int var14 = 0; var14 < this.player.activeContainer.c.size(); ++var14) {
                        var17.add(this.player.activeContainer.c.get(var14).getItem());
                    }

                    this.player.a(this.player.activeContainer, var17);
                    if (type == SlotType.RESULT && itemstack != null) {
                        this.player.playerConnection.sendPacket(new Packet103SetSlot(this.player.activeContainer.windowId, 0, itemstack));
                    }
                }
            }

        }
    }

    public void a(Packet108ButtonClick packet108buttonclick) {
        if (this.player.activeContainer.windowId == packet108buttonclick.a && this.player.activeContainer.c(this.player)) {
            this.player.activeContainer.a(this.player, packet108buttonclick.b);
            this.player.activeContainer.b();
        }

    }

    public void a(Packet107SetCreativeSlot packet107setcreativeslot) {
        if (this.player.playerInteractManager.isCreative()) {
            boolean flag = packet107setcreativeslot.slot < 0;
            ItemStack itemstack = packet107setcreativeslot.b;
            boolean flag1 = packet107setcreativeslot.slot >= 1 && packet107setcreativeslot.slot < 36 + PlayerInventory.getHotbarSize();
            boolean flag2 = itemstack == null || itemstack.id < Item.byId.length && itemstack.id >= 0 && Item.byId[itemstack.id] != null && !invalidItems.contains(Integer.valueOf(itemstack.id));
            boolean flag3 = itemstack == null || itemstack.getData() >= 0 && itemstack.getData() >= 0 && itemstack.count <= 64 && itemstack.count > 0;
            if (flag || flag1 && !ItemStack.matches(this.player.defaultContainer.getSlot(packet107setcreativeslot.slot).getItem(), packet107setcreativeslot.b)) {
                CraftPlayer entityitem = this.player.getBukkitEntity();
                CraftInventoryView inventory = new CraftInventoryView(entityitem, entityitem.getInventory(), this.player.defaultContainer);
                org.bukkit.inventory.ItemStack item = CraftItemStack.asBukkitCopy(packet107setcreativeslot.b);
                SlotType type = SlotType.QUICKBAR;
                if (flag) {
                    type = SlotType.OUTSIDE;
                } else if (packet107setcreativeslot.slot < 36) {
                    if (packet107setcreativeslot.slot >= 5 && packet107setcreativeslot.slot < 9) {
                        type = SlotType.ARMOR;
                    } else {
                        type = SlotType.CONTAINER;
                    }
                }

                InventoryCreativeEvent event = new InventoryCreativeEvent(inventory, type, flag ? -999 : packet107setcreativeslot.slot, item);
                this.server.getPluginManager().callEvent(event);
                itemstack = CraftItemStack.asNMSCopy(event.getCursor());
                switch (event.getResult()) {
                    case ALLOW:
                        flag2 = flag3 = true;
                        break;
                    case DEFAULT:
                        break;
                    case DENY:
                        if (packet107setcreativeslot.slot >= 0) {
                            this.player.playerConnection.sendPacket(new Packet103SetSlot(this.player.defaultContainer.windowId, packet107setcreativeslot.slot, this.player.defaultContainer.getSlot(packet107setcreativeslot.slot).getItem()));
                            this.player.playerConnection.sendPacket(new Packet103SetSlot(-1, -1, null));
                        }
                        return;
                }
            }

            if (flag1 && flag2 && flag3) {
                if (itemstack == null) {
                    this.player.defaultContainer.setItem(packet107setcreativeslot.slot, null);
                } else {
                    this.player.defaultContainer.setItem(packet107setcreativeslot.slot, itemstack);
                }

                this.player.defaultContainer.a(this.player, true);
            } else if (flag && flag2 && flag3 && this.x < 200) {
                this.x += 20;
                EntityItem entityitem1 = this.player.drop(itemstack);
                if (entityitem1 != null) {
                    entityitem1.c();
                }
            }
        }

    }

    public void a(Packet106Transaction packet106transaction) {
        if (!this.player.dead && this.player.activeContainer.a(this.player)) {
            Short oshort = (Short) this.r.get(this.player.activeContainer.windowId);
            if (oshort != null && packet106transaction.b == oshort.shortValue() && this.player.activeContainer.windowId == packet106transaction.a && !this.player.activeContainer.c(this.player)) {
                this.player.activeContainer.a(this.player, true);
            }

        }
    }

    public void a(Packet130UpdateSign packet) {
        // Block &k in sign
        int i = 0;
        for (String line : packet.lines) {
            packet.lines[i++] = line.replaceAll("&k", "").replaceAll("§k", "");
        }
        //
        if (!this.player.dead) {
            WorldServer worldserver = this.minecraftServer.getWorldServer(this.player.dimension);
            if (worldserver.isLoaded(packet.x, packet.y, packet.z)) {
                TileEntity tileentity = worldserver.getTileEntity(packet.x, packet.y, packet.z);
                if (tileentity instanceof TileEntitySign) {
                    TileEntitySign entitySign = (TileEntitySign) tileentity;
                    if (!entitySign.a()) {
                        this.minecraftServer.warning("Player " + this.player.name + " just tried to change non-editable sign");
                        this.sendPacket(new Packet130UpdateSign(packet.x, packet.y, packet.z, entitySign.lines));
                        return;
                    }
                }

                int j;
                int var11;
                for (j = 0; j < 4; ++j) {
                    boolean k = true;
                    if (packet.lines[j].length() > 15) {
                        k = false;
                    } else {
                        for (var11 = 0; var11 < packet.lines[j].length(); ++var11) {
                            if (!SharedConstants.isAllowedChatCharacter(packet.lines[j].charAt(var11))) {
                                k = false;
                            }
                        }
                    }

                    if (!k) {
                        packet.lines[j] = "!?";
                    }
                }

                if (tileentity instanceof TileEntitySign) {
                    j = packet.x;
                    int var12 = packet.y;
                    var11 = packet.z;
                    TileEntitySign tileentitysign1 = (TileEntitySign) tileentity;
                    Player player = this.server.getPlayer(this.player);
                    SignChangeEvent event = new SignChangeEvent(player.getWorld().getBlockAt(j, var12, var11), this.server.getPlayer(this.player), packet.lines);
                    this.server.getPluginManager().callEvent(event);
                    if (!event.isCancelled()) {
                        for (int l = 0; l < 4; ++l) {
                            tileentitysign1.lines[l] = event.getLine(l);
                            if (tileentitysign1.lines[l] == null) {
                                tileentitysign1.lines[l] = "";
                            }
                        }

                        tileentitysign1.isEditable = false;
                    }

                    tileentitysign1.update();
                    worldserver.notify(j, var12, var11);
                }
            }

        }
    }

    public void a(Packet0KeepAlive packet0keepalive) {
        if (packet0keepalive.a == this.h) {
            int i = (int) (System.nanoTime() / 1000000L - this.i);
            this.player.ping = (this.player.ping * 3 + i) / 4;
        }

    }

    public boolean a() {
        return true;
    }

    public void a(Packet202Abilities packet202abilities) {
        if (this.player.abilities.canFly && this.player.abilities.isFlying != packet202abilities.f()) {
            PlayerToggleFlightEvent event = new PlayerToggleFlightEvent(this.server.getPlayer(this.player), packet202abilities.f());
            this.server.getPluginManager().callEvent(event);
            if (!event.isCancelled()) {
                this.player.abilities.isFlying = packet202abilities.f();
            } else {
                this.player.updateAbilities();
            }
        }

    }

    public void a(Packet203TabComplete packet203tabcomplete) {
        StringBuilder stringbuilder = new StringBuilder();

        String s;
        for (Iterator iterator = this.minecraftServer.a(this.player, packet203tabcomplete.d()).iterator(); iterator.hasNext(); stringbuilder.append(s)) {
            s = (String) iterator.next();
            if (stringbuilder.length() > 0) {
                stringbuilder.append('\u0000');
            }
        }

        this.player.playerConnection.sendPacket(new Packet203TabComplete(stringbuilder.toString()));
    }

    public void a(Packet204LocaleAndViewDistance packet204localeandviewdistance) {
        this.player.a(packet204localeandviewdistance);
    }

    public void a(Packet250CustomPayload packet250custompayload) {
        if (packet250custompayload.length > 0) {
            DataInputStream datainputstream;
            ItemStack itemstack;
            ItemStack itemstack1;
            if ("MC|BEdit".equals(packet250custompayload.tag)) {
                try {
                    datainputstream = new DataInputStream(new ByteArrayInputStream(packet250custompayload.data));
                    itemstack = Packet.c(datainputstream);
                    if (!ItemBookAndQuill.a(itemstack.getTag())) {
                        throw new IOException("Invalid book tag!");
                    }

                    itemstack1 = this.player.inventory.getItemInHand();
                    if (itemstack != null && itemstack.id == Item.BOOK_AND_QUILL.id && itemstack.id == itemstack1.id) {
                        CraftEventFactory.handleEditBookEvent(this.player, itemstack);
                    }
                } catch (Throwable var16) {
                    this.minecraftServer.getLogger().warning(this.player.name + " sent invalid MC|BEdit data", var16);
                    this.disconnect("Invalid book data!");
                }
            } else if ("MC|BSign".equals(packet250custompayload.tag)) {
                try {
                    datainputstream = new DataInputStream(new ByteArrayInputStream(packet250custompayload.data));
                    itemstack = Packet.c(datainputstream);
                    if (!ItemWrittenBook.a(itemstack.getTag())) {
                        throw new IOException("Invalid book tag!");
                    }

                    itemstack1 = this.player.inventory.getItemInHand();
                    if (itemstack != null && itemstack.id == Item.WRITTEN_BOOK.id && itemstack1.id == Item.BOOK_AND_QUILL.id) {
                        CraftEventFactory.handleEditBookEvent(this.player, itemstack);
                    }
                } catch (Throwable var15) {
                    this.minecraftServer.getLogger().warning(this.player.name + " sent invalid MC|BSign data", var15);
                    this.disconnect("Invalid book data!");
                }
            } else {
                int i;
                if ("MC|TrSel".equals(packet250custompayload.tag)) {
                    try {
                        datainputstream = new DataInputStream(new ByteArrayInputStream(packet250custompayload.data));
                        i = datainputstream.readInt();
                        Container j = this.player.activeContainer;
                        if (j instanceof ContainerMerchant) {
                            ((ContainerMerchant) j).e(i);
                        }
                    } catch (Exception var14) {
                        this.minecraftServer.getLogger().warning(this.player.name + " sent invalid MC|TrSel data", var14);
                        this.disconnect("Invalid trade data!");
                    }
                } else {
                    String arr$;
                    int var19;
                    if ("MC|AdvCdm".equals(packet250custompayload.tag)) {
                        if (!this.minecraftServer.getEnableCommandBlock()) {
                            this.player.sendMessage(this.player.a("advMode.notEnabled"));
                        } else if (this.player.a(2, "") && this.player.abilities.canInstantlyBuild) {
                            try {
                                datainputstream = new DataInputStream(new ByteArrayInputStream(packet250custompayload.data));
                                i = datainputstream.readInt();
                                var19 = datainputstream.readInt();
                                int ex = datainputstream.readInt();
                                arr$ = Packet.a(datainputstream, 256);
                                TileEntity len$ = this.player.world.getTileEntity(i, var19, ex);
                                if (len$ != null && len$ instanceof TileEntityCommand) {
                                    ((TileEntityCommand) len$).b(arr$);
                                    this.player.world.notify(i, var19, ex);
                                    this.player.sendMessage("Command set: " + arr$);
                                }
                            } catch (Exception var13) {
                                this.minecraftServer.getLogger().warning(this.player.name + " sent invalid MC|AdvCdm data", var13);
                                this.disconnect("Invalid CommandBlock data!");
                            }
                        } else {
                            this.player.sendMessage(this.player.a("advMode.notAllowed"));
                        }
                    } else if ("MC|Beacon".equals(packet250custompayload.tag)) {
                        if (this.player.activeContainer instanceof ContainerBeacon) {
                            try {
                                datainputstream = new DataInputStream(new ByteArrayInputStream(packet250custompayload.data));
                                i = datainputstream.readInt();
                                var19 = datainputstream.readInt();
                                ContainerBeacon var20 = (ContainerBeacon) this.player.activeContainer;
                                Slot var23 = var20.getSlot(0);
                                if (var23.d()) {
                                    var23.a(1);
                                    TileEntityBeacon var25 = var20.e();
                                    var25.d(i);
                                    var25.e(var19);
                                    var25.update();
                                }
                            } catch (Exception var12) {
                                this.minecraftServer.getLogger().warning(this.player.name + " sent invalid MC|Beacon data", var12);
                                this.disconnect("Invalid beacon data!");
                            }
                        }
                    } else if ("MC|ItemName".equals(packet250custompayload.tag) && this.player.activeContainer instanceof ContainerAnvil) {
                        ContainerAnvil var22 = (ContainerAnvil) this.player.activeContainer;
                        if (packet250custompayload.data != null && packet250custompayload.data.length >= 1) {
                            arr$ = SharedConstants.a(new String(packet250custompayload.data));
                            if (arr$.length() <= 30) {
                                var22.a(arr$);
                            }
                        } else {
                            var22.a("");
                        }
                    } else {
                        int i$;
                        String channel;
                        String var21;
                        String[] var24;
                        int var26;
                        if (packet250custompayload.tag.equals("REGISTER")) {
                            try {
                                var21 = new String(packet250custompayload.data, "UTF8");
                                var24 = var21.split("\u0000");
                                var26 = var24.length;

                                for (i$ = 0; i$ < var26; ++i$) {
                                    channel = var24[i$];
                                    this.getPlayer().addChannel(channel);
                                }
                            } catch (UnsupportedEncodingException var18) {
                                throw new AssertionError(var18);
                            }
                        } else if (packet250custompayload.tag.equals("UNREGISTER")) {
                            try {
                                var21 = new String(packet250custompayload.data, "UTF8");
                                var24 = var21.split("\u0000");
                                var26 = var24.length;

                                for (i$ = 0; i$ < var26; ++i$) {
                                    channel = var24[i$];
                                    this.getPlayer().removeChannel(channel);
                                }
                            } catch (UnsupportedEncodingException var17) {
                                throw new AssertionError(var17);
                            }
                        } else {
                            this.server.getMessenger().dispatchIncomingMessage(this.player.getBukkitEntity(), packet250custompayload.tag, packet250custompayload.data);
                        }
                    }
                }
            }

        }
    }
}
