package net.minecraft.server.v1_5_R3;

/**
 * Created by Junhyeong Lim on 2017-05-06.
 */

import org.spigotmc.SpigotConfig;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FileIOThread implements Runnable {
    public static final FileIOThread a = new FileIOThread();
    private List b = Collections.synchronizedList(new ArrayList());
    private volatile long c = 0L;
    private volatile long d = 0L;
    private volatile boolean e = false;

    private FileIOThread() {
        Thread var1 = new Thread(this, "File IO Thread");
        var1.setPriority(1);
        var1.start();
    }

    public void run() {
        while (true) {
            this.b();
        }
    }

    private void b() {
        for (int var1 = 0; var1 < this.b.size(); ++var1) {
            IAsyncChunkSaver var2 = (IAsyncChunkSaver) this.b.get(var1);
            boolean var3 = var2.c();
            if (!var3) {
                this.b.remove(var1--);
                ++this.d;
            }

            if (SpigotConfig.enableFileIOThreadSleep) {
                try {
                    Thread.sleep(this.e ? 0L : 10L);
                } catch (InterruptedException var6) {
                    var6.printStackTrace();
                }
            }
        }

        if (this.b.isEmpty()) {
            try {
                Thread.sleep(25L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
            }
        }

    }

    public void a(IAsyncChunkSaver var1) {
        if (!this.b.contains(var1)) {
            ++this.c;
            this.b.add(var1);
        }
    }

    public void a() {
        this.e = true;

        while (this.c != this.d) {
            try {
                Thread.sleep(10L);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }

        this.e = false;
    }
}
