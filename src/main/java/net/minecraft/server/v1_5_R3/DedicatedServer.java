package net.minecraft.server.v1_5_R3;

import cloud.swiftnode.bukkit.PrimaryThreadRestrictProxySelector;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.libs.joptsimple.OptionSet;
import org.bukkit.craftbukkit.v1_5_R3.LoggerOutputStream;
import org.bukkit.craftbukkit.v1_5_R3.command.CraftRemoteConsoleCommandSender;
import org.bukkit.event.server.ServerCommandEvent;
import org.spigotmc.SpigotConfig;
import org.spigotmc.netty.NettyServerConnection;

import java.io.File;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.ProxySelector;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;

public class DedicatedServer extends MinecraftServer implements IMinecraftServer {
    private final List k = Collections.synchronizedList(new ArrayList<>());
    private final IConsoleLogManager l = new ConsoleLogManager("Minecraft-Server", (String) null, (String) null);
    public PropertyManager propertyManager;
    private RemoteStatusListener m;
    private RemoteControlListener n;
    private boolean generateStructures;
    private EnumGamemode q;
    private ServerConnection r;
    private boolean s = false;

    public DedicatedServer(OptionSet options) {
        super(options);
    }

    public static void animatePrint(String arg) {
        boolean first = true;
        String[] lines = arg.split("\n");
        int mostMany = 0;
        for (String line : lines) {
            char[] chars = line.toCharArray();
            if (mostMany < chars.length) {
                mostMany = chars.length;
            }
        }
        int lineSize = lines.length;
        String[] sections = new String[lineSize];
        for (int lengthIndex = 0; lengthIndex < mostMany; lengthIndex++) {
            if (first) {
                first = false;
            } else {
                try {
                    new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            for (int sizeIndex = 0; sizeIndex < lineSize; sizeIndex++) {
                String section = sections[sizeIndex];
                if (section == null) {
                    section = "";
                }
                if (!(lines[sizeIndex].toCharArray().length <= lengthIndex)) {
                    section += lines[sizeIndex].toCharArray()[lengthIndex];
                }
                System.out.println(section);
                sections[sizeIndex] = section;
            }
        }
    }

    protected boolean init() throws UnknownHostException {
        // Intro
        animatePrint(
                "   ______                          ________                __\n" +
                        "  / ____/________  ____  ____ _   / ____/ /___  __  ______/ /\n" +
                        " / /   / ___/ __ \\/ __ \\/ __ `/  / /   / / __ \\/ / / / __  / \n" +
                        "/ /___/ /  / /_/ / / / / /_/ /  / /___/ / /_/ / /_/ / /_/ /  \n" +
                        "\\____/_/   \\____/_/ /_/\\__, /   \\____/_/\\____/\\__,_/\\__,_/   \n" +
                        "                      /____/                                 \n" +
                        "\n" +
                        "   Recoded by EntryPoint\n" +
                        "\n");
        ThreadCommandReader threadcommandreader = new ThreadCommandReader(this);
        threadcommandreader.setDaemon(true);
        threadcommandreader.start();
        System.setOut(new PrintStream(new LoggerOutputStream(this.getLogger().getLogger(), Level.INFO), true));
        System.setErr(new PrintStream(new LoggerOutputStream(this.getLogger().getLogger(), Level.SEVERE), true));
        this.getLogger().info("마인크래프트 서버 버전 1.5.2 시작");
        if (Runtime.getRuntime().maxMemory() / 1024L / 1024L < 512L) {
            this.getLogger().warning("더 많은 램을 사용하여 서버를 시작하려면 다음 옵션으로 시작하십시오. \"java -Xmx1024M -Xms1024M -jar minecraft_server.jar\"");
        }

        this.getLogger().info("설정 파일 로딩 중");
        this.propertyManager = new PropertyManager(this.options, this.getLogger());
        if (this.I()) {
            this.d("127.0.0.1");
        } else {
            this.setOnlineMode(this.propertyManager.getBoolean("online-mode", true));
            this.d(this.propertyManager.getString("server-ip", ""));
        }

        this.setSpawnAnimals(this.propertyManager.getBoolean("spawn-animals", true));
        this.setSpawnNPCs(this.propertyManager.getBoolean("spawn-npcs", true));
        this.setPvP(this.propertyManager.getBoolean("pvp", true));
        this.setAllowFlight(this.propertyManager.getBoolean("allow-flight", false));
        this.setTexturePack(this.propertyManager.getString("texture-pack", ""));
        this.setMotd(this.propertyManager.getString("motd", "A Minecraft Server"));
        this.setForceGamemode(this.propertyManager.getBoolean("force-gamemode", false));
        if (this.propertyManager.getInt("difficulty", 1) < 0) {
            this.propertyManager.a("difficulty", Integer.valueOf(0));
        } else if (this.propertyManager.getInt("difficulty", 1) > 3) {
            this.propertyManager.a("difficulty", Integer.valueOf(3));
        }

        this.generateStructures = this.propertyManager.getBoolean("generate-structures", true);
        int i = this.propertyManager.getInt("gamemode", EnumGamemode.SURVIVAL.a());
        this.q = WorldSettings.a(i);
        this.getLogger().info("기본 게임 모드: " + this.q);
        InetAddress inetaddress = null;
        if (this.getServerIp().length() > 0) {
            inetaddress = InetAddress.getByName(this.getServerIp());
        }

        if (this.G() < 0) {
            this.setPort(this.propertyManager.getInt("server-port", 25565));
        }

        this.a(new DedicatedPlayerList(this));
        SpigotConfig.init();
        SpigotConfig.registerCommands();
        this.getLogger().info("keypair 생성 중");
        this.a(MinecraftEncryption.b());
        this.getLogger().info("다음에서 마인크래프트 서버 실행 중 " + (this.getServerIp().length() == 0 ? "*" : this.getServerIp()) + ":" + this.G());

        try {
            this.r = SpigotConfig.listeners.get(0).netty ? new NettyServerConnection(this, inetaddress, this.G()) : new DedicatedServerConnection(this, inetaddress, this.G());
        } catch (Throwable var19) {
            this.getLogger().warning("**** 포트 바인드에 실패했습니다!");
            this.getLogger().warning("예외 : {0}", var19.toString());
            this.getLogger().warning("서버가 이미 해당 포트에서 실행 중일 수 있습니다.");
            return false;
        }

        if (!this.getOnlineMode()) {
            this.getLogger().warning("**** 서버가 오프라인 모드에서 실행 중입니다!");
            this.getLogger().warning("서버는 사용자 이름을 인증하지 않습니다.");
            this.getLogger().warning("이로 인해 인터넷에 접속하지 않고도 게임을 할 수 있게되며 해커가 선택한 사용자 이름과 연결할 수 있는 가능성도 열리게됩니다.");
            this.getLogger().warning("이것을 바꾸려면, server.properties 파일의 \"online-mode\" 옵션을 \"true\" 로 바꾸십시오.");
        }

        this.convertable = new WorldLoaderServer(this.server.getWorldContainer());
        long j = System.nanoTime();
        if (this.J() == null) {
            this.l(this.propertyManager.getString("level-name", "world"));
        }

        String s = this.propertyManager.getString("level-seed", "");
        String s1 = this.propertyManager.getString("level-type", "DEFAULT");
        String s2 = this.propertyManager.getString("generator-settings", "");
        long k = (new Random()).nextLong();
        if (s.length() > 0) {
            try {
                long l = Long.parseLong(s);
                if (l != 0L) {
                    k = l;
                }
            } catch (NumberFormatException var18) {
                k = (long) s.hashCode();
            }
        }

        WorldType worldtype = WorldType.getType(s1);
        if (worldtype == null) {
            worldtype = WorldType.NORMAL;
        }

        this.d(this.propertyManager.getInt("max-build-height", 256));
        this.d((this.getMaxBuildHeight() + 8) / 16 * 16);
        this.d(MathHelper.a(this.getMaxBuildHeight(), 64, 256));
        this.propertyManager.a("max-build-height", this.getMaxBuildHeight());
        this.getLogger().info("월드 \"" + this.J() + "\" 준비 중");
        this.a(this.J(), this.J(), k, worldtype, s2);
        long i1 = System.nanoTime() - j;
        String s3 = String.format("%.3fs", (double) i1 / 1.0E9D);
        this.getLogger().info("구동 완료 (" + s3 + ")! 도움이 필요하면 \"help\" 또는 \"?\" 를 입력하십시오.");
        if (this.propertyManager.getBoolean("enable-query", false)) {
            this.getLogger().info("Starting GS4 status listener");
            this.m = new RemoteStatusListener(this);
            this.m.a();
        }

        if (this.propertyManager.getBoolean("enable-rcon", false)) {
            this.getLogger().info("Starting remote control listener");
            this.n = new RemoteControlListener(this);
            this.n.a();
            this.remoteConsole = new CraftRemoteConsoleCommandSender();
        }

        if (this.server.getBukkitSpawnRadius() > -1) {
            this.getLogger().info("\'settings.spawn-radius\' in bukkit.yml has been moved to \'spawn-protection\' in server.properties. I will move your config for you.");
            this.propertyManager.properties.remove("spawn-protection");
            this.propertyManager.getInt("spawn-protection", this.server.getBukkitSpawnRadius());
            this.server.removeBukkitSpawnRadius();
            this.propertyManager.savePropertiesFile();
        }
        // Swiftnode world save timer
        new Thread(() -> {
            while (true) {
                MinecraftServer.getServer().methodProfiler.a("save");
                Bukkit.getConsoleSender().sendMessage(
                        ChatColor.translateAlternateColorCodes('&', SpigotConfig.worldSaveStartMessage));
                worlds.forEach(world -> {
                    if (world.spigotConfig.autoSavePeriod > 0)
                        world.getWorld().save(false);
                });
                Bukkit.getConsoleSender().sendMessage(
                        ChatColor.translateAlternateColorCodes('&', SpigotConfig.worldSaveEndMessage));
                try {
                    Thread.sleep(SpigotConfig.worldSavePeriod);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }).start();

        // Swiftnode restrict the networking on primary thread
        ProxySelector.setDefault(
                new PrimaryThreadRestrictProxySelector(ProxySelector.getDefault()));
        return true;
    }

    public PropertyManager getPropertyManager() {
        return this.propertyManager;
    }

    public boolean getGenerateStructures() {
        return this.generateStructures;
    }

    public EnumGamemode getGamemode() {
        return this.q;
    }

    public int getDifficulty() {
        return Math.max(0, Math.min(3, this.propertyManager.getInt("difficulty", 1)));
    }

    public boolean isHardcore() {
        return this.propertyManager.getBoolean("hardcore", false);
    }

    protected void a(CrashReport crashreport) {
        while (this.isRunning()) {
            this.an();

            try {
                Thread.sleep(10L);
            } catch (InterruptedException var3) {
                var3.printStackTrace();
            }
        }

    }

    public CrashReport b(CrashReport crashreport) {
        crashreport = super.b(crashreport);
        crashreport.g().a("Is Modded", new CrashReportModded(this));
        crashreport.g().a("Type", new CrashReportType(this));
        return crashreport;
    }

    protected void p() {
        System.exit(0);
    }

    public void r() {
        super.r();
        this.an();
    }

    public boolean getAllowNether() {
        return this.propertyManager.getBoolean("allow-nether", true);
    }

    public boolean getSpawnMonsters() {
        return this.propertyManager.getBoolean("spawn-monsters", true);
    }

    public void a(MojangStatisticsGenerator mojangstatisticsgenerator) {
        mojangstatisticsgenerator.a("whitelist_enabled", this.ao().getHasWhitelist());
        mojangstatisticsgenerator.a("whitelist_count", this.ao().getWhitelisted().size());
        super.a(mojangstatisticsgenerator);
    }

    public boolean getSnooperEnabled() {
        return this.propertyManager.getBoolean("snooper-enabled", true);
    }

    public void issueCommand(String s, ICommandListener icommandlistener) {
        this.k.add(new ServerCommand(s, icommandlistener));
    }

    public void an() {
        while (!this.k.isEmpty()) {
            ServerCommand servercommand = (ServerCommand) this.k.remove(0);
            ServerCommandEvent event = new ServerCommandEvent(this.console, servercommand.command);
            this.server.getPluginManager().callEvent(event);
            servercommand = new ServerCommand(event.getCommand(), servercommand.source);
            this.server.dispatchServerCommand(this.console, servercommand);
        }

    }

    public boolean T() {
        return true;
    }

    public DedicatedPlayerList ao() {
        return (DedicatedPlayerList) super.getPlayerList();
    }

    public ServerConnection ae() {
        return this.r;
    }

    public int a(String s, int i) {
        return this.propertyManager.getInt(s, i);
    }

    public String a(String s, String s1) {
        return this.propertyManager.getString(s, s1);
    }

    public boolean a(String s, boolean flag) {
        return this.propertyManager.getBoolean(s, flag);
    }

    public void a(String s, Object object) {
        this.propertyManager.a(s, object);
    }

    public void a() {
        this.propertyManager.savePropertiesFile();
    }

    public String b_() {
        File file1 = this.propertyManager.c();
        return file1 != null ? file1.getAbsolutePath() : "No settings file";
    }

    public void ap() {
        ServerGUI.a(this);
        this.s = true;
    }

    public boolean ag() {
        return this.s;
    }

    public String a(EnumGamemode enumgamemode, boolean flag) {
        return "";
    }

    public boolean getEnableCommandBlock() {
        return this.propertyManager.getBoolean("enable-command-block", false);
    }

    public int getSpawnProtection() {
        return this.propertyManager.getInt("spawn-protection", super.getSpawnProtection());
    }

    public boolean a(World world, int i, int j, int k, EntityHuman entityhuman) {
        if (world.worldProvider.dimension != 0) {
            return false;
        } else if (this.ao().getOPs().isEmpty()) {
            return false;
        } else if (this.ao().isOp(entityhuman.name)) {
            return false;
        } else if (this.getSpawnProtection() <= 0) {
            return false;
        } else {
            ChunkCoordinates chunkcoordinates = world.getSpawn();
            int l = MathHelper.a(i - chunkcoordinates.x);
            int i1 = MathHelper.a(k - chunkcoordinates.z);
            int j1 = Math.max(l, i1);
            return j1 <= this.getSpawnProtection();
        }
    }

    public IConsoleLogManager getLogger() {
        return this.l;
    }

    public PlayerList getPlayerList() {
        return this.ao();
    }
}
