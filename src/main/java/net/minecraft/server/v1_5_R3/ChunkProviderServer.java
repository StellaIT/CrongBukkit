package net.minecraft.server.v1_5_R3;

import org.bukkit.craftbukkit.v1_5_R3.CraftServer;
import org.bukkit.craftbukkit.v1_5_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_5_R3.SpigotTimings;
import org.bukkit.craftbukkit.v1_5_R3.chunkio.ChunkIOExecutor;
import org.bukkit.craftbukkit.v1_5_R3.util.LongHash;
import org.bukkit.craftbukkit.v1_5_R3.util.LongHashSet;
import org.bukkit.craftbukkit.v1_5_R3.util.LongObjectHashMap;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkPopulateEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.generator.BlockPopulator;

import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class ChunkProviderServer implements IChunkProvider {
    public LongHashSet unloadQueue = new LongHashSet();
    public Chunk emptyChunk;
    public IChunkProvider chunkProvider;
    public boolean forceChunkLoad = false;
    public LongObjectHashMap<Chunk> chunks = new CachedChunkMap();
    public WorldServer world;
    private IChunkLoader e;

    public ChunkProviderServer(WorldServer worldserver, IChunkLoader ichunkloader, IChunkProvider ichunkprovider) {
        this.emptyChunk = new EmptyChunk(worldserver, 0, 0);
        this.world = worldserver;
        this.e = ichunkloader;
        this.chunkProvider = ichunkprovider;
    }

    public boolean isChunkLoaded(int i, int j) {
        return this.chunks.containsKey(LongHash.toLong(i, j));
    }

    public void queueUnload(int i, int j) {
        if (this.world.worldProvider.e()) {
            ChunkCoordinates c = this.world.getSpawn();
            int k = i * 16 + 8 - c.x;
            int l = j * 16 + 8 - c.z;
            short short1 = 128;
            if (k < -short1 || k > short1 || l < -short1 || l > short1 || !this.world.keepSpawnInMemory) {
                this.unloadQueue.add(i, j);
                Chunk c1 = this.chunks.get(LongHash.toLong(i, j));
                if (c1 != null) {
                    c1.mustSave = true;
                }
            }
        } else {
            this.unloadQueue.add(i, j);
            Chunk c2 = this.chunks.get(LongHash.toLong(i, j));
            if (c2 != null) {
                c2.mustSave = true;
            }
        }

    }

    public void a() {
        Iterator iterator = this.chunks.values().iterator();

        while (iterator.hasNext()) {
            Chunk chunk = (Chunk) iterator.next();
            this.queueUnload(chunk.x, chunk.z);
        }

    }

    public Chunk getChunkAt(int i, int j) {
        return this.getChunkAt(i, j, null);
    }

    public Chunk getChunkAt(int i, int j, Runnable runnable) {
        this.unloadQueue.remove(i, j);
        Chunk chunk = this.chunks.get(LongHash.toLong(i, j));
        boolean newChunk = false;
        ChunkRegionLoader loader = null;
        if (this.e instanceof ChunkRegionLoader) {
            loader = (ChunkRegionLoader) this.e;
        }

        if (chunk == null && runnable != null && loader != null && loader.chunkExists(this.world, i, j)) {
            ChunkIOExecutor.queueChunkLoad(this.world, loader, this, i, j, runnable);
            return null;
        } else {
            if (chunk == null) {
                SpigotTimings.syncChunkLoadTimer.startTiming();
                chunk = this.loadChunk(i, j);
                if (chunk == null) {
                    if (this.chunkProvider == null) {
                        chunk = this.emptyChunk;
                    } else {
                        try {
                            chunk = this.chunkProvider.getOrCreateChunk(i, j);
                        } catch (Throwable var10) {
                            CrashReport crashreport = CrashReport.a(var10, "Exception generating new chunk");
                            CrashReportSystemDetails crashreportsystemdetails = crashreport.a("Chunk to be generated");
                            crashreportsystemdetails.a("Location", String.format("%d,%d", new Object[]{Integer.valueOf(i), Integer.valueOf(j)}));
                            crashreportsystemdetails.a("Position hash", Long.valueOf(LongHash.toLong(i, j)));
                            crashreportsystemdetails.a("Generator", this.chunkProvider.getName());
                            throw new ReportedException(crashreport);
                        }
                    }

                    newChunk = true;
                }

                this.chunks.put(LongHash.toLong(i, j), chunk);
                if (chunk != null) {
                    chunk.addEntities();
                }

                CraftServer server = this.world.getServer();
                if (server != null) {
                    server.getPluginManager().callEvent(new ChunkLoadEvent(chunk.bukkitChunk, newChunk));
                }

                chunk.a(this, this, i, j);
                SpigotTimings.syncChunkLoadTimer.stopTiming();
            }

            if (runnable != null) {
                runnable.run();
            }

            return chunk;
        }
    }


    public Chunk getChunkIfLoaded(int x, int z) {
        return this.chunks.get(LongHash.toLong(x, z));
    }

    public Chunk getOrCreateChunk(int i, int j) {
        Chunk chunk = this.chunks.get(LongHash.toLong(i, j));
        chunk = chunk == null ? (!this.world.isLoading && !this.forceChunkLoad ? this.emptyChunk : this.getChunkAt(i, j)) : chunk;
        if (chunk == this.emptyChunk) {
            return chunk;
        } else {
            if (i != chunk.x || j != chunk.z) {
                this.world.getLogger().severe("Chunk (" + chunk.x + ", " + chunk.z + ") stored at  (" + i + ", " + j + ") in world \'" + this.world.getWorld().getName() + "\'");
                this.world.getLogger().severe(chunk.getClass().getName());
                Throwable ex = new Throwable();
                ex.fillInStackTrace();
                ex.printStackTrace();
            }

            return chunk;
        }
    }

    public Chunk loadChunk(int i, int j) {
        if (this.e == null) {
            return null;
        } else {
            try {
                Chunk chunk = this.e.a(this.world, i, j);
                if (chunk != null) {
                    chunk.setLastSaved(this.world.getTime());
                    chunk.n = this.world.getTime();
                    if (this.chunkProvider != null) {
                        this.chunkProvider.recreateStructures(i, j);
                    }
                }

                return chunk;
            } catch (Exception var4) {
                var4.printStackTrace();
                return null;
            }
        }
    }

    public void saveChunkNOP(Chunk chunk) {
        if (this.e != null) {
            try {
                this.e.b(this.world, chunk);
            } catch (Exception var3) {
                var3.printStackTrace();
            }
        }

    }

    public void saveChunk(Chunk chunk) {
        if (this.e != null) {
            try {
                chunk.setLastSaved(this.world.getTime());
                chunk.n = this.world.getTime();
                this.e.a(this.world, chunk);
            } catch (Exception var3) {
                var3.printStackTrace();
            }
        }

    }

    public void getChunkAt(IChunkProvider ichunkprovider, int i, int j) {
        Chunk chunk = this.getOrCreateChunk(i, j);
        if (!chunk.done) {
            chunk.done = true;
            if (this.chunkProvider != null) {
                this.chunkProvider.getChunkAt(ichunkprovider, i, j);
                BlockSand.instaFall = true;
                Random random = new Random();
                random.setSeed(this.world.getSeed());
                long xRand = random.nextLong() / 2L * 2L + 1L;
                long zRand = random.nextLong() / 2L * 2L + 1L;
                random.setSeed((long) i * xRand + (long) j * zRand ^ this.world.getSeed());
                CraftWorld world = this.world.getWorld();
                if (world != null) {
                    Iterator i$ = world.getPopulators().iterator();

                    while (i$.hasNext()) {
                        BlockPopulator populator = (BlockPopulator) i$.next();
                        populator.populate(world, random, chunk.bukkitChunk);
                    }
                }

                BlockSand.instaFall = false;
                this.world.getServer().getPluginManager().callEvent(new ChunkPopulateEvent(chunk.bukkitChunk));
                chunk.e();
            }
        }

    }

    public boolean saveChunks(boolean flag, IProgressUpdate iprogressupdate) {
        int i = 0;
        Iterator iterator = this.chunks.values().iterator();

        while (iterator.hasNext()) {
            Chunk chunk = (Chunk) iterator.next();
            if (flag) {
                this.saveChunkNOP(chunk);
            }

            if (chunk.a(flag)) {
                this.saveChunk(chunk);
                chunk.l = false;
                ++i;
                if (!flag && i >= world.spigotConfig.maxAutoSaveChunksPerTick) {
                    return false;
                }
            }
        }

        return true;
    }

    public void b() {
        if (this.e != null) {
            this.e.b();
        }

    }

    public boolean unloadChunks() {
        if (!this.world.savingDisabled) {
            CraftServer server = this.world.getServer();

            for (int i = 0; i < 100 && !this.unloadQueue.isEmpty(); ++i) {
                long chunkcoordinates = this.unloadQueue.popFirst();
                Chunk chunk = this.chunks.get(chunkcoordinates);
                if (chunk != null) {
                    // Swiftnode start
                    if (chunk.scheduledForUnload != null && chunk.scheduledForUnload >= System.currentTimeMillis())
                        continue;
                    // Swiftnode end
                    ChunkUnloadEvent event = new ChunkUnloadEvent(chunk.bukkitChunk);
                    server.getPluginManager().callEvent(event);
                    if (!event.isCancelled()) {
                        chunk.removeEntities();
                        this.saveChunk(chunk);
                        this.saveChunkNOP(chunk);
                        this.chunks.remove(chunkcoordinates);
                    }
                }
            }

            if (this.e != null) {
                this.e.a();
            }
        }

        return this.chunkProvider.unloadChunks();
    }

    public boolean canSave() {
        return !this.world.savingDisabled;
    }

    public String getName() {
        return "ServerChunkCache: " + this.chunks.values().size() + " Drop: " + this.unloadQueue.size();
    }

    public List getMobsFor(EnumCreatureType enumcreaturetype, int i, int j, int k) {
        return this.chunkProvider.getMobsFor(enumcreaturetype, i, j, k);
    }

    public ChunkPosition findNearestMapFeature(World world, String s, int i, int j, int k) {
        return this.chunkProvider.findNearestMapFeature(world, s, i, j, k);
    }

    public int getLoadedChunks() {
        return this.chunks.values().size();
    }

    public void recreateStructures(int i, int j) {
    }
}
