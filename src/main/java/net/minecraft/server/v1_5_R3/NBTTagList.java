package net.minecraft.server.v1_5_R3;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NBTTagList extends NBTBase {
    private List<NBTBase> list = new ArrayList<>();
    private byte type;

    public NBTTagList() {
        super("");
    }

    public NBTTagList(String var1) {
        super(var1);
    }

    void write(DataOutput var1) {
        if (!this.list.isEmpty()) {
            this.type = this.list.get(0).getTypeId();
        } else {
            this.type = 1;
        }

        try {
            var1.writeByte(this.type);
            var1.writeInt(this.list.size());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        for (NBTBase aList : this.list) {
            aList.write(var1);
        }

    }

    void load(DataInput input) {
        int number = 0;
        try {
            this.type = input.readByte();
            if (this.type == 9) {
                throw new RuntimeException("Received the crash type.");
            }
            number = input.readInt();
            this.list = new ArrayList<>();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        for (int var3 = 0; var3 < number; ++var3) {
            NBTBase var4 = NBTBase.createTag(this.type, null);
            var4.load(input);
            this.list.add(var4);
        }
    }

    public byte getTypeId() {
        return 9;
    }

    public String toString() {
        return "" + this.list.size() + " entries of type " + NBTBase.getTagName(this.type);
    }

    public void add(NBTBase var1) {
        this.type = var1.getTypeId();
        this.list.add(var1);
    }

    public NBTBase get(int var1) {
        return this.list.get(var1);
    }

    public int size() {
        return this.list.size();
    }

    public NBTBase clone() {
        NBTTagList tag = new NBTTagList(this.getName());
        tag.type = this.type;

        for (NBTBase base : this.list) {
            NBTBase var4 = base.clone();
            tag.list.add(var4);
        }

        return tag;
    }

    public boolean equals(Object objectTag) {
        if (super.equals(objectTag)) {
            NBTTagList var2 = (NBTTagList) objectTag;
            if (this.type == var2.type) {
                return this.list.equals(var2.list);
            }
        }

        return false;
    }

    public int hashCode() {
        return super.hashCode() ^ this.list.hashCode();
    }
}
