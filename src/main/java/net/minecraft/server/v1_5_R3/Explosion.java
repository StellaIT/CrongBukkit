package net.minecraft.server.v1_5_R3;

/**
 * Created by EntryPoint on 2016-12-29.
 */

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_5_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_5_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_5_R3.event.CraftEventFactory;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.spigotmc.SpigotConfig;

import java.util.*;
import java.util.HashMap;

public class Explosion {
    public boolean a = false;
    public boolean b = true;
    public double posX;
    public double posY;
    public double posZ;
    public Entity source;
    public float size;
    public List blocks = new ArrayList();
    public boolean wasCanceled = false;
    private int i = 16;
    private Random j = new Random();
    private World world;
    private Map l = new HashMap();

    public Explosion(World world, Entity entity, double d0, double d1, double d2, float f) {
        this.world = world;
        this.source = entity;
        this.size = (float) Math.max((double) f, 0.0D);
        this.posX = d0;
        this.posY = d1;
        this.posZ = d2;
    }

    public void a() {
        if (this.size >= 0.1F) {
            float f = this.size;
            HashSet hashset = new HashSet();

            int i;
            int j;
            int k;
            double d0;
            double d1;
            double d2;
            for (i = 0; i < this.i; ++i) {
                for (j = 0; j < this.i; ++j) {
                    for (k = 0; k < this.i; ++k) {
                        if (i == 0 || i == this.i - 1 || j == 0 || j == this.i - 1 || k == 0 || k == this.i - 1) {
                            double d3 = (double) ((float) i / ((float) this.i - 1.0F) * 2.0F - 1.0F);
                            double d4 = (double) ((float) j / ((float) this.i - 1.0F) * 2.0F - 1.0F);
                            double d5 = (double) ((float) k / ((float) this.i - 1.0F) * 2.0F - 1.0F);
                            double d6 = Math.sqrt(d3 * d3 + d4 * d4 + d5 * d5);
                            d3 /= d6;
                            d4 /= d6;
                            d5 /= d6;
                            float f1 = this.size * (0.7F + this.world.random.nextFloat() * 0.6F);
                            d0 = this.posX;
                            d1 = this.posY;
                            d2 = this.posZ;

                            for (float f2 = 0.3F; f1 > 0.0F; f1 -= f2 * 0.75F) {
                                int l = MathHelper.floor(d0);
                                int i1 = MathHelper.floor(d1);
                                int j1 = MathHelper.floor(d2);
                                int k1 = this.world.getTypeId(l, i1, j1);
                                if (k1 > 0) {
                                    Block block = Block.byId[k1];
                                    float damagee = this.source != null ? this.source.a(this, this.world, l, i1, j1, block) : block.a(this.source);
                                    f1 -= (damagee + 0.3F) * f2;
                                }

                                if (f1 > 0.0F && (this.source == null || this.source.a(this, this.world, l, i1, j1, k1, f1)) && i1 < 256 && i1 >= 0) {
                                    hashset.add(new ChunkPosition(l, i1, j1));
                                }

                                d0 += d3 * (double) f2;
                                d1 += d4 * (double) f2;
                                d2 += d5 * (double) f2;
                            }
                        }
                    }
                }
            }

            this.blocks.addAll(hashset);
            this.size *= 2.0F;
            i = MathHelper.floor(this.posX - (double) this.size - 1.0D);
            j = MathHelper.floor(this.posX + (double) this.size + 1.0D);
            k = MathHelper.floor(this.posY - (double) this.size - 1.0D);
            int l1 = MathHelper.floor(this.posY + (double) this.size + 1.0D);
            int i2 = MathHelper.floor(this.posZ - (double) this.size - 1.0D);
            int j2 = MathHelper.floor(this.posZ + (double) this.size + 1.0D);
//            List list = this.world.getEntities(this.source, AxisAlignedBB.a().a((double)i, (double)k, (double)i2, (double)j, (double)l1, (double)j2));
            List list = this.world.getEntities(this.source, AxisAlignedBB.a().a((double) i, (double) k, (double) i2, (double) j, (double) l1, (double) j2), (Entity entity) -> !entity.dead);
            Vec3D vec3d = this.world.getVec3DPool().create(this.posX, this.posY, this.posZ);

            for (int k2 = 0; k2 < list.size(); ++k2) {
                Entity entity = (Entity) list.get(k2);
                double d7 = entity.f(this.posX, this.posY, this.posZ) / (double) this.size;
                if (d7 <= 1.0D) {
                    d0 = entity.locX - this.posX;
                    d1 = entity.locY + (double) entity.getHeadHeight() - this.posY;
                    d2 = entity.locZ - this.posZ;
                    double d8 = (double) MathHelper.sqrt(d0 * d0 + d1 * d1 + d2 * d2);
                    if (d8 != 0.0D) {
                        d0 /= d8;
                        d1 /= d8;
                        d2 /= d8;
//                        double d9 = (double)this.world.a(vec3d, entity.boundingBox);
                        double d9 = this.getBlockDensity(vec3d, entity.boundingBox);
                        double d10 = (1.0D - d7) * d9;
                        CraftEntity var50 = entity == null ? null : entity.getBukkitEntity();
                        int damageDone = (int) ((d10 * d10 + d10) / 2.0D * 8.0D * (double) this.size + 1.0D);
                        if (var50 != null) {
                            if (this.source == null) {
                                EntityDamageByBlockEvent damager = new EntityDamageByBlockEvent(null, var50, DamageCause.BLOCK_EXPLOSION, damageDone);
                                Bukkit.getPluginManager().callEvent(damager);
                                if (!damager.isCancelled()) {
                                    var50.setLastDamageCause(damager);
                                    entity.damageEntity(DamageSource.explosion(this), damager.getDamage());
                                    double d11 = EnchantmentProtection.a(entity, d10);
                                    entity.motX += d0 * d11;
                                    entity.motY += d1 * d11;
                                    entity.motZ += d2 * d11;
                                    if (entity instanceof EntityHuman) {
                                        this.l.put(entity, this.world.getVec3DPool().create(d0 * d10, d1 * d10, d2 * d10));
                                    }
                                }
                            } else {
                                CraftEntity var49 = this.source.getBukkitEntity();
                                DamageCause damageCause;
                                if (var49 instanceof TNTPrimed) {
                                    damageCause = DamageCause.BLOCK_EXPLOSION;
                                } else {
                                    damageCause = DamageCause.ENTITY_EXPLOSION;
                                }

                                EntityDamageByEntityEvent event = new EntityDamageByEntityEvent(var49, var50, damageCause, damageDone);
                                Bukkit.getPluginManager().callEvent(event);
                                if (!event.isCancelled()) {
                                    entity.getBukkitEntity().setLastDamageCause(event);
                                    entity.damageEntity(DamageSource.explosion(this), event.getDamage());
                                    entity.motX += d0 * d10;
                                    entity.motY += d1 * d10;
                                    entity.motZ += d2 * d10;
                                    if (entity instanceof EntityHuman) {
                                        this.l.put(entity, this.world.getVec3DPool().create(d0 * d10, d1 * d10, d2 * d10));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            this.size = f;
        }
    }

    public void a(boolean flag) {
        this.world.makeSound(this.posX, this.posY, this.posZ, "random.explode", 4.0F, (1.0F + (this.world.random.nextFloat() - this.world.random.nextFloat()) * 0.2F) * 0.7F);
        if (this.size >= 2.0F && this.b) {
            this.world.addParticle("hugeexplosion", this.posX, this.posY, this.posZ, 1.0D, 0.0D, 0.0D);
        } else {
            this.world.addParticle("largeexplode", this.posX, this.posY, this.posZ, 1.0D, 0.0D, 0.0D);
        }

        Iterator iterator;
        ChunkPosition chunkposition;
        int i;
        int j;
        int k;
        int l;
        if (this.b) {
            CraftWorld i1 = this.world.getWorld();
            CraftEntity explode = this.source == null ? null : this.source.getBukkitEntity();
            Location location = new Location(i1, this.posX, this.posY, this.posZ);
            ArrayList blockList = new ArrayList();

            org.bukkit.block.Block block1;
            for (int event = this.blocks.size() - 1; event >= 0; --event) {
                ChunkPosition block = (ChunkPosition) this.blocks.get(event);
                block1 = i1.getBlockAt(block.x, block.y, block.z);
                if (block1.getType() != Material.AIR) {
                    blockList.add(block1);
                }
            }

            EntityExplodeEvent var33 = new EntityExplodeEvent(explode, location, blockList, 0.3F);
            this.world.getServer().getPluginManager().callEvent(var33);
            this.blocks.clear();
            Iterator var34 = var33.blockList().iterator();

            while (var34.hasNext()) {
                block1 = (org.bukkit.block.Block) var34.next();
                ChunkPosition coords = new ChunkPosition(block1.getX(), block1.getY(), block1.getZ());
                this.blocks.add(coords);
            }

            if (var33.isCancelled()) {
                this.wasCanceled = true;
                return;
            }

            iterator = this.blocks.iterator();

            while (iterator.hasNext()) {
                chunkposition = (ChunkPosition) iterator.next();
                i = chunkposition.x;
                j = chunkposition.y;
                k = chunkposition.z;
                l = this.world.getTypeId(i, j, k);
                this.world.spigotConfig.antiXrayInstance.updateNearbyBlocks(this.world, i, j, k);
                if (flag) {
                    double d0 = (double) ((float) i + this.world.random.nextFloat());
                    double d1 = (double) ((float) j + this.world.random.nextFloat());
                    double d2 = (double) ((float) k + this.world.random.nextFloat());
                    double d3 = d0 - this.posX;
                    double d4 = d1 - this.posY;
                    double d5 = d2 - this.posZ;
                    double d6 = (double) MathHelper.sqrt(d3 * d3 + d4 * d4 + d5 * d5);
                    d3 /= d6;
                    d4 /= d6;
                    d5 /= d6;
                    double d7 = 0.5D / (d6 / (double) this.size + 0.1D);
                    d7 *= (double) (this.world.random.nextFloat() * this.world.random.nextFloat() + 0.3F);
                    d3 *= d7;
                    d4 *= d7;
                    d5 *= d7;
                    this.world.addParticle("explode", (d0 + this.posX * 1.0D) / 2.0D, (d1 + this.posY * 1.0D) / 2.0D, (d2 + this.posZ * 1.0D) / 2.0D, d3, d4, d5);
                    this.world.addParticle("smoke", d0, d1, d2, d3, d4, d5);
                }

                if (l > 0) {
                    Block var35 = Block.byId[l];
                    if (var35.a(this)) {
                        var35.dropNaturally(this.world, i, j, k, this.world.getData(i, j, k), var33.getYield(), 0);
                    }

                    this.world.setTypeIdAndData(i, j, k, 0, 0, 3);
                    var35.wasExploded(this.world, i, j, k, this);
                }
            }
        }

        if (this.a) {
            iterator = this.blocks.iterator();

            while (iterator.hasNext()) {
                chunkposition = (ChunkPosition) iterator.next();
                i = chunkposition.x;
                j = chunkposition.y;
                k = chunkposition.z;
                l = this.world.getTypeId(i, j, k);
                int var32 = this.world.getTypeId(i, j - 1, k);
                if (l == 0 && Block.s[var32] && this.j.nextInt(3) == 0 && !CraftEventFactory.callBlockIgniteEvent(this.world, i, j, k, this).isCancelled()) {
                    this.world.setTypeIdUpdate(i, j, k, Block.FIRE.id);
                }
            }
        }

    }

    public Map b() {
        return this.l;
    }

    public EntityLiving c() {
        return this.source == null ? null : (this.source instanceof EntityTNTPrimed ? ((EntityTNTPrimed) this.source).getSource() : (this.source instanceof EntityLiving ? (EntityLiving) this.source : null));
    }

    private float getBlockDensity(Vec3D vec3d, AxisAlignedBB aabb) {
        if (!SpigotConfig.optimizeExplosions) {
            return this.world.a(vec3d, aabb);
        }
        CacheKey key = new CacheKey(this, aabb);
        Float blockDensity = this.world.explosionDensityCache.get(key);
        if (blockDensity == null) {
            blockDensity = this.world.a(vec3d, aabb);
            this.world.explosionDensityCache.put(key, blockDensity);
        }
        return blockDensity;
    }

    static class CacheKey {
        private final World world;
        private final double posX, posY, posZ;
        private final double minX, minY, minZ;
        private final double maxX, maxY, maxZ;

        public CacheKey(Explosion explosion, AxisAlignedBB aabb) {
            this.world = explosion.world;
            this.posX = explosion.posX;
            this.posY = explosion.posY;
            this.posZ = explosion.posZ;
            this.minX = aabb.a;
            this.minY = aabb.b;
            this.minZ = aabb.c;
            this.maxX = aabb.d;
            this.maxY = aabb.e;
            this.maxZ = aabb.f;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CacheKey cacheKey = (CacheKey) o;
            if (Double.compare(cacheKey.posX, posX) != 0) return false;
            if (Double.compare(cacheKey.posY, posY) != 0) return false;
            if (Double.compare(cacheKey.posZ, posZ) != 0) return false;
            if (Double.compare(cacheKey.minX, minX) != 0) return false;
            if (Double.compare(cacheKey.minY, minY) != 0) return false;
            if (Double.compare(cacheKey.minZ, minZ) != 0) return false;
            if (Double.compare(cacheKey.maxX, maxX) != 0) return false;
            if (Double.compare(cacheKey.maxY, maxY) != 0) return false;
            if (Double.compare(cacheKey.maxZ, maxZ) != 0) return false;
            return world.equals(cacheKey.world);
        }

        @Override
        public int hashCode() {
            int result;
            long temp;
            result = world.hashCode();
            temp = Double.doubleToLongBits(posX);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(posY);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(posZ);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(minX);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(minY);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(minZ);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(maxX);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(maxY);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(maxZ);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }
    }
}
